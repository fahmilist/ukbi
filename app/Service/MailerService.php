<?php
namespace App\Service;

use App\Util\Constant;
use Illuminate\Support\Facades\Mail;
use App\Config;

class MailerService {
    public static function orderSuccess($order, $emailTo){
        $config = Config::find(1);
        try {
            Mail::send('frontend.email.orderSuccess', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($emailTo)
                        ->subject('['.$config->title.'] Berhasil Order');
                });

            Mail::send('frontend.email.orderSuccess', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($config->email)
                        ->subject('['.$config->title.'] New Order');
                });

            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }

    public static function orderProcess($order,$emailTo){
        $config = Config::find(1);
        try {
            Mail::send('frontend.email.orderProcess', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($emailTo)
                        ->subject('['.$config->title.'] Pesanan Diproses');
                });

            Mail::send('frontend.email.orderProcess', $order,
                function ($message) use ($config) {
                    $message
                        ->to($config->email)
                        ->subject('['.$config->title.'] Pesanan Diproses');
                });

            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }

    public static function orderConfirmation($order,$emailTo){
        $config = Config::find(1);
        try {
            Mail::send('frontend.email.orderConfirmation', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($emailTo)
                        ->subject('['.$config->title.'] Verifikasi Pesanan');
                });

            Mail::send('frontend.email.orderConfirmation', $order,
                function ($message) use ($config) {
                    $message
                        ->to($config->email)
                        ->subject('['.$config->title.'] Verifikasi Pesanan');
                });

            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }

    public static function orderDelivery($order,$emailTo){
        $config = Config::find(1);
        try {
            Mail::send('frontend.email.orderDelivery', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($emailTo)
                        ->subject('['.$config->title.'] Pengiriman Pesanan');
                });

            Mail::send('frontend.email.orderDelivery', $order,
                function ($message) use ($config) {
                    $message
                        ->to($config->email)
                        ->subject('['.$config->title.'] Pengiriman Pesanan');
                });

            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }

    public static function orderComplete($order,$emailTo){
        $config = Config::find(1);
        try {
            Mail::send('frontend.email.orderComplete', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($emailTo)
                        ->subject('['.$config->title.'] Pesanan Selesai');
                });

            Mail::send('frontend.email.orderComplete', $order,
                function ($message) use ($config) {
                    $message
                        ->to($config->email)
                        ->subject('['.$config->title.'] Pesanan Selesai');
                });

            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }

    public static function orderCanceled($order,$emailTo){
        $config = Config::find(1);
        $order['CONF'] = $config;
        try {
            Mail::send('frontend.email.orderCanceled', $order,
                function ($message) use ($emailTo,$config) {
                    $message
                        ->to($emailTo)
                        ->subject('['.$config->title.'] Pesanan Dibatalkan');
                });

            Mail::send('frontend.email.orderCanceled', $order,
                function ($message) use ($config) {
                    $message
                        ->to($config->email)
                        ->subject('['.$config->title.'] Pesanan Dibatalkan');
                });

            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }

    public static function contact($contact){
        $config = Config::find(1);
        try {
            Mail::send('frontend.email.contact', ['contact'=>$contact],
                function ($message) use ($contact,$config) {
                    $message
                        ->to($contact['email'],$config->email)
                        ->subject('['.$config->title.'] '.$contact['subject']);
                });
            $result['status'] = true;
        }catch(\Exception $msg){
            \Log::error($msg);
            $result['status'] = false;
            $result['message'] = $msg;
        }

        return $result;
    }
}