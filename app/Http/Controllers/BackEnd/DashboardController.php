<?php

namespace App\Http\Controllers\BackEnd;
use App\Book;
use App\Http\Controllers\Controller;

use App\Tracker;
use App\User;
use App\Util\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends BackEndController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['sidebar'] = 'dashboard';
        $data['user'] = User::where('role',Constant::USER_ROLE_USER)->count();
        $data['book'] = Book::count();
        $visitor = Tracker::select('date','id');

        if(!empty($request->dateFrom)){
            $visitor->where('date','>=',$request->dateFrom);
            $data['dateFrom'] = $request->dateFrom;
        }
        if(!empty($request->dateTo)){
            $visitor->where('date','<=',$request->dateTo);
            $data['dateTo'] = $request->dateTo;
        }

        $data['visitor'] = $visitor->count('id');

        //set Asset Chart
        $periode = [];
        $currentDate = Carbon::now();
        $periode[] = [
            'periode' => $currentDate->format('M-Y'),
            'firstDay' => strtotime($currentDate->startOfMonth()),
            'lastDay' => strtotime($currentDate->endOfMonth()),
            'value' => 0,
        ];

        for($i=1;$i<=12;$i++){
            $currentDate = Carbon::now()->subMonths($i);
            $periode[] = [
                'periode' => $currentDate->format('M-Y'),
                'firstDay' => strtotime($currentDate->startOfMonth()),
                'lastDay' => strtotime($currentDate->endOfMonth()),
                'value' => 0,
            ];
        }

        foreach ($visitor->get() as $visit){
            $orderDate = strtotime($visit->date);

            foreach($periode as $key => $prd){
                if(($orderDate >= $prd['firstDay'])&&($orderDate <= $prd['lastDay'])){
                    $periode[$key]['value']++;
                }
            }
        }

        $data['chart'] = array_reverse($periode,true);;

        return view('backend.dashboard',$data);
    }
}
