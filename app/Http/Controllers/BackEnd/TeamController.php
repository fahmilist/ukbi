<?php

namespace App\Http\Controllers\BackEnd;
use App\Http\Controllers\Controller;

use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DataTables;
use Validator;
use Response;
use Input;
use Excel;

class TeamController extends BackEndController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'team';
        return view('backend.team.index',$data);
    }

    public function save(Request $request)
    {
        $validate_rule = array();
        $validate_rule['name'] = 'required';
        $validate_rule['role'] = 'required';
        //$validate_rule['division'] = 'required';

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $team = Team::find($request->id);

        if(empty($team->id)){
            $team = new Team();
        }

        if ($request->hasFile('image')) {
            if(!empty($team->image) && file_exists('storage/'.$team->image) && ($team->image != 'default.png')){
                unlink('storage/'.$team->image);
            }
            $image = $request->file('image');
            $teamimage = $image->store('team','public');

            $team->image = $teamimage;
        }

        $team->fill((array)$request->all());

        if($team->save()){
            return Response::json(array('status'=>true,'message'=>'Data Pengurus berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data Pengurus Gagal simpan, coba lagi'));
        }
    }

    public function getData($id){
        return Response::json(Team::find($id));
    }

    public function indexData(Request $request){
        $teams = Team::orderBy('id','desc')->get();

        return DataTables::of($teams)
            ->addColumn('image',function($team) {
                return '<img src="'.url('storage/'.$team->image).'" width="100" height="100">';
            })
            ->addColumn('aksi',function($team) {
                return '<a onclick="editData('.$team->id.')" class="btn btn-info btn-xs">Edit</a>'.' '.
                    '<a onclick="deleteData('.$team->id.')"class="btn btn-danger btn-xs">Delete</a>';
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('name') && !empty($request->name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['name'], $request->get('name')) ? true : false;
                    });
                }

                if ($request->has('role') && !empty($request->role) ) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['role'], $request->get('role')) ? true : false;
                    });
                }

                if ($request->has('division') && !empty($request->division)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['division'], $request->get('division')) ? true : false;
                    });
                }
            })
            ->escapeColumns([])->make(true);
    }

    public function delete($id){
        $delete = Team::find($id);
        $delete->deleted = 1;
        $delete->save();
    }
}
