<?php

namespace App\Http\Controllers\BackEnd;
use App\Country;
use App\Http\Controllers\Controller;

use App\Job;
use App\Util\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use DataTables;
use Validator;
use Response;
use Input;
use Excel;

class UserController extends BackEndController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'user';
        $data['countries'] = Country::all();
        $data['jobs'] = Job::all();
        return view('backend.user.index',$data);
    }

    public function save(Request $request)
    {
        $validate_rule = array();
        $validate_rule['name'] = 'required';
        if($request->id == 0){
            $validate_rule['email'] = 'required|unique:users,email';
            $validate_rule['password'] = 'required';
        }
        $validate_rule['role'] = 'required';

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();
            
            return response()->json($data);
        }

        $user = User::find($request->id);

        if(empty($user->id)){
            $user = new User();
        }

        if(!empty($request->password)){
            $user->password = bcrypt($request->password);
        }

        $user->fill((array)$request->all());

        if($user->save()){
            return Response::json(array('status'=>true,'message'=>'Data pengguna berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data pengguna Gagal simpan, coba lagi'));
        }

    }

    public function getUser($id){
        return Response::json(User::find($id));
    }

    public function indexData(Request $request){
      $users = User::with(['country','job'])->orderBy('updated_at','desc')->get();

      return DataTables::of($users)
        ->addColumn('country',function($users) {
          return @$users->country->name;
        })
        ->addColumn('job',function($users) {
          return @$users->job->name;
        })
        ->addColumn('aksi',function($users) {
          return '<a onclick="editPengguna('.$users->id.')" class="btn btn-info btn-xs">Edit</a>'.' '.
          '<a onclick="deletePengguna('.$users->id.')"class="btn btn-danger btn-xs">Delete</a>';
        })
        ->filter(function ($instance) use ($request) {
            if ($request->has('username') && !empty($request->username)) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains(strtoupper($row['username']), strtoupper($request->get('username'))) ? true : false;
                });
            }

            if ($request->has('name') && !empty($request->name)) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains(strtoupper($row['name']), strtoupper($request->get('name'))) ? true : false;
                });
            }

            if ($request->has('email') && !empty($request->email) ) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains(strtoupper($row['email']), strtoupper($request->get('email'))) ? true : false;
                });
            }


            if ($request->has('phone') && !empty($request->phone) ) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['phone'], $request->get('phone')) ? true : false;
                });
            }

            if ($request->has('role') && !empty($request->role) ) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return $row['role'] == $request->get('role') ? true : false;
                });
            }

            if ($request->has('job') && !empty($request->job) ) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return $row['job'] == $request->get('job') ? true : false;
                });
            }

            if ($request->has('country') && !empty($request->country) ) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return $row['country'] == $request->get('country') ? true : false;
                });
            }
        })
        ->escapeColumns([])->make(true);
    }

    public function delete($id){
        $delete = User::find($id);
        $delete->deleted = 1;
        $delete->save();
    }

    public function export()
    {
        return Excel::create( 'export_user_'.time() , function($excel) {

            // Set the title
            $excel->setTitle('Export Data Pengguna');

            // Call them separately
            $excel->setDescription('Data pengguna');

            $users = User::with(['country','job'])->where('role','!=',Constant::USER_ROLE_ADMIN)->get();

            foreach ($users as $key => $user){
                $users[$key]->country = @$user->country->name;
                $users[$key]->job = @$user->job->name;
            }

            $excel->sheet('pengguna', function($sheet) use ($users) {

                // Sheet manipulation
                $sheet->loadView('backend.user.export',['users'=>$users,'model'=>User::class]);

                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                    )
                ));
            });
        })
        ->download('xls');
    }
}
