<?php

namespace App\Http\Controllers\BackEnd;
use App\Blog;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use DataTables;
use Validator;
use Response;
use Input;
use Excel;
use File;

class BlogController extends BackEndController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'blog';
        return view('backend.blog.index',$data);
    }

    public function detail($id){
        $data['sidebar'] = 'blog';
        $data['blog'] = Blog::find($id);
        $data['model'] = Blog::class;
        return view('backend.blog.detail',$data);
    }

    public function save(Request $request)
    {
        $validate_rule = array();
        $validate_rule['title'] = 'required';
        $validate_rule['description'] = 'required';
        $validate_rule['content'] = 'required';

        if($request->id == 0){
            $validate_rule['image'] = 'required';
            $validate_rule['featured'] = 'required';
        }

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $blog = Blog::find($request->id);

        if(empty($blog->id)){
            $blog = new Blog();
        }


        $blog->createdBy = \Auth::user()->id;

        $blog->fill((array)$request->all());

        $imageBlog = $blog->image;
        $featuredBlog = $blog->featured;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            if(!empty($imageBlog) && file_exists('storage/'.$imageBlog)){
                unlink('storage/'.$imageBlog);
            }
            $imageBlog = $image->store('blog','public');
        }

        if ($request->hasFile('featured')) {
            $featured = $request->file('featured');
            if(!empty($featuredBlog) && file_exists('storage/'.$featuredBlog)){
                unlink('storage/'.$featuredBlog);
            }
            $featuredBlog = $featured->store('blog','public');
        }

        $blog->image = $imageBlog;
        $blog->featured = $featuredBlog;

        if($blog->save()){
            return Response::json(array('status'=>true,'message'=>'Data artikel berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data artikel Gagal simpan, coba lagi'));
        }

    }

    public function indexData(Request $request){
        $blogs = Blog::with(['creator'])->orderBy('created_at','desc')->get();

        return DataTables::of($blogs)
            ->addColumn('featured',function($blog) {
                return '<img src="'.url('storage/'.$blog->featured).'" width="100" height="100">';
            })
            ->addColumn('creator',function($blog) {
                return @$blog->creator->name;
            })
            ->addColumn('aksi',function($blog) {
                return '<a href="'.route('admin.blog.detail',['id'=>$blog->id]).'" class="btn btn-info btn-xs">Edit</a>'.' '.
                    '<a onclick="deleteData('.$blog->id.')"class="btn btn-danger btn-xs">Delete</a>';
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('title') && !empty($request->title)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains(strtoupper($row['title']), strtoupper($request->get('title'))) ? true : false;
                    });
                }

                if ($request->has('description') && !empty($request->description)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains(strtoupper($row['description']), strtoupper($request->get('description'))) ? true : false;
                    });
                }
            })
            ->escapeColumns([])->make(true);
    }

    public function delete(Request $request){
        $delete = Blog::find($request->id);
        $delete->deleted = 1;
        $delete->save();
    }
}
