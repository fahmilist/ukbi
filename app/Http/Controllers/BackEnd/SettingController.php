<?php

namespace App\Http\Controllers\BackEnd;
use App\Http\Controllers\Controller;

use App\Config;
use App\Util\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DataTables;
use Validator;
use Response;
use Input;

class SettingController extends BackEndController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'setting';
        $data['config'] = Config::find(1);
        return view('backend.setting.detail',$data);
    }

    public function about()
    {
        $data['sidebar'] = 'setting';
        $data['config'] = Config::find(1);
        return view('backend.setting.about',$data);
    }

    public function faq()
    {
        $data['sidebar'] = 'setting';
        $data['config'] = Config::find(1);
        return view('backend.setting.faq',$data);
    }

    public function slider()
    {
        $data['sidebar'] = 'setting';
        $data['config'] = Config::find(1);
        return view('backend.setting.slider',$data);
    }

    public function save(Request $request)
    {
        $validate_rule = array();
        $validate_rule['title'] = 'required';
        $validate_rule['email'] = 'required';
        $validate_rule['phone'] = 'required';
        $validate_rule['whatsapp'] = 'required';
        $validate_rule['address'] = 'required';

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $config = Config::find($request->idSetting);
        $logoImage = $config->logo;
        $logoSecondImage = $config->logoSecond;
        $iconImage = $config->icon;

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            if(!empty($logoImage) && file_exists('storage/'.$logoImage)){
                unlink('storage/'.$logoImage);
            }
            $logoImage = $logo->store('config','public');
        }

        if ($request->hasFile('logoSecond')) {
            $logoSecond = $request->file('logoSecond');
            if(!empty($logoSecondImage) && file_exists('storage/'.$logoSecondImage)){
                unlink('storage/'.$logoSecondImage);
            }
            $logoSecondImage = $logoSecond->store('config','public');
        }

        if ($request->hasFile('icon')) {
            $icon = $request->file('icon');
            if(!empty($iconImage) && file_exists('storage/'.$iconImage)){
                unlink('storage/'.$iconImage);
            }
            $iconImage = $icon->store('config','public');
        }

        $config->fill((array)$request->all());
        $config->logo = $logoImage;
        $config->logoSecond = $logoSecondImage;
        $config->icon = $iconImage;

        if($config->save()){
            return Response::json(array('status'=>true,'message'=>'Data Pengaturan berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data Pengaturan Gagal simpan, coba lagi'));
        }
    }

    public function aboutSave(Request $request)
    {
        $validate_rule = array();
        $validate_rule['title'] = 'required';
        $validate_rule['content1'] = 'required';

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $config = Config::find($request->idSetting);
        $about = json_decode($config->aboutDetail);
        $bannerImage = $about->banner;
        $image1Image = $about->image1;
        $image2Image = $about->image2;

        if ($request->hasFile('image1')) {
            $image1 = $request->file('image1');
            if(!empty($image1Image) && file_exists('storage/'.$image1Image)){
                unlink('storage/'.$image1Image);
            }
            $image1Image = $image1->store('config','public');
        }

        if ($request->hasFile('image2')) {
            $image2 = $request->file('image2');
            if(!empty($image2Image) && file_exists('storage/'.$image2Image)){
                unlink('storage/'.$image2Image);
            }
            $image2Image = $image2->store('config','public');
        }

        $about->title = $request->title;
        $about->image1 = $image1Image;
        $about->content1 = $request->content1;
        $about->image2 = $image2Image;

        $config->aboutDetail = json_encode($about);

        if($config->save()){
            return Response::json(array('status'=>true,'message'=>'Data Pengaturan berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data Pengaturan Gagal simpan, coba lagi'));
        }
    }

    public function faqSave(Request $request)
    {
        $validate_rule = array();
        $validate_rule['title'] = 'required';

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $config = Config::find($request->idSetting);
        $faq = json_decode($config->faq);
        $bannerImage = $faq->banner;

        if ($request->hasFile('banner')) {
            $banner = $request->file('banner');
            if(!empty($bannerImage) && file_exists('storage/'.$bannerImage)){
                unlink('storage/'.$bannerImage);
            }
            $bannerImage = $banner->store('config','public');
        }

        $faq->title = $request->title;
        $faq->banner = $bannerImage;
        $listQuestion = [];

        foreach ($request->question as $key => $question){
            $listQuestion [] = [
                'title' => $question,
                'content' => $request->answer[$key],
            ];
        }

        $faq->list = $listQuestion;

        $config->faq = json_encode($faq);

        if($config->save()){
            return Response::json(array('status'=>true,'message'=>'Data Pengaturan berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data Pengaturan Gagal simpan, coba lagi'));
        }
    }

    public function sliderSave (Request $request)
    {
        $validate_rule = array();
        /*$validate_rule['title'] = 'required';
        $validate_rule['description'] = 'required';
        $validate_rule['image'] = 'required';
        $validate_rule['link'] = 'required';
        $validate_rule['linkText'] = 'required';*/

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $config = Config::find(1);
        $sliders = json_decode($config->slider);

        if($request->id ==0){
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $sliderImage = $image->store('config/slider','public');
            }

            $sliders[] = [
                'title' => $request->title,
                'description' => $request->description,
                'image' => $sliderImage,
                'link' => $request->link,
                'linkText' => $request->linkText,
            ];
        }else{
            $sliderImage = $sliders[$request->id]->image;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                if(!empty($sliderImage) && file_exists('storage/'.$sliderImage)){
                    unlink('storage/'.$sliderImage);
                }
                $sliderImage = $image->store('config/slider','public');
            }

            $sliders[$request->id] = [
                'title' => $request->title,
                'description' => $request->description,
                'image' => $sliderImage,
                'link' => $request->link,
                'linkText' => $request->linkText,
            ];
        }

        $config->slider = json_encode($sliders);

        if($config->save()){
            return Response::json(array('status'=>true,'message'=>'Data Pengaturan berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data Pengaturan Gagal simpan, coba lagi'));
        }
    }

    public function SliderDelete(Request $request){
        $config = Config::find(1);
        $sliders = json_decode($config->slider);
        if(!empty($sliders[$request->id]->image) && file_exists('storage/'.$sliders[$request->id]->image)){
            unlink('storage/'.$sliders[$request->id]->image);
        }
        array_splice($sliders, $request->id,1);
        $config->slider = json_encode($sliders);
        $config->save();
    }
}
