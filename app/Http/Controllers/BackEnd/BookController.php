<?php

namespace App\Http\Controllers\BackEnd;
use App\Book;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use DataTables;
use Validator;
use Response;
use Input;
use Excel;
use File;

class BookController extends BackEndController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sidebar'] = 'book';
        return view('backend.book.index',$data);
    }

    public function detail($id){
        $data['sidebar'] = 'book';
        $data['book'] = Book::with('page')->find($id);
        if($id != 0){
            Session::put('folder',$data['book']->folder);
        }
        return view('backend.book.detail',$data);
    }

    public function save(Request $request)
    {
        $validate_rule = array();
        $validate_rule['name'] = 'required';
        $validate_rule['publish'] = 'required';
        $validate_rule['totalPage'] = 'required';
        $validate_rule['description'] = 'required';
        $validate_rule['content'] = 'required';

        if($request->idBook == 0){
            $validate_rule['image'] = 'required';
            $validate_rule['featured'] = 'required';
        }

        $validation = Validator::make($request->all(),$validate_rule);

        if($validation->fails()){
            $data['status'] = false;
            $data['message'] = 'Periksa Data Kembali!';
            $data['error'] = $validation->errors();

            return response()->json($data);
        }

        $book = Book::find($request->idBook);

        if(empty($book->id)){
            $book = new Book();
            $folder = uniqid();
            $book->folder = $folder;

            File::makeDirectory(public_path('storage/page/'.$folder));
        }

        $book->fill((array)$request->all());

        $imageBook = $book->image;
        $featuredBook = $book->featured;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            if(!empty($imageBook) && file_exists('storage/'.$imageBook)){
                unlink('storage/'.$imageBook);
            }
            $imageBook = $image->store('book','public');
        }

        if ($request->hasFile('featured')) {
            $featured = $request->file('featured');
            if(!empty($featuredBook) && file_exists('storage/'.$featuredBook)){
                unlink('storage/'.$featuredBook);
            }
            $featuredBook = $featured->store('book','public');
        }

        $book->image = $imageBook;
        $book->featured = $featuredBook;

        if($book->save()){
            $book->page()->delete();
            foreach ($request->pages as $page){
                $ex = explode('-',$page);
                $data['name'] = $ex[0];
                $data['page'] = $ex[1];
                $data['parent'] = $ex[2];
                $book->page()->create($data);
            }
            return Response::json(array('status'=>true,'message'=>'Data buku berhasil disimpan'));
        }else{
            return Response::json(array('status'=>false,'message'=>'Data buku Gagal simpan, coba lagi'));
        }

    }

    public function indexData(Request $request){
        $books = Book::with(['page'])->orderBy('created_at','desc')->get();

        return DataTables::of($books)
            ->addColumn('featured',function($book) {
                return '<img src="'.url('storage/'.$book->featured).'" width="100" height="100">';
            })
            ->addColumn('aksi',function($book) {
                return '<a href="'.route('admin.book.detail',['id'=>$book->id]).'" class="btn btn-info btn-xs">Edit</a>'.' '.
                    '<a onclick="deleteData('.$book->id.')"class="btn btn-danger btn-xs">Delete</a>';
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('name') && !empty($request->name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains(strtoupper($row['name']), strtoupper($request->get('name'))) ? true : false;
                    });
                }

                if ($request->has('description') && !empty($request->description)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains(strtoupper($row['description']), strtoupper($request->get('description'))) ? true : false;
                    });
                }
            })
            ->escapeColumns([])->make(true);
    }

    public function delete(Request $request){
        $delete = Book::find($request->id);
        $delete->deleted = 1;
        $delete->save();
    }

    public function configBook(){
        $newFiles = [];

        $files = File::files(public_path('tmp/TBBBI/'));
        foreach($files as $key => $file){
            $cleanFile = str_replace(public_path(),'',$file);

            $files[$key] = $cleanFile;

            $cleanFile = str_replace('tmp/TBBBI/','',$cleanFile);
            $cleanFile = str_replace('Layout Tata Bahasa Baku BI 171208 Final','',$cleanFile);
            $cleanFile = str_replace('.png','.jpg',$cleanFile);

            $newFiles[] = $cleanFile;

            File::copy(public_path($files[$key]),public_path('tmp/TBBBI/new/'.$newFiles[$key]));
        }

        return json_encode($newFiles);

    }
}
