<?php

namespace App\Http\Controllers\FrontEnd;

use App\Book;
use App\BookMark;
use App\Config;
use App\User;
use App\Util\Constant;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Session;
use Validator;

class BookController extends FrontEndController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $books = Book::orderBy('created_at','desc')->paginate(10);
        return view('frontend.book.index',[
            'books'=>$books,
        ]);
    }

    public function detail($id)
    {
        $book = Book::find($id);
        if( !empty(\Auth::user()->id) ){
            $bookMarks = BookMark::where('book_id',$id)->where('user_id', \Auth::user()->id)->get();
        }else{
            $bookMarks = [];
        }
        $indexBook = [];
        foreach (@$book->page as $index){
            $parent = str_replace(' ','',strtoupper($index->parent));
            $data['page'] = $index->page;
            if(!empty($parent)){
                $indexParent = array_search($parent,array_column($indexBook,'parentId'));
                if($indexParent !== false){
                    $data['title'] = $index->name;
                    $indexBook[$indexParent]['child'][] = $data;
                }else{
                    $data['title'] = $index->parent;
                    $indexBook[] = [
                        'parentId' => $parent,
                        'parent' => $data,
                        'child' => [],
                    ];
                }
            }
        }
        return view('frontend.book.detail',[
            'book'=>$book,
            'indexBook'=> $indexBook,
            'bookMarks'=> $bookMarks,
        ]);
    }

    public function markSave(Request $request){
        if( \Auth::user()->id ){
            $bookMark = new BookMark();
            $bookMark->name = $request->name;
            $bookMark->book_id = $request->id;
            $bookMark->page = $request->page;
            $bookMark->user_id = \Auth::user()->id;
            $bookMark->save();

            $bookMark->status = true;

            return json_encode($bookMark);
        }else{
            return json_encode([
                'status'=>false,
                'message'=> 'Autentikasi Gagal!'
            ]);
        }
    }

    public function markDelete(Request $request){
        $bookMark = BookMark::find($request->id);
        $bookMark->delete();
    }

}



