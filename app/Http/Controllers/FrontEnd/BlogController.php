<?php

namespace App\Http\Controllers\FrontEnd;

use App\Blog;
use App\Config;
use App\Util\Constant;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Session;
use Validator;

class BlogController extends FrontEndController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $blogs = Blog::with('creator')->orderBy('created_at','desc')->paginate(10);
        return view('frontend.blog.index',[
            'blogs'=>$blogs,
        ]);
    }

    public function detail($id)
    {
        $blog = Blog::find($id);
        $others = Blog::limit(5)->where('id','!=',$id)->inRandomOrder()->get();
        return view('frontend.blog.detail',[
            'blog'=>$blog,
            'others'=>$others,
        ]);
    }

}



