<?php

namespace App\Http\Controllers\FrontEnd;

use App\Blog;
use App\Book;
use App\Config;

use App\Service\MailerService;
use App\Team;
use Illuminate\Http\Request;
use Session;

class PageController extends FrontEndController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $books = Book::limit(4)->orderBy('created_at','desc')->get();
        $teams = Team::orderBy('id','asc')->get();
        $blogs = Blog::with('creator')->limit(3)->orderBy('created_at','desc')->get();
        return view('frontend.pages.home',[
            'books'=>$books,
            'teams'=>$teams,
            'blogs'=>$blogs,
        ]);
    }

    public function sendContact(Request $request)
    {
        $contact = array();
        $contact['name'] = $request->name;
        $contact['email'] = $request->email;
        $contact['subject'] = $request->subject;
        $contact['message'] = $request->message;
        MailerService::contact($contact);

        return redirect()->back()->with('success','Terima kasih telah menghubungi kami');
    }

    public function register(){
        return redirect('/');
    }

    public function email(){
        $order = Order::with(['user','orderProduct.product','orderAddress.city.province','orderConfirmation'])->find(199);
        return MailerService::orderSuccess(['order'=>$order],'yohankinata@gmail.com');
        return view('frontend.email.contact',['order'=>$order]);
    }

    public function password(){
        return view('auth.passwords.email');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'username' => $request->username]
        );
    }
    
    public function path(){
        return public_path('/');
    }
}



