<?php

namespace App\Http\Controllers\FrontEnd;

use App\Config;
use App\User;
use App\Util\Constant;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Session;
use Validator;

class MemberController extends FrontEndController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save(Request $request){
        $validate_rule = array();
        $validate_message = array();
        $validate_rule['email'] = 'required|unique:users,email';
        $validate_message['email.required'] = '*Email tidak boleh kosong!';
        $validate_message['email.unique'] = '*Email sudah digunakan! Mohon gunakan email lainnya';

        $validation = Validator::make($request->all(),$validate_rule,$validate_message);

        if($validation->fails()){
            $data['status'] = false;
            $data['title'] = 'Periksa Data Kembali!';
            $data['message'] = 'Email sudah digunakan! Mohon gunakan email lain';
            return redirect()->back()->with('error',$data['title'])->with('message',$data['message']);
        }

        $user = new User();
        $user->fill((array) $request->all());
        $user->password = bcrypt($request->password);
        $user->username = $request->email;
        $user->role = Constant::USER_ROLE_USER;
        $user->save();

        $data['status'] = true;
        $data['title'] = 'Registrasi Berhasil';
        $data['message'] = 'Berhasil mendaftar, lakukan login selanjutnya';

        return redirect()->back()->with('success',$data['title'])->with('message',$data['message']);
    }

}



