<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Province;
use App\City;
use RajaOngkir;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function setProvince()
    {
        $provinces = RajaOngkir::Provinsi()->all();
        $data_provinces = array();
        foreach ($provinces as $province) {
            $data['id'] = $province['province_id'];
            $data['name'] = $province['province'];
            array_push($data_provinces, $data);
        }

        $insert = Province::insert($data_provinces);
        echo json_encode(Province::all());
        //echo json_encode($provinces);
    }

    public function setCity()
    {
        $cities = RajaOngkir::Kota()->all();
        $data_city = array();
        foreach ($cities as $city) {
            $data['id'] = $city['city_id'];
            $data['name'] = $city['type'].' '.$city['city_name'];
            $data['provinceId'] = $city['province_id'];
            array_push($data_city, $data);
        }

        $insert = City::insert($data_city);
        echo json_encode(City::all());
        //echo json_encode($cities);
    }
}
