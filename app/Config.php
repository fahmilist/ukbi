<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    public $timestamps = false;
    protected $table = 'config';

    protected $fillable = [
        'title',
        'titleBanner',
        'descriptionBanner',
        'logo',
        'logoSecond',
        'icon',
        'email',
        'phone',
        'address',
        'lat',
        'lng',
        'about',
        'aboutDetail',
        'faq',
        'slider',
        'facebook',
        'instagram',
        'whatsapp',
        'bank',
    ];

    const FORM_FIELD = [
        'title' => 'text',
        'email' => 'email',
        'phone' => 'text',
        'facebook' => 'text',
        'instagram' => 'text',
        'whatsapp' => 'text',
        'address' => 'textarea',
        'about' => 'textarea',
        'titleBanner' => 'text',
        'descriptionBanner' => 'textarea',
        'logo' => 'file',
        'logoSecond' => 'file',
        'icon' => 'file',
    ];

    const FORM_LABEL = [
        'title' => 'Judul Website',
        'tagline' => 'Tag Line',
        'email' => 'Email Admin',
        'phone' => 'Telepon Admin',
        'facebook' => 'Facebook Link',
        'instagram' => 'Instagram Link',
        'whatsapp' => 'Whatsapp Admin',
        'address' => 'Alamat Kantor',
        'about' => 'Deskripsi Singkat',
        'titleBanner' => 'Headline Beranda',
        'descriptionBanner' => 'Deskripsi Headline Beranda',
        'logo' => 'Logo Depan',
        'logoSecond' => 'Logo Belakang',
        'icon' => 'Fav Icon Website',
    ];

    const FORM_HELP_LIST = ['whatsapp','logo','logoSecond','icon'];

    const FORM_LABEL_HELP = [
        'whatsapp' => 'Gunakan Nomor dengan Kode Negara (62)',
        'logo' => 'Format File(PNG,JPG),Ukuran Max(100kb), Ukuran Gambar (200 x 191)',
        'logoSecond' => 'Format File(PNG,JPG),Ukuran Max(100kb), Ukuran Gambar (180 x 80)',
        'icon' => 'Format File(PNG,JPG),Ukuran Max(100kb), Ukuran Gambar (17 x 17)',
    ];

    const ABOUT_FIELD = [
        'title' => 'text',
        'image1' => 'file',
        'content1' => 'textarea',
        'image2' => 'file',
    ];

    const ABOUT_LABEL = [
        'title' => 'Judul',
        'banner' => 'Gambar Header',
        'header1' => 'Judul Section 1',
        'image1' => 'Gambar Section 1',
        'content1' => 'Konten Section 1',
        'image2' => 'Gambar Section 2',
        'header2' => 'Judul Section 2',
        'content2' => 'Konten Section 2',
    ];

    const ABOUT_LABEL_LIST = ['banner','image1','image2'];

    const ABOUT_LABEL_HELP = [
        'banner' => 'Format File(PNG,JPG),Ukuran Max(100kb), Ukuran Gambar (1920 x 239)',
        'image1' => 'Format File(PNG,JPG),Ukuran Max(400kb), Ukuran Gambar (1200 x 1200)',
        'image2' => 'Format File(PNG,JPG),Ukuran Max(400kb), Ukuran Gambar (1200 x 1200)',
    ];

    const FAQ_FIELD = [
        'title' => 'text',
        'banner' => 'file',
    ];

    const FAQ_LABEL = [
        'title' => 'Judul',
        'banner' => 'Gambar Header',
    ];

    const FAQ_LABEL_LIST = ['banner'];

    const FAQ_LABEL_HELP = [
        'banner' => 'Format File(PNG,JPG),Ukuran Max(100kb), Ukuran Gambar (1920 x 239)',
    ];

}
