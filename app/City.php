<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;
    protected $table = 'cities';

    public function province()
    {
    	return $this->belongsTo(Province::class,'provinceId');
    }

    public function product()
    {
    	return $this->hasMany(Product::class,'cityId');
    }

    public function orderAddress(){
        return $this->hasMany(OrderAddress::class,'cityId');
    }
}
