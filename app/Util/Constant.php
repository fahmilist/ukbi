<?php

namespace App\Util;

class Constant {

    const USER_ROLE_USER = 'USER';
    const USER_ROLE_ADMIN = 'ADMIN';

    const USER_ROLES = [
        self::USER_ROLE_USER => 'Pengguna',
        self::USER_ROLE_ADMIN => 'Super Admin',
    ];

    const REGISTRATION_MANUAL = 0;
    const REGISTRATION_SYSTEM = 1;

    const REGISTRATION_TYPE = [
      self::REGISTRATION_MANUAL => 'Tamu',
      self::REGISTRATION_SYSTEM => 'Member',
    ];

    const PRODUCT_LABEL_NONE = '';
    const PRODUCT_LABEL_FEATURED = 1;
    const PRODUCT_LABEL_POPULAR = 2;
    const PRODUCT_LABEL_PROMO = 3;
    const PRODUCT_LABEL_MEMBER = 4;

    const PRODUCT_LABELS = [
      self::PRODUCT_LABEL_NONE => 'Tidak Ada',
      self::PRODUCT_LABEL_FEATURED => 'Unggulan',
      self::PRODUCT_LABEL_POPULAR => 'Populer',
      self::PRODUCT_LABEL_PROMO => 'Promo',
      self::PRODUCT_LABEL_MEMBER => 'Diskon',
    ];

    const PRODUCT_STATUS_INACTIVE = 0;
    const PRODUCT_STATUS_ACTIVE = 1;
    const PRODUCT_STATUS_DRAFT = 2;

    const PRODUCT_STATUS = [
      self::PRODUCT_STATUS_ACTIVE => 'ACTIVE',
      self::PRODUCT_STATUS_INACTIVE => 'INACTIVE',
      self::PRODUCT_STATUS_DRAFT => 'DRAFT',
    ];


    const PRODUCT_GENERAL = 0;
    const PRODUCT_MEMBER = 1;

    const PRODUCT_TYPE = [
        self::PRODUCT_GENERAL => 'Umum',
        self::PRODUCT_MEMBER => 'Member',
    ];

    const ORDER_GUEST = 0;
    const ORDER_MEMBER = 1;

    const ORDER_TYPE = [
        self::ORDER_GUEST => 'Tamu',
        self::ORDER_MEMBER => 'Member',
    ];

    const ORDER_UNPAID = 'UNPAID';
    const ORDER_PROCESS = 'PROCESS';
    const ORDER_CONFIRM = 'CONFIRM';
    const ORDER_DELIVERY = 'DELIVERY';
    const ORDER_COMPLETE = 'COMPLETE';
    const ORDER_CANCELED = 'CANCELED';
    const ORDER_EXPIRED = 'EXPIRED';

    const ORDER_STATUS = [
        self::ORDER_UNPAID => 'Belum Bayar',
        self::ORDER_PROCESS => 'Diprosess',
        self::ORDER_CONFIRM => 'Dikonfirmasi',
        self::ORDER_DELIVERY => 'Pengiriman',
        self::ORDER_COMPLETE => 'Selesai',
        self::ORDER_CANCELED => 'Dibatalkan',
        self::ORDER_EXPIRED => 'Expired',
    ];

    const ORDER_LABEL = [
        self::ORDER_UNPAID => 'default',
        self::ORDER_PROCESS => 'success',
        self::ORDER_CONFIRM => 'primary',
        self::ORDER_DELIVERY => 'info',
        self::ORDER_COMPLETE => 'success',
        self::ORDER_CANCELED => 'danger',
        self::ORDER_EXPIRED => 'warning',
    ];

    const ORDER_EMAIL_SUCCESS = '';
    const ORDER_EMAIL_PROCESS = '';
    const ORDER_EMAIL_CONFIRM = '';
    const ORDER_EMAIL_DELIVERY = '';
    const ORDER_EMAIL_COMPLETE = '';
    const ORDER_EMAIL_CANCELED = '';
    const ORDER_EMAIL_EXPIRED = '';

    const SCHOOL_LEVEL_TK = 'TK';
    const SCHOOL_LEVEL_SD = 'SD';
    const SCHOOL_LEVEL_SMP = 'SMP';
    const SCHOOL_LEVEL_SMA = 'SMA';
    const SCHOOL_LEVEL_LIST = [
        self::SCHOOL_LEVEL_TK => 'TK/Play Ground/KB',
        self::SCHOOL_LEVEL_SD => 'SD/Sederajat',
        self::SCHOOL_LEVEL_SMP => 'SMP/Sederajat',
        self::SCHOOL_LEVEL_SMA => 'SMA/Sederajat',
    ];

    const SCHOOL_RATING_A = 'A';
    const SCHOOL_RATING_B = 'B';
    const SCHOOL_RATING_C = 'C';
    const SCHOOL_RATING_NO = 'NO';
    const SCHOOL_RATING_LIST = [
        self::SCHOOL_RATING_A => 'Akreditasi A',
        self::SCHOOL_RATING_B => 'Akreditasi B',
        self::SCHOOL_RATING_C => 'Akreditasi C',
        self::SCHOOL_RATING_NO => 'Belum Terakreditasi',
    ];



    const COURIER = [
        'jne' => 'Jalur Nugraha Ekakurir (JNE)',
        'pos'=> 'POS Indonesia (POS)',
        'tiki'=> 'Citra Van Titipan Kilat (TIKI)',
        'rpx'=> 'Priority Cargo and Package (PCP)',
        'esl'=> 'Eka Sari Lorena (ESL)',
        'pcp'=>'Priority Cargo and Package (PCP)',
        'pandu'=>'Pandu Logistics (PANDU)',
        'wahana'=>'Wahana Prestasi Logistik (WAHANA)',
        'sicepat'=>'SiCepat Express (SICEPAT)',
        'jnt'=>'J&T Express (J&T)',
        'pahala'=>'Pahala Kencana Express (PAHALA)',
        'sap'=>'SAP Express (SAP)',
        'jet'=>'JET Express (JET)',
        'dse'=>'21 Express (DSE)',
        'slis'=>'Solusi Ekspres (SLIS)',
        'first'=>'First Logistics (FIRST)',
        'ncs'=>'Nusantara Card Semesta (NCS)	',
        'star'=>'Star Cargo (STAR)',
        'ninja'=>'Ninja Xpress (NINJA)',
        'lion'=>'Lion Parcel (LION)',
    ];

}