<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public $timestamps = false;
    protected $table = 'members';

    protected $fillable = [
        'userId',
        'memberName',
        'memberEmail',
        'memberPhone',
        'fax',
        'masterName',
        'masterEmail',
        'masterPhone',
        'institutionName',
        'institutionMaster',
        'level',
        'rating',
        'image',
        'certificate',
        'npsnImage',
        'ratingImage',
        'logo',
        'maleCount',
        'femaleCount',
        'sppAverage',
        'maleTeacher',
        'femaleTeacher',
        'year',
        'website',
        'lat',
        'long',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'userId');
    }
}
