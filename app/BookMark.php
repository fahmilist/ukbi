<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookMark extends Model
{
    protected $table = 'bookMarks';
    public $timestamps = false;
    protected $fillable = [
        'book_id',
        'user_id',
        'name',
        'page',
    ];

    public function book(){
        return $this->belongsTo(Book::class,'book_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
