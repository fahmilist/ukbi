<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookPage extends Model
{
    protected $table = 'bookPages';
    protected $fillable = [
        'book_id',
        'parent',
        'name',
        'page',
    ];

    public function book(){
        return $this->belongsTo(Book::class,'book_id');
    }
}
