<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\City;
use App\Province;
use App\Util\Constant;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'role',
        'phone',
        'countryId',
        'jobId',
    ];

    const exportDataUser = [
        'name' => 'Nama',
        'email' => 'Email',
        'phone' => 'Telepon',
        'country' => 'Negara',
        'job' => 'Profesi',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function country(){
    return $this->belongsTo(Country::class,'countryId');
}

    public function job(){
        return $this->belongsTo(Job::class,'jobId');
    }

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('users.deleted', 0);
    }
}
