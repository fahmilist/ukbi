<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $timestamps = false;
    protected $table = 'provinces';

    public function city()
    {
        return $this->hasMany(City::class,'provinceId');
    }

    public function orderAddress(){
        return $this->hasMany(OrderAddress::class,'provinceId');
    }
}
