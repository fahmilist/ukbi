<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = [
        'title',
        'description',
        'content',
        'createdBy',
    ];

    const FORM_FIELD = [
        'title' => 'text',
        'description' => 'textarea',
        'content' => 'ckeditor',
        'image' => 'file',
        'featured' => 'file',
    ];

    const FORM_LABEL = [
        'title' => 'Judul',
        'description' => 'Deskripsi Singkat',
        'content' => 'Isi Artikel',
        'image' => 'Gambar Detail',
        'featured' => 'Gambar Depan',
    ];

    const FORM_HELP_LIST = ['image','featured','description'];

    const FORM_LABEL_HELP = [
        'image' => 'Format File(PNG,JPG),Ukuran Max(1000kb), Ukuran Gambar (17 x 17)',
        'featured' => 'Format File(PNG,JPG),Ukuran Max(1000kb), Ukuran Gambar (17 x 17)',
        'description' => 'Maksimal 190 karakter',
    ];

    public function creator(){
        return $this->belongsTo(User::class,'createdBy');
    }

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('blogs.deleted', 0);
    }
}
