<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $fillable = [
        'name',
        'description',
        'totalPage',
        'skipPage',
        'content',
        'publish',
        'folder',
    ];

    const FORM_FIELD = [
        'name' => 'text',
        'publish' => 'date',
        'totalPage' => 'text',
        'skipPage' => 'text',
        'description' => 'textarea',
        'content' => 'textarea',
        'image' => 'file',
        'featured' => 'file',
    ];

    const FORM_LABEL = [
        'name' => 'Judul',
        'description' => 'Deskripsi Singkat',
        'publish' => 'Tanggal Rilis',
        'totalPage' => 'Total Halaman',
        'skipPage' => 'Total Halaman Awal',
        'content' => 'Isi Buku',
        'image' => 'Kover Lengkap',
        'featured' => 'Kover Depan',
    ];

    const FORM_HELP_LIST = ['image','featured'];

    const FORM_LABEL_HELP = [
        'image' => 'Format File(PNG,JPG),Ukuran Max(1000kb), Ukuran Gambar (17 x 17)',
        'featured' => 'Format File(PNG,JPG),Ukuran Max(1000kb), Ukuran Gambar (17 x 17)',
    ];

    public function page(){
        return $this->hasMany(BookPage::class,'book_id')->orderBy('page','asc');
    }

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('books.deleted', 0);
    }
}
