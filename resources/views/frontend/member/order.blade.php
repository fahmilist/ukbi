@extends('frontend.layouts.template')

@section('pageTitle','Keranjang Belanja')

@push('customCss')

@endpush

@section('content')

    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="{{route('home')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Beranda
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="{{route('member')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Member
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
				Daftar Pesanan
			</span>
        </div>
    </div>


    <!-- Shoping Cart -->
    <form action="{{route('saveCart')}}" id="formCheckout" method="post" class="bg0 p-t-75 p-b-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xl-12 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="wrap-table-shopping-cart">
                            <table class="table table-hover table-striped">
                                <tr class="table_head">
                                    <th class="column-1">Kode</th>
                                    <th class="column-2">Jumlah</th>
                                    <th class="column-3">Tanggal Transaksi</th>
                                    <th class="column-4">Produk</th>
                                    <th class="column-5">Pengiriman</th>
                                    <th class="column-6">Status</th>
                                    <th class="column-7">Aksi</th>
                                </tr>
                            @if(count($orders)>0)
                                @foreach($orders as $key => $order)
                                <tr>
                                    <td class="column-1">{{$order->code}}</td>
                                    <td class="column-2">Rp {{number_format($order->totalPrice)}}</td>
                                    <td class="column-3">{{$order->created_at}}</td>
                                    <td class="column-4">{{count($order->orderProduct)}}</td>
                                    <td class="column-5">{{$order->shippingMethod}}</td>
                                    <td class="column-6">
                                        {{
                                            \App\Util\Constant::ORDER_STATUS[$order->status]
                                        }}
                                    </td>
                                    <td class="column-7">
                                        @if($order->status == \App\Util\Constant::ORDER_UNPAID)

                                        <a href="{{url('confirm/'.$order->code)}}" target="_blank">
                                            <div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                                Konfirmasi
                                            </div>
                                        </a>

                                        @elseif($order->status != \App\Util\Constant::ORDER_UNPAID && $order->status != \App\Util\Constant::ORDER_EXPIRED)
                                        <a href="{{url('order-download/'.$order->id)}}" target="_blank">
                                            <div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                                Cetak
                                            </div>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr class="table_row" id="itemCart">
                                    <td class="column-1" colspan="4">
                                        <h5>Daftar Pesanan Kosong :(</h5>
                                    </td>
                                    <td class="column-5">
                                        <a href="{{route('product')}}">
                                        <div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                            Belanja
                                        </div>
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@push('customJs')
    
@endpush