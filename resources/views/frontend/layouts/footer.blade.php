<!-- Page Kontak -->
    <footer class="footer-area relative sky-bg" id="kontak-page">
        <div class="absolute footer-bg"></div>
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                        <div class="page-title">
                            <h2>Kontak Kami</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <address class="side-icon-boxes">
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="{{url('frontend/images/location-arrow.png')}}" alt="">
                                </div>
                                <p><strong>Alamat: </strong> {{@$CONF->address}} Indonesia</p>
                            </div>
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="{{url('frontend/images/phone-arrow.png')}}" alt="">
                                </div>
                                <p><strong>Telepon: </strong>
                                    <a href="callto:{{@$CONF->phone}}">{{@$CONF->phone}}</a><br>
                                    <a href="callto:{{@$CONF->whatsapp}}">{{@$CONF->whatsapp}}</a>
                                </p>
                            </div>
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="{{url('frontend/images/mail-arrow.png')}}" alt="">
                                </div>
                                <p><strong>Email: </strong>
                                    <a href="mailto:hello@kemendikbud.com">{{@$CONF->email}}</a>
                                </p>
                            </div>
                        </address>
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <form action="{{route('sendContact')}}" method="post" class="contact-form">
                            {{ csrf_field() }}
                            <div class="form-double">
                                <input type="text" id="form-name" name="name" placeholder="Tulis Nama.." class="form-control" required="required">
                                <input type="email" id="form-email" name="email" class="form-control" placeholder="Tulis Alamat Email.." required="required">
                            </div>
                            <input type="text" id="form-subject" name="subject" class="form-control" placeholder="Tulis Judul Subjek..">
                            <textarea name="message" id="form-message" name="message" rows="5" class="form-control" placeholder="Tulis pesan..." required="required"></textarea>
                            <button type="submit" class="button">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 pull-right">
                        <ul class="social-menu text-right x-left">
                            <li><a href="{{@$CONF->facebook}}"><i class="ti-facebook"></i></a></li>
                            <li><a href="{{@$CONF->instagram}}"><i class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <p>{{@$CONF->about}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p>&copy;Copyright {{\Carbon\Carbon::now()->format('Y')}}. Kincat All Right Reserved </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
	<span class="symbol-btn-back-to-top">
		<i class="zmdi zmdi-chevron-up"></i>
	</span>
</div>