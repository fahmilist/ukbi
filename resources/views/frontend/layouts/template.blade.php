<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $CONF->title }} || @yield('pageTitle')</title>

    @include('frontend.layouts.css')

    @stack('customCss')
</head>
<body data-spy="scroll" data-target="#primary-menu">

    <div class="preloader">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>

    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>

    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')

    {{--@include('frontend.layouts.preview')--}}

    @include('frontend.layouts.modal')

    @include('frontend.layouts.js')

    @stack('customJs')
    
</body>
</html>
