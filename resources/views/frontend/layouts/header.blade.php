    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{url('/')}}/#beranda-page" class="navbar-brand logo">
                    <img src="{{ url('frontend/images/acuan-kebahasaan.png') }}" alt="kemendikbud.go.id">
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('/')}}/#beranda-page">Beranda</a></li>
                    <li><a href="{{url('/')}}/#tentang-page">Tentang</a></li>
                    <li><a href="{{url('/')}}/#statistik-page">Statistik</a></li>
                    <li><a href="{{url('/')}}/#pengurus-page">Pengurus</a></li>
                    <li><a href="{{url('/')}}/#artikel-page">Artikel</a></li>
                    <li><a href="{{url('/')}}/#kontak-page">Kontak</a></li>
                    @if(empty(\Auth::user()))
                    <li><a href="#signup" data-toggle="modal" data-target=".log-sign">Masuk</a></li>
                    @else
                    <li><a href="{{route('logout')}}">Keluar</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->