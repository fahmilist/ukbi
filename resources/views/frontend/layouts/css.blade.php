	<!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" href="{{url('storage/'.$CONF->icon)}}"/>
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ url('frontend/css/font-awesome.min.css') }}">
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{ url('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/animate.css') }}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{ url('frontend/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/style.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ url('frontend/css/book-style.css') }}">
    <link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') }}">

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->