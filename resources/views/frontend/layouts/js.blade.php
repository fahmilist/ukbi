    <!--Vendor-JS-->
    <script src="{{ url('frontend/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <script src="{{ url('frontend/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ url('frontend/js/vendor/bootstrap.min.js') }}"></script>
    <!--Plugin-JS-->
    <script src="{{ url('frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ url('frontend/js/contact-form.js') }}"></script>
    <script src="{{ url('frontend/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ url('frontend/js/scrollUp.min.js') }}"></script>
    <script src="{{ url('frontend/js/magnific-popup.min.js') }}"></script>
    <script src="{{ url('frontend/js/wow.min.js') }}"></script>
    <!--Main-active-JS-->
    <script src="{{ url('frontend/js/main.js') }}"></script>
    <!-- LoginJS -->
    <!-- Sweet Alert -->
    <script src="{{url('backend/assets/global/sweetalert.min.js')}}"></script>
    <!-- Sweet Alert -->
    <script>
        $(window, document, undefined).ready(function() {

        $('.input').blur(function() {
        var $this = $(this);
        if ($this.val())
            $this.addClass('used');
        else
            $this.removeClass('used');
        });

        });

        $('#tab1').on('click' , function(){
        $('#tab1').addClass('login-shadow');
        $('#tab2').removeClass('signup-shadow');
        });

        $('#tab2').on('click' , function(){
        $('#tab2').addClass('signup-shadow');
        $('#tab1').removeClass('login-shadow');

        });

        // ===== Scroll to Top ====
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
    </script>
    <script type="text/javascript">
        var success = '{!! session('success') !!}';
        var error = '{!! session('error') !!}';

        if(success){
            swal({
                title: success,
                text: '{{session('message')}}',
                icon: 'success'
            });
        }

        if(error){
            swal({
                title: error,
                text: '{{session('message')}}',
                icon: 'error'
            });
        }
    </script>