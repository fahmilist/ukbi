@extends('frontend.layouts.template')

@section('pageTitle','Detail Buku')

@push('customCss')
    <style type="text/css">
        .mainmenu-area.affix-top {
            background-color: #1e3163;
        }
    </style>
@endpush

@section('content')
    <section class="gray-bg section-padding" id="tentang-page">
        <div class="container">
            <div class="row">
                <div class="col-md-9 text-left">
                    <div class="row">
                        <div class="page-title">
                            <h2>{{@$book->name}}</h2>
                            {!! @$book->content !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="t">
                            <div class="tc rel">
                                <div class="zoom-viewport">
                                    <div class="book" id="book">
                                        <div class="page" ondblclick="setZoom(1)"><img src="{{ url('storage/'.$book->featured) }}" alt="" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p class="remark-book">* Arahkan kursor ke pojok halaman atau gunakan arah pada keyboard untuk mengganti halaman. Klik 2 kali pada tombol 'Zoom' untuk memperbesar tampilan.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="panel">
                        <a class="btn btn-app" href="#search-collapse" title="cari halaman ini" data-parent="#accordion" data-toggle="collapse">
                            <i class="fa fa-search"></i><br> Cari
                        </a>
                        <a class="btn btn-app" href="#save-collapse" title="tandai halaman ini" data-parent="#accordion" data-toggle="collapse">
                            <i class="fa fa-bookmark"></i><br> Tandai
                        </a>

                        <div class="col-md-12" id="accordion">
                            <div class="panel search">
                                <div id="search-collapse" class="collapse in">
                                    <ul class="search-form">
                                        <li><input type="number" class="form-control" name="searchPage" placeholder="masukan nomor halaman.."></li>
                                        <li><button class="btn btn-md btn-info form-control"  style="height: 46px;margin-left: 4px;" type="button" onclick="setPage(0)">Cari Halaman</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel bookmark">
                                <div id="save-collapse" class="collapse">
                                    <ul class="search-form">
                                        <li>
                                            <ul style="padding-left: 15px;margin-bottom:20px;" class="bookmarkList">
                                                @foreach($bookMarks as $bookMark)
                                                    <li style="cursor: pointer;text-align: left" class="bookmark-{{ $bookMark->id }}"><a onclick="setPage({{ $bookMark->page }})">{{ $bookMark->name }}</a> <a href="javascript:;" onclick="removePage({{ $bookMark->id }})"><i class="fa fa-remove"></i></a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li><input type="text" class="form-control" name="savePage" placeholder="beri nama halaman.."></li>
                                        <li><button class="btn btn-md btn-info form-control" style="height: 46px;margin-left: 4px;" type="button" onclick="savePage({{ @$book->id }})">Simpan Halaman</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 jquery-accordion-menu txt-uppercase mg20" id="jquery-accordion-menu">

                        <div class="accordion js-accordion">
                            {{-- use active if u want to recently open --}}
                            {{-- <div class="accordion__item js-accordion-item active"> --}}
                            <div class="accordion-header txt-center"><strong>daftar isi</strong></div>
                            @foreach($indexBook as $index)
                                <div class="accordion__item js-accordion-item">
                                    <div class="accordion-header js-accordion-header" onclick="setPage({{ $index['parent']['page'] }})">{{ $index['parent']['title'].' ('.$index['parent']['page'].')' }}</div>
                                    @if(count($index['child']) > 0)
                                        <div class="accordion-body js-accordion-body">
                                            <div class="accordion-body__contents">
                                                <ul class="nav">
                                                    @foreach($index['child'] as $key => $indexChild)
                                                        <li><a href="javascript:;" onclick="setPage({{ $indexChild['page'] }})">{{ $indexChild['title'].' ('.$indexChild['page'].')' }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div> <!-- end of accordion body -->
                                    @endif
                                </div><!-- end of accordion item -->
                            @endforeach
                        </div><!-- end of accordion -->
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('customJs')
    <!-- Turn JS -->
    <script type="text/javascript" src="{{ url('frontend/book-preview/extras/jquery.mousewheel.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('frontend/book-preview/lib/hash.js') }}"></script>
    <script type="text/javascript" src="{{ url('frontend/book-preview/lib/turn.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('frontend/book-preview/js/book-style.js') }}"></script>

    <script type="text/javascript">

        var book = $("#book");
        var scale = 0;

        var currentPage = 0;
        var totalPage = {{@$book->totalPage}};
        var skipBook = {{@$book->skipPage}};

        (function () {
            'use strict';

            var module = {
                ratio: 1.38,
                init: function (id) {
                    var me = this;

                    // if older browser then don't run javascript
                    if (document.addEventListener) {
                        this.el = document.getElementById(id);
                        this.resize();
                        this.plugins();

                        // on window resize, update the plugin size
                        window.addEventListener('resize', function (e) {
                            var size = me.resize();
                            $(me.el).turn('size', size.width, size.height);
                        });
                    }
                },
                resize: function () {
                    // reset the width and height to the css defaults
                    this.el.style.width = '';
                    this.el.style.height = '';

                    var width = this.el.clientWidth,
                        height = Math.round(width / this.ratio),
                        padded = Math.round(document.body.clientHeight * 0.9);

                    // if the height is too big for the window, constrain it
                    if (height > padded) {
                        height = padded;
                        width = Math.round(height * this.ratio);
                    }

                    // set the width and height matching the aspect ratio
                    this.el.style.width = width + 'px';
                    this.el.style.height = height + 'px';

                    return {
                        width: width,
                        height: height
                    };
                },
                plugins: function () {
                    // run the plugin
                    $(this.el).turn({
                        gradients: true,
                        acceleration: true
                    });
                    // hide the body overflow
                    document.body.className = 'hide-overflow';
                }
            };

            module.init('book');

            setInit();
        }());

        function setInit() {
            book.turn('pages',totalPage);
            for(i=1;i<=5;i++){
                addPage(i,book);
            }
        }

        book.bind("turning", function(event, page, view) {
            var range = $(this).turn('range', page);
            for (page = range[0]; page<=range[1]; page++)
                addPage(page, $(this));
        });

        book.bind("missing", function(event, pages) {
            for (var i = 0; i < pages.length; i++) {
                $(this).turn("addPage", $("<div />"), pages[i]);
            }
        });

        $(document).keydown(function(e){

            var previous = 37, next = 39;

            switch (e.keyCode) {
                case previous:

                    book.turn('previous');

                    break;
                case next:

                    book.turn('next');

                    break;
            }

        });

        book.bind("end", function(event, pageObject, turned){
            event.preventDefault();
        });

        function loadPage(page) {

            var img = $('<img />');
            img.load(function() {
                var container = $('.book .p'+page);
                img.css({width: '100%', height: '100%'});
                img.appendTo($('.book .p'+page));
            });

            img.attr('src', '{{url('storage/page/'.@$book->folder)}}/' +  page + '.jpg');

        }

        function addPage(page, book) {
            // Check if the page is not in the book
            if (!book.turn('hasPage', page)) {
                // Add the page
                var position = page%2==0?'right':'left';
                var element = '<div><button type="button" class="btn btn-primary zoom-button" alt="Perbesar" title="Perbesar" style="'+position+': 5%" onclick="setZoom('+page+')"><i class="fa fa-search"></i> Zoom</button></div>';
                // Get the data for this page
                if (book.turn('addPage', element, page)) {
                    loadPage(page);
                }
            }
        }

        function setPage(page) {
            @if(empty(\Auth::user()->id))
                return swal({
                title: 'Autentikasi Gagal',
                text: 'Fitur ini hanya untuk pengguna yang terautentikasi',
                icon: 'error',
                timer: '3000'
            });
            @endif

            if(page == 0 ) page = parseFloat($('[name=searchPage]').val());
            var current = (page+skipBook);
            if(current>totalPage) return;
            book.turn("page", current);
        }

        function savePage(id){
            swal({
                title: "Yakin Simpan Data Halaman?",
                text : "Data halaman akan disimpan",
                icon: "warning",
                buttons: {
                    cancel:true,
                    confirm: {
                        text:'Simpan!',
                        closeModal: false,
                    },
                },
            })
                .then((process) => {
                if(process){

                    @if(empty(\Auth::user()->id))
                        return swal({
                        title: 'Autentikasi Gagal',
                        text: 'Fitur ini hanya untuk pengguna yang terautentikasi',
                        icon: 'error',
                        timer: '3000'
                    });
                    @endif

                    if($('[name=savePage]').val() == ''){
                        return swal({
                            title: 'Gagal Simpan Halaman',
                            text: 'Nama Halaman Tidak Boleh Kosong',
                            icon: 'error',
                            timer: '3000'
                        });
                    }

                    $.ajax({
                        url: "{{ route('book.mark.save') }}",
                        type: "POST",
                        dataType: 'json',
                        data: {
                            '_token': '{{csrf_token()}}',
                            'id':id,
                            'name':$('[name=savePage]').val(),
                            'page':book.turn('page'),
                        },
                        success: function(data) {
                            if(data.status){
                                swal({
                                    title: 'Berhasil Simpan Halaman!',
                                    text: 'Halaman berhasil di simpan',
                                    icon: 'success',
                                    timer: '3000'
                                });
                                var newBookmark = '<li style="cursor: pointer;text-align: left" class="bookmark-'+data.id+'"><a onclick="setPage('+data.page+')">'+data.name+'</a> <a href="javascript:;" onclick="removePage('+data.id+')"><i class="fa fa-remove"></i></a></li>';
                                $('[name=savePage]').val('');
                                $('.bookmarkList').append(newBookmark);
                            }else{
                                swal({
                                    title: 'Gagal Simpan Halaman!',
                                    text: data.message,
                                    icon: 'error',
                                    timer: '3000'
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '3000'
                            });
                        }
                    });
                }else{
                    swal('Data Halaman tidak jadi disimpan');
        }
        });
        }

        function setZoom(page){
            $('#book .page-wrapper .p'+page).zoom({ on:'click' });
        }

        function removePage(id){
            swal({
                title: "Yakin Hapus Data Halaman?",
                text : "Data halaman akan dihapus",
                icon: "warning",
                buttons: {
                    cancel:true,
                    confirm: {
                        text:'Hapus!',
                        closeModal: false,
                    },
                },
            })
                .then((process) => {
                if(process){

                    @if(empty(\Auth::user()->id))
                        return swal({
                        title: 'Autentikasi Gagal',
                        text: 'Fitur ini hanya untuk pengguna yang terautentikasi',
                        icon: 'error',
                        timer: '3000'
                    });
                    @endif

                    $.ajax({
                        url: "{{ route('book.mark.delete') }}",
                        type: "POST",
                        data: {
                            '_token': '{{csrf_token()}}',
                            'id':id,
                        },
                        success: function(data) {
                            swal({
                                title: 'Berhasil Hapus Halaman!',
                                text: 'Halaman berhasil dihapus',
                                icon: 'success',
                                timer: '3000'
                            });
                            $('.bookmark-'+id).remove();
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '3000'
                            });
                        }
                    });
                }else{
                    swal('Data Halaman tidak jadi dihapus');
        }
        });
        }
    </script>

    <script type="text/javascript" src="{{ url('frontend/js/jquery.zoom.js') }}"></script>
@endpush