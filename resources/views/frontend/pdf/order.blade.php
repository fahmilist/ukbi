@extends('frontend.layouts.pdf')

@section('content')
<table>
    <tr>
        <td  class="main-header" style="color: #484848; font-size: 14px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
            Informasi pesanan <b>{{strtoupper($order->code)}}</b>, berikut penjelasan detail pesanan:
        </td>
    </tr>
    <tr>
        <td>
            <b>Detil Pesanan</b>
            <table style="margin-top:10px;width: 100%">
                @if(!empty($order->invoice))
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Nomor Resi</th>
                    <td>{{$order->invoice}}</td>
                </tr>
                @endif
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Tanggal Pemesanan</th>
                    <td>{{date('l, d F Y (H:i)',strtotime($order->created_at))}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Pengiriman</th>
                    <td>{{$order->shippingType.' - '.$order->shippingMethod}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Subtotal</th>
                    <td>Rp. {{number_format($order->amount)}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Shipping</th>
                    <td>Rp. {{number_format($order->shippingFee)}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Unique Code</th>
                    <td>Rp. {{number_format($order->uniquePrice)}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Total</th>
                    <td>Rp. {{number_format($order->totalPrice)}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Keterangan</th>
                    <td>{{$order->note}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td class="break"></td></tr>
    <tr>
        <td>
            <b>Detil Konfirmasi</b>
            <table style="margin-top:10px;width: 100%">
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Tanggal Konfirmasi</th>
                    <td>{{date('l, d F Y',strtotime($order->orderConfirmation->first()->confirmationDate))}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Nama Rekening</th>
                    <td>{{$order->orderConfirmation->first()->name}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Nomor Rekening</th>
                    <td>{{$order->orderConfirmation->first()->accountNumber.' ('.$order->orderConfirmation->first()->bankName.')'}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Jumlah Pembayaran</th>
                    <td>Rp. {{number_format($order->orderConfirmation->first()->amount)}}</td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Bukti Bayar</th>
                    <td><a href="{{url('storage/'.$order->orderConfirmation->first()->image)}}">Lihat</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td class="break"></td></tr>
    <tr>
        <td>
            <b>Detil Produk</b>
            <table style="margin-top:10px;width: 100%;text-align: center;" border="1px #000 solid">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Varian</th>
                    <th>Jumlah</th>
                </tr>
                @foreach($order->orderProduct as $key => $orderProduct)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{ucwords($orderProduct->product->name)}}</td>
                    <td>Rp {{number_format($orderProduct->product->price)}}</td>
                    <td>{{ucwords($orderProduct->color.' | '.$orderProduct->size)}}</td>
                    <td>{{ucwords($orderProduct->qty)}}</td>
                </tr>
                @endforeach
            </table>
        </td>
    </tr>
    <tr><td class="break"></td></tr>
    <tr>
        <td>
            <b>Alamat tujuan pengiriman</b>
            <br>
            {{ucwords($order->user->name)}}
            <br>
            {{$order->orderAddress->address}}
            <br>
            {{$order->orderAddress->kecamatan->nama}}
            <br>
            {{$order->orderAddress->kabupaten->nama.' - '.$order->orderAddress->provinsi->nama.' ( '.$order->orderAddress->postcode.' )'}}
            <br>
            {{$order->user->phone}}
        </td>
    </tr>
</table>
@endsection
