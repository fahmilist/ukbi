@extends('frontend.layouts.template')

@section('pageTitle','Daftar Artikel')

@push('customCss')
    <link rel="stylesheet" href="{{url('frontend/css/artikel-style.css')}}">
    <style type="text/css">
        .mainmenu-area.affix-top {
            background-color: #1e3163;
        }
    </style>
@endpush

@section('content')
    <div id="artikel-page" class="section md-padding">
        <!-- Page Blog -->
        <section id="artikel-page">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                        <div class="page-title">
                            <h2>Artikel</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                        </div>
                    </div>
                </div>
                <div class="row blog-row">
                    @foreach(@$blogs as $blog)
                        <div class="col-md-4 m-b-20">
                            <div class="single-blog">
                                <div class="blog-photo">
                                    <img src="{{ url('storage/'.$blog->featured) }}" alt="{{@$blog->title}}">
                                </div>
                                <div class="blog-content">
                                    <h3>{{@$blog->title}}</h3>
                                    <ul class="blog-meta">
                                        <li><span class="fa fa-user"></span> {{@$blog->creator->name}}</li>
                                        <li><span class="fa fa-calendar"></span> {{\Carbon\Carbon::parse(@$blog->created_at)->format('d F, Y')}}</li>
                                    </ul>
                                    <p>{{ @$blog->description }}</p>
                                    <a href="{{ route('blog.detail',['id'=>@$blog->id,'title'=>str_replace(' ','-',@$blog->title)]) }}">Baca Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row text-center">
                    {{ $blogs->links() }}
                </div>
            </div>
        </section>

    </div>
@endsection

@push('customJs')

@endpush