@extends('frontend.layouts.template')

@section('pageTitle','Detail Artikel')

@push('customCss')
    <link rel="stylesheet" href="{{url('frontend/css/artikel-style.css')}}">
    <style type="text/css">
        .mainmenu-area.affix-top {
            background-color: #1e3163;
        }
    </style>
@endpush

@section('content')
    <div id="artikel-page" class="section md-padding">
        <div class="container">
            <div class="row v-center">
                <div class="col-xs-12 col-md-7 header-text">
                    <h2>{{@$blog->title}}</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">{{@$blog->title}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Container -->
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Main -->
                <main id="main" class="col-md-9">
                    <div class="blog">
                        @if(!empty($blog->image))
                        <div class="blog-img">
                            <img class="img-responsive" src="{{ url('storage/'.$blog->image) }}" alt="{{@$blog->title}}">
                        </div>
                        @endif
                        <div class="blog-content">
                            <ul class="blog-meta">
                                <li><i class="fa fa-user"></i>{{ @$blog->creator->name }}</li>
                                <li><i class="fa fa-clock-o"></i>{{ \Carbon\Carbon::parse(@$blog->created_at)->format('D F, Y') }}</li>
                            </ul>
                            {!! @$blog->content !!}
                        </div>
                    </div>
                </main>
                <!-- /Main -->
                <!-- Aside -->
                <aside id="aside" class="col-md-3">
                    {{--<!-- Search -->
                    <div class="widget">

                        <div class="widget-search">

                            <input class="search-input" type="text" placeholder="search">

                            <button class="search-btn" type="button"><i class="fa fa-search"></i></button>

                        </div>

                    </div>

                    <!-- /Search -->--}}
                    <!-- Posts sidebar -->

                    <div class="widget">
                        <h3 class="title">Artikel Lainnya</h3>
                        <!-- single post -->
                        @foreach(@$others as $other)
                        <div class="widget-post">
                            <a href="{{ route('blog.detail',['id'=>@$other->id,'title'=>str_replace(' ','-',@$other->title)]) }}">
                                <img src="{{ url('storage/'.$other->featured) }}" alt="{{ @$other->title }}" width="100" height="100"> {{ @$other->title }}
                            </a>
                            <ul class="blog-meta">
                                <li>{{ \Carbon\Carbon::parse(@$other->created_at)->format('D F, Y') }}</li>
                            </ul>
                        </div>
                        @endforeach
                        <!-- /single post -->
                    </div>
                    <!-- /Posts sidebar -->
                </aside>
                <!-- /Aside -->
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->
    </div>
@endsection

@push('customJs')

@endpush