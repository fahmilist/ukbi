@extends('frontend.layouts.template')

@section('pageTitle','Beranda')

@push('customCss')

@endpush

@section('content')
    <!-- Separate-area -->
    <section class="navbar-bg section-menu" id="beranda-page">
        
    </section>
	<!-- Page Beranda -->
    <header class="header-area overlay full-height relative">
        <div class="absolute anlge-bg"></div>        
        <div class="container">            
            <div class="row">                
				<div class="col-md-6 col-sm-6 pd-top home-text">
					<div class="header-text">
						<h2>{{ @$CONF->titleBanner }}</h2>
					</div>
                    <p>{{@$CONF->descriptionBanner}}</p>

                    <a href="{{ route('books') }}" class="m-t-20"><button type="button" class="button">Daftar Buku</button></a>
                </div>
				<div class="col-md-6 col-sm-6 home-image">
					<div id="about-slider" class="screen-box screen-slider home-slider">
                        @foreach(@$books as $book)
                        <div class="item">
                            <img src="{{ url('storage/'.$book->featured) }}" alt="{{$book->name}}">
                        </div>
                        @endforeach
					</div>
				</div>
            </div>
        </div>
    </header>

    <!-- Page Tentang -->
    <section class="gray-bg section-padding" id="tentang-page">
        <div class="container">
                <!-- why choose us content -->
				<div class="col-md-6">
					<div class="page-title2">
						<h2>Tentang Kami</h2>
					</div>
					<p class="text-justify">{{ json_decode(@$CONF->aboutDetail)->content1 }}</p>
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div id="about-slider" class="screen-box about-slider screen-slider">
                        <div class="item">
                            <img src="{{ url('storage/'.json_decode(@$CONF->aboutDetail)->image1) }}" alt="" style="">
                        </div>
                        <div class="item">
                            <img src="{{ url('storage/'.json_decode(@$CONF->aboutDetail)->image2) }}" alt="" style="">
                        </div>
					</div>
				</div>
				<!-- /About slider -->
            </div>
        </div>
    </section>

	<!-- Page Tentang -->
    <section class="angle-bg sky-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @foreach(@$books as $book)
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>{{@$book->name}}</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>{{@$book->description}}</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="{{ route('book.detail',['id'=>@$book->id,'name'=>@$book->folder]) }}" class="button">Baca Selengkapnya</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="{{ url('storage/'.@$book->image) }}" alt="{{@$book->name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators caption-indector">
                            @foreach(@$books as $key => $book)
                            <li data-target="#caption_slide" data-slide-to="{{$key}}" {{ $key==0?'class="active"':'' }}>
                                <strong>{{$key+1}} </strong>{{@$book->name}}
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Statistik -->
    <section class="gray-bg section-padding" id="statistik-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Statistik</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3>{{\App\Tracker::count()}}</h3>
                        </div>
                        <h3>Jumlah Pengunjung</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3>{{\App\Book::count()}}</h3>
                        </div>
                        <h3>Jumlah Buku</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3>{{\App\User::where('role',\App\Util\Constant::USER_ROLE_USER)->count()}}</h3>
                        </div>
                        <h3>Jumlah Pendaftar</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Pengurus -->
    <section class="section-padding sky-bg client-area overlay" id="pengurus-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Pengurus</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="clients">
                    <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                        @foreach(@$teams as $team)
                        <div class="item">
                            <div class="single-team">
                            <div class="team-photo">
                                <img src="{{ url('storage/'.$team->image) }}" alt="{{@$team->name}}">
                            </div>
                            <h5>{{@$team->name}}</h5>
                            <h6 class="role">{{@$team->role}}</h6>
                            <h6 class="division">{{(@$team->division)}}</h6>
                            </div>
                        </div>
                        @endforeach
                    <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Blog -->
    <section class="section-padding gray-bg" id="artikel-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Artikel</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row blog-row">
                @foreach(@$blogs as $blog)
                <div class="col-md-4">
                    <div class="single-blog">
                        <div class="blog-photo">
                            <img src="{{ url('storage/'.$blog->featured) }}" alt="{{@$blog->title}}">
                        </div>
                        <div class="blog-content">
                            <h3>{{@$blog->title}}</h3>
                            <ul class="blog-meta">
                                <li><span class="fa fa-user"></span> {{@$blog->creator->name}}</li>
                                <li><span class="fa fa-calendar"></span> {{\Carbon\Carbon::parse(@$blog->created_at)->format('d F, Y')}}</li>
                            </ul>
                            <p>{{ @$blog->description }}</p>
                            <a href="{{ route('blog.detail',['id'=>@$blog->id,'title'=>str_replace(' ','-',@$blog->title)]) }}">Baca Selengkapnya</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row text-center m-t-20">
                <a href="{{ route('blogs') }}"><button type="button" class="button">Daftar Artikel</button></a>
            </div>
        </div>
    </section>

@endsection

@push('customJs')

@endpush