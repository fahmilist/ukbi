@extends('backend.layouts.template')

@section('pageTitle','Detail Artikel')

@push('customCss')

@endpush

@section('content')
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Beranda</a> <i class="fa fa-circle"></i></li>
                    <li><a href="{{route('admin.blogs')}}">Artikel</a> <i class="fa fa-circle"></i></li>
                    <li><span>Detail Artikel</span></li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-dark"></i> 
                                    <span
                                    class="caption-subject font-dark bold uppercase">   Detail Artikel
                                    </span>
                            </div>
                            <div class="actions">
                                
                            </div>

                        </div>

                        <div class="portlet-body form">
                            <form action="javascript:;" id="formBlog" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="{{empty($blog->id)?0:$blog->id}}">
                                {{csrf_field()}}
                                <h4>Informasi Artikel</h4>
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($model::FORM_FIELD as $keyField => $field)
                                @if($i%2==0)
                                <div class="row">
                                @endif
                                    <div class="col-md-{{ $field=='ckeditor'?'12':'6' }}">
                                        <div class="form-group" id="{{$keyField}}">
                                            <label class="control-label">{{$model::FORM_LABEL[$keyField]}}</label>
                                            @if($field == 'text')
                                            <input type="{{$field}}" name="{{$keyField}}" class="form-control" value="{{@$blog->$keyField}}" placeholder="{{$model::FORM_LABEL[$keyField]}}">
                                            @elseif($field=='date')
                                            <input type="text" name="{{$keyField}}" class="form-control datepickerinput" maxlength="" value="{{@$blog->$keyField}}" placeholder="{{$model::FORM_LABEL[$keyField]}}">
                                            @elseif($field == 'file')
                                            <input type="{{$field}}" name="{{$keyField}}" class="form-control" value="{{@$blog->$keyField}}" placeholder="{{$model::FORM_LABEL[$keyField]}}">
                                            <br>
                                            @if(!empty(@$blog->id))
                                            <a href="{{url('storage/'.@$blog->$keyField)}}" target="_blank" title="preview"><img style="background-color: #e1e1e1;max-width: 200px" src="{{url('storage/'.@$blog->$keyField)}}"  height="100"></a>
                                            @endif
                                            @elseif($field == 'select')
                                                @if($keyField == 'member')
                                                    <select class="form-control" name="{{$keyField}}">
                                                        @foreach(\App\Util\Constant::PRODUCT_TYPE as $type => $label)
                                                            <option value="{{$type}}" {{$type==@$blog->member?'selected':''}}>{{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select class="form-control" name="{{$keyField}}">
                                                        @foreach(\App\Util\Constant::PRODUCT_STATUS as $status => $label)
                                                            <option value="{{$status}}" {{$status==@$blog->status?'selected':''}}>{{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            @elseif($field == 'ckeditor')
                                                <textarea class="form-control ckeditor" id="{{$keyField}}" name="{{$keyField}}">{{@$blog->$keyField}}</textarea>
                                            @else
                                            <textarea class="form-control" name="{{$keyField}}">{{@$blog->$keyField}}</textarea>
                                            @endif
                                            <div class="help-block">{{ in_array($keyField,$model::FORM_HELP_LIST)? $model::FORM_LABEL_HELP[$keyField]:''}}</div>
                                            <div id="{{$keyField}}_error" class="help-block help-block-error"> </div>
                                        </div>
                                    </div>
                                @if(($i+1)%2==0)
                                </div>
                                @endif
                                @php
                                    $i++;
                                @endphp
                                @endforeach
                            </form>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9 text-right">
                                    <a href="{{route('admin.blogs')}}" class="btn default" >Kembali</a>
                                    <button type="button" class="btn btn-primary" onclick="save()">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page content -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection 

@push('customJs')
    <script type="text/javascript">
        function save(){
            CKEDITOR.instances['content'].updateElement();

            swal({
                title: "Yakin Simpan Data Artikel?",
                text : "Data Artikel akan disimpan",
                icon: "warning",
                buttons: {
                    cancel:true,
                    confirm: {
                        text:'Simpan!',
                        closeModal: false,
                    },
                },
            })
            .then((process) => {
                if(process){

                    $('.form-group').removeClass('has-error');
                    $('.help-block-error').html('');

                    $.ajax({
                        url: "{{ route('blog.save') }}",
                        type: "POST",
                        data: new FormData($("#formBlog")[0]),
                        processData: false,
                        contentType: false,
                        async:false,
                        success: function(response) {
                            if(response.status){
                                swal({
                                    title: 'Berhasil Simpan Data Artikel',
                                    text: response.message,
                                    icon: 'success',
                                    timer: '1500'
                                }).then((done)=>{
                                    location.href = '{{route("admin.blogs")}}';
                                });
                            }else{
                                swal({
                                    title: 'Gagal Simpan Data Artikel',
                                    text: response.message,
                                    icon: 'error',
                                    timer: '1500'
                                });
                                var error_arr = ['title','description','content','image','featured'];
                                for(var i=0;i < error_arr.length;i++){
                                    if(error_arr[i] in response.error){
                                        $('#'+error_arr[i]).addClass('has-error');
                                        $('#'+error_arr[i]+'_error').html(response.error[error_arr[i]]);
                                    }
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '1500'
                            });
                        }
                    });
                }else{
                    swal('Data Artikel tidak jadi disimpan');
                }
            });
        }   
    </script>
@endpush