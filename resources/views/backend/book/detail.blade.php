@extends('backend.layouts.template')

@section('pageTitle','Detail Buku')

@push('customCss')

@endpush

@section('content')
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Beranda</a> <i class="fa fa-circle"></i></li>
                    <li><a href="{{route('admin.books')}}">Buku</a> <i class="fa fa-circle"></i></li>
                    <li><span>Detail Buku</span></li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-dark"></i> 
                                    <span
                                    class="caption-subject font-dark bold uppercase">   Detail Buku
                                    </span>
                            </div>
                            <div class="actions">
                                
                            </div>

                        </div>

                        <div class="portlet-body form">
                            <form action="javascript:;" id="formBook" enctype="multipart/form-data">
                                <input type="hidden" name="idBook" value="{{empty($book->id)?0:$book->id}}">
                                {{csrf_field()}}
                                <h4>Informasi Book</h4>
                                @php
                                    $i = 0;
                                @endphp
                                @foreach(\App\Book::FORM_FIELD as $keyField => $field)
                                @if($i%2==0)
                                <div class="row">
                                @endif
                                    <div class="col-md-6">
                                        <div class="form-group" id="{{$keyField}}">
                                            <label class="control-label">{{\App\Book::FORM_LABEL[$keyField]}}</label>
                                            @if($field == 'text')
                                            <input type="{{$field}}" name="{{$keyField}}" class="form-control" value="{{@$book->$keyField}}" placeholder="{{\App\Book::FORM_LABEL[$keyField]}}">
                                            @elseif($field=='date')
                                            <input type="text" name="{{$keyField}}" class="form-control datepickerinput" value="{{@$book->$keyField}}" placeholder="{{\App\Book::FORM_LABEL[$keyField]}}">
                                            @elseif($field == 'file')
                                            <input type="{{$field}}" name="{{$keyField}}" class="form-control" value="{{@$book->$keyField}}" placeholder="{{\App\Book::FORM_LABEL[$keyField]}}">
                                            <br>
                                            @if(!empty(@$book->id))
                                            <a href="{{url('storage/'.@$book->$keyField)}}" target="_blank" title="preview"><img style="background-color: #e1e1e1;max-width: 200px" src="{{url('storage/'.@$book->$keyField)}}"  height="100"></a>
                                            @endif
                                            @elseif($field == 'select')
                                                @if($keyField == 'member')
                                                    <select class="form-control" name="{{$keyField}}">
                                                        @foreach(\App\Util\Constant::PRODUCT_TYPE as $type => $label)
                                                            <option value="{{$type}}" {{$type==@$book->member?'selected':''}}>{{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select class="form-control" name="{{$keyField}}">
                                                        @foreach(\App\Util\Constant::PRODUCT_STATUS as $status => $label)
                                                            <option value="{{$status}}" {{$status==@$book->status?'selected':''}}>{{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            @else
                                            <textarea class="form-control" name="{{$keyField}}">{{@$book->$keyField}}</textarea>
                                            @endif
                                            <div class="help-block">{{ in_array($keyField,\App\Book::FORM_HELP_LIST)? \App\Book::FORM_LABEL_HELP[$keyField]:''}}</div>
                                            <div id="{{$keyField}}_error" class="help-block help-block-error"> </div>
                                        </div>
                                    </div>
                                @if(($i+1)%2==0)
                                </div>
                                @endif
                                @php
                                    $i++;
                                @endphp
                                @endforeach
                                <hr>
                                <h4>Informasi Daftar Isi</h4>
                                <div class="row">
                                    <small style="color:red;padding-left: 15px">* Kosongkan kolom parent jika tidak memiliki parent</small>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Parent</label>
                                            <input type="text" class="form-control" name="detailParent0" placeholder="Nama Parent"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Nama</label>
                                            <input type="text" class="form-control" name="detailName0" placeholder="Nama Daftar Isi"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Halaman</label>
                                            <input type="number" class="form-control" name="detailPage0" placeholder="Halaman"/>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label">Tambah</label>
                                        <button class="btn btn-sm btn-success" type="button" onclick="saveSub(0)"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="col-md-7">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center">Parent</th>
                                                    <th style="text-align: center">Nama</th>
                                                    <th style="text-align: center">Halaman</th>
                                                    <th style="text-align: center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="listSubCat">
                                            @if(!empty($book->id))
                                                @foreach(@$book->page as $key => $page)
                                                <tr class="tr_{{$key+1}}">
                                                    <td><input type="text" class="form-control" name="detailParent{{$key+1}}" onchange="saveSub({{$key+1}})" placeholder="Nama Parent" value="{{@$page->parent}}"/></td>
                                                    <td><input type="text" class="form-control" name="detailName{{$key+1}}" onchange="saveSub({{$key+1}})" placeholder="Nama Daftar Isi" value="{{@$page->name}}"/></td>
                                                    <td><input type="number" class="form-control" name="detailPage{{$key+1}}" onchange="saveSub({{$key+1}})" placeholder="Halaman" value="{{@$page->page}}"/></td>
                                                    <td class="text-center">
                                                        <button class="btn btn-sm btn-danger" onclick="deleteSub({{$key+1}})"><i class="fa fa-remove"></i></button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot id="listSub">
                                            @if(!empty($book->id))
                                                @foreach(@$book->page as $key => $page)
                                                <input type="hidden" name="pages[]" class="tr_{{$key+1}}" value="{{$page->name.'-'.$page->page.'-'.$page->parent}}">
                                                @endforeach
                                            @endif
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                @if(!empty($book->id))
                                <hr>
                                <h4>Informasi Folder</h4>
                                <div class="row">
                                    <iframe src="{{url('elfinder?folder='.$book->folder)}}" style="width : 100%; min-height: 400px" frameborder="0"></iframe>
                                </div>
                                @endif
                            </form>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9 text-right">
                                    <a href="{{route('admin.books')}}" class="btn default" >Kembali</a>
                                    <button type="button" class="btn btn-primary" onclick="save()">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page content -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection 

@push('customJs')
    <script type="text/javascript">
        var no_page = 0;
        var arr_sub = [];
        @if(!empty($book->id))
        no_page = {{$book->page->count()}};

        @foreach($book->page as $key => $page)
        arr_sub.push({{$key+1}});
        @endforeach
        @endif

        function save(){
            swal({
                title: "Yakin Simpan Data Buku?",
                text : "Data Buku akan disimpan",
                icon: "warning",
                buttons: {
                    cancel:true,
                    confirm: {
                        text:'Simpan!',
                        closeModal: false,
                    },
                },
            })
            .then((process) => {
                if(process){

                    if(arr_sub.length == 0){
                        return swal({
                            title: 'Periksa Data Kembali',
                            text: 'Minimal Satu Daftar Isi',
                            icon: 'error',
                            timer: '1500'
                        });
                    }

                    $('.form-group').removeClass('has-error');
                    $('.help-block-error').html('');

                    $.ajax({
                        url: "{{ route('book.save') }}",
                        type: "POST",
                        data: new FormData($("#formBook")[0]),
                        processData: false,
                        contentType: false,
                        async:false,
                        success: function(response) {
                            if(response.status){
                                swal({
                                    title: 'Berhasil Simpan Data Buku',
                                    text: response.message,
                                    icon: 'success',
                                    timer: '1500'
                                }).then((done)=>{
                                    location.href = '{{route("admin.books")}}';
                                });
                            }else{
                                swal({
                                    title: 'Gagal Simpan Data Buku',
                                    text: response.message,
                                    icon: 'error',
                                    timer: '1500'
                                });
                                var error_arr = ['name','publish','description','content','image','featured'];
                                for(var i=0;i < error_arr.length;i++){
                                    if(error_arr[i] in response.error){
                                        $('#'+error_arr[i]).addClass('has-error');
                                        $('#'+error_arr[i]+'_error').html(response.error[error_arr[i]]);
                                    }
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '1500'
                            });
                        }
                    });
                }else{
                    swal('Data Buku tidak jadi disimpan');
                }
            });
        }   
    </script>

    <script type="text/javascript">
        function saveSub(id){
            var current = id;
            if(current == 0){
                no_page++;
                current = no_page;
            }
            var parent = $('[name=detailParent'+id+']').val();
            var name = $('[name=detailName'+id+']').val();
            var page = $('[name=detailPage'+id+']').val();
            if(id == 0){
                var row = '<tr class="tr_'+current+'">\n' +
                    '                    <td style="text-align: center;font-weight: 100;"><input type="text" class="form-control" name="detailParent'+current+'" onchange="saveSub('+current+')" placeholder="Nama Parent" value="'+parent+'"/></td>\n' +
                    '                    <td style="text-align: center;font-weight: 100;"><input type="text" class="form-control" name="detailName'+current+'" onchange="saveSub('+current+')" placeholder="Nama Daftar Isi" value="'+name+'"/></td>\n' +
                    '                    <td style="text-align: center;font-weight: 100;"><input type="number" class="form-control" name="detailPage'+current+'" onchange="saveSub('+current+')" placeholder="Halaman" value="'+page+'"/></td>\n' +
                    '                    <td style="text-align: center">\n' +
                    '                      <button class="btn btn-sm btn-danger" onclick="deleteSub('+current+')"><i class="fa fa-remove"></i></button>\n' +
                    '                    </td>\n' +
                    '                  </tr>';
                $('#listSub').append(row);
                $('#listSub').append('<input type="hidden" name="pages[]" class="tr_'+current+'" value="'+name+'-'+page+'-'+parent+'">');
                arr_sub.push(current);
            }else{
                $('.tr_'+current).val(name+'-'+page+'-'+parent);
            }
        }

        function deleteSub(id){
            console.log(id);
            var index = arr_sub.indexOf(id);
            if(index === -1) return;
            arr_sub.splice(index,1);
            $('.tr_'+id).remove();
        }
    </script>
@endpush