@extends('backend.layouts.template')

@section('pageTitle','Detail Pengguna')

@push('customCss')

@endpush

@section('content')
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li><a href="{{route('admin.dashboard')}}">Beranda</a> <i class="fa fa-circle"></i></li>
                    <li><a href="{{route('users')}}">Member</a> <i class="fa fa-circle"></i></li>
                    <li><span>Detail Member</span></li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-dark"></i> 
                                    <span
                                    class="caption-subject font-dark bold uppercase">   Informasi Akun
                                    </span>
                            </div>
                            <div class="actions">
                                
                            </div>

                        </div>

                        <div class="portlet-body form">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-striped">
                                        @foreach($user::LeftDetail as $left => $valLeft)
                                        <tr>
                                            <th>{{$valLeft}}</th>
                                            <td>{{$user->$left}}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-striped">
                                        @foreach($user::RightDetail as $right => $valRight)
                                        <tr>
                                            <th>{{$valRight}}</th>
                                            <td>{{$user->$right}}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if(empty($user->member))
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9 text-right">
                                    <a href="{{route('users')}}" class="btn default" >Kembali</a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            @if(!empty($user->member))
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-dark"></i> 
                                    <span
                                    class="caption-subject font-dark bold uppercase">   Informasi Member
                                    </span>
                            </div>
                            <div class="actions">
                                
                            </div>

                        </div>

                        <div class="portlet-body form">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-striped">
                                        @foreach($user::LeftMemberDetail as $left => $valLeft)
                                        <tr>
                                            <th>{{$valLeft}}</th>
                                            <td>{{$user->member->$left}}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-striped">
                                        @foreach($user::RightMemberDetail as $right => $valRight)
                                        <tr>
                                            <th>{{$valRight}}</th>
                                            <td>{{$user->member->$right}}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9 text-right">
                                    <a href="{{route('users')}}" class="btn default" >Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- page content -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection 

@push('customJs')

@endpush