<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed "
			data-keep-expanded="false" data-auto-scroll="true"
			data-slide-speed="200" style="padding-top: 20px">
			<li class="nav-item {{@$sidebar === 'dashboard'? 'active' : ''}} "><a
				href="{{url('admin')}}" class="nav-link nav-toggle"> <i
					class="icon-home"></i> <span class="title">Beranda</span> <span
					class="arrow {{@$sidebar === 'dashboard'? '' : 'hidden'}}"></span>
			</a></li>
			@if(\Auth::user()->role == \App\Util\Constant::USER_ROLE_ADMIN)
			<li class="nav-item {{@$sidebar === 'user'? 'active' : ''}}"><a
				href="{{route('users')}}" class="nav-link nav-toggle"> <i
					class="fa fa-user"></i> <span class="title">Pengguna</span> <span
					class="arrow {{@$sidebar === 'user'? '' : 'hidden'}}"></span>
			</a></li>
			<li class="nav-item {{@$sidebar === 'book'? 'active' : ''}}"><a
				href="{{route('admin.books')}}" class="nav-link nav-toggle"> <i
					class="fa fa-book"></i> <span class="title">Buku</span> <span
					class="arrow {{@$sidebar === 'book'? '' : 'hidden'}}"></span>
			</a></li>

			<li class="nav-item {{@$sidebar === 'team'? 'active' : ''}}"><a
				href="{{route('teams')}}" class="nav-link nav-toggle"> <i
				class="fa fa-users"></i> <span class="title">Pengurus</span> <span
				class="arrow {{@$sidebar === 'team'? '' : 'hidden'}}"></span>
			</a></li>
			<li class="nav-item {{@$sidebar === 'blog'? 'active' : ''}}"><a
				href="{{route('admin.blogs')}}" class="nav-link nav-toggle"> <i
				class="fa fa-pencil"></i> <span class="title">Artikel</span> <span
				class="arrow {{@$sidebar === 'blog'? '' : 'hidden'}}"></span>
			</a></li>
			<li class="nav-item {{@$sidebar === 'setting'? 'active' : ''}}">
				<a href="javascript" class="nav-link nav-toggle"> <i
						class="fa fa-gears"></i> <span class="title">Pengaturan</span>
				<span class="arrow {{@$sidebar === 'setting'? '' : 'hidden'}}"></span>
			</a>
				<ul class="sub-menu">
					<li>
						<a href="{{route('settings')}}">
							<i class="icon-share"></i>
							Umum</a>
					</li>
					<li>
						<a href="{{route('abouts')}}">
							<i class="icon-bulb"></i>
							Tentang</a>
					</li>
				</ul>
			</li>
			@endif
		</ul>
	</div>
</div>