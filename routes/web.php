<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// PAGES //
Route::get('/', 'FrontEnd\PageController@index')->name('home');
Route::get('/about', 'FrontEnd\PageController@about')->name('about');
Route::get('/books', 'FrontEnd\BookController@index')->name('books');
Route::get('/book/{id}/{folder}', 'FrontEnd\BookController@detail')->name('book.detail');
Route::post('/book/mark-save', 'FrontEnd\BookController@markSave')->name('book.mark.save');
Route::post('/book/mark-delete', 'FrontEnd\BookController@markDelete')->name('book.mark.delete');
Route::get('/blogs', 'FrontEnd\BlogController@index')->name('blogs');
Route::get('/blog/{id}/{slug}', 'FrontEnd\BlogController@detail')->name('blog.detail');
Route::post('/contact', 'FrontEnd\PageController@sendContact')->name('sendContact');

Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin:ADMIN']], function () {
    //DASHBOARD
    Route::get('/', 'BackEnd\DashboardController@index')->name('admin.dashboard');
    //Member
    Route::get('/users', 'BackEnd\UserController@index')->name('users');
    Route::get('/userData', 'BackEnd\UserController@indexData')->name('user.data');
    Route::get('/userGet/{id}', 'BackEnd\UserController@getUser')->name('user.get');
    Route::get('/userDetail/{id}', 'BackEnd\UserController@detail')->name('user.detail');
    Route::post('/userSave', 'BackEnd\UserController@save')->name('user.save');
    Route::post('/userDelete/{id}', 'BackEnd\UserController@delete')->name('user.delete');
    Route::get('/userExport', 'BackEnd\UserController@export')->name('user.export');

    //BOOK
    Route::get('/books', 'BackEnd\BookController@index')->name('admin.books');
    Route::get('/bookData', 'BackEnd\BookController@indexData')->name('book.data');
    Route::get('/bookDetail/{id}', 'BackEnd\BookController@detail')->name('admin.book.detail');
    Route::post('/bookSave', 'BackEnd\BookController@save')->name('book.save');
    Route::post('/bookDelete', 'BackEnd\BookController@delete')->name('book.delete');

    //TEAM
    Route::get('/teams', 'BackEnd\TeamController@index')->name('teams');
    Route::get('/teamData', 'BackEnd\TeamController@indexData')->name('team.data');
    Route::get('/teamGet/{id}', 'BackEnd\TeamController@getData')->name('team.get');
    Route::post('/teamSave', 'BackEnd\TeamController@save')->name('team.save');
    Route::post('/teamDelete/{id}', 'BackEnd\TeamController@delete')->name('team.delete');

    //BOOK
    Route::get('/books', 'BackEnd\BookController@index')->name('admin.books');
    Route::get('/bookData', 'BackEnd\BookController@indexData')->name('book.data');
    Route::get('/bookDetail/{id}', 'BackEnd\BookController@detail')->name('admin.book.detail');
    Route::post('/bookSave', 'BackEnd\BookController@save')->name('book.save');
    Route::post('/bookDelete', 'BackEnd\BookController@delete')->name('book.delete');

    //BLOG
    Route::get('/blogs', 'BackEnd\BlogController@index')->name('admin.blogs');
    Route::get('/blogData', 'BackEnd\BlogController@indexData')->name('blog.data');
    Route::get('/blogDetail/{id}', 'BackEnd\BlogController@detail')->name('admin.blog.detail');
    Route::post('/blogSave', 'BackEnd\BlogController@save')->name('blog.save');
    Route::post('/blogDelete', 'BackEnd\BlogController@delete')->name('blog.delete');

    //SETTING
    Route::get('/settings', 'BackEnd\SettingController@index')->name('settings');
    Route::post('/settingSave', 'BackEnd\SettingController@save')->name('setting.save');
    Route::get('/abouts', 'BackEnd\SettingController@about')->name('abouts');
    Route::post('/aboutSave', 'BackEnd\SettingController@aboutSave')->name('about.save');
    Route::get('/faqs', 'BackEnd\SettingController@faq')->name('faqs');
    Route::post('/faqSave', 'BackEnd\SettingController@faqSave')->name('faq.save');
    Route::get('/sliders', 'BackEnd\SettingController@slider')->name('sliders');
    Route::post('/sliderSave', 'BackEnd\SettingController@sliderSave')->name('slider.save');
    Route::post('/sliderDelete', 'BackEnd\SettingController@sliderDelete')->name('slider.delete');

    //ADMIN PURPOSE TO RESTRUCTURE PAGE BOOK
    Route::get('/configBook', 'BackEnd\BookController@configBook')->name('admin.book.config');
});
//SHIPPING//

Route::get('/login', 'FrontEnd\PageController@index')->name('login');
Route::get('/password/reset', 'FrontEnd\PageController@password')->name('password.request');
Route::get('/password/reset/{token}', 'FrontEnd\PageController@showResetForm')->name('password.reset');

Route::post('/register','FrontEnd\MemberController@save')->name('save.registration');

Route::get('/email','FrontEnd\PageController@email');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('storage:link');
    echo $exitCode;
});

//Route::get('/path','FrontEnd\PageController@path');

