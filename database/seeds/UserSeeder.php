<?php

use Illuminate\Database\Seeder;
use App\City;
use App\Kecamatan;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'kincat only',
            'username' => 'kincat',
            'email' => 'kincat@gmail.com',
            'phone' => '123456',
            'provinceId' =>'110000',
            'cityId'=>'110600',
            'districtId' => '110611',
            'password' => bcrypt('1234'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role'	=> 'ADMIN'
        ]);

        DB::table('users')->insert([
            'name' => 'kincat only',
            'username' => 'kincat1',
            'email' => 'kincat1@gmail.com',
            'phone' => '123456',
            'provinceId' =>'110000',
            'cityId'=>'110600',
            'districtId' => '110611',
            'password' => bcrypt('1234'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role'  => 'USER'
        ]);

        $faker = Faker\Factory::create();

        $files = Storage::files('public/users');
        $photos = array();
        foreach ($files as $file) {
            array_push($photos, str_replace('public/', '', $file));
        }

        $countDocs = count($photos)-1;

        $districts = array();
        foreach (Kecamatan::all() as $district) {
            $district['district'] = $district->id;
            $district['city'] = $district->kabupaten->id;
            $district['province'] = $district->kabupaten->provinsi->id;
            array_push($districts, $district);
        }

        $users = array();

        for ($i=0; $i < 50; $i++) { 
            $data['name'] = $faker->name;
            $data['username'] = $faker->email;
            $data['email'] = $faker->email;
            $data['phone'] = $faker->e164PhoneNumber;
            $data['password'] = bcrypt('1234');
            $data['role'] = 'USER';
            $data['created_at'] = $faker->dateTime();
            $data['updated_at'] = $faker->dateTime();
            $data['updated'] = mt_rand(0,1);
            $data['photo'] = $photos[mt_rand(0,$countDocs)];
            $data['registerType'] = mt_rand(0,1);

            //set city and province
            $cit_index = mt_rand(0,(count($districts)-1));
            $data['provinceId'] = $districts[$cit_index]['province'];
            $data['cityId'] = $districts[$cit_index]['city'];
            $data['districtId'] = $districts[$cit_index]['district'];
            $data['subdistrict'] = $faker->sentence($nbWords = 2, $variableNbWords = true);
            $data['address'] = $faker->paragraph($nb = 5, $asText = true);
            $data['postcode'] = mt_rand(1000,9999);

            array_push($users, $data);
        }

        DB::table('users')->insert($users);


    }
}
