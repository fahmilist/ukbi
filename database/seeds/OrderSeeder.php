<?php

use Illuminate\Database\Seeder;

use App\Kecamatan;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $orders = array();

        $districts = array();
        foreach (Kecamatan::all() as $district) {
            $district['district'] = $district->id;
            $district['city'] = $district->kabupaten->id;
            $district['province'] = $district->kabupaten->provinsi->id;
            array_push($districts, $district);
        }

        for ($i=1; $i <= 213; $i++) { 
            $data['orderId'] = $i;
            //set city and province
            $cit_index = mt_rand(0,(count($districts)-1));
            $data['provinceId'] = $districts[$cit_index]['province'];
            $data['cityId'] = $districts[$cit_index]['city'];
            $data['districtId'] = $districts[$cit_index]['district'];
            $data['subdistrict'] = $faker->sentence($nbWords = 2, $variableNbWords = true);
            $data['address'] = $faker->paragraph($nb = 5, $asText = true);
            $data['postcode'] = mt_rand(1000,9999);

            array_push($orders, $data);
        }

        DB::table('orderaddress')->insert($orders);
    }
}
