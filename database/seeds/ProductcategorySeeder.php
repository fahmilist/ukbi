<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\SubCategory;

class ProductcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productCategory = array();
        $sub = array();
        foreach (SubCategory::all() as $subCategory) {
        	array_push($sub, $subCategory->id);
        }

        $countSub = count($sub)-1;

        foreach (Product::all() as $product) {
        	for ($i=0; $i < mt_rand(1,4); $i++) { 
        		$data['productId'] = $product->id;
        		$data['categoryId'] = $sub[mt_rand(0,$countSub)];
        		array_push($productCategory, $data);
        	}
        }

        DB::table('productcategory')->insert($productCategory);
    }
}
