<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = Storage::files('public/categories');
        $photos = array();
        foreach ($files as $file) {
            array_push($photos, str_replace('public/', '', $file));
        }

        $countDocs = count($photos)-1;

        $faker = Faker\Factory::create();

        $categories = array();

        for ($i=0; $i < 5; $i++) { 
            $data['name'] = $faker->word;
            $data['description'] = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $data['image'] = $photos[$i];

            array_push($categories, $data);
        }

        DB::table('categories')->insert($categories);
    }
}
