<?php

use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $subcategories = array();

        for ($i=0; $i < 30; $i++) { 
            $data['name'] = $faker->sentence($nbWords = 2, $variableNbWords = true);
            $data['categoryId'] = mt_rand(1,5);

            array_push($subcategories, $data);
        }

        DB::table('subcategories')->insert($subcategories);
    }
}
