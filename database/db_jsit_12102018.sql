-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10 Des 2018 pada 01.13
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jsit`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'categories/default.png',
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `image`, `deleted`) VALUES
(1, 'est', 'Eligendi volupt', 'categories/banner-01.jpg', 0),
(2, 'commodi', 'Impedit magni o', 'categories/banner-02.jpg', 0),
(3, 'aperiam', 'Veritatis repel', 'categories/banner-03.jpg', 0),
(4, 'qui', 'Quidem reiciend', 'categories/banner-01.jpg', 0),
(5, 'unde', 'Dolor nihil in ', 'categories/banner-02.jpg', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `provinceId` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cities`
--

INSERT INTO `cities` (`id`, `provinceId`, `name`) VALUES
(1, 21, 'Kabupaten Aceh Barat'),
(2, 21, 'Kabupaten Aceh Barat Daya'),
(3, 21, 'Kabupaten Aceh Besar'),
(4, 21, 'Kabupaten Aceh Jaya'),
(5, 21, 'Kabupaten Aceh Selatan'),
(6, 21, 'Kabupaten Aceh Singkil'),
(7, 21, 'Kabupaten Aceh Tamiang'),
(8, 21, 'Kabupaten Aceh Tengah'),
(9, 21, 'Kabupaten Aceh Tenggara'),
(10, 21, 'Kabupaten Aceh Timur'),
(11, 21, 'Kabupaten Aceh Utara'),
(12, 32, 'Kabupaten Agam'),
(13, 23, 'Kabupaten Alor'),
(14, 19, 'Kota Ambon'),
(15, 34, 'Kabupaten Asahan'),
(16, 24, 'Kabupaten Asmat'),
(17, 1, 'Kabupaten Badung'),
(18, 13, 'Kabupaten Balangan'),
(19, 15, 'Kota Balikpapan'),
(20, 21, 'Kota Banda Aceh'),
(21, 18, 'Kota Bandar Lampung'),
(22, 9, 'Kabupaten Bandung'),
(23, 9, 'Kota Bandung'),
(24, 9, 'Kabupaten Bandung Barat'),
(25, 29, 'Kabupaten Banggai'),
(26, 29, 'Kabupaten Banggai Kepulauan'),
(27, 2, 'Kabupaten Bangka'),
(28, 2, 'Kabupaten Bangka Barat'),
(29, 2, 'Kabupaten Bangka Selatan'),
(30, 2, 'Kabupaten Bangka Tengah'),
(31, 11, 'Kabupaten Bangkalan'),
(32, 1, 'Kabupaten Bangli'),
(33, 13, 'Kabupaten Banjar'),
(34, 9, 'Kota Banjar'),
(35, 13, 'Kota Banjarbaru'),
(36, 13, 'Kota Banjarmasin'),
(37, 10, 'Kabupaten Banjarnegara'),
(38, 28, 'Kabupaten Bantaeng'),
(39, 5, 'Kabupaten Bantul'),
(40, 33, 'Kabupaten Banyuasin'),
(41, 10, 'Kabupaten Banyumas'),
(42, 11, 'Kabupaten Banyuwangi'),
(43, 13, 'Kabupaten Barito Kuala'),
(44, 14, 'Kabupaten Barito Selatan'),
(45, 14, 'Kabupaten Barito Timur'),
(46, 14, 'Kabupaten Barito Utara'),
(47, 28, 'Kabupaten Barru'),
(48, 17, 'Kota Batam'),
(49, 10, 'Kabupaten Batang'),
(50, 8, 'Kabupaten Batang Hari'),
(51, 11, 'Kota Batu'),
(52, 34, 'Kabupaten Batu Bara'),
(53, 30, 'Kota Bau-Bau'),
(54, 9, 'Kabupaten Bekasi'),
(55, 9, 'Kota Bekasi'),
(56, 2, 'Kabupaten Belitung'),
(57, 2, 'Kabupaten Belitung Timur'),
(58, 23, 'Kabupaten Belu'),
(59, 21, 'Kabupaten Bener Meriah'),
(60, 26, 'Kabupaten Bengkalis'),
(61, 12, 'Kabupaten Bengkayang'),
(62, 4, 'Kota Bengkulu'),
(63, 4, 'Kabupaten Bengkulu Selatan'),
(64, 4, 'Kabupaten Bengkulu Tengah'),
(65, 4, 'Kabupaten Bengkulu Utara'),
(66, 15, 'Kabupaten Berau'),
(67, 24, 'Kabupaten Biak Numfor'),
(68, 22, 'Kabupaten Bima'),
(69, 22, 'Kota Bima'),
(70, 34, 'Kota Binjai'),
(71, 17, 'Kabupaten Bintan'),
(72, 21, 'Kabupaten Bireuen'),
(73, 31, 'Kota Bitung'),
(74, 11, 'Kabupaten Blitar'),
(75, 11, 'Kota Blitar'),
(76, 10, 'Kabupaten Blora'),
(77, 7, 'Kabupaten Boalemo'),
(78, 9, 'Kabupaten Bogor'),
(79, 9, 'Kota Bogor'),
(80, 11, 'Kabupaten Bojonegoro'),
(81, 31, 'Kabupaten Bolaang Mongondow (Bolmong)'),
(82, 31, 'Kabupaten Bolaang Mongondow Selatan'),
(83, 31, 'Kabupaten Bolaang Mongondow Timur'),
(84, 31, 'Kabupaten Bolaang Mongondow Utara'),
(85, 30, 'Kabupaten Bombana'),
(86, 11, 'Kabupaten Bondowoso'),
(87, 28, 'Kabupaten Bone'),
(88, 7, 'Kabupaten Bone Bolango'),
(89, 15, 'Kota Bontang'),
(90, 24, 'Kabupaten Boven Digoel'),
(91, 10, 'Kabupaten Boyolali'),
(92, 10, 'Kabupaten Brebes'),
(93, 32, 'Kota Bukittinggi'),
(94, 1, 'Kabupaten Buleleng'),
(95, 28, 'Kabupaten Bulukumba'),
(96, 16, 'Kabupaten Bulungan (Bulongan)'),
(97, 8, 'Kabupaten Bungo'),
(98, 29, 'Kabupaten Buol'),
(99, 19, 'Kabupaten Buru'),
(100, 19, 'Kabupaten Buru Selatan'),
(101, 30, 'Kabupaten Buton'),
(102, 30, 'Kabupaten Buton Utara'),
(103, 9, 'Kabupaten Ciamis'),
(104, 9, 'Kabupaten Cianjur'),
(105, 10, 'Kabupaten Cilacap'),
(106, 3, 'Kota Cilegon'),
(107, 9, 'Kota Cimahi'),
(108, 9, 'Kabupaten Cirebon'),
(109, 9, 'Kota Cirebon'),
(110, 34, 'Kabupaten Dairi'),
(111, 24, 'Kabupaten Deiyai (Deliyai)'),
(112, 34, 'Kabupaten Deli Serdang'),
(113, 10, 'Kabupaten Demak'),
(114, 1, 'Kota Denpasar'),
(115, 9, 'Kota Depok'),
(116, 32, 'Kabupaten Dharmasraya'),
(117, 24, 'Kabupaten Dogiyai'),
(118, 22, 'Kabupaten Dompu'),
(119, 29, 'Kabupaten Donggala'),
(120, 26, 'Kota Dumai'),
(121, 33, 'Kabupaten Empat Lawang'),
(122, 23, 'Kabupaten Ende'),
(123, 28, 'Kabupaten Enrekang'),
(124, 25, 'Kabupaten Fakfak'),
(125, 23, 'Kabupaten Flores Timur'),
(126, 9, 'Kabupaten Garut'),
(127, 21, 'Kabupaten Gayo Lues'),
(128, 1, 'Kabupaten Gianyar'),
(129, 7, 'Kabupaten Gorontalo'),
(130, 7, 'Kota Gorontalo'),
(131, 7, 'Kabupaten Gorontalo Utara'),
(132, 28, 'Kabupaten Gowa'),
(133, 11, 'Kabupaten Gresik'),
(134, 10, 'Kabupaten Grobogan'),
(135, 5, 'Kabupaten Gunung Kidul'),
(136, 14, 'Kabupaten Gunung Mas'),
(137, 34, 'Kota Gunungsitoli'),
(138, 20, 'Kabupaten Halmahera Barat'),
(139, 20, 'Kabupaten Halmahera Selatan'),
(140, 20, 'Kabupaten Halmahera Tengah'),
(141, 20, 'Kabupaten Halmahera Timur'),
(142, 20, 'Kabupaten Halmahera Utara'),
(143, 13, 'Kabupaten Hulu Sungai Selatan'),
(144, 13, 'Kabupaten Hulu Sungai Tengah'),
(145, 13, 'Kabupaten Hulu Sungai Utara'),
(146, 34, 'Kabupaten Humbang Hasundutan'),
(147, 26, 'Kabupaten Indragiri Hilir'),
(148, 26, 'Kabupaten Indragiri Hulu'),
(149, 9, 'Kabupaten Indramayu'),
(150, 24, 'Kabupaten Intan Jaya'),
(151, 6, 'Kota Jakarta Barat'),
(152, 6, 'Kota Jakarta Pusat'),
(153, 6, 'Kota Jakarta Selatan'),
(154, 6, 'Kota Jakarta Timur'),
(155, 6, 'Kota Jakarta Utara'),
(156, 8, 'Kota Jambi'),
(157, 24, 'Kabupaten Jayapura'),
(158, 24, 'Kota Jayapura'),
(159, 24, 'Kabupaten Jayawijaya'),
(160, 11, 'Kabupaten Jember'),
(161, 1, 'Kabupaten Jembrana'),
(162, 28, 'Kabupaten Jeneponto'),
(163, 10, 'Kabupaten Jepara'),
(164, 11, 'Kabupaten Jombang'),
(165, 25, 'Kabupaten Kaimana'),
(166, 26, 'Kabupaten Kampar'),
(167, 14, 'Kabupaten Kapuas'),
(168, 12, 'Kabupaten Kapuas Hulu'),
(169, 10, 'Kabupaten Karanganyar'),
(170, 1, 'Kabupaten Karangasem'),
(171, 9, 'Kabupaten Karawang'),
(172, 17, 'Kabupaten Karimun'),
(173, 34, 'Kabupaten Karo'),
(174, 14, 'Kabupaten Katingan'),
(175, 4, 'Kabupaten Kaur'),
(176, 12, 'Kabupaten Kayong Utara'),
(177, 10, 'Kabupaten Kebumen'),
(178, 11, 'Kabupaten Kediri'),
(179, 11, 'Kota Kediri'),
(180, 24, 'Kabupaten Keerom'),
(181, 10, 'Kabupaten Kendal'),
(182, 30, 'Kota Kendari'),
(183, 4, 'Kabupaten Kepahiang'),
(184, 17, 'Kabupaten Kepulauan Anambas'),
(185, 19, 'Kabupaten Kepulauan Aru'),
(186, 32, 'Kabupaten Kepulauan Mentawai'),
(187, 26, 'Kabupaten Kepulauan Meranti'),
(188, 31, 'Kabupaten Kepulauan Sangihe'),
(189, 6, 'Kabupaten Kepulauan Seribu'),
(190, 31, 'Kabupaten Kepulauan Siau Tagulandang Biaro (Sitaro)'),
(191, 20, 'Kabupaten Kepulauan Sula'),
(192, 31, 'Kabupaten Kepulauan Talaud'),
(193, 24, 'Kabupaten Kepulauan Yapen (Yapen Waropen)'),
(194, 8, 'Kabupaten Kerinci'),
(195, 12, 'Kabupaten Ketapang'),
(196, 10, 'Kabupaten Klaten'),
(197, 1, 'Kabupaten Klungkung'),
(198, 30, 'Kabupaten Kolaka'),
(199, 30, 'Kabupaten Kolaka Utara'),
(200, 30, 'Kabupaten Konawe'),
(201, 30, 'Kabupaten Konawe Selatan'),
(202, 30, 'Kabupaten Konawe Utara'),
(203, 13, 'Kabupaten Kotabaru'),
(204, 31, 'Kota Kotamobagu'),
(205, 14, 'Kabupaten Kotawaringin Barat'),
(206, 14, 'Kabupaten Kotawaringin Timur'),
(207, 26, 'Kabupaten Kuantan Singingi'),
(208, 12, 'Kabupaten Kubu Raya'),
(209, 10, 'Kabupaten Kudus'),
(210, 5, 'Kabupaten Kulon Progo'),
(211, 9, 'Kabupaten Kuningan'),
(212, 23, 'Kabupaten Kupang'),
(213, 23, 'Kota Kupang'),
(214, 15, 'Kabupaten Kutai Barat'),
(215, 15, 'Kabupaten Kutai Kartanegara'),
(216, 15, 'Kabupaten Kutai Timur'),
(217, 34, 'Kabupaten Labuhan Batu'),
(218, 34, 'Kabupaten Labuhan Batu Selatan'),
(219, 34, 'Kabupaten Labuhan Batu Utara'),
(220, 33, 'Kabupaten Lahat'),
(221, 14, 'Kabupaten Lamandau'),
(222, 11, 'Kabupaten Lamongan'),
(223, 18, 'Kabupaten Lampung Barat'),
(224, 18, 'Kabupaten Lampung Selatan'),
(225, 18, 'Kabupaten Lampung Tengah'),
(226, 18, 'Kabupaten Lampung Timur'),
(227, 18, 'Kabupaten Lampung Utara'),
(228, 12, 'Kabupaten Landak'),
(229, 34, 'Kabupaten Langkat'),
(230, 21, 'Kota Langsa'),
(231, 24, 'Kabupaten Lanny Jaya'),
(232, 3, 'Kabupaten Lebak'),
(233, 4, 'Kabupaten Lebong'),
(234, 23, 'Kabupaten Lembata'),
(235, 21, 'Kota Lhokseumawe'),
(236, 32, 'Kabupaten Lima Puluh Koto/Kota'),
(237, 17, 'Kabupaten Lingga'),
(238, 22, 'Kabupaten Lombok Barat'),
(239, 22, 'Kabupaten Lombok Tengah'),
(240, 22, 'Kabupaten Lombok Timur'),
(241, 22, 'Kabupaten Lombok Utara'),
(242, 33, 'Kota Lubuk Linggau'),
(243, 11, 'Kabupaten Lumajang'),
(244, 28, 'Kabupaten Luwu'),
(245, 28, 'Kabupaten Luwu Timur'),
(246, 28, 'Kabupaten Luwu Utara'),
(247, 11, 'Kabupaten Madiun'),
(248, 11, 'Kota Madiun'),
(249, 10, 'Kabupaten Magelang'),
(250, 10, 'Kota Magelang'),
(251, 11, 'Kabupaten Magetan'),
(252, 9, 'Kabupaten Majalengka'),
(253, 27, 'Kabupaten Majene'),
(254, 28, 'Kota Makassar'),
(255, 11, 'Kabupaten Malang'),
(256, 11, 'Kota Malang'),
(257, 16, 'Kabupaten Malinau'),
(258, 19, 'Kabupaten Maluku Barat Daya'),
(259, 19, 'Kabupaten Maluku Tengah'),
(260, 19, 'Kabupaten Maluku Tenggara'),
(261, 19, 'Kabupaten Maluku Tenggara Barat'),
(262, 27, 'Kabupaten Mamasa'),
(263, 24, 'Kabupaten Mamberamo Raya'),
(264, 24, 'Kabupaten Mamberamo Tengah'),
(265, 27, 'Kabupaten Mamuju'),
(266, 27, 'Kabupaten Mamuju Utara'),
(267, 31, 'Kota Manado'),
(268, 34, 'Kabupaten Mandailing Natal'),
(269, 23, 'Kabupaten Manggarai'),
(270, 23, 'Kabupaten Manggarai Barat'),
(271, 23, 'Kabupaten Manggarai Timur'),
(272, 25, 'Kabupaten Manokwari'),
(273, 25, 'Kabupaten Manokwari Selatan'),
(274, 24, 'Kabupaten Mappi'),
(275, 28, 'Kabupaten Maros'),
(276, 22, 'Kota Mataram'),
(277, 25, 'Kabupaten Maybrat'),
(278, 34, 'Kota Medan'),
(279, 12, 'Kabupaten Melawi'),
(280, 8, 'Kabupaten Merangin'),
(281, 24, 'Kabupaten Merauke'),
(282, 18, 'Kabupaten Mesuji'),
(283, 18, 'Kota Metro'),
(284, 24, 'Kabupaten Mimika'),
(285, 31, 'Kabupaten Minahasa'),
(286, 31, 'Kabupaten Minahasa Selatan'),
(287, 31, 'Kabupaten Minahasa Tenggara'),
(288, 31, 'Kabupaten Minahasa Utara'),
(289, 11, 'Kabupaten Mojokerto'),
(290, 11, 'Kota Mojokerto'),
(291, 29, 'Kabupaten Morowali'),
(292, 33, 'Kabupaten Muara Enim'),
(293, 8, 'Kabupaten Muaro Jambi'),
(294, 4, 'Kabupaten Muko Muko'),
(295, 30, 'Kabupaten Muna'),
(296, 14, 'Kabupaten Murung Raya'),
(297, 33, 'Kabupaten Musi Banyuasin'),
(298, 33, 'Kabupaten Musi Rawas'),
(299, 24, 'Kabupaten Nabire'),
(300, 21, 'Kabupaten Nagan Raya'),
(301, 23, 'Kabupaten Nagekeo'),
(302, 17, 'Kabupaten Natuna'),
(303, 24, 'Kabupaten Nduga'),
(304, 23, 'Kabupaten Ngada'),
(305, 11, 'Kabupaten Nganjuk'),
(306, 11, 'Kabupaten Ngawi'),
(307, 34, 'Kabupaten Nias'),
(308, 34, 'Kabupaten Nias Barat'),
(309, 34, 'Kabupaten Nias Selatan'),
(310, 34, 'Kabupaten Nias Utara'),
(311, 16, 'Kabupaten Nunukan'),
(312, 33, 'Kabupaten Ogan Ilir'),
(313, 33, 'Kabupaten Ogan Komering Ilir'),
(314, 33, 'Kabupaten Ogan Komering Ulu'),
(315, 33, 'Kabupaten Ogan Komering Ulu Selatan'),
(316, 33, 'Kabupaten Ogan Komering Ulu Timur'),
(317, 11, 'Kabupaten Pacitan'),
(318, 32, 'Kota Padang'),
(319, 34, 'Kabupaten Padang Lawas'),
(320, 34, 'Kabupaten Padang Lawas Utara'),
(321, 32, 'Kota Padang Panjang'),
(322, 32, 'Kabupaten Padang Pariaman'),
(323, 34, 'Kota Padang Sidempuan'),
(324, 33, 'Kota Pagar Alam'),
(325, 34, 'Kabupaten Pakpak Bharat'),
(326, 14, 'Kota Palangka Raya'),
(327, 33, 'Kota Palembang'),
(328, 28, 'Kota Palopo'),
(329, 29, 'Kota Palu'),
(330, 11, 'Kabupaten Pamekasan'),
(331, 3, 'Kabupaten Pandeglang'),
(332, 9, 'Kabupaten Pangandaran'),
(333, 28, 'Kabupaten Pangkajene Kepulauan'),
(334, 2, 'Kota Pangkal Pinang'),
(335, 24, 'Kabupaten Paniai'),
(336, 28, 'Kota Parepare'),
(337, 32, 'Kota Pariaman'),
(338, 29, 'Kabupaten Parigi Moutong'),
(339, 32, 'Kabupaten Pasaman'),
(340, 32, 'Kabupaten Pasaman Barat'),
(341, 15, 'Kabupaten Paser'),
(342, 11, 'Kabupaten Pasuruan'),
(343, 11, 'Kota Pasuruan'),
(344, 10, 'Kabupaten Pati'),
(345, 32, 'Kota Payakumbuh'),
(346, 25, 'Kabupaten Pegunungan Arfak'),
(347, 24, 'Kabupaten Pegunungan Bintang'),
(348, 10, 'Kabupaten Pekalongan'),
(349, 10, 'Kota Pekalongan'),
(350, 26, 'Kota Pekanbaru'),
(351, 26, 'Kabupaten Pelalawan'),
(352, 10, 'Kabupaten Pemalang'),
(353, 34, 'Kota Pematang Siantar'),
(354, 15, 'Kabupaten Penajam Paser Utara'),
(355, 18, 'Kabupaten Pesawaran'),
(356, 18, 'Kabupaten Pesisir Barat'),
(357, 32, 'Kabupaten Pesisir Selatan'),
(358, 21, 'Kabupaten Pidie'),
(359, 21, 'Kabupaten Pidie Jaya'),
(360, 28, 'Kabupaten Pinrang'),
(361, 7, 'Kabupaten Pohuwato'),
(362, 27, 'Kabupaten Polewali Mandar'),
(363, 11, 'Kabupaten Ponorogo'),
(364, 12, 'Kabupaten Pontianak'),
(365, 12, 'Kota Pontianak'),
(366, 29, 'Kabupaten Poso'),
(367, 33, 'Kota Prabumulih'),
(368, 18, 'Kabupaten Pringsewu'),
(369, 11, 'Kabupaten Probolinggo'),
(370, 11, 'Kota Probolinggo'),
(371, 14, 'Kabupaten Pulang Pisau'),
(372, 20, 'Kabupaten Pulau Morotai'),
(373, 24, 'Kabupaten Puncak'),
(374, 24, 'Kabupaten Puncak Jaya'),
(375, 10, 'Kabupaten Purbalingga'),
(376, 9, 'Kabupaten Purwakarta'),
(377, 10, 'Kabupaten Purworejo'),
(378, 25, 'Kabupaten Raja Ampat'),
(379, 4, 'Kabupaten Rejang Lebong'),
(380, 10, 'Kabupaten Rembang'),
(381, 26, 'Kabupaten Rokan Hilir'),
(382, 26, 'Kabupaten Rokan Hulu'),
(383, 23, 'Kabupaten Rote Ndao'),
(384, 21, 'Kota Sabang'),
(385, 23, 'Kabupaten Sabu Raijua'),
(386, 10, 'Kota Salatiga'),
(387, 15, 'Kota Samarinda'),
(388, 12, 'Kabupaten Sambas'),
(389, 34, 'Kabupaten Samosir'),
(390, 11, 'Kabupaten Sampang'),
(391, 12, 'Kabupaten Sanggau'),
(392, 24, 'Kabupaten Sarmi'),
(393, 8, 'Kabupaten Sarolangun'),
(394, 32, 'Kota Sawah Lunto'),
(395, 12, 'Kabupaten Sekadau'),
(396, 28, 'Kabupaten Selayar (Kepulauan Selayar)'),
(397, 4, 'Kabupaten Seluma'),
(398, 10, 'Kabupaten Semarang'),
(399, 10, 'Kota Semarang'),
(400, 19, 'Kabupaten Seram Bagian Barat'),
(401, 19, 'Kabupaten Seram Bagian Timur'),
(402, 3, 'Kabupaten Serang'),
(403, 3, 'Kota Serang'),
(404, 34, 'Kabupaten Serdang Bedagai'),
(405, 14, 'Kabupaten Seruyan'),
(406, 26, 'Kabupaten Siak'),
(407, 34, 'Kota Sibolga'),
(408, 28, 'Kabupaten Sidenreng Rappang/Rapang'),
(409, 11, 'Kabupaten Sidoarjo'),
(410, 29, 'Kabupaten Sigi'),
(411, 32, 'Kabupaten Sijunjung (Sawah Lunto Sijunjung)'),
(412, 23, 'Kabupaten Sikka'),
(413, 34, 'Kabupaten Simalungun'),
(414, 21, 'Kabupaten Simeulue'),
(415, 12, 'Kota Singkawang'),
(416, 28, 'Kabupaten Sinjai'),
(417, 12, 'Kabupaten Sintang'),
(418, 11, 'Kabupaten Situbondo'),
(419, 5, 'Kabupaten Sleman'),
(420, 32, 'Kabupaten Solok'),
(421, 32, 'Kota Solok'),
(422, 32, 'Kabupaten Solok Selatan'),
(423, 28, 'Kabupaten Soppeng'),
(424, 25, 'Kabupaten Sorong'),
(425, 25, 'Kota Sorong'),
(426, 25, 'Kabupaten Sorong Selatan'),
(427, 10, 'Kabupaten Sragen'),
(428, 9, 'Kabupaten Subang'),
(429, 21, 'Kota Subulussalam'),
(430, 9, 'Kabupaten Sukabumi'),
(431, 9, 'Kota Sukabumi'),
(432, 14, 'Kabupaten Sukamara'),
(433, 10, 'Kabupaten Sukoharjo'),
(434, 23, 'Kabupaten Sumba Barat'),
(435, 23, 'Kabupaten Sumba Barat Daya'),
(436, 23, 'Kabupaten Sumba Tengah'),
(437, 23, 'Kabupaten Sumba Timur'),
(438, 22, 'Kabupaten Sumbawa'),
(439, 22, 'Kabupaten Sumbawa Barat'),
(440, 9, 'Kabupaten Sumedang'),
(441, 11, 'Kabupaten Sumenep'),
(442, 8, 'Kota Sungaipenuh'),
(443, 24, 'Kabupaten Supiori'),
(444, 11, 'Kota Surabaya'),
(445, 10, 'Kota Surakarta (Solo)'),
(446, 13, 'Kabupaten Tabalong'),
(447, 1, 'Kabupaten Tabanan'),
(448, 28, 'Kabupaten Takalar'),
(449, 25, 'Kabupaten Tambrauw'),
(450, 16, 'Kabupaten Tana Tidung'),
(451, 28, 'Kabupaten Tana Toraja'),
(452, 13, 'Kabupaten Tanah Bumbu'),
(453, 32, 'Kabupaten Tanah Datar'),
(454, 13, 'Kabupaten Tanah Laut'),
(455, 3, 'Kabupaten Tangerang'),
(456, 3, 'Kota Tangerang'),
(457, 3, 'Kota Tangerang Selatan'),
(458, 18, 'Kabupaten Tanggamus'),
(459, 34, 'Kota Tanjung Balai'),
(460, 8, 'Kabupaten Tanjung Jabung Barat'),
(461, 8, 'Kabupaten Tanjung Jabung Timur'),
(462, 17, 'Kota Tanjung Pinang'),
(463, 34, 'Kabupaten Tapanuli Selatan'),
(464, 34, 'Kabupaten Tapanuli Tengah'),
(465, 34, 'Kabupaten Tapanuli Utara'),
(466, 13, 'Kabupaten Tapin'),
(467, 16, 'Kota Tarakan'),
(468, 9, 'Kabupaten Tasikmalaya'),
(469, 9, 'Kota Tasikmalaya'),
(470, 34, 'Kota Tebing Tinggi'),
(471, 8, 'Kabupaten Tebo'),
(472, 10, 'Kabupaten Tegal'),
(473, 10, 'Kota Tegal'),
(474, 25, 'Kabupaten Teluk Bintuni'),
(475, 25, 'Kabupaten Teluk Wondama'),
(476, 10, 'Kabupaten Temanggung'),
(477, 20, 'Kota Ternate'),
(478, 20, 'Kota Tidore Kepulauan'),
(479, 23, 'Kabupaten Timor Tengah Selatan'),
(480, 23, 'Kabupaten Timor Tengah Utara'),
(481, 34, 'Kabupaten Toba Samosir'),
(482, 29, 'Kabupaten Tojo Una-Una'),
(483, 29, 'Kabupaten Toli-Toli'),
(484, 24, 'Kabupaten Tolikara'),
(485, 31, 'Kota Tomohon'),
(486, 28, 'Kabupaten Toraja Utara'),
(487, 11, 'Kabupaten Trenggalek'),
(488, 19, 'Kota Tual'),
(489, 11, 'Kabupaten Tuban'),
(490, 18, 'Kabupaten Tulang Bawang'),
(491, 18, 'Kabupaten Tulang Bawang Barat'),
(492, 11, 'Kabupaten Tulungagung'),
(493, 28, 'Kabupaten Wajo'),
(494, 30, 'Kabupaten Wakatobi'),
(495, 24, 'Kabupaten Waropen'),
(496, 18, 'Kabupaten Way Kanan'),
(497, 10, 'Kabupaten Wonogiri'),
(498, 10, 'Kabupaten Wonosobo'),
(499, 24, 'Kabupaten Yahukimo'),
(500, 24, 'Kabupaten Yalimo'),
(501, 5, 'Kota Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE `config` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public/logo.png',
  `logoSecond` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public/icon.png',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `aboutDetail` text COLLATE utf8mb4_unicode_ci,
  `faq` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`id`, `title`, `tagline`, `logo`, `logoSecond`, `icon`, `email`, `phone`, `address`, `lat`, `lng`, `about`, `aboutDetail`, `faq`, `slider`, `facebook`, `instagram`, `whatsapp`) VALUES
(1, 'JSIT Commerce', 'Free shipping for standard order over Rp 1.000.000', 'config/logo-03.png', 'config/logo-02.png', 'config/icon.png', 'info@jsit.com', '+1234567', 'Jl.Depok Raya No.35 Kota Depok Jawa Barat', '-6.385589', '106.830711', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur maximus vulputate hendrerit. Praesent faucibus erat vitae rutrum gravida. Vestibulum tempus mi enim, in molestie sem fermentum quis.', '', 'x', '[{"title":"Women Collection 2018","description":"NEW SEASON","image":"config\\/slider\\/slide-01.jpg","link":"http:\\/\\/localhost\\/jsit_shop\\/public\\/product","linkText":"Shop Now"},{"title":"Men New-Season","description":"Jackets & Coats","image":"config\\/slider\\/slide-02.jpg","link":"http:\\/\\/localhost\\/jsit_shop\\/public\\/blog","linkText":"Purchase Now"},{"title":"Men Collection 2018","description":"New arrivals","image":"config\\/slider\\/slide-03.jpg","link":"http:\\/\\/localhost\\/jsit_shop\\/public\\/contact","linkText":"Read Now"}]', 'http://www.facebook.com', 'http://www.instagram.com', '6289680667362');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(252, '2014_10_12_000000_create_users_table', 1),
(253, '2014_10_12_100000_create_password_resets_table', 1),
(254, '2018_11_29_075219_create_categories', 1),
(255, '2018_11_29_075428_create_subCategories', 1),
(256, '2018_11_29_075543_create_provinces', 1),
(257, '2018_11_29_075627_create_cities', 1),
(258, '2018_11_29_075740_create_products', 1),
(259, '2018_11_29_080354_create_orders', 1),
(260, '2018_11_29_081022_create_order_product', 1),
(261, '2018_11_29_081158_create_order_address', 1),
(262, '2018_11_29_081405_create_order_confirmation', 1),
(263, '2018_11_29_081754_create_product_category', 1),
(264, '2018_11_29_081902_create_config', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orderaddress`
--

CREATE TABLE `orderaddress` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderId` int(11) NOT NULL,
  `provinceId` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `orderconfirmation`
--

CREATE TABLE `orderconfirmation` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderId` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bankName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmationDate` datetime NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `orderproduct`
--

CREATE TABLE `orderproduct` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `orderType` int(11) NOT NULL,
  `amount` double NOT NULL,
  `shippingType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shippingFee` double NOT NULL,
  `shippingAddress` int(11) DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `productcategory`
--

CREATE TABLE `productcategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `productId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `productcategory`
--

INSERT INTO `productcategory` (`id`, `productId`, `categoryId`) VALUES
(1, 1, 9),
(2, 2, 14),
(3, 2, 5),
(4, 3, 18),
(5, 3, 15),
(6, 4, 12),
(7, 4, 13),
(8, 5, 2),
(9, 5, 6),
(10, 5, 10),
(11, 6, 8),
(12, 6, 10),
(13, 7, 1),
(14, 8, 27),
(15, 9, 4),
(16, 9, 30),
(17, 10, 22),
(18, 10, 7),
(19, 10, 1),
(20, 11, 21),
(21, 12, 8),
(22, 12, 10),
(23, 13, 21),
(24, 14, 18),
(25, 14, 10),
(26, 15, 27),
(27, 15, 11),
(28, 15, 6),
(29, 16, 2),
(30, 16, 6),
(31, 17, 6),
(32, 18, 29),
(33, 18, 12),
(34, 19, 28),
(35, 19, 29),
(36, 20, 15),
(37, 20, 14),
(38, 20, 17),
(39, 21, 30),
(40, 21, 21),
(41, 21, 21),
(42, 21, 9),
(43, 22, 25),
(44, 22, 13),
(45, 22, 14),
(46, 22, 15),
(47, 23, 26),
(48, 23, 27),
(49, 23, 1),
(50, 23, 15),
(51, 24, 13),
(52, 25, 3),
(53, 25, 28),
(54, 25, 27),
(55, 26, 29),
(56, 26, 18),
(57, 26, 1),
(58, 27, 25),
(59, 28, 13),
(60, 28, 23),
(61, 29, 19),
(62, 30, 19),
(63, 30, 4),
(64, 31, 3),
(65, 31, 10),
(66, 32, 17),
(67, 32, 29),
(68, 32, 24),
(69, 32, 28),
(70, 33, 26),
(71, 33, 8),
(72, 34, 24),
(73, 34, 17),
(74, 34, 30),
(75, 35, 27),
(76, 35, 4),
(77, 36, 10),
(78, 36, 9),
(79, 36, 16),
(80, 37, 21),
(81, 37, 23),
(82, 38, 19),
(83, 38, 4),
(84, 39, 23),
(85, 39, 6),
(86, 40, 26),
(87, 40, 13),
(88, 41, 16),
(89, 41, 7),
(90, 42, 19),
(91, 43, 2),
(92, 43, 7),
(93, 43, 22),
(94, 43, 10),
(95, 44, 22),
(96, 44, 7),
(97, 44, 23),
(98, 45, 25),
(99, 46, 4),
(100, 46, 2),
(101, 47, 28),
(102, 47, 13),
(103, 48, 17),
(104, 49, 18),
(105, 49, 6),
(106, 50, 6),
(107, 50, 28),
(108, 51, 30),
(109, 51, 8),
(110, 52, 24),
(111, 53, 12),
(112, 54, 5),
(113, 54, 7),
(114, 55, 1),
(115, 55, 25),
(116, 56, 3),
(117, 56, 24),
(118, 57, 26),
(119, 58, 19),
(120, 59, 3),
(121, 59, 5),
(122, 59, 27),
(123, 60, 5),
(124, 60, 27),
(125, 61, 10),
(126, 61, 10),
(127, 62, 28),
(128, 62, 2),
(129, 62, 21),
(130, 63, 15),
(131, 64, 19),
(132, 64, 27),
(133, 64, 1),
(134, 64, 23),
(135, 65, 15),
(136, 65, 28),
(137, 65, 27),
(138, 66, 11),
(139, 66, 26),
(140, 66, 6),
(141, 67, 21),
(142, 67, 17),
(143, 68, 11),
(144, 68, 14),
(145, 69, 26),
(146, 69, 4),
(147, 69, 27),
(148, 69, 20),
(149, 70, 6),
(150, 70, 8),
(151, 71, 23),
(152, 71, 27),
(153, 71, 22),
(154, 72, 6),
(155, 73, 5),
(156, 74, 7),
(157, 75, 14),
(158, 75, 5),
(159, 75, 5),
(160, 76, 10),
(161, 76, 4),
(162, 76, 29),
(163, 77, 11),
(164, 78, 26),
(165, 79, 19),
(166, 79, 30),
(167, 80, 23),
(168, 80, 2),
(169, 81, 16),
(170, 81, 18),
(171, 81, 26),
(172, 82, 16),
(173, 83, 6),
(174, 83, 21),
(175, 84, 28),
(176, 84, 17),
(177, 84, 23),
(178, 85, 27),
(179, 85, 17),
(180, 85, 15),
(181, 86, 15),
(182, 86, 12),
(183, 87, 30),
(184, 87, 20),
(185, 88, 20),
(186, 89, 25),
(187, 90, 18),
(188, 91, 1),
(189, 91, 24),
(190, 91, 15),
(191, 92, 26),
(192, 92, 21),
(193, 93, 4),
(194, 93, 17),
(195, 93, 8),
(196, 94, 28),
(197, 94, 4),
(198, 94, 13),
(199, 94, 6),
(200, 95, 14),
(201, 96, 14),
(202, 96, 13),
(203, 96, 3),
(204, 97, 28),
(205, 97, 18),
(206, 98, 10),
(207, 98, 7),
(208, 99, 6),
(209, 99, 26),
(210, 100, 11),
(211, 100, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinceId` int(11) NOT NULL DEFAULT '0',
  `cityId` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL,
  `price` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `material` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` int(11) NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `provinceId`, `cityId`, `stock`, `price`, `description`, `weight`, `material`, `color`, `size`, `note`, `label`, `images`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'PD-0', 'Aut est molestiae.', 6, 152, 85, 62705, 'Corporis qui voluptatem quasi rerum provident. Cum omnis officiis delectus totam excepturi laboriosam. Quibusdam et alias voluptas recusandae vero. Enim qui repellendus et ipsa veritatis.<br><br>Adipisci sint ut voluptate quia sunt impedit. Id veritatis laudantium non saepe ut voluptas laborum. Nulla eum sit autem qui.<br><br>Distinctio qui et ut debitis corrupti sit est. In laboriosam consequatur nemo eos. Voluptas omnis quibusdam unde deserunt saepe possimus et magnam.<br><br>Et ipsum porro est. Repellat error architecto tempora ab reprehenderit iusto odio. Beatae et cum quia nisi.', '0.1', 'cotton', 'red,red,white,white', 'L,M', 'Expedita tempore velit omnis quibusdam. Eaque laboriosam architecto error debitis ad. Maiores ducimus sed doloremque delectus. Quod sed saepe maiores perferendis ad molestias. Enim voluptatibus amet velit ipsam. Voluptatem molestiae dolorem nihil facilis blanditiis. Dolor vel qui et nam voluptatem. Sunt qui excepturi quod repudiandae rerum natus et.', 3, '["products\\/bulldozer.png","products\\/cliff.png","products\\/dump-truck.png","products\\/autumn.png","products\\/river.png","products\\/haunted-house.png","products\\/park-1.png","products\\/hunting.png","products\\/route.png","products\\/farm-1.png","products\\/haunted-house.png","products\\/digger.png","products\\/walkie-talkie.png","products\\/car.png","products\\/road.png","products\\/boat-1.png","products\\/cycling.png","products\\/boat.png"]', 0, '1974-09-06 11:36:59', '1990-01-12 23:39:09'),
(2, 'PD-1', 'Nostrum quo voluptas omnis.', 14, 205, 16, 51551, 'Esse accusamus aut soluta rerum. Voluptatem ut et voluptatem beatae aut dignissimos. Quia eveniet autem a aut perspiciatis sed.<br><br>Aut cum veniam ipsa eius voluptates iusto. Sit inventore quis dolorem porro.<br><br>Quo neque nostrum cupiditate tenetur ut. Est est in voluptatum aliquid deleniti. Nostrum saepe veniam nulla voluptas corporis. Quod ut eveniet qui aliquid ut. Voluptates id eum et vel itaque dolores neque dignissimos.<br><br>Consequuntur id non sed ut consequatur autem. Perspiciatis natus enim eum quidem non consequatur inventore. Quo quibusdam est non aliquid. Itaque beatae necessitatibus a dignissimos.', '1.02', 'nilon', 'black,black,white', 'L,L,M', 'Quae qui quasi repellendus quos iste vel id qui. Officia tempore repudiandae aut veritatis sit quas nam. Ut quae ab quis dicta. Non vel consequuntur tempora labore cupiditate. Enim eligendi dicta ut nulla esse. Vero in unde quae nesciunt laudantium dolorem. Optio omnis et saepe nisi atque et delectus culpa. Enim provident vel et. Esse provident at repudiandae est beatae labore. Et qui adipisci sed ullam accusamus nobis dolor. Sequi et quisquam libero occaecati ut. Ullam distinctio quis inventore fugiat est ullam blanditiis. Libero laborum qui esse delectus optio.', 0, '["products\\/farm-2.png","products\\/river-1.png","products\\/camping-5.png","products\\/lagoon.png","products\\/cycling.png","products\\/beach-3.png"]', 0, '1989-06-04 21:34:22', '1987-11-05 17:09:44'),
(3, 'PD-2', 'Nesciunt dolor dignissimos.', 21, 20, 27, 53592, 'Dolorum et impedit voluptatum tempore id. Tenetur et molestiae deserunt. Rerum assumenda in deserunt dolor in. Neque officia dolorem id.<br><br>Quia consectetur et atque dignissimos eos. Nisi pariatur id vel sapiente at quisquam. Quisquam voluptatem expedita facilis et autem voluptatum. Quos quasi alias dolorem eos.<br><br>Accusamus corporis dicta cumque et. Molestiae explicabo dolorem repellat quia. Aut quaerat molestias ipsam reprehenderit quos.<br><br>Minima autem voluptatem eos eum alias qui suscipit alias. Aliquam odio laborum officiis maxime inventore. Veritatis omnis officiis est aut aut occaecati.', '0.62', 'nilon', 'blue', 'XXL', 'Aut et labore perferendis at nobis repellendus totam. Consequatur ipsa minima possimus commodi aliquam necessitatibus neque. Magni ut dolores velit. Alias itaque et fugit dolore et totam saepe. Quasi voluptatem et a laborum. In fugit libero adipisci quos impedit qui. Voluptatem officiis id quidem autem. Iste porro vel natus in quos aut. Amet ducimus nulla deserunt explicabo dolore nemo.', 3, '["products\\/taj-mahal.png","products\\/desert-2.png","products\\/shower.png","products\\/beach-1.png","products\\/camping-3.png","products\\/beach-3.png","products\\/sydney-opera-house.png","products\\/park-1.png","products\\/car.png","products\\/dump-truck.png","products\\/beach-2.png","products\\/forest.png","products\\/haunted-house.png"]', 0, '2004-04-17 04:17:47', '2004-12-20 05:56:28'),
(4, 'PD-3', 'Aut non iure dolores.', 33, 121, 67, 86731, 'Inventore numquam neque recusandae aliquid expedita qui iste cum. Minus ut iure velit optio nam. Quia laudantium mollitia enim mollitia ipsa accusantium.<br><br>Earum eos totam sit facere amet qui numquam quae. Eum cum dolor ipsum quia. Dignissimos ut nihil debitis eos placeat est blanditiis.<br><br>Rerum explicabo consequatur qui amet aliquam et. Nobis eligendi et eius cupiditate in quo. Quae voluptate aut reiciendis nobis soluta magnam reprehenderit. Voluptatem perferendis deserunt voluptatem et.<br><br>Earum tempore reprehenderit rem quae fuga perspiciatis. Praesentium voluptatem quibusdam aspernatur natus nesciunt. Illo officia fuga quam autem rerum voluptatibus.', '1.27', 'metal', 'black', 'XL', 'Deserunt quasi tenetur hic perferendis illo sequi sequi. Similique animi veniam consectetur sequi sunt aperiam. Amet atque saepe eum ullam. Enim nisi non vero dolore quia aperiam. Tenetur eum soluta sed vel reprehenderit. Reprehenderit et est quasi maxime doloremque nesciunt. Sed officiis nostrum dolorem ratione laboriosam minima. Modi consequatur iste provident deleniti omnis officia. Voluptatum id dicta fugiat aut consectetur. Molestiae soluta quo qui omnis ad. Sit nihil officiis laudantium blanditiis. Dolores est impedit ad et autem et laudantium. Magnam fuga fugiat unde laborum.', 2, '["products\\/sydney-opera-house.png","products\\/train.png","products\\/beach-3.png","products\\/river-2.png","products\\/beach-2.png","products\\/road-3.png","products\\/forest-2.png","products\\/tornado.png","products\\/snowman.png","products\\/coliseum.png","products\\/shore.png","products\\/sky.png","products\\/autumn.png","products\\/desert-2.png","products\\/whale.png","products\\/farm-2.png","products\\/lagoon-1.png","products\\/camping.png","products\\/walkie-talkie.png"]', 0, '2006-03-12 20:48:31', '2001-09-28 22:20:30'),
(5, 'PD-4', 'Aut delectus rerum quidem quia.', 34, 413, 69, 67022, 'Nihil et aut aperiam deleniti maiores officia explicabo velit. Inventore modi esse aspernatur ut atque tempora rerum.<br><br>Aut ad aut dolorum minus. Et exercitationem alias voluptatem. Illo similique accusantium odit totam corrupti modi culpa.<br><br>Quia tenetur omnis voluptas iste quaerat modi repudiandae. Quod vero totam nihil ad velit qui odit. Ab libero minima sit dolores voluptates.<br><br>Et eveniet debitis qui voluptas qui quia. Impedit eum nihil et tenetur. Aut expedita voluptatem repellendus sint. Nostrum et doloribus quisquam vitae mollitia.', '1.34', 'silver', 'black,green,white,blue', 'L,L', 'Velit aut repellat sed voluptas omnis quas iure. Porro dolorem optio expedita id. Et doloribus deleniti quia aut. Repudiandae autem consectetur occaecati voluptate nihil perferendis. Sit magnam eius eaque quaerat occaecati. Dolor ipsa non mollitia pariatur. Quis et id qui ipsa voluptatem esse qui. Deleniti vitae assumenda rerum eum omnis aspernatur quia nesciunt.', 3, '["products\\/beach-3.png","products\\/hunting.png","products\\/barbecue.png","products\\/road.png","products\\/hills.png","products\\/camping-6.png","products\\/digger.png","products\\/car.png","products\\/circus.png","products\\/park.png","products\\/forest-2.png","products\\/sea-1.png"]', 0, '1998-09-08 03:55:53', '2014-05-03 09:47:15'),
(6, 'PD-5', 'Temporibus repellat nulla delectus.', 10, 169, 76, 69744, 'Reprehenderit dignissimos et nam quae est a. Repellendus nisi dolore ipsum est animi dolor iste. Voluptatem illo magnam architecto nihil adipisci. Porro enim optio voluptas ullam rem tenetur adipisci omnis.<br><br>Commodi omnis temporibus quibusdam non quae. Voluptatem aliquid ipsum illum molestiae. Eum cupiditate tempore ut sunt. Omnis autem esse tempore reprehenderit iste.<br><br>Temporibus veniam cupiditate ipsam quisquam cupiditate aspernatur in. Ut iste eaque porro et saepe quaerat doloremque molestiae. Repellendus omnis et voluptates fuga. Et minus at sunt earum eum.<br><br>Qui error ut asperiores et fugit. Doloremque maiores ipsum repellat nemo illum ullam. Nulla ipsum distinctio eos et. Nulla suscipit placeat ad soluta aut in eveniet laudantium.', '1.42', 'nilon', 'white,red,blue,white,white', 'M', 'Harum ipsam id dolore voluptas voluptate. Minus mollitia aut omnis. Quis inventore est commodi numquam. Ducimus libero itaque molestiae ipsam qui consectetur assumenda quo. Aliquid sed sit sed animi maxime. Et blanditiis corrupti illo sit eligendi. Deleniti amet corporis pariatur quis illum perferendis. Mollitia non voluptatem eos animi illo labore. Molestiae illo asperiores voluptatibus nihil aspernatur consequatur.', 1, '["products\\/camping-3.png","products\\/cliff.png","products\\/mountain.png","products\\/forest-2.png","products\\/hospital.png","products\\/desert-2.png","products\\/road-3.png","products\\/camping.png","products\\/road-2.png","products\\/park.png","products\\/photo-camera.png","products\\/boat.png","products\\/camping-5.png","products\\/reindeer.png","products\\/cityscape-4.png","products\\/sea.png","products\\/lagoon.png","products\\/planet-earth.png","products\\/camping-5.png"]', 0, '1979-04-28 07:13:31', '1984-05-22 11:16:02'),
(7, 'PD-6', 'Incidunt magni et minus.', 31, 73, 61, 18822, 'Officiis ducimus voluptatem aut. Magni sunt molestiae et quaerat eos id consequuntur. Quibusdam eum adipisci aperiam veniam.<br><br>Totam ipsa qui sint quia. Vel numquam similique voluptas dolor est accusamus alias. Vero ipsa quia rerum.<br><br>Saepe enim iste repudiandae. Rem laboriosam explicabo ad. Aliquid libero facere voluptas voluptates impedit sed dolorum. Omnis quasi dicta qui provident ipsa.<br><br>Molestiae corrupti ad eius illo. Itaque placeat assumenda in. Dolorum similique illo vel fuga quis eius. Accusamus distinctio nisi nam ut recusandae.', '1.34', 'metal', 'white,green', 'XXL,M,XXL', 'Nobis voluptas asperiores reiciendis ut. Et harum at voluptatem quo voluptas ea voluptas. Rerum odio ut aspernatur tempore hic qui aut. Quia vero corporis iste qui. Molestiae velit et qui vitae. Deleniti sequi unde et. Magni occaecati a est sit tenetur. Et voluptatum sit cum et doloribus ratione occaecati.', 1, '["products\\/palace.png","products\\/cityscape-2.png","products\\/mountains.png","products\\/shore-1.png","products\\/beach.png","products\\/camping-7.png","products\\/lake-1.png","products\\/barbecue.png","products\\/farm.png","products\\/taj-mahal.png","products\\/camping-4.png","products\\/beach-4.png","products\\/forest-3.png","products\\/forest-1.png","products\\/camping-4.png","products\\/caravan.png","products\\/barbecue.png","products\\/cycling.png"]', 0, '1974-08-25 16:37:51', '2001-09-01 16:33:54'),
(8, 'PD-7', 'Quos nisi quisquam enim.', 10, 497, 89, 50039, 'Nostrum aspernatur et qui et magnam. Sint deleniti quia totam velit laboriosam nihil quisquam. Fugit et dolorem rerum architecto. Expedita dolorem totam ullam enim quae sint.<br><br>Voluptas sit praesentium aut facere excepturi dolorem praesentium rerum. Sit blanditiis dolore dolor repellat quis soluta tenetur. Odit iste omnis est pariatur et placeat magni. Dolorem dignissimos quia porro necessitatibus laborum.<br><br>Suscipit dolorem ad fugiat corporis alias quia praesentium. Dignissimos quam voluptatibus asperiores quam est consequuntur. Ut esse eaque officiis et velit illum eos recusandae.<br><br>At in officia ut reiciendis perferendis aut. Veritatis quo iste enim maxime aut ea voluptatem. Aut et velit non.', '1.18', 'gold', 'white,red,blue', 'XXL,L,L', 'Perferendis enim rem perferendis voluptates quos autem nihil. Minima excepturi consectetur aliquid. Odio temporibus vitae in eum dolores sit. Illo quam quibusdam provident. Aut quia ut officia in. Maxime cupiditate consequuntur aut placeat sint occaecati magni. Necessitatibus ea quos ut assumenda modi harum. Amet id dolorem eaque dolorum aspernatur veritatis voluptatibus.', 3, '["products\\/dump-truck.png","products\\/road-1.png","products\\/dump-truck.png","products\\/camping-1.png","products\\/photo-camera.png","products\\/mountain.png"]', 0, '2003-07-31 03:27:12', '1972-03-31 11:24:40'),
(9, 'PD-8', 'Autem facilis.', 11, 75, 52, 31673, 'Esse qui voluptatem ipsa consequatur aut. Consequatur dolor non non voluptatem aut. Neque et quidem nisi ipsum pariatur ex dolore. Impedit veritatis delectus totam explicabo vitae dolorem. Voluptas aut animi ut mollitia minima.<br><br>Possimus esse recusandae atque. Quod quo ea cupiditate dolores accusamus dolore et. Possimus expedita quo sit aut vel tempora.<br><br>Provident quia quis esse. Aut fugit laudantium placeat sequi. Minima rerum eius necessitatibus perferendis.<br><br>Et laborum maiores atque totam magnam eaque. Id voluptas totam ut culpa temporibus. Quia aut sapiente magnam.', '1.87', 'gold', 'white', 'XL,M,M', 'Porro officia ut non doloremque. Doloremque non alias exercitationem voluptates suscipit et. Odio impedit repellendus est totam et et et. Error quas nulla quasi. Consequatur beatae corporis blanditiis ullam et omnis. Sapiente praesentium qui sequi nobis. Qui tempore occaecati alias ad debitis tempore sunt sed. Earum voluptatem nesciunt aliquam enim. Vel doloribus est id voluptas velit. Iure adipisci non reiciendis autem repellendus ut odit. Fugit nesciunt asperiores ut natus aut enim.', 1, '["products\\/eiffel-tower.png","products\\/forest.png","products\\/forest.png","products\\/camping.png","products\\/hospital.png","products\\/sea-bottom.png","products\\/cityscape-1.png","products\\/mountains.png","products\\/camping-4.png","products\\/tornado.png","products\\/cliff.png","products\\/caravan.png","products\\/cityscape.png","products\\/route.png","products\\/mountain.png","products\\/shower.png","products\\/boat-2.png","products\\/forest-1.png","products\\/dump-truck.png","products\\/road-1.png","products\\/forest-3.png","products\\/forest-1.png"]', 0, '1999-02-15 04:35:13', '1970-06-11 08:34:42'),
(10, 'PD-9', 'Veniam qui omnis.', 10, 398, 32, 75890, 'Deserunt recusandae quo qui sit. Deserunt aut ipsa veritatis. Dolores aspernatur aut sint et tempore.<br><br>Quidem sunt reiciendis ducimus quos dolore. Fugiat est ut perferendis voluptas quas. Iste ea laborum eaque asperiores.<br><br>Dolore est id animi similique sit expedita veritatis. Ipsum est facilis corporis. Sequi et qui rerum commodi sed eius. Id accusantium et iste repellendus quisquam.<br><br>Iure ullam odio nam consequatur dolor reprehenderit perspiciatis. Quo dolorum est non expedita dignissimos debitis inventore. Dolor veniam dignissimos porro corporis. Provident odit officiis commodi placeat ea.', '0.71', 'metal', 'blue,black,green,black', 'L,M', 'Cumque ut ut ut quaerat harum voluptatem. At qui vitae facilis doloremque autem aliquid. Et quas vel voluptas aperiam est eaque alias. Temporibus sed in non consequatur consequatur eius qui explicabo. Harum ducimus ut quia iusto. Eos excepturi consequatur maxime doloremque dolores tenetur sit voluptatibus. Ut atque facere temporibus et quasi. Et quisquam suscipit amet ut. Voluptates id nobis reprehenderit est minima.', 3, '["products\\/park-2.png","products\\/mountain.png","products\\/farm-1.png","products\\/mountains.png","products\\/iceberg.png","products\\/walkie-talkie.png","products\\/snowman.png","products\\/coliseum.png","products\\/bulldozer.png","products\\/camping-2.png","products\\/sea.png","products\\/barbecue.png","products\\/forest-3.png","products\\/beach-2.png","products\\/tractor.png","products\\/reindeer.png"]', 0, '2013-04-21 10:39:55', '1994-05-29 20:39:33'),
(11, 'PD-10', 'Ratione inventore reprehenderit.', 18, 225, 51, 50079, 'Facere ipsum provident exercitationem rem quam inventore. Qui reiciendis iusto quia ut. Laboriosam debitis eum illum eius sed.<br><br>In dolorem dolorem eligendi delectus perspiciatis quo. Maiores accusamus omnis sed est omnis eum sit facilis. Hic quo et illo veniam. Sint harum id alias ipsum.<br><br>Officia corrupti praesentium ducimus sit. Sint suscipit impedit velit. Facilis quisquam et aperiam eos.<br><br>Ut soluta quod neque est ea sapiente. Deserunt autem est aspernatur odio. Aut ad molestiae corporis non quia deserunt praesentium. Ut quibusdam qui et nihil accusamus temporibus.', '0.11', 'wood', 'red,red', 'L', 'Quia et aliquid natus vel voluptas reiciendis quo assumenda. Rerum sunt sunt ut architecto. Nam eaque sed atque. Asperiores ab sit omnis cupiditate soluta ipsum consequuntur. Enim vel quisquam quo dolorem illo eos in. Reiciendis nostrum qui voluptatem. Alias vel non doloribus nihil. Nihil impedit placeat sed.', 1, '["products\\/farm.png","products\\/farm-2.png","products\\/beach.png","products\\/sky.png","products\\/planet-earth.png"]', 0, '1978-02-21 05:42:32', '1998-11-08 15:41:36'),
(12, 'PD-11', 'Enim dolores aspernatur.', 12, 364, 69, 27145, 'Quisquam ab enim vel odit velit. Iusto aperiam impedit quam dolores ut eos neque beatae. Illo sed eum eos facere quis sint. Accusantium tempore laudantium maiores vero consequatur.<br><br>Ducimus facilis aliquid occaecati et sit. Molestiae inventore ea dignissimos possimus. Laudantium accusamus voluptatem voluptatem.<br><br>Quas unde enim earum rerum dolor delectus. Qui eveniet repellendus itaque quam iste. Asperiores et mollitia consequatur non dolorum nisi consequatur ut. Alias sint veniam omnis provident numquam.<br><br>Qui explicabo non quidem consequatur. Omnis autem quasi nobis rerum animi molestiae hic. Beatae enim quis voluptates magnam minus pariatur minima sit. Fuga sapiente repudiandae dolorum ut.', '1.64', 'wood', 'yellow', 'XXL,M', 'Iste inventore dicta ipsam. Odio et quam molestiae quos recusandae aut ratione. Error ipsa ea tenetur dolores recusandae nemo velit excepturi. Culpa quia itaque ut asperiores nisi blanditiis et. Magnam facere maxime neque laborum mollitia facere asperiores. Quia at beatae rerum aut velit. Rerum natus nisi explicabo error. At qui enim dolor nulla explicabo a enim. Ratione et molestiae laudantium. Quidem eum harum eius fuga sit vel. Quia et et expedita nisi. Aliquam ut quidem eos consequatur expedita mollitia similique.', 0, '["products\\/camping-1.png","products\\/beach.png","products\\/palace.png","products\\/taj-mahal.png","products\\/cityscape-3.png","products\\/cityscape-3.png","products\\/iceberg.png","products\\/sea-bottom.png","products\\/cycling.png","products\\/church.png","products\\/volcano.png"]', 0, '1972-12-28 16:19:03', '1978-12-25 14:27:26'),
(13, 'PD-12', 'Qui soluta tempore soluta eos.', 1, 114, 83, 62948, 'Enim laborum optio in. Quam porro in sed quod praesentium.<br><br>Reprehenderit et dicta eum porro tempore et. Voluptatem non est perferendis consectetur. Velit natus deserunt eius provident saepe.<br><br>Hic dolorem cum enim ullam. Molestiae doloremque nulla sed cumque commodi et rerum. Ad eos laboriosam nihil nobis blanditiis qui unde.<br><br>Sint ea maxime saepe. Aut magnam consequatur deserunt omnis adipisci dignissimos. Incidunt sed qui veritatis esse. Quo nihil aut eos iste vel.', '1.74', 'wool', 'blue,black,green', 'XXL,XL,M', 'Hic ex quasi mollitia ut omnis. Minima qui nemo et. Nostrum eum beatae suscipit illo ea cum. Magnam omnis non et. Enim labore et et ex corporis. Repudiandae saepe rerum inventore sit quia. Qui aspernatur harum dicta magnam mollitia praesentium quasi. Porro qui sunt incidunt. Assumenda vel officiis vero inventore excepturi quidem sint. Iusto ab aperiam voluptate ut. Quia ducimus aut dolores ut totam. Accusantium repudiandae animi in cum sed quod. Quas iste et doloribus odit et. Repellat similique aperiam earum voluptatem suscipit ut enim. In est necessitatibus quam odit incidunt molestiae totam saepe. Tempora recusandae est amet autem fuga. Saepe corporis non assumenda consequatur est.', 2, '["products\\/palace.png","products\\/cityscape-3.png","products\\/desert-2.png","products\\/shower.png","products\\/photo-camera.png","products\\/route.png","products\\/camping-6.png","products\\/road-1.png","products\\/beach.png","products\\/planet-earth.png","products\\/desert-2.png","products\\/cycling.png","products\\/digger.png","products\\/car.png","products\\/coliseum.png","products\\/dump-truck.png","products\\/haunted-house.png"]', 0, '1993-01-09 08:46:53', '1980-06-19 22:40:13'),
(14, 'PD-13', 'Odit qui eos.', 14, 136, 19, 55732, 'Praesentium eum aut suscipit quaerat animi. Quod ullam possimus quis voluptatem soluta unde et. Totam voluptates dolorem eius fugiat. Enim earum dolorem ipsa at suscipit alias id et.<br><br>Voluptatem in sint laudantium perspiciatis. Veniam et doloremque expedita voluptatibus rerum et corrupti. Qui quia et voluptas beatae vel consequuntur cupiditate.<br><br>Dolore sint laborum placeat pariatur repudiandae. Et suscipit hic odit labore earum cumque. Dicta debitis dolores dolores odit doloremque. Dolor est atque reiciendis maiores modi dolorum ullam.<br><br>Officia eos sed placeat. Vitae architecto laborum aperiam id deserunt. Et consequatur ipsam impedit et. Labore dolor sed repellendus et.', '0.59', 'cloth', 'blue,white', 'L,XL', 'Magnam molestias sit id eius voluptas porro dolorum aut. Consequuntur cumque consequatur sed laudantium perferendis. Animi inventore quidem illum accusantium laborum ut. Tenetur modi sunt aperiam tempore et dolorem voluptatem. Deleniti nam ea quibusdam voluptatem voluptatum sint necessitatibus est. Quia delectus culpa enim sit. Laborum neque saepe nemo in voluptatibus. Nobis itaque tenetur corporis enim sed vero et. Distinctio omnis molestias cupiditate. Dolorem sit quod sint repellendus blanditiis. Necessitatibus repudiandae dicta sequi error deserunt cum ut.', 0, '["products\\/forest.png","products\\/backpack.png","products\\/vacations.png","products\\/forest.png"]', 0, '2014-03-04 12:52:44', '1971-10-06 15:20:50'),
(15, 'PD-14', 'Consequatur est tempore pariatur.', 13, 203, 50, 5370, 'Dolores doloribus deserunt sapiente repellendus odio. Omnis sed quod voluptatem veniam tenetur quis. Consectetur veniam exercitationem ipsum. Aut cum et non nesciunt.<br><br>Cupiditate qui sed ea officia nostrum et. Voluptatem rem ut amet sit iusto est. Accusamus minus et quaerat necessitatibus.<br><br>Autem voluptate non itaque omnis nobis. Deserunt et voluptatem nemo omnis sed placeat iure. Sit et animi tenetur.<br><br>Iste minus sapiente adipisci saepe accusantium. Ipsum quibusdam earum eius at consequatur totam omnis. Qui deserunt nesciunt voluptate ipsam id distinctio. Ab aut ut est sunt. Quo ipsum iste itaque rerum laborum.', '1.03', 'metal', 'blue,white,blue', 'M,XXL', 'Quo aut laborum nisi dignissimos autem sint. Exercitationem mollitia vel repellat beatae. Autem tempora quae aut rem. Et suscipit eligendi atque adipisci. Deserunt quia nobis ut eum et et. Doloremque qui incidunt voluptate animi sunt repudiandae. Rerum sunt illum perferendis sit aut adipisci quos. Illo voluptate doloribus labore aperiam alias odit qui. Enim sed pariatur voluptatibus ut dolor est qui velit. Libero illo nulla repellendus ab architecto molestias. Ut molestiae quo quis ea molestiae minus. Quia animi ut vel. Odio dignissimos accusantium dolorem. Explicabo incidunt hic qui voluptate reprehenderit minima. Eum odio adipisci incidunt voluptates. Deserunt provident inventore rerum sed in cum. Assumenda inventore voluptas dolorem autem et dolore et.', 0, '["products\\/shower.png","products\\/road-3.png","products\\/map.png","products\\/coliseum.png","products\\/river-3.png","products\\/park-1.png","products\\/shower.png"]', 0, '1992-03-27 09:07:56', '2002-11-24 02:40:12'),
(16, 'PD-15', 'Est quam sit non.', 24, 180, 95, 9892, 'Est et totam temporibus sed. Ad qui veniam vel illum repellendus iste quam. Ipsa a suscipit repellat accusantium voluptatem aliquid incidunt.<br><br>Odit ratione aut qui nesciunt. Qui laudantium ex qui dolores ut. Id ratione deleniti accusamus soluta id. Aspernatur quas est ipsa perspiciatis.<br><br>Quisquam et voluptate culpa ad debitis. Reprehenderit non soluta dolorem suscipit assumenda sequi omnis incidunt. Voluptatibus quo tempora voluptatum dolor culpa.<br><br>Numquam eligendi dicta sed est quas officia dicta. Ratione dicta voluptatum cumque omnis. Architecto quia nihil accusamus aut id ullam.', '1.72', 'gold', 'blue,white,black', 'XL,M,L,L', 'Quidem eligendi dolorem explicabo vero. Et minus ipsa quia accusamus ipsum officia. Maiores consequatur maxime dolores. Eum aut autem inventore exercitationem. Provident aut eligendi fugit sit. Illo alias voluptatem praesentium ut dolores. Voluptatem et necessitatibus sit dolore qui dolores. Atque veritatis quam consequuntur pariatur. Ut aut eveniet iure in. Sint delectus commodi reprehenderit ut dolore. Ullam enim nemo deleniti voluptatibus. Qui cum optio eos et repudiandae officiis ut. Ut repudiandae quibusdam alias rem hic. Doloribus eos culpa cum et.', 2, '["products\\/vacations.png","products\\/school.png","products\\/dump-truck.png","products\\/cityscape-1.png","products\\/autumn.png","products\\/deer.png","products\\/camping.png","products\\/lake.png","products\\/reindeer.png","products\\/shore-1.png","products\\/photo-camera.png"]', 0, '1998-08-14 18:01:27', '2018-01-19 10:27:41'),
(17, 'PD-16', 'Possimus velit aliquam.', 29, 26, 94, 99230, 'Est atque harum aut id veritatis. Autem distinctio dignissimos quae aut. Nam ratione in nulla necessitatibus non aut accusantium. Sit occaecati et suscipit soluta dolorem ut eos.<br><br>Officia accusantium sed accusantium exercitationem nisi ex ut earum. Praesentium consectetur expedita tenetur vel accusamus ullam. Distinctio eius culpa sit voluptatibus.<br><br>Placeat tempora dolorem sint eveniet. Voluptas totam consequatur et corrupti.<br><br>Ipsam ut iusto qui sed. Officiis saepe corporis commodi non quis perspiciatis. Rerum expedita error aut quaerat possimus. Ut aut optio laboriosam velit est ut exercitationem eius.', '0.81', 'wool', 'white,red', 'XL', 'Qui eum dolorem aliquam deleniti quo. Voluptatem dolor ut officiis dolore. Sunt fuga impedit doloribus earum accusamus repudiandae ipsam. Ut ut magnam hic rerum provident voluptatum qui. Corrupti tenetur qui aliquid qui alias. Minima repudiandae rem doloremque et eligendi sit facere. Omnis enim magnam eos dolore doloremque. Sed tempore quibusdam ea dicta. Qui quaerat ipsam et doloremque corporis. Ad et ut facere mollitia.', 1, '["products\\/church.png","products\\/lagoon-1.png","products\\/church.png","products\\/field.png","products\\/desert-3.png","products\\/hotel.png","products\\/farm-1.png","products\\/deer.png","products\\/sea.png"]', 0, '1977-03-06 02:07:48', '1983-06-18 13:48:03'),
(18, 'PD-17', 'Dolores est dolorem.', 14, 221, 39, 91680, 'Iure aut voluptas earum voluptas ad. Ea culpa adipisci repellendus soluta fugiat. Voluptates consequatur perspiciatis totam consequatur voluptatem fugit exercitationem. Exercitationem ex sunt non et.<br><br>Unde ducimus occaecati velit deserunt nisi voluptatem. Sit delectus id corrupti et et. Et quia veniam autem et velit delectus dolore. Voluptatem enim nisi est.<br><br>Eaque sit sed quia praesentium et. Iure aut similique error molestiae a ut mollitia. Sint ea est omnis ipsam eum.<br><br>Saepe id animi quod ut ut. Voluptates recusandae dignissimos sunt architecto. Consequatur soluta iste fugiat temporibus dolor unde aspernatur.', '0.17', 'silver', 'red,white,red', 'L,XXL', 'Provident et atque iusto libero odit impedit. Veniam nihil voluptate temporibus. Velit vel quo eveniet. Vel eum natus occaecati velit neque molestias ut. Voluptatem sequi blanditiis excepturi et. Corrupti ducimus beatae blanditiis rem molestiae ad. Doloribus tenetur dolorem enim ab harum assumenda. Sit dolores quia cupiditate sed maiores. Quia blanditiis architecto perferendis in ut ea. Harum facere exercitationem quasi vitae eos voluptatem. Quaerat voluptatum dolor ad odit quod molestias est aperiam. Amet qui odio aut qui occaecati porro. Incidunt iure facilis cupiditate. Sint deleniti et dicta quia eius distinctio. Quasi ut sunt nihil eligendi amet provident quia deserunt. Sint suscipit qui quidem magni animi.', 2, '["products\\/boat-1.png","products\\/camping-1.png","products\\/field.png","products\\/great-wall-of-china.png","products\\/parachute.png","products\\/shore.png","products\\/bulldozer.png","products\\/photo-camera.png","products\\/great-wall-of-china.png","products\\/default.png","products\\/park-2.png","products\\/new-york.png","products\\/farm-1.png","products\\/cycling.png","products\\/bulldozer.png","products\\/taj-mahal.png"]', 0, '2016-10-12 07:32:19', '1977-07-30 10:58:59'),
(19, 'PD-18', 'Rerum vero non fugit.', 12, 364, 19, 38439, 'Provident ipsam quos molestiae ab aut enim. Soluta perferendis nostrum quasi adipisci architecto. Numquam sint modi tempora quibusdam esse non.<br><br>Vel quis recusandae quia nihil quo quos temporibus. Unde doloribus a qui sit suscipit. Consectetur molestiae praesentium molestias labore voluptatem aut. Est quo et quaerat voluptas id reiciendis ut. Inventore suscipit placeat in.<br><br>Quisquam sunt qui et repudiandae tempora. Commodi natus ea beatae ut quidem. Dolorem repellendus id voluptas odit impedit. Sint molestiae dolorum enim.<br><br>Autem consectetur et ipsa consequuntur. Quibusdam quia autem et placeat aut. Voluptatem corporis quam aut atque aut pariatur consequatur. Ad ut velit sit inventore.', '1.45', 'metal', 'white,black', 'XL,L,M', 'Molestias mollitia similique ut explicabo ut dolores. Tenetur totam ullam ut. Quia magnam error voluptatibus itaque aperiam quas dolores eos. Excepturi cum dolorem hic ut est explicabo. Nemo in quidem assumenda modi ratione nobis repellendus. Sapiente et minima qui repudiandae sunt. Nisi omnis sint assumenda esse quo temporibus perferendis. Sit sapiente cupiditate vel itaque nam delectus repellendus aut. Ratione tempore facere ipsa nihil. Reiciendis vero aperiam provident tempore error aut. Saepe et et illo libero et voluptas. Et vero tenetur et autem. Optio unde tenetur debitis illo qui est sed. Dolorem modi autem necessitatibus est. Doloremque inventore nihil harum provident facilis. Quam ut qui delectus quia reiciendis tenetur consectetur. Ut ullam id error aut ea.', 3, '["products\\/desert.png","products\\/desert-1.png","products\\/camping-6.png","products\\/new-york.png","products\\/barbecue.png","products\\/tornado.png","products\\/road.png","products\\/camping-1.png","products\\/digger.png","products\\/park-2.png","products\\/river-2.png","products\\/sea-bottom.png","products\\/car.png","products\\/eiffel-tower.png","products\\/forest-1.png","products\\/great-wall-of-china.png","products\\/volcano.png","products\\/hunting.png"]', 0, '1994-10-12 10:14:08', '2011-02-25 07:32:53'),
(20, 'PD-19', 'Molestias eos id aut.', 27, 362, 90, 37225, 'Mollitia et ut cupiditate. In necessitatibus in voluptatem dolor provident omnis dolores. Expedita rerum laudantium asperiores aut quis. Voluptatem quasi aut qui sit nam dolores eum.<br><br>Sit magni inventore quam ea culpa itaque minima. Error tempora mollitia commodi quo. Repellendus provident dicta sed enim labore amet aliquid. At error qui quam ut neque est.<br><br>Neque aliquam ullam error numquam vero. Eum et officia adipisci nihil.<br><br>Fugit atque est eos tenetur. At unde laborum ducimus est natus harum. Illo delectus quia ut ut deserunt aut.', '0.65', 'cloth', 'yellow,red,black,white', 'XXL,L', 'Perferendis ut minima qui sed. Quaerat at corporis explicabo adipisci itaque distinctio. Aut ex est tenetur quos accusamus reprehenderit. Molestiae aut harum blanditiis neque. Ipsam sit expedita ut. Cupiditate inventore ducimus in deleniti blanditiis aut natus. Minima sint inventore doloremque sapiente. Illum aperiam debitis in animi et et.', 1, '["products\\/camping-5.png","products\\/lake-1.png","products\\/desert-2.png","products\\/train.png","products\\/beach-1.png","products\\/backpack.png","products\\/walkie-talkie.png","products\\/bulldozer.png","products\\/car.png","products\\/photo-camera.png","products\\/shore.png","products\\/map.png","products\\/camping-4.png","products\\/bulldozer.png","products\\/river-3.png","products\\/car.png","products\\/lake-1.png","products\\/dump-truck.png"]', 0, '1987-03-15 01:24:27', '1972-08-06 11:10:01'),
(21, 'PD-20', 'Voluptatem modi quo.', 21, 358, 72, 51377, 'Autem eligendi nam molestiae ipsam voluptatem accusantium. Vero cupiditate iste facere exercitationem magni voluptatem. Ut eligendi architecto quia a quae ut in.<br><br>Unde aut et aperiam unde facilis voluptate. Facilis sapiente cupiditate officiis ullam quo neque quo. Qui accusantium numquam sequi exercitationem.<br><br>Ut voluptatem itaque odio doloremque facere accusantium qui. Aut occaecati at sed cupiditate. Odio non amet dolores dolorum.<br><br>Quia quibusdam praesentium voluptatem consequatur nam non. Sapiente consequuntur id hic sint ea dolor sint. Tempore voluptas amet et tenetur ut iusto.', '0.28', 'cotton', 'yellow', 'XXL,M,L,M', 'Et alias ut molestias provident accusantium expedita. Nostrum expedita est perferendis autem. Rem quibusdam numquam alias nesciunt. Dolorem vitae eaque iusto ex quia sint. Cupiditate et adipisci dolores esse ducimus iste aliquid. Aut quia eligendi voluptatum aliquid adipisci eius est. Voluptate earum amet non ut. Doloremque occaecati veniam amet ducimus eum quis. Saepe ipsa dicta est qui id numquam rem. Et dignissimos sequi libero exercitationem et optio. Eligendi ut a sed dolorum commodi exercitationem dignissimos.', 1, '["products\\/park.png","products\\/map.png","products\\/road-3.png","products\\/desert-3.png","products\\/boat-1.png","products\\/tornado.png","products\\/river.png","products\\/beach-3.png","products\\/route.png","products\\/train.png","products\\/desert-2.png","products\\/road-3.png","products\\/road-1.png","products\\/road.png","products\\/road.png","products\\/new-york.png","products\\/camping-3.png","products\\/shower.png"]', 0, '2004-12-20 03:28:00', '2003-02-19 14:20:42'),
(22, 'PD-21', 'Veniam occaecati.', 14, 221, 83, 55758, 'Est explicabo quis ducimus ut eos reprehenderit rerum. Molestias voluptates voluptatem eaque voluptatum perferendis quo pariatur. Doloribus labore ut quisquam voluptatem provident animi. Unde necessitatibus sunt velit quia dolores quas.<br><br>Ullam delectus error et reprehenderit sit. Voluptate quo aliquam ducimus laboriosam eos omnis. Ipsum et sunt consequatur est ut.<br><br>Soluta consequuntur facere praesentium molestias aperiam est. Dolorum delectus voluptatum odit in. Praesentium nostrum rerum blanditiis consequatur.<br><br>Corporis voluptas consequatur consequatur et. Natus rerum consequuntur perspiciatis magni. Impedit sed necessitatibus enim aliquid voluptatem itaque. Esse reiciendis atque sed sit aspernatur molestiae dolorum.', '1.3', 'nilon', 'black', 'L,L', 'Ipsa rerum dignissimos reprehenderit recusandae illum voluptas. Voluptatem odio perspiciatis iste sequi possimus laudantium. Molestiae ipsum cum eos nesciunt. Aut porro praesentium odit excepturi ullam magni vitae sed. Amet qui et qui error. Velit veritatis dolor a soluta mollitia dolores optio. Doloremque alias odit quidem vero quo. Sunt eligendi cum voluptatem in vero repudiandae officiis. Adipisci qui reiciendis ea et. Ducimus ut blanditiis et et molestiae et id. Est est voluptatem perferendis laudantium dolores eius. Repellat nesciunt dolorum ex sapiente aspernatur illum. Qui ea nostrum eaque rerum. Dolore et sapiente ad a hic. Et id id fugiat totam. Excepturi laudantium et porro qui minus voluptatem.', 3, '["products\\/river-2.png","products\\/walkie-talkie.png","products\\/camping-1.png","products\\/camping-1.png","products\\/lagoon.png","products\\/sea-bottom.png","products\\/train.png","products\\/church.png","products\\/church.png","products\\/planet-earth.png","products\\/car.png"]', 0, '2018-06-04 20:23:02', '1975-07-20 12:36:07'),
(23, 'PD-22', 'Magni fugiat accusamus.', 30, 199, 25, 57568, 'Maxime omnis qui amet nesciunt. Fugiat deleniti esse necessitatibus eos. Voluptate quisquam omnis corporis quia iste ea. Debitis aliquid eum temporibus vero.<br><br>Dignissimos excepturi quo commodi officia. Alias qui et labore. Dolor voluptatum est ut voluptas pariatur dolorum saepe.<br><br>Voluptatem corporis molestias rerum adipisci et praesentium. Earum non cumque labore eligendi. Assumenda aut odit sit sed harum explicabo tenetur magni. Repudiandae inventore quia consequatur aut praesentium ut.<br><br>Dolorum esse laborum nam. Deserunt ut temporibus voluptatem.', '0.89', 'wool', 'red,red', 'XL,L,XXL', 'Quia et quidem aut sint. Est ex libero magni ducimus totam. Necessitatibus at rem voluptatem veritatis sint. A ipsa incidunt ipsa voluptatum ipsam enim. Qui voluptatem blanditiis soluta sit alias qui. Nihil dolorem quia error quod. Veniam qui quia deleniti nemo quis. Dolor repellat aut voluptatem in velit ut sit. Corporis sequi nisi maiores quaerat. Id corporis ut sit aut ducimus vel aliquid. Iste quo voluptatem aliquid nobis quo quod dolores.', 3, '["products\\/road-2.png","products\\/compass.png","products\\/road.png","products\\/field.png"]', 0, '1993-04-09 07:30:59', '1990-03-12 10:23:00'),
(24, 'PD-23', 'Est nesciunt sed amet.', 24, 180, 44, 34976, 'Doloremque reprehenderit dolor molestiae facere rerum repellat magnam non. Aliquam est modi tempore sed ut illo. Et aut nisi eum tenetur quibusdam illo cum.<br><br>Est quo rerum unde magnam voluptas nobis. Impedit perspiciatis aperiam officiis aliquam et illo beatae sunt. Illo sint consequuntur ipsum et velit.<br><br>Quaerat voluptatibus est eaque rerum dolores. Sit alias nam exercitationem vel eum ea. Labore temporibus nesciunt est eligendi voluptatem. Aliquam qui nihil ut maiores tempore.<br><br>Numquam perferendis dolore laboriosam molestiae neque. Enim vero nesciunt ullam omnis reprehenderit maxime. Accusamus dolores doloribus vel quia dolor.', '0.87', 'gold', 'red', 'XXL', 'Sed harum voluptates consequuntur molestias. Autem eos ut consectetur saepe sed magnam. Aliquid incidunt voluptate et voluptate culpa ipsam. Aperiam quod cum mollitia voluptatem non illo. Quia quis cupiditate et sed nam. Ut ad deleniti mollitia nostrum sint incidunt. Rerum nostrum odio quam. Dolore nobis ipsum dolorum quam. Qui expedita iusto et cupiditate eaque tenetur eaque.', 3, '["products\\/camping-4.png","products\\/caravan.png","products\\/farm.png"]', 0, '1997-07-12 18:51:42', '2017-11-06 21:26:39'),
(25, 'PD-24', 'Officia voluptatibus ut.', 31, 84, 94, 57423, 'Dolores doloribus similique sed aliquid sint nesciunt debitis. Cum suscipit enim atque beatae harum sit suscipit. Veniam laborum laboriosam repellat animi at.<br><br>Quo quia ipsam voluptates similique dicta. Ducimus et voluptatem magnam numquam. Consequatur necessitatibus qui ex earum provident.<br><br>Consequatur ea omnis aut animi minus in. Esse saepe vel vel. Distinctio alias dicta et commodi numquam odit.<br><br>Nam modi assumenda voluptatum illum. Iure laboriosam excepturi maiores porro at. Aut repellat quos ipsam quas.', '0.63', 'silver', 'red,white', 'XL,XXL', 'Quae omnis et quasi dicta aliquam repudiandae. Voluptates ullam quia iste veritatis accusamus. Soluta ea ut eum aut accusantium. Ratione occaecati et dolore est itaque laudantium. Unde consequuntur accusamus facere ea voluptatibus sunt. Id magni corporis eum. In quia inventore voluptatum dolorem expedita accusantium et. Qui expedita nulla ipsum earum quos magnam placeat. Accusamus officia ut eligendi sint cupiditate. Quas ut et quo qui voluptatem laudantium. Saepe ducimus qui non at. Accusamus et iste ullam nulla iusto fugit vel. Deserunt sunt facere totam adipisci. Est non id non sed suscipit fugiat possimus. Enim sed non architecto atque deserunt facilis sunt accusamus. Dolor aliquam vel maiores.', 1, '["products\\/camping-7.png","products\\/route.png","products\\/lagoon-1.png","products\\/beach.png","products\\/farm-2.png","products\\/boat-2.png","products\\/cityscape.png","products\\/desert-2.png","products\\/road-2.png","products\\/walkie-talkie.png","products\\/farm-2.png","products\\/forest-1.png","products\\/parachute.png","products\\/shore.png","products\\/cityscape-1.png","products\\/snowman.png","products\\/forest.png","products\\/lagoon-1.png"]', 0, '2006-04-30 20:37:50', '1991-05-02 07:29:29'),
(26, 'PD-25', 'Maiores eos beatae.', 11, 243, 8, 12215, 'Eum est quaerat repellendus ut cumque minima neque. Ut ea occaecati corrupti consequuntur voluptatem voluptatem sit. Perferendis non provident quo rerum ea earum. Tempora quia inventore quibusdam non et.<br><br>Suscipit a enim eum id. Non et corporis unde voluptatem. Dolore est dolores porro ut modi voluptas. Nam ut dolore aspernatur officiis esse.<br><br>Doloribus commodi beatae soluta cum aut. Voluptatem nesciunt ab in debitis et dolores autem error. Ipsa dolorem illum cum.<br><br>Eligendi a ratione rem iure nihil cumque. Nobis quibusdam ea consequatur quis. Aut voluptatem enim fugiat nihil tenetur dolorem tempora. Molestiae laudantium eum explicabo aut laborum vitae. Laudantium qui consequuntur aut rerum asperiores exercitationem.', '1.42', 'wool', 'white,yellow,white', 'XXL,L,XXL', 'Excepturi quis id quisquam nisi aut aut. Consequuntur quam reprehenderit nobis maiores cum sequi repudiandae. Cupiditate maiores molestias dignissimos inventore consequuntur voluptatem. Odit omnis vero recusandae tempore ducimus dolorem nesciunt. Exercitationem id quibusdam ullam et eius. Nihil accusamus voluptatem vel omnis repudiandae et ut quisquam. Tenetur adipisci quaerat iure natus id sed provident. Est cumque officiis quas tempora non itaque. Ullam ad explicabo ea est consequuntur numquam. Perspiciatis aut voluptates esse nam quos. Sit iste fuga tempore.', 1, '["products\\/beach-3.png","products\\/compass.png","products\\/great-wall-of-china.png","products\\/haunted-house.png","products\\/circus.png"]', 0, '1974-11-03 10:45:53', '2018-11-17 02:49:20'),
(27, 'PD-26', 'Excepturi commodi ut.', 21, 9, 96, 24218, 'Aspernatur ipsam consequatur ratione rerum atque id. Laudantium ducimus iste illum consequatur nihil ut assumenda. Nesciunt labore impedit fuga exercitationem exercitationem consectetur. Iste cupiditate molestiae debitis necessitatibus enim. Dolores sint nam suscipit fuga tenetur temporibus temporibus.<br><br>Libero nam quidem nesciunt aut quia quaerat. Assumenda perferendis quaerat perferendis quam. Itaque debitis reiciendis adipisci accusamus et.<br><br>Ut recusandae reprehenderit earum repellat libero doloribus quam. Minus quos laudantium possimus voluptatem et est. Corrupti qui excepturi et mollitia. Eum nulla expedita aut voluptatem.<br><br>Soluta qui et ex est sapiente quam distinctio. Deleniti saepe nisi quis id provident vero qui natus. Dicta reprehenderit est reiciendis in voluptates consequuntur quas fuga.', '0.99', 'gold', 'black,red,blue', 'L', 'Natus sit dolorem illum ut quod id quibusdam. Sed et velit et possimus possimus. Cupiditate occaecati perferendis ea nam velit blanditiis. Voluptatem architecto aut quis aut. Consequatur est provident quibusdam aspernatur ex beatae reiciendis. Debitis magnam rerum aut ipsam. Natus aut ipsam autem ullam facilis quibusdam. Officia id impedit ipsa magnam rerum. Reiciendis odio nemo quia id in enim occaecati. Nesciunt dolor quia at magni et ipsa. Non molestiae et et eum ut et. Ullam quasi enim sunt quam aut in sit.', 3, '["products\\/route.png","products\\/taj-mahal.png","products\\/sky.png","products\\/camping-5.png","products\\/beach.png","products\\/shore-1.png","products\\/cityscape-3.png","products\\/autumn.png","products\\/road.png"]', 0, '2008-06-28 14:25:55', '1990-06-24 05:05:23'),
(28, 'PD-27', 'Accusamus magnam.', 11, 342, 96, 78396, 'Rerum saepe facilis optio. Sed recusandae eligendi non veniam esse consectetur repellat. In iste asperiores asperiores a.<br><br>Praesentium non voluptatem minus tempore laboriosam. Nam ad incidunt doloribus dolorem magni iste explicabo animi. Dolores qui consectetur nihil placeat.<br><br>Et repellat tempora id eius excepturi. Sit quis corrupti laboriosam consequatur sint voluptatem. Occaecati laborum ea suscipit et.<br><br>Modi quaerat sit possimus qui qui voluptate vero. Omnis sint possimus et ut pariatur aut beatae. Perferendis inventore corporis et cupiditate enim.', '1.71', 'wood', 'white,green,white', 'XXL,L,XL', 'Voluptate voluptas nulla et aut. Optio rem illo est et voluptas. Quibusdam dolor minus voluptatem voluptatem et nulla voluptatem at. Qui occaecati cum repellendus maiores voluptatem aspernatur asperiores ea. Modi eaque reprehenderit autem sapiente et ad. Est omnis asperiores est qui incidunt sunt. Reiciendis id recusandae eaque incidunt saepe sed. Aliquam architecto saepe odio repellendus qui. Beatae ipsum quae pariatur ipsam et cupiditate. Quia vel quaerat voluptatem labore. Nam occaecati molestiae optio blanditiis. Id accusamus dolores itaque qui tempore aut. Voluptas iure voluptas quia quas aliquam natus sit. Fuga pariatur ad ratione soluta soluta animi minima. Explicabo corrupti sint eos accusamus quaerat officiis quis et.', 2, '["products\\/forest-1.png","products\\/great-wall-of-china.png","products\\/circus.png","products\\/whale.png","products\\/beach.png","products\\/cityscape-3.png"]', 0, '2003-04-18 02:49:06', '2012-06-13 07:01:30'),
(29, 'PD-28', 'Et laborum eligendi deserunt.', 10, 41, 8, 67891, 'Temporibus commodi asperiores corporis repellendus et. Voluptas voluptas vel beatae sunt velit provident quasi. Labore vero dolorem tenetur et. Numquam consequuntur unde ea sed modi corporis sunt.<br><br>Et temporibus vel placeat corporis illo illo. Dolor est officia non quisquam eaque tempora.<br><br>Nihil molestiae non sequi minus molestias praesentium. Delectus sit voluptates aut corrupti est consequatur natus. Et excepturi omnis veniam eos repudiandae explicabo labore. Quia debitis qui consequatur impedit.<br><br>Nisi occaecati debitis consequatur sint. Necessitatibus perferendis nihil iste sed eum perferendis. Et rerum consequuntur aliquam ex et assumenda dolorum. Occaecati quo omnis autem hic.', '0', 'cloth', 'white,red,red,white', 'M,M', 'Autem porro cum quod reprehenderit consequuntur. Repudiandae quam rem eos minima est. Nihil odit molestias voluptate explicabo ut veniam eius pariatur. Ut atque quia eius. Aut voluptatem ex adipisci numquam fugiat aliquid qui. Sint sed aspernatur blanditiis amet et pariatur. Voluptate deleniti dignissimos et. Autem odio odio et quia repellendus. Est quia soluta perspiciatis laboriosam recusandae doloremque architecto. Cupiditate consequuntur facilis dolorum mollitia exercitationem ipsum sit. Sed vel eum omnis voluptas beatae. Placeat voluptas dolor ut rerum nesciunt hic quos. Dignissimos accusantium cum corporis tempore voluptas. Quasi enim a earum doloribus blanditiis quia at. Maxime qui culpa corrupti quia.', 0, '["products\\/hotel.png","products\\/backpack.png","products\\/sea-1.png"]', 0, '2003-05-07 07:18:47', '2016-05-06 09:49:56'),
(30, 'PD-29', 'Magnam saepe nobis.', 22, 239, 5, 26243, 'Voluptas et et dolorem sequi rerum ex eum assumenda. Quisquam laborum eos sunt et odit nisi numquam. Impedit aut sapiente voluptas beatae eius animi.<br><br>Molestiae voluptatem tenetur dolor et nemo eius ratione. Sapiente est et nisi ducimus. Rerum dolore numquam magni in ratione perferendis aut.<br><br>Quidem atque sit consequatur autem totam odit nemo ipsa. Modi saepe officiis eos quos. Beatae asperiores ab quaerat alias non. Rerum quas et quas debitis cum.<br><br>Perferendis molestias quia dolore assumenda animi. Aut adipisci sint natus non. Est nesciunt dolores cupiditate voluptas minima.', '0.5', 'wood', 'blue,black', 'M,XXL', 'Cum nihil consequatur eos veritatis aspernatur. Non et quibusdam voluptate ea molestiae est molestiae. Ad veritatis quibusdam omnis asperiores qui. Voluptatem vel eum non vitae corrupti dolorem. Harum optio ut sint necessitatibus unde earum. Sed et iusto atque aliquam. Deserunt ab qui laudantium molestiae qui accusantium. Et vitae temporibus architecto. Nemo nihil vero asperiores et in doloremque aut. Est molestiae dolorem voluptatum culpa omnis voluptatem doloribus. Beatae deleniti consequatur voluptas possimus. Aliquam perferendis voluptatem fuga eligendi sed. Repudiandae consequuntur occaecati ut quo. Consectetur aspernatur odio voluptas sed est laboriosam autem aspernatur. Ut consequatur consequatur ipsum commodi aut. Corrupti sed quibusdam unde cupiditate rerum quia laborum. Doloribus nihil omnis optio minus.', 3, '["products\\/camping-4.png","products\\/haunted-house.png","products\\/beach-2.png","products\\/whale.png","products\\/school.png","products\\/camping.png","products\\/whale.png","products\\/whale.png","products\\/cycling.png"]', 0, '1998-11-08 19:13:50', '1974-07-29 14:04:11');
INSERT INTO `products` (`id`, `code`, `name`, `provinceId`, `cityId`, `stock`, `price`, `description`, `weight`, `material`, `color`, `size`, `note`, `label`, `images`, `deleted`, `created_at`, `updated_at`) VALUES
(31, 'PD-30', 'Illo et fugiat.', 12, 228, 15, 68684, 'Autem dolor est at illo. Sint fuga praesentium consequatur in.<br><br>Nisi voluptatem animi sunt est nemo quam suscipit totam. Quos totam quia provident illum et. Repudiandae ut ad aut omnis dolores non.<br><br>Similique quo et laboriosam et laborum. Vel sed quisquam tempora quo. Enim ratione et quaerat quia cupiditate.<br><br>Dolores qui voluptas enim rerum. Vel rerum laudantium reprehenderit aspernatur. Ut ea dignissimos optio ex neque. Illo similique ratione rerum porro.', '0.34', 'wood', 'blue,red,green,white,yellow', 'M,XL,L', 'Eligendi et voluptas fuga. Cumque nemo aspernatur odit enim blanditiis consequatur voluptatibus. Nostrum consectetur officia autem consequatur. Facere illo velit totam ea eos cumque fugiat. Sint voluptas repellat eaque distinctio. Totam quae nisi consequatur qui placeat commodi magnam. Consequatur aliquam vero tenetur. Est illo quo nobis sed excepturi. Molestiae voluptatem repellendus sint sint saepe unde ut. Architecto unde quo dignissimos consequatur. Perspiciatis blanditiis quisquam voluptatibus consequatur sed corporis voluptas.', 1, '["products\\/farm.png","products\\/sky.png","products\\/road.png","products\\/hospital.png","products\\/tractor.png","products\\/river-3.png","products\\/mountains.png","products\\/valley.png"]', 0, '1992-02-11 06:00:20', '2011-01-31 08:16:45'),
(32, 'PD-31', 'Facilis facilis eius deleniti.', 10, 445, 16, 39047, 'Numquam quo molestias error dicta veniam. Vero rerum provident ut dolorem est rem sed. Exercitationem consectetur odit ducimus. Sit vero est in.<br><br>Illum molestiae deleniti qui. Ut possimus modi eos minus sequi et velit. Iste vel necessitatibus suscipit sunt rerum.<br><br>Porro perspiciatis reiciendis enim voluptatum aperiam deserunt consequatur natus. Sunt totam nisi aut est. Atque ut non dolorum et.<br><br>Illo et porro non voluptatem enim quod harum velit. Dolor quia quos veritatis minus eum. Consequatur libero et et ut aut eos ut.', '1.26', 'cloth', 'white,white,green,green', 'M', 'Magnam molestias necessitatibus consequuntur dolores dolores quia. Et eaque autem est consequuntur. Animi sit repellat quibusdam. Omnis molestiae quisquam aliquam. Ex dolores aut sit omnis eaque quas aut. Non rerum consectetur voluptas nulla et. Sed est beatae ratione repudiandae. Ut sint debitis sint labore. Voluptate nam suscipit ad vel repudiandae perferendis placeat. Illum vitae magnam architecto velit autem dolores. Hic repudiandae natus voluptatem sed quas sint sit quasi. Voluptatem repudiandae veniam quidem quo nihil ad ut amet. Iure facere repellat sed in. Enim nihil et alias. Rerum consequuntur occaecati nihil odio nostrum magni et.', 3, '["products\\/camping-1.png","products\\/sea-1.png","products\\/lake-1.png","products\\/photo-camera.png","products\\/camping-1.png"]', 0, '1991-09-10 21:30:05', '1999-12-05 23:19:42'),
(33, 'PD-32', 'Accusantium esse et.', 21, 11, 56, 24803, 'Molestiae quam perferendis asperiores ut voluptatem harum. Beatae ab enim officia minus et aut omnis omnis. Aut dolor consectetur voluptates accusantium sit velit.<br><br>Odit soluta qui rerum consequatur quam atque similique. Est nihil dolor veniam. Magnam eaque voluptate cum molestiae.<br><br>Eum debitis dicta quibusdam veritatis consectetur. Praesentium consequatur libero voluptas corrupti. Voluptates id minus distinctio corporis.<br><br>Quia aut ut ut dicta quia et distinctio. Quis consequatur dolor nihil ratione quisquam. Voluptatem beatae cumque corrupti fugit. Unde provident sit dolorum totam eos fugit dignissimos.', '0.07', 'gold', 'black,black,white,white,white', 'L,XL,M,M', 'Perferendis illum iure aspernatur quibusdam voluptatum. Nesciunt aperiam voluptates architecto nemo minus accusantium. Atque quos quos temporibus. Reiciendis veritatis est officia at. Soluta placeat optio sed ut sed ducimus et. Eum modi voluptas commodi consequatur aliquam necessitatibus. Omnis esse aliquam explicabo et minus. Voluptatum atque eius sunt autem repellat aut. Facere recusandae harum autem velit laboriosam explicabo quisquam. A ratione consequatur quos quaerat. Alias voluptatem nostrum rerum et voluptas voluptatibus. Vel est incidunt est maxime fugiat modi aut. Impedit qui quia qui. Voluptas velit ea beatae placeat.', 2, '["products\\/sea-1.png","products\\/deer.png","products\\/beach-1.png","products\\/cityscape-2.png","products\\/lake.png","products\\/barbecue.png","products\\/beach-2.png","products\\/church.png","products\\/car.png","products\\/planet-earth.png"]', 0, '1990-11-19 21:43:51', '1992-01-30 11:26:58'),
(34, 'PD-33', 'Dolorem in impedit quo.', 6, 189, 72, 82403, 'Assumenda qui porro voluptate aperiam magni itaque. Consequatur in ut et iure architecto aut iusto rerum. Facilis ut itaque suscipit iure autem velit molestiae aut. Facere blanditiis assumenda nulla numquam.<br><br>Incidunt sequi sit ullam aut facere est. Magni consequuntur doloremque non quasi. Dicta distinctio iure accusantium tenetur consequatur ab.<br><br>Vel nulla commodi officiis in doloribus velit cumque. Et libero libero et dolorum unde. Eos et maxime quis iste. Sit vitae hic minima placeat placeat.<br><br>Molestias rerum aliquid fuga odio similique. Sequi quia sint fugit minus. Quia incidunt at minus voluptatem ea architecto magnam. Labore mollitia magnam eius rerum reiciendis et.', '0.01', 'cotton', 'red,white,red', 'XL,XXL,L', 'Similique rerum nostrum culpa aut. Non similique quae est ratione ipsam fugit. Nisi voluptatum sed rerum quo corrupti. Possimus corporis repellat fugit qui omnis quis est. Cumque enim non officiis iusto dicta. Minima ut temporibus officia eligendi. Velit tempore adipisci saepe rerum. Minus qui perspiciatis similique animi. Qui nesciunt inventore molestiae veniam minus in. Occaecati similique vitae illo est nisi debitis numquam nam. Atque pariatur cupiditate a et blanditiis. In omnis illo provident. Perspiciatis dolores voluptates earum in earum. Quis distinctio consequatur cumque ratione. Nesciunt quasi molestias esse.', 0, '["products\\/road-2.png","products\\/boat.png","products\\/hospital.png","products\\/mountain.png","products\\/mountain.png","products\\/cliff.png","products\\/desert-3.png","products\\/tractor.png","products\\/hunting.png","products\\/new-york.png","products\\/lagoon.png","products\\/hospital.png","products\\/lake-1.png","products\\/camping-4.png","products\\/coliseum.png","products\\/whale.png","products\\/road-1.png","products\\/lagoon.png"]', 0, '2002-04-29 23:54:48', '2012-09-01 23:08:47'),
(35, 'PD-34', 'Iste repellendus harum.', 26, 166, 59, 48738, 'Harum ratione quibusdam odio quam aut et quidem. Sed quo est et animi. Eius ullam praesentium suscipit et accusantium et fugiat quas. Est illo qui laudantium aut exercitationem.<br><br>Illum occaecati temporibus aut hic eos. Quos cum dolor velit. Saepe aspernatur at ut similique.<br><br>Et culpa repellendus voluptate cumque esse. Laboriosam nobis eos delectus aspernatur voluptatem sapiente. Aperiam repudiandae molestias et aliquam eaque perspiciatis quis aliquid. Quia alias sunt dolore recusandae autem consequatur qui.<br><br>Atque est adipisci non neque incidunt officia eum. Atque placeat quia ut exercitationem esse.', '1.09', 'wool', 'blue,blue', 'M,M,XL,XXL', 'Dolore voluptatibus eos magnam laboriosam. Itaque quia est nesciunt corporis velit. Quae cupiditate atque est aspernatur provident ab non. Officiis et qui ipsa illum impedit suscipit. Eum sunt quaerat aliquid voluptate. Fuga expedita asperiores incidunt et. Nesciunt beatae non est nostrum. Optio veniam eos laudantium aliquid laboriosam doloremque assumenda.', 2, '["products\\/river-3.png","products\\/hospital.png","products\\/road-2.png","products\\/camping-4.png","products\\/forest-2.png","products\\/beach-2.png","products\\/road-2.png","products\\/taj-mahal.png","products\\/road-3.png","products\\/coliseum.png","products\\/camping-4.png","products\\/sea-1.png","products\\/palace.png","products\\/beach-2.png","products\\/cityscape.png","products\\/great-wall-of-china.png","products\\/bulldozer.png","products\\/camping-1.png","products\\/barbecue.png","products\\/parachute.png","products\\/camping-7.png"]', 0, '1978-03-31 12:34:55', '1989-06-09 19:19:02'),
(36, 'PD-35', 'Totam ut qui accusamus.', 26, 148, 51, 11390, 'Aut aut enim voluptatem vitae commodi quidem. Mollitia consequatur consequatur minima mollitia autem. Aliquam rerum minima ad.<br><br>Hic quos enim amet alias dignissimos. Omnis incidunt soluta eos voluptas. Perspiciatis consequatur reprehenderit recusandae voluptate et inventore et.<br><br>Cumque necessitatibus molestiae aut architecto repellendus in maiores porro. Explicabo veniam quia tenetur qui qui at eligendi. Voluptatibus et et eos est non est sint nisi. Ut occaecati autem accusamus non id.<br><br>Et sed velit dignissimos suscipit atque voluptatem dicta et. Blanditiis sint adipisci laboriosam ex voluptas ad non.', '0.92', 'gold', 'white,green,red', 'M', 'Delectus amet explicabo occaecati quibusdam incidunt eaque est. Quisquam eius sint neque odit. Adipisci beatae fugiat accusamus sunt et. Nihil ducimus aliquid ut qui reprehenderit qui omnis. Perspiciatis est dolorem facere omnis quidem commodi libero. Quia magni eveniet beatae maiores. Ducimus facere occaecati quos eum earum. In molestiae vero iure itaque voluptatem. Recusandae veniam minus eum ut aspernatur non ea non.', 2, '["products\\/shore.png","products\\/car.png","products\\/park-2.png","products\\/road-2.png","products\\/eiffel-tower.png","products\\/camping-3.png","products\\/train.png","products\\/digger.png","products\\/sea-bottom.png","products\\/hills.png","products\\/camping-3.png","products\\/desert-2.png","products\\/default.png","products\\/mountain.png","products\\/boat-2.png","products\\/backpack.png","products\\/cityscape-3.png","products\\/sea.png"]', 0, '2010-07-31 08:28:14', '1974-08-18 11:21:06'),
(37, 'PD-36', 'Deleniti qui deleniti.', 8, 293, 28, 71146, 'Vero et atque eum neque. Laboriosam adipisci cumque sapiente est animi. Laboriosam soluta facilis voluptatem iusto qui placeat voluptates. Aperiam unde quasi voluptatibus omnis.<br><br>Quaerat nemo earum vel nobis aut. Est quae et saepe deserunt. Vitae tenetur voluptas illo nostrum accusantium a beatae quas.<br><br>Pariatur maiores quia magnam et impedit nihil optio eum. Dignissimos minus velit delectus quisquam veniam nisi. Iusto omnis vel similique perferendis sit eveniet eius. Possimus dolorum rerum sed quo et qui.<br><br>Deserunt perspiciatis qui illum recusandae doloribus. Minus consectetur aut magnam fugiat.', '0.27', 'metal', 'blue,black', 'XL,L,L,XXL', 'Quaerat deserunt harum nesciunt nisi molestias quae quo. Fugit dolore recusandae ipsum aut quis dolor. Odit non est doloribus laudantium possimus. Consequatur sit est a. Ut delectus perspiciatis expedita. Rerum quaerat aut aut qui eos qui. Veritatis quae molestiae est animi. Quia perspiciatis repellat officiis sapiente occaecati maxime nobis.', 1, '["products\\/whale.png","products\\/hotel.png","products\\/circus.png","products\\/barbecue.png","products\\/camping-5.png","products\\/church.png","products\\/cliff.png"]', 0, '1993-04-28 10:10:52', '1988-07-16 03:34:16'),
(38, 'PD-37', 'Fuga deserunt.', 10, 169, 76, 93492, 'Ipsa id unde cum ratione ut repudiandae laborum. Sunt dignissimos deleniti mollitia hic dolore inventore omnis. Hic natus neque eum doloremque et sed.<br><br>Quas aut fugiat beatae et voluptatem in est. Ut earum deleniti veniam sunt aliquid ipsa. Corrupti laborum commodi magnam nesciunt dolore dolorem nam. Occaecati aut quia error facere odio quia dolorum. Voluptatem qui facilis illo.<br><br>Officia dicta veritatis et cupiditate ipsam doloribus alias. Earum quas saepe sit laudantium ut aut alias. Ducimus recusandae enim totam facere itaque unde officiis deserunt. Sunt nobis et ratione non.<br><br>Molestias omnis dolor praesentium rerum. Deserunt sed voluptas autem quae. Dolor fugiat necessitatibus quia. Voluptate commodi voluptatem deserunt incidunt omnis.', '1.75', 'cotton', 'red,red', 'XL', 'Sunt consequatur sit tenetur totam corrupti et voluptatibus. Tempora excepturi sequi itaque architecto et. Rem rerum tempore perferendis vitae recusandae. Similique ut corporis quis sint soluta explicabo harum tenetur. Dolorem quod libero dignissimos asperiores animi distinctio neque. Eum provident hic laborum. Provident repellat perspiciatis et. Quam exercitationem et adipisci quae dolorem nam. Quae suscipit non aut omnis. Veritatis voluptas non enim est eveniet. Omnis rerum ratione sit voluptas minima est. Dolores repellendus velit cupiditate autem error. Aliquam non expedita eaque totam consequatur in debitis. Quaerat laudantium quibusdam corrupti perferendis.', 2, '["products\\/lagoon-1.png","products\\/volcano.png","products\\/beach.png","products\\/lagoon-1.png","products\\/camping-6.png","products\\/digger.png","products\\/palace.png","products\\/desert.png","products\\/circus.png","products\\/coliseum.png","products\\/boat.png","products\\/parachute.png","products\\/boat-2.png","products\\/circus.png"]', 0, '1997-07-08 17:50:23', '2016-03-26 22:18:46'),
(39, 'PD-38', 'Animi at repudiandae non officiis.', 29, 119, 47, 60538, 'Sunt aut eos assumenda ratione. Quod quia ut facilis asperiores asperiores totam et qui. Eos voluptatem recusandae officia neque laboriosam officia. Odio quidem eum consequuntur esse incidunt deserunt.<br><br>Quis vero facilis id sint deleniti. Architecto esse consequatur et ipsa. Eligendi sit est est. Deserunt sit incidunt debitis veritatis aut eligendi.<br><br>Et expedita voluptas magni necessitatibus accusantium cupiditate. Aperiam iusto earum saepe dolores tenetur. Dolor fugiat id laboriosam soluta magnam quos reprehenderit. Eum eaque quis pariatur fugiat cupiditate deserunt repellat.<br><br>Architecto eaque tempore dignissimos. Eum omnis ut qui qui quaerat et autem eligendi. Sit cumque et eum consequuntur soluta sint aut aliquid. Quam ab rerum ut dolorem quia.', '0.57', 'nilon', 'white,yellow', 'L', 'Dolor officia quas atque magnam quod odio sunt id. Facere dignissimos quidem consequuntur accusamus modi adipisci. Aut nostrum provident non consequatur quaerat aut laudantium. Sunt nulla placeat voluptate ut. Quibusdam totam quod ipsa est voluptatem nulla sit. Debitis quia iure ducimus quidem quia accusantium nisi. Aut sed natus et eaque qui. Corporis sequi ipsum consequatur hic. Beatae necessitatibus totam aut dignissimos non. Voluptatem impedit est ipsa modi et voluptatem. Et consectetur quia eos ullam autem sint. Voluptates molestiae aut praesentium itaque quia. Voluptatem modi illo distinctio et earum id repellat. Quo ullam molestiae maxime eos dolore suscipit minus ex. Consequatur molestiae illum adipisci eum voluptates harum. Pariatur unde est illum dolorem maiores officia minima ullam. Similique aperiam aut id pariatur non sint blanditiis.', 1, '["products\\/haunted-house.png"]', 0, '1989-04-27 03:20:39', '1996-02-13 17:45:32'),
(40, 'PD-39', 'Ea eos illo minima eos.', 30, 85, 17, 10318, 'Dicta quia porro nesciunt eum ullam. Sed sed ut ullam non.<br><br>Consequatur aperiam totam quasi perspiciatis. Quaerat molestias nostrum tenetur ab. Aliquam eum atque omnis odit reiciendis qui voluptates aut. Et sit quod amet reiciendis quae quibusdam nihil.<br><br>Aut magni necessitatibus et quo sed illum provident et. Dolorem magni autem doloribus soluta.<br><br>Quia veniam a maxime dolores molestiae. Eius corporis velit et. Consequatur fuga corrupti sunt iste laudantium ipsam. Debitis non illo ad placeat voluptatem.', '1.19', 'metal', 'red,blue,blue', 'M,M', 'Quis quas vel veniam sed non libero. Aut iusto provident dignissimos officiis sapiente omnis dolores. Rerum aliquam cupiditate vero fuga ut aut. Et deserunt omnis minus impedit consequatur in. Rem quo rem esse est. Et amet veniam consequuntur. Maiores nobis et nam. Nostrum maxime tenetur culpa aut. Rem minus quis suscipit fugit id hic et. Qui dicta qui tenetur mollitia est perferendis velit necessitatibus. Quis in natus et neque amet incidunt aut. Expedita amet in officiis sed et. Voluptatem ut dignissimos officiis possimus est officiis. Dolorum excepturi perferendis sequi dolore. Sint iure qui nulla repudiandae.', 0, '["products\\/boat.png","products\\/road.png","products\\/sea-1.png","products\\/desert-3.png","products\\/shore-1.png","products\\/tractor.png","products\\/map.png","products\\/desert-2.png","products\\/farm.png","products\\/snowman.png","products\\/road-2.png"]', 0, '1994-04-15 18:55:42', '1984-07-01 09:48:22'),
(41, 'PD-40', 'Nam animi animi modi.', 14, 432, 67, 32800, 'Ratione provident consequatur maxime accusamus eos. Nihil itaque repudiandae eos perspiciatis quae magnam nulla. Magni fugiat aut dolores.<br><br>Eveniet consectetur tempora aut nemo. Esse soluta voluptatem est esse et.<br><br>Ut sapiente nihil esse aut. Omnis quis sint voluptates atque eum molestias. Necessitatibus praesentium necessitatibus omnis.<br><br>Nulla iure vitae quia magnam id vel sint. Maiores eos totam voluptatem est necessitatibus est consequuntur. Nihil quasi rem nobis magnam. Omnis est qui iusto doloremque odio.', '0.25', 'cloth', 'white', 'L,L,L', 'Nesciunt tempore sed ut nisi distinctio minima. Deserunt est earum dolorem quo. Voluptas eum cum asperiores enim eos. Modi vero nostrum velit reiciendis nisi quia. Non ea nemo ea iusto dolor ad ea. Alias quia quaerat aut voluptate itaque a iure tempore. Omnis architecto molestiae quis quisquam et aut. Itaque neque ea debitis laudantium cupiditate ut. Consequuntur sit culpa officiis nihil ut tenetur ipsum. Nesciunt aut repudiandae dicta exercitationem harum assumenda. Debitis sint et asperiores voluptatibus quam mollitia. Consequuntur doloribus consequatur accusantium nostrum hic. Assumenda voluptatibus ut exercitationem non. Nesciunt repellendus reprehenderit qui. Tenetur minus neque voluptas autem qui facere. Aspernatur dolor necessitatibus distinctio veniam labore.', 3, '["products\\/coliseum.png","products\\/cityscape.png","products\\/boat-2.png","products\\/camping-6.png","products\\/boat-2.png","products\\/boat.png"]', 0, '2002-04-08 01:26:13', '2014-12-28 12:01:57'),
(42, 'PD-41', 'Sed odit quaerat.', 9, 78, 24, 4588, 'Consectetur excepturi ipsam saepe ut. Ut fuga repellat recusandae et earum voluptas praesentium. Eligendi eveniet quasi qui quo in necessitatibus.<br><br>Doloribus dignissimos ut odio provident culpa eaque. Iste dicta quod ipsam et. Eos sequi id numquam. Vel quasi voluptatem ut inventore itaque necessitatibus. Ut necessitatibus iste vel accusantium consequatur quos quis.<br><br>Quam recusandae sapiente ea natus rerum. Voluptatem voluptas modi dolores quo. Excepturi ab aut et error. Vitae labore exercitationem ut et.<br><br>Voluptate voluptas quia qui consequatur distinctio ut in. Et numquam tempora deserunt voluptatem ab. Qui nemo dolorem enim aut recusandae. Nisi consequatur iste doloribus aspernatur non tenetur.', '1.84', 'cloth', 'blue,white', 'L,M', 'Ipsum facilis perferendis omnis dolor recusandae. Impedit sunt aut ut non excepturi. Quas et dolor labore impedit ullam eius. Pariatur mollitia natus perferendis dolore. Id veritatis minus nesciunt. Pariatur officiis quas quia amet autem ea. Corporis accusamus dolores nostrum ab. Nesciunt veritatis magni repellat repudiandae.', 1, '["products\\/river.png","products\\/palace.png","products\\/parachute.png","products\\/park.png","products\\/camping.png","products\\/farm.png","products\\/caravan.png","products\\/cliff.png","products\\/lagoon-1.png","products\\/camping-1.png"]', 0, '1994-02-27 05:40:55', '2002-01-18 18:40:15'),
(43, 'PD-42', 'Atque voluptatum excepturi.', 9, 428, 87, 24904, 'Ut mollitia explicabo quis dolorem mollitia. Cupiditate esse et nam quo. Adipisci dicta voluptas nihil ut voluptatem.<br><br>Odit et quis suscipit sequi dolorem et. Sint est unde nisi et id consequatur. Illum quas expedita aperiam et esse et similique nisi. Atque qui minus maxime velit ut recusandae.<br><br>Repudiandae mollitia et voluptas sed quasi vero. Corporis qui voluptas soluta fuga modi fuga.<br><br>Magni perferendis animi suscipit reprehenderit. Accusamus commodi eveniet quia quia rerum. Excepturi voluptatum aperiam adipisci fugiat et.', '1.91', 'cotton', 'blue,blue,white,white,blue', 'XL,L', 'Atque commodi aut voluptatem esse delectus magnam. Ipsam fugiat ut a alias omnis exercitationem. Magnam architecto inventore libero et quia accusantium. Ut libero voluptatem placeat reprehenderit deleniti. Ea est qui sequi. Molestiae quas placeat quam et voluptatem. Quis alias eos sed accusamus. Beatae cum delectus facere possimus provident aspernatur temporibus. Inventore ea nihil corporis est molestias quis. Error autem labore laborum autem. Inventore corrupti omnis rerum quod possimus similique. Velit porro eligendi ipsum ipsam. Sit repellendus incidunt facere recusandae velit. Quos perferendis adipisci quod nostrum maxime.', 3, '["products\\/beach-4.png","products\\/reindeer.png","products\\/forest-2.png","products\\/mountain.png","products\\/cliff.png","products\\/beach.png","products\\/forest-3.png","products\\/hills.png","products\\/forest-3.png","products\\/church.png","products\\/church.png","products\\/cityscape-4.png","products\\/train.png","products\\/river-1.png","products\\/school.png","products\\/cityscape.png","products\\/farm.png","products\\/river-2.png","products\\/haunted-house.png","products\\/caravan.png","products\\/mountains.png","products\\/beach-3.png","products\\/deer.png","products\\/valley.png","products\\/lagoon.png","products\\/forest.png","products\\/lake-1.png"]', 0, '2016-02-05 20:59:34', '1996-09-17 20:29:10'),
(44, 'PD-43', 'Perspiciatis sunt voluptatem.', 7, 77, 60, 41078, 'Harum reprehenderit assumenda ut quasi quis consectetur. Rerum mollitia at consequatur est. Molestiae veniam facilis assumenda vitae blanditiis.<br><br>Veniam et cum numquam provident iusto pariatur. Saepe nihil ipsa ut sint molestiae provident. Nisi consequuntur sit quibusdam dolore nesciunt.<br><br>Tenetur necessitatibus quisquam expedita dicta ad maiores nostrum quo. Velit eos qui dolore ea eos deserunt dolorem aperiam.<br><br>In unde sed odit dolores sed ut. Fugiat modi quia mollitia id. Et dolor id magni et modi expedita sint. Dolorem in sint neque quae voluptatum esse.', '1.6', 'cloth', 'blue', 'XXL,L', 'Soluta aut error perferendis ut quo aliquam quo consequatur. Praesentium consectetur fuga doloremque et quod accusantium ipsa. Occaecati labore consequatur in vitae asperiores. Nihil inventore cumque soluta alias. Repudiandae expedita eligendi rem nam. Quam reiciendis alias eum consequatur temporibus voluptatum. Iusto voluptas sunt possimus vel rem magni. Reiciendis officia deleniti quibusdam natus. Nulla cupiditate ea ut. Suscipit perspiciatis voluptatibus eum inventore est dolorem magni quod. Eveniet mollitia fugit ipsum quam quia sapiente at eos. Soluta culpa dicta eligendi ea. Omnis est maxime doloremque veritatis laudantium. Distinctio officiis quod est officiis animi. Quibusdam ab eos ex et rerum.', 1, '["products\\/route.png","products\\/farm-1.png","products\\/train.png","products\\/taj-mahal.png","products\\/cityscape-4.png","products\\/cliff.png","products\\/road-2.png"]', 0, '1974-01-27 19:19:01', '1993-12-20 11:04:01'),
(45, 'PD-44', 'Odit recusandae eaque temporibus dolorem.', 33, 297, 31, 33396, 'Consequatur animi alias deleniti dolor. Voluptate cumque aspernatur sit iste nihil est sunt. Iste perspiciatis magnam expedita fugiat commodi. Aperiam recusandae quos non.<br><br>Doloremque quo nisi deleniti dolorum assumenda. Voluptatem quasi magni occaecati omnis voluptate. Aut voluptatibus vitae voluptatem atque hic repellat.<br><br>Quae nemo aut error asperiores. Accusamus autem dolores placeat voluptatibus architecto qui nostrum. Et blanditiis maxime error modi assumenda et.<br><br>Aut voluptatibus dolor molestiae voluptatem est. Velit iusto laudantium voluptates optio fuga.', '1.86', 'cotton', 'blue,blue,white', 'XXL', 'Et ea tempora non autem eligendi odit delectus quasi. Quis quas excepturi unde aut consequuntur. Mollitia qui et nihil nemo. Velit fugit harum fugit nihil ut similique. Ad voluptatum repellat aut ut. Tenetur voluptatum omnis et hic blanditiis quis mollitia tempore. Et deserunt esse maiores atque ducimus vero. Qui illo qui non molestiae. Quia vel eveniet molestiae voluptas nesciunt. Porro culpa sequi doloremque cupiditate accusamus.', 3, '["products\\/whale.png","products\\/hotel.png","products\\/circus.png","products\\/barbecue.png","products\\/camping-5.png","products\\/church.png","products\\/cliff.png"]', 0, '2007-07-04 23:47:38', '2015-07-05 12:34:18'),
(46, 'PD-45', 'Harum id repellat.', 24, 373, 32, 68348, 'Neque ullam sint doloribus suscipit architecto. Dolores excepturi sint iure aut. Beatae est et porro doloribus sed sequi dolorem eos. Molestiae doloremque dolore id officia.<br><br>In mollitia alias omnis. Cumque quia consectetur sunt porro sit ad.<br><br>Ipsa quis rem et cumque consequatur. Ullam dolores molestiae delectus sed nemo pariatur perspiciatis. Eos doloribus veritatis ratione ipsum sed.<br><br>Sint libero sequi repellat hic laboriosam qui expedita. Qui voluptatem non omnis esse repudiandae. Tempora consequatur nisi rerum accusantium et doloremque. Vel nisi excepturi corrupti explicabo architecto aut.', '0.53', 'cloth', 'blue', 'XL,M', 'Fugiat nisi odio voluptas dolores. Enim omnis consequatur rerum quisquam dolores fuga consequuntur sed. Ea dignissimos dolorem fugiat praesentium architecto. Amet fugiat est quisquam. Voluptatem omnis pariatur beatae voluptate consequatur aut iste. Quisquam dicta sit illo cupiditate sequi. Sit suscipit et ipsam ut. Quia voluptatem eos magnam ab dicta nisi qui ex. Accusamus blanditiis suscipit eos ea et repellendus sed velit. Aut sint et natus. Voluptatem explicabo quas soluta odio. Numquam aut officia ullam dolor officiis dolor. Similique eum sed porro consequatur corporis. Nihil beatae ut nulla vel deleniti cumque.', 1, '["products\\/train.png","products\\/park-1.png","products\\/lagoon-1.png","products\\/school.png","products\\/iceberg.png","products\\/cityscape-3.png"]', 0, '2010-05-13 18:15:11', '2011-08-16 07:33:47'),
(47, 'PD-46', 'Accusantium ipsum praesentium est.', 22, 118, 78, 86127, 'Corrupti numquam consequatur sed minus et. Consequatur minima excepturi omnis aut rerum. Ut occaecati dolores harum est. Beatae nesciunt dolores amet quia explicabo odio dolor dolorum.<br><br>Rerum pariatur ut voluptates quo laborum animi numquam totam. Aut provident aut minima. Illum dolor voluptatem ipsa quidem magnam a corporis. Doloremque et impedit voluptatum pariatur.<br><br>Iusto odit deserunt similique dignissimos. Non quod voluptatum laboriosam saepe quidem. Tempora necessitatibus recusandae voluptates. Distinctio sequi et ipsam distinctio impedit est omnis.<br><br>Nihil reiciendis quam nesciunt et. Ipsum consequatur quisquam dolor excepturi quia dolores unde. Ea alias et ipsum laboriosam et ad. Consequatur ea quis voluptas ipsam aut.', '1.34', 'silver', 'blue', 'XXL,L', 'Accusantium dolor dicta qui expedita nemo voluptates excepturi. Deserunt accusantium iste fugiat voluptas. Nisi accusamus id sit est. Velit doloremque itaque consequatur sunt modi. Et perspiciatis dolorum enim amet. Fuga repellat impedit qui quia deleniti. Nulla aut nobis et rerum. Illo sapiente dolore aut tempore iusto. Reiciendis qui ea ratione rem excepturi soluta. Minus consequatur voluptatum nostrum veritatis. Est asperiores velit assumenda eum qui nisi eveniet. Earum illo doloremque non est reiciendis. Qui ipsa ad recusandae. Ducimus suscipit porro fugit laudantium sequi cumque eum.', 1, '["products\\/tractor.png","products\\/boat-1.png","products\\/shore-1.png","products\\/mountain.png","products\\/backpack.png","products\\/cityscape-3.png","products\\/park.png","products\\/camping-3.png","products\\/forest-3.png","products\\/camping-5.png","products\\/eiffel-tower.png"]', 0, '1996-12-11 22:25:32', '1993-09-10 09:01:22'),
(48, 'PD-47', 'Enim vero dolor.', 15, 66, 81, 51950, 'Minus voluptatem tempore blanditiis. Quis ducimus natus placeat fugiat id.<br><br>Sit velit est et id consequatur. Odio deleniti blanditiis dolorem eum dolores asperiores. Illum nisi quibusdam dolores unde. Voluptas debitis dolorum eveniet dolorum eos.<br><br>Vel rerum aut voluptatem sequi optio reiciendis. Ratione id at qui quo aut ducimus voluptate blanditiis. At voluptate numquam repudiandae accusamus blanditiis exercitationem voluptas. Modi ea eum quo ipsam.<br><br>Quis recusandae voluptatem delectus a harum aliquam accusamus. Ut omnis iure aliquam suscipit aut officia iste rerum. Qui autem ut dolorem eveniet. Rerum quo debitis soluta vel sunt.', '1.79', 'metal', 'red,white,white,red', 'XL,M,M', 'Eos libero accusamus sint cupiditate et saepe qui. Consectetur voluptates ad est eum laudantium voluptas repellat autem. Ipsum dolorum voluptatem dolores iure qui odit quo. Doloribus ut ipsa excepturi et quis voluptatem. Ad repellat veritatis qui repellat quam et expedita iste. Pariatur distinctio voluptatibus in expedita. Culpa et qui error aut suscipit. Pariatur atque tempore unde. Sed rerum voluptas amet eos doloremque voluptas tenetur. Beatae culpa et eligendi qui ullam est rerum. Enim aperiam modi alias est est accusamus aliquam cum.', 2, '["products\\/shore-1.png","products\\/cityscape-4.png","products\\/whale.png","products\\/sky.png","products\\/camping-3.png","products\\/park.png","products\\/camping-3.png","products\\/boat-2.png"]', 0, '2013-08-29 10:35:21', '1982-09-15 14:35:00'),
(49, 'PD-48', 'Voluptate qui.', 2, 30, 61, 56951, 'Adipisci dolorum sit dicta et. Sit et qui hic nulla id architecto. Aut aut eum doloremque sunt tempora aspernatur tempore. Fugit sint hic eveniet saepe rerum numquam sint autem.<br><br>Est beatae at autem. Et ut vero quam non enim omnis. Dolorem non sint earum possimus voluptatem ducimus eligendi error. Omnis neque quia dolorum voluptas. Odio sit vero minus omnis cumque impedit.<br><br>Exercitationem non sit natus. Autem amet dolores dolor consequuntur quis. Aut et similique ut aut quae. Assumenda hic eos et totam soluta.<br><br>Impedit molestiae quos rem placeat. Iusto aut laudantium vitae eum iure porro impedit. Qui sed laboriosam cumque tempora aliquid suscipit quam. Enim enim repudiandae soluta sit sint animi.', '1.52', 'metal', 'red,green', 'XL,XL,XL', 'Voluptas mollitia facilis quae recusandae. Cumque alias tempora enim ullam. Maxime distinctio autem consequatur. Porro soluta reprehenderit fuga nostrum et. Exercitationem alias eum omnis libero. Reprehenderit provident maiores excepturi inventore illo. Tempora corporis voluptatem quis. Ullam possimus dolorum porro in earum vel nam id. Et qui adipisci quasi eius itaque et optio porro. Rerum sed ad voluptatem reiciendis.', 2, '["products\\/sydney-opera-house.png","products\\/circus.png","products\\/digger.png","products\\/vacations.png","products\\/forest-2.png","products\\/road-2.png","products\\/forest.png","products\\/road-2.png","products\\/tractor.png"]', 0, '1988-08-04 06:41:10', '2004-10-31 16:33:00'),
(50, 'PD-49', 'Et velit molestiae.', 10, 196, 24, 28506, 'Ipsa quia nisi molestiae rerum est. A fugit et quam recusandae saepe id. Deserunt dolor facere iure earum. Odit ex ut voluptates eos.<br><br>Et et architecto laudantium et aliquid eius veritatis sunt. Deleniti provident doloribus et assumenda est enim. Vitae aspernatur voluptas natus qui.<br><br>Rerum mollitia velit consequuntur quod qui. Omnis excepturi quia consequatur ex sit quisquam est. Est perspiciatis iure doloribus deserunt dolore.<br><br>Neque harum fugit quasi aut ut veritatis. Molestias harum odio qui necessitatibus hic. Tempora nisi aut minima nam. Voluptas est similique eaque vitae.', '1.69', 'cotton', 'blue,blue,white', 'XXL,XL,XL', 'Incidunt occaecati rerum velit quasi omnis unde. Distinctio sint blanditiis facilis numquam. Quos sunt consequuntur sequi porro ad nesciunt iusto. Iste ullam quia culpa iure aut quis nihil dolor. Nam quam aliquam dicta provident. Officia voluptatibus quibusdam ea et occaecati blanditiis totam. Sed reiciendis vel nostrum aut animi. Laudantium et laborum rerum consequuntur. Est sit voluptatum perspiciatis magni totam consequatur fugit sit. Et consequuntur qui harum sunt.', 0, '["products\\/camping-7.png","products\\/beach-2.png","products\\/volcano.png","products\\/valley.png","products\\/river.png","products\\/parachute.png","products\\/route.png","products\\/circus.png","products\\/compass.png","products\\/sydney-opera-house.png","products\\/forest-3.png","products\\/new-york.png","products\\/school.png"]', 0, '1986-09-11 10:02:13', '2011-09-28 11:04:24'),
(51, 'PD-50', 'Assumenda dolorum consectetur quia.', 31, 267, 35, 76520, 'Ea commodi veritatis illum reprehenderit excepturi. Aperiam eos molestiae ea inventore tenetur dolor deleniti. Tenetur aliquid architecto consequatur qui. Magni laboriosam rerum ea est.<br><br>Quibusdam labore repellat quos perferendis ut tempore. Ipsa eum ut vitae ea animi sed aut. Ipsa et quas dolor quisquam.<br><br>Ut voluptas qui maiores omnis. Maxime aspernatur unde reprehenderit reprehenderit. Autem vitae eius sed sed beatae maiores cumque. Fuga adipisci repudiandae hic ut.<br><br>Consequuntur temporibus dolorem est rerum provident voluptatem dolor. Ea asperiores asperiores odit id. Ut expedita quos illo quo.', '0.21', 'nilon', 'white,red', 'XXL,M', 'Ut maiores rerum possimus sed necessitatibus. Quasi modi a in harum doloremque autem. Omnis ea et ipsam debitis odit autem voluptas. Veritatis sed eum consequatur quos. Non asperiores dicta tenetur alias eos dolor. Mollitia sed debitis harum ut. Autem consectetur laborum dolorum quaerat eos. Rerum voluptas possimus vel ea dicta. Et sit et natus adipisci. A ducimus quisquam ut ut aut.', 2, '["products\\/car.png","products\\/river-1.png","products\\/road-3.png","products\\/hunting.png"]', 0, '1975-03-20 07:55:03', '1993-03-31 01:36:03'),
(52, 'PD-51', 'Perferendis voluptas consequatur neque asperiores.', 34, 52, 38, 79429, 'Qui illum et ut earum voluptas quos. Facere saepe facere dolor corporis culpa corrupti.<br><br>Veniam enim asperiores voluptatem ad sapiente atque tempore maiores. Voluptatum omnis in corrupti nostrum sit dicta. Nam odio occaecati at minima assumenda reprehenderit.<br><br>Quo animi delectus dolor quidem sequi velit. Nisi aliquam laudantium nostrum et. Molestiae quod maiores velit minus nostrum exercitationem quae.<br><br>Fugit ut labore aspernatur omnis praesentium consequatur ut. Rem et est quos veritatis.', '0.35', 'silver', 'red,blue', 'M,M', 'Sit et eligendi aut aspernatur et et eveniet aut. Et unde aut odit quam sequi repellat consequatur nam. Harum doloribus inventore ducimus vel officia. Sint reprehenderit velit recusandae tempora. Est itaque quibusdam dicta dolores. Quae suscipit laboriosam dolores et maxime animi odio quae. In vitae quaerat inventore. Sed soluta hic fugiat sit vitae blanditiis. Sint et provident quia facere. Aut itaque sequi aliquam ex perspiciatis. Adipisci error a veritatis ea magni doloribus laudantium sed. Eum est eos voluptatem ut nostrum sed qui. Dolorem nihil molestiae suscipit quis deleniti libero. Unde velit eligendi ab eos sunt.', 2, '["products\\/road-1.png","products\\/camping-5.png","products\\/iceberg.png","products\\/whale.png","products\\/park-2.png","products\\/map.png","products\\/beach-4.png"]', 0, '1988-04-28 14:26:17', '1998-08-30 14:23:01'),
(53, 'PD-52', 'Nostrum deserunt repellat voluptates occaecati.', 32, 93, 9, 41894, 'Libero aliquid deserunt consequuntur sed quasi aliquam. Sit voluptas dolores labore quibusdam reprehenderit vitae. Ut molestiae dignissimos maxime beatae veritatis. Ut eveniet aspernatur et amet deserunt distinctio voluptatem.<br><br>Enim cum laborum est. Vel sit vero velit consequatur. Esse ut culpa rerum ut aspernatur. Ut rem et ea molestiae recusandae.<br><br>Repudiandae impedit dolorum facilis rerum. Modi voluptatem cum quis itaque. Rerum maiores dolorem sit qui.<br><br>Deserunt commodi ratione voluptatum veniam officiis nulla. Deleniti hic nobis doloremque in ut voluptas. Consequatur vel nam rerum veritatis laudantium eum optio repellat.', '0.16', 'silver', 'white,white,red,blue', 'M', 'Et qui qui et omnis sunt tempore similique ipsum. Eius qui rerum veniam pariatur aut. Ab illo et debitis totam. Exercitationem est dolores inventore. Asperiores cupiditate quis quae. Vitae est illo sunt mollitia voluptas ad. Ullam est accusantium voluptatem est at alias a ut. Qui sint consequuntur dolore eveniet ea odio cum. Totam vel alias alias pariatur maxime dicta. Reprehenderit qui temporibus facere dolorem dolorum inventore placeat.', 0, '["products\\/desert-3.png","products\\/tractor.png","products\\/beach-3.png","products\\/backpack.png","products\\/shore.png","products\\/shore.png","products\\/lagoon-1.png","products\\/farm-1.png"]', 0, '1977-02-19 13:12:24', '1993-11-23 10:56:28'),
(54, 'PD-53', 'Neque quisquam ratione.', 4, 175, 49, 21668, 'Fuga sed autem sint. Sed voluptates similique voluptas nobis vero corporis dolorum. Dolorum eveniet veritatis cumque aut. Quas amet cumque quo omnis eaque quia earum.<br><br>Fuga culpa totam architecto sunt esse. Voluptates occaecati aliquam animi quaerat asperiores quis placeat.<br><br>Qui animi ut sed aut. Suscipit occaecati esse tenetur quam ratione facere. Vitae velit dolores at vitae enim praesentium deleniti aliquam.<br><br>Quos sit sit non et nemo. Officia modi nihil odit voluptatem aliquam incidunt. Quasi qui eligendi inventore voluptatem ipsa sit. Quia aliquid non ut corrupti eius.', '0.33', 'wool', 'blue,white,blue', 'XXL,L', 'Non necessitatibus laboriosam iure et velit error. Velit atque illum dolor voluptatem a et dicta. Quae cupiditate corrupti impedit autem vero. Hic quis voluptatibus odio beatae. Eveniet quis nobis sit. Ducimus sint placeat sit in. Repellendus voluptatum vitae quod perferendis deserunt quia. Consequuntur sunt nam qui consequuntur soluta. Dolore eum nulla sed sapiente facere esse. Exercitationem ipsam ut repellendus labore. Eaque facilis autem et quos repudiandae. Consequatur ipsam tempora voluptatem eum quisquam illo impedit. Ipsum animi consequuntur maiores natus. Reprehenderit provident perspiciatis doloremque quos. Dicta eum quis officiis facere labore.', 0, '["products\\/camping-1.png","products\\/cycling.png","products\\/barbecue.png","products\\/new-york.png","products\\/caravan.png","products\\/mountain.png","products\\/great-wall-of-china.png","products\\/road-2.png"]', 0, '2000-07-21 03:10:36', '2015-05-26 20:51:25'),
(55, 'PD-54', 'Et eveniet et architecto.', 10, 177, 38, 75347, 'Distinctio reprehenderit molestiae voluptate quas et sunt. Iure in laudantium cumque ipsa. Voluptas exercitationem aspernatur molestiae id quia autem. Eligendi eos aliquid suscipit aut.<br><br>Quo reiciendis corporis culpa quidem eius. Ex quaerat voluptates sit unde ipsum molestiae itaque. At repellendus est reiciendis eum culpa quas. Consequuntur suscipit aspernatur maiores fugiat.<br><br>Non omnis quaerat fugit. Consequatur in adipisci non rerum non voluptatem molestiae asperiores. Aut sed qui natus inventore.<br><br>Possimus perspiciatis aut aliquam suscipit aliquam quam. Ut nisi ad fugiat saepe officia et sed. Nam deleniti consectetur qui ab. Laudantium voluptatem sit quod at exercitationem ipsam voluptatem. Odit occaecati consequatur est iste inventore aliquid labore occaecati.', '0.7', 'metal', 'blue,white,green,white,blue', 'L,XXL', 'Totam sed odio qui provident aut. Pariatur quam vero blanditiis error at. Omnis omnis fuga tenetur aliquid placeat tempore quisquam. Voluptatem repudiandae expedita aperiam doloremque labore facilis. Nostrum quia quos eos ab iste aperiam. Dolorem adipisci quo consequatur consectetur sunt nesciunt ab. Eligendi nesciunt nemo similique eos sit ipsam eos. Aut autem minima est facere similique. Eos cupiditate quos ex dignissimos porro qui voluptatem.', 1, '["products\\/cityscape.png","products\\/camping-3.png","products\\/beach-1.png","products\\/new-york.png","products\\/river.png","products\\/cityscape-4.png","products\\/hotel.png","products\\/digger.png","products\\/road.png","products\\/iceberg.png","products\\/forest-2.png","products\\/map.png","products\\/road-1.png","products\\/lake-1.png","products\\/camping-6.png","products\\/train.png"]', 0, '2000-04-22 15:17:18', '1985-02-26 23:08:48'),
(56, 'PD-55', 'Dolorum aperiam eos.', 3, 331, 76, 4363, 'Modi a saepe quo libero in natus. Quia voluptas sunt quia quis. Voluptatibus consectetur distinctio laborum numquam. Laborum labore architecto architecto consequatur temporibus voluptates et.<br><br>Mollitia magnam dicta eaque eos rem id. Neque amet maxime vel non aut dolor. Soluta sit distinctio aut. Est aut consequatur doloribus animi similique neque.<br><br>Aperiam rem officia odit sint dolores sequi sit id. At est est similique vel aliquid. Reiciendis temporibus corrupti voluptatem doloremque eligendi sed. Doloremque laudantium possimus velit animi at natus non.<br><br>Animi esse culpa accusamus porro alias. Et sit qui dolores hic dolores. Odit laborum assumenda odit dignissimos autem omnis ipsum.', '0.55', 'cotton', 'blue', 'L,L', 'Rerum dolores ut illo dolores autem ratione doloremque. Magni commodi aperiam magnam beatae reiciendis excepturi. Voluptates consequuntur voluptatem nisi. Id culpa et perspiciatis voluptatibus debitis velit sit. Unde voluptatum qui vitae dolorum aut. Et earum facere a cupiditate nam quam. Voluptatem deserunt error doloremque laudantium laudantium nobis. Et tenetur quidem est ad recusandae magnam. Sit voluptate sit laboriosam sunt nulla consequatur. Reiciendis non dolorem quam iusto mollitia quo cupiditate. Maxime ut tempore omnis rem. Voluptas eveniet suscipit vel saepe in eos pariatur. Consectetur dolore quo sit similique temporibus.', 0, '["products\\/forest-1.png","products\\/park-2.png","products\\/eiffel-tower.png","products\\/hills.png","products\\/river-1.png","products\\/planet-earth.png","products\\/park.png","products\\/backpack.png","products\\/iceberg.png","products\\/iceberg.png","products\\/coliseum.png","products\\/camping-7.png","products\\/cycling.png","products\\/camping-2.png","products\\/cityscape-3.png"]', 0, '1990-01-12 11:12:08', '1977-01-25 22:19:50'),
(57, 'PD-56', 'Ad consequuntur pariatur.', 11, 179, 53, 63990, 'Minima dicta quis rerum ut. Voluptas dicta maxime aut amet dolore. Totam minima id voluptates voluptatem id provident eos.<br><br>Iste maxime dolor tempore et corrupti debitis. Sed ut et sequi minus. Facere est quam enim magnam autem.<br><br>Iusto qui laudantium aut. Magnam est et magni ea. Ut reprehenderit laborum esse. Architecto aut velit iste deleniti voluptas vel aperiam.<br><br>Repellat dolores temporibus modi corporis et voluptas. Natus sed ut magni sint. Similique nihil ex similique labore. Labore qui est dolores tempore cum minus.', '1.16', 'wood', 'white,white,blue', 'XXL', 'Odio corrupti neque et est atque necessitatibus modi et. Qui provident quis odit sunt expedita ad. Inventore quisquam voluptas ad quo libero. Aut hic vel laudantium et vel aut libero sit. Omnis recusandae recusandae optio voluptates voluptas beatae. Ullam quia totam nam culpa aut. Aut quos iste quia odio voluptas vel excepturi. Quod ea et cumque numquam est iure. Consequatur beatae praesentium placeat inventore. Id sit ea rerum consequuntur non ad magnam eligendi. Qui harum nam maxime aut accusantium repellat iusto explicabo. Odit et et et sit. Inventore et eveniet aspernatur nostrum illum inventore. Dolores non est et ullam quo quia expedita eos. Incidunt eos sit mollitia tempore. Impedit dolore sit consequuntur molestias illo quos. Sit quis aperiam aut dignissimos quia.', 3, '["products\\/beach-3.png","products\\/walkie-talkie.png","products\\/hospital.png","products\\/sea.png","products\\/lagoon-1.png","products\\/river.png","products\\/beach.png","products\\/hills.png","products\\/hotel.png","products\\/walkie-talkie.png","products\\/photo-camera.png","products\\/eiffel-tower.png"]', 0, '1980-08-31 03:48:03', '2010-09-19 08:50:52'),
(58, 'PD-57', 'Eveniet optio quod eaque.', 24, 263, 47, 985, 'Quia dicta cumque qui placeat eum ratione in. Qui officiis dolore magnam aut dolorem et distinctio. Qui praesentium quam deserunt reiciendis quidem nobis. Occaecati vel omnis consequatur aliquam et eaque.<br><br>Ad sunt earum voluptatibus aliquam ex molestias. Laudantium excepturi voluptas vero qui omnis. Et sed tenetur dicta iure hic. Est necessitatibus in cumque voluptatem sit.<br><br>Non vitae sint beatae provident similique blanditiis qui. Est quidem tempore rerum. Et sit voluptas eos in quae quo.<br><br>Sapiente minima aut quisquam ducimus ipsum quos inventore facere. Aut eligendi esse ipsam ut ut qui accusantium. Vitae eligendi et dignissimos officia.', '0.84', 'cotton', 'green,white,green', 'XL,XL', 'Distinctio et velit nam doloremque maxime cupiditate quidem aperiam. Quas qui delectus ipsam nisi. Voluptatum rerum rerum cupiditate facere. Cupiditate officiis quo itaque aut corporis ducimus. Aperiam non eius est soluta. Nisi voluptatem cumque modi magnam omnis voluptates magni. Velit neque quo consequatur consequatur id magni repudiandae quam. Sed eligendi et a. Eligendi voluptatibus repellat voluptatem nisi. Sunt nesciunt et ad et voluptate. Omnis non et est consequatur est rerum aliquam voluptatem. Ea pariatur adipisci dolores asperiores. Dolor libero non eaque nobis tempora.', 2, '["products\\/coliseum.png","products\\/hotel.png","products\\/cityscape-4.png","products\\/camping-4.png","products\\/river-2.png","products\\/school.png","products\\/hunting.png","products\\/beach-4.png","products\\/desert-3.png","products\\/planet-earth.png","products\\/farm.png","products\\/snowman.png","products\\/barbecue.png","products\\/cityscape-2.png","products\\/reindeer.png"]', 0, '1988-10-14 19:20:52', '2003-02-07 18:50:18'),
(59, 'PD-58', 'Iste blanditiis qui unde.', 30, 198, 84, 77204, 'Veritatis accusamus ea mollitia deserunt non modi voluptate. Maxime dolor et voluptatem sint hic porro. Ut pariatur fuga corporis libero voluptas. Enim doloremque dolor et. Ut quos vel dolores in repudiandae modi est.<br><br>Quae eos vel qui. Aspernatur inventore esse dolor adipisci dolorum est. Ullam optio veritatis aliquam commodi ut non eum. Dolorem architecto harum aut cum enim.<br><br>Dolor expedita quia facilis in odit esse. Nesciunt voluptatem similique aut ducimus consectetur saepe et. Est et magnam non vel qui. Ipsam nisi rerum minima vel asperiores.<br><br>Quasi voluptas quo voluptatem est rem nobis perspiciatis. Neque est quisquam sunt quod. At temporibus quo aut tempore odio. Eos non et saepe vero sequi omnis.', '0.69', 'wood', 'black', 'L', 'Itaque esse nulla iure nisi. Eos cupiditate et ipsa dolore adipisci corporis ea. Atque occaecati mollitia sequi eligendi molestiae. Minima tenetur velit commodi qui veniam et. Saepe omnis vitae qui consectetur facilis voluptas. Aut cum eligendi ducimus ut repudiandae non neque. Pariatur aperiam explicabo omnis iste quia. Vel sit eos nisi quae consequatur quis.', 0, '["products\\/farm-1.png","products\\/tractor.png","products\\/barbecue.png","products\\/lake-1.png","products\\/river-3.png","products\\/camping-7.png","products\\/forest-3.png","products\\/field.png","products\\/parachute.png","products\\/parachute.png","products\\/beach-2.png","products\\/backpack.png","products\\/mountains.png","products\\/hunting.png"]', 0, '1998-08-19 16:18:03', '1986-03-16 23:28:47'),
(60, 'PD-59', 'Provident velit culpa est.', 23, 435, 63, 90684, 'Unde at mollitia natus in veniam nulla. Tempore alias sit quia error qui quidem. Vel est eos amet magni aut. Molestiae eum sapiente a dolorum excepturi culpa ipsa.<br><br>Velit reiciendis veniam laudantium quia tempora non voluptas. Dolorum animi vel odio ad molestiae quia cumque. Beatae quos excepturi placeat eum quia. Corporis mollitia numquam et consectetur.<br><br>Et voluptatem asperiores repellat ut ex cumque. Cumque qui quos distinctio et quas voluptatem. Debitis repellat expedita consectetur debitis. Quidem dolorum accusantium facilis quae ipsam id soluta.<br><br>Sunt nostrum perspiciatis in aliquam nam. Assumenda est sint aut ut sint. Dignissimos non libero facilis velit. Modi excepturi quae error.', '0.89', 'gold', 'red,blue,red,blue', 'XXL,L', 'Cupiditate quam repellendus eligendi. Possimus alias at provident. Nesciunt sit nemo ad sunt ipsum cupiditate ea. Natus perferendis quidem ut numquam mollitia repellendus. Doloremque dicta natus reiciendis recusandae fuga quae. Et occaecati voluptatum et unde amet distinctio. Exercitationem officia quo et reiciendis perspiciatis vero ut. Hic amet impedit praesentium amet quis. Molestiae debitis amet reiciendis modi amet inventore. Velit ut saepe at. Mollitia molestias impedit magnam. Assumenda et non optio sit velit. Laboriosam dolores facilis doloribus accusamus et.', 0, '["products\\/road-3.png","products\\/mountains.png","products\\/camping-2.png","products\\/forest-2.png","products\\/train.png","products\\/desert-1.png","products\\/boat-2.png","products\\/cityscape.png","products\\/church.png","products\\/hills.png","products\\/parachute.png","products\\/cityscape-4.png","products\\/lagoon.png","products\\/boat.png","products\\/cycling.png","products\\/desert-1.png"]', 0, '2015-02-06 08:04:33', '1987-10-18 04:51:41');
INSERT INTO `products` (`id`, `code`, `name`, `provinceId`, `cityId`, `stock`, `price`, `description`, `weight`, `material`, `color`, `size`, `note`, `label`, `images`, `deleted`, `created_at`, `updated_at`) VALUES
(61, 'PD-60', 'Velit neque molestias.', 33, 242, 16, 45486, 'Totam debitis ut cum. Cumque quisquam consequuntur omnis suscipit aliquid voluptate. Cum sunt tempora nulla facilis.<br><br>Et error nobis ratione unde repellendus eaque deserunt. Quibusdam at et provident occaecati corrupti molestiae. Veniam aut vel id aperiam.<br><br>Perspiciatis beatae nesciunt non voluptatem accusantium molestiae. Cum voluptates exercitationem pariatur voluptas. Est et sequi molestias laboriosam voluptatem. Qui quam et optio. Quam omnis illum error qui.<br><br>Nostrum nihil ullam ut harum. Neque nemo nostrum dicta sapiente cum et. Amet sit ex nostrum totam reiciendis beatae ducimus.', '1.12', 'wool', 'black', 'L,M', 'Fuga facilis porro qui deserunt totam ab. Delectus corrupti similique qui. Accusamus necessitatibus aliquam nostrum repellat ut velit. Esse velit nemo ab vitae. Occaecati nesciunt perferendis velit commodi saepe vel. Sunt velit consequuntur est. Omnis fugit sed assumenda occaecati tempora aut molestiae repellendus. Vero deleniti quis in praesentium nisi quidem. Magnam distinctio laboriosam placeat eaque id perferendis quas. Totam et suscipit quia quo consequuntur unde deserunt veniam. Eaque aut impedit dolorum necessitatibus explicabo quae. Sit laboriosam eos laboriosam dolores maxime ipsam. Non aut quasi quasi vel dolorem. Accusantium eaque ut molestiae ut mollitia minima provident. Eveniet enim dolor eligendi similique ut aut. Voluptatibus quis fugiat aut eius et quia. Sunt sit animi ex ut blanditiis.', 1, '["products\\/cityscape-4.png","products\\/river-3.png","products\\/hotel.png","products\\/forest.png","products\\/cityscape-2.png","products\\/boat.png","products\\/lake.png","products\\/bulldozer.png","products\\/river-1.png","products\\/sea-bottom.png","products\\/school.png","products\\/park-2.png","products\\/palace.png","products\\/camping.png","products\\/whale.png","products\\/planet-earth.png","products\\/map.png","products\\/lagoon.png","products\\/beach-4.png","products\\/cliff.png","products\\/vacations.png","products\\/eiffel-tower.png"]', 0, '2006-07-10 19:15:11', '2001-07-24 12:17:38'),
(62, 'PD-61', 'Maxime voluptatem dolorem iusto.', 10, 163, 28, 34809, 'Qui et non dolores nostrum. Unde cum iusto consequatur est totam eos. Dolor sapiente cumque aut exercitationem nisi non.<br><br>Qui maxime totam est vel non esse. Et officiis quia dolores incidunt quis quo consequatur exercitationem. Autem eius voluptas consequatur aut rerum nam.<br><br>Accusantium totam eaque deleniti eos. Dolores et omnis qui exercitationem dignissimos adipisci ex.<br><br>Enim exercitationem in quam tenetur voluptatum provident minus. Quia et ea nostrum cupiditate natus. Sunt provident earum fuga assumenda omnis. Pariatur repudiandae ut dolores quam non illum.', '0.56', 'cloth', 'yellow', 'M,L,L', 'Sed voluptas quis rerum amet et. Sunt ex eum et ex assumenda minima aut. Quaerat fuga necessitatibus asperiores dolore officiis perspiciatis id. Aspernatur sint qui rerum nisi. Et nemo vitae blanditiis sit. Qui a nulla a nisi. Doloribus quidem voluptas totam praesentium quaerat enim. Quasi magni minima cupiditate et doloremque impedit voluptatem.', 2, '["products\\/farm.png","products\\/beach-1.png","products\\/tractor.png","products\\/cityscape-1.png","products\\/route.png","products\\/boat-1.png","products\\/train.png","products\\/mountain.png","products\\/hunting.png","products\\/boat.png","products\\/cliff.png","products\\/hills.png","products\\/dump-truck.png"]', 0, '1972-02-13 03:35:36', '2005-06-28 14:29:10'),
(63, 'PD-62', 'Assumenda odio dolores.', 11, 369, 83, 33314, 'Beatae debitis quia soluta deleniti aspernatur qui. Ut rem ullam qui velit. Ea a quasi nulla. Accusantium dicta blanditiis quae dolores. Omnis ut voluptas alias accusantium.<br><br>Quam aut eos adipisci inventore nulla consequatur. Magni vel non qui quis culpa quia. Eius esse omnis eum fugiat unde quis. Explicabo illo aut facere saepe assumenda labore cum.<br><br>Dolorum sunt minima sapiente dolorem nesciunt dolorem ducimus. Explicabo soluta quis sint sint sed quam quaerat. Quod adipisci et facilis quis vitae. Incidunt enim incidunt ad qui et maxime dicta ipsa.<br><br>Consequatur ut tempore facilis quo dolorem sequi est. Error quis dolorum laborum et. Esse ut ad et ut. A cumque quas nisi sint molestiae impedit dolore.', '0.71', 'nilon', 'red,red', 'XXL,XXL,L', 'Deserunt voluptatem possimus deleniti rem. Perferendis sed perferendis aspernatur eveniet aut voluptates optio quae. Rem voluptates autem aperiam nesciunt quos praesentium nihil reprehenderit. Non recusandae officia fuga suscipit necessitatibus impedit. Magnam dolorum quia omnis aspernatur maxime assumenda. Rerum est est animi quis aut. Sed quidem qui dignissimos quia qui. Fugit temporibus quo quia eius. Rerum voluptatem voluptas iste sed aut. Unde nesciunt dolorem sequi sint ab. Consequatur perspiciatis quisquam eius. Et sit voluptatum iure atque distinctio. Vel in ex sed omnis.', 1, '["products\\/route.png","products\\/cityscape.png","products\\/map.png","products\\/cityscape-3.png"]', 0, '1970-03-28 20:26:19', '1985-12-05 05:34:43'),
(64, 'PD-63', 'Sunt doloremque ea voluptatum.', 10, 433, 89, 24333, 'Et aspernatur consequatur laborum error dolorem sed cupiditate. Nihil quia fugit dolor molestiae repellat. Soluta soluta facilis consequuntur excepturi explicabo facere. Corporis aut suscipit tempore. Totam velit et ut perferendis.<br><br>Nesciunt expedita laboriosam suscipit exercitationem adipisci possimus necessitatibus explicabo. Impedit tempore ea assumenda adipisci esse provident. Vel dignissimos natus eveniet consequatur. Delectus facere perferendis et occaecati.<br><br>Eum reiciendis dolores accusantium qui ea suscipit. Iste dolor dolore animi hic. Sed nobis nisi maxime corporis odit. Deleniti nisi dolorum veritatis accusantium quasi ut distinctio.<br><br>Sequi alias magnam aut omnis et odit eum. Voluptas similique perspiciatis asperiores et. In vitae voluptatem deserunt et et mollitia qui.', '0.84', 'silver', 'red,blue', 'M', 'Ad debitis nihil magni sed expedita est molestias et. Ut eos et recusandae molestiae. Adipisci corrupti minima omnis rerum porro maiores nostrum. Optio maiores eos blanditiis molestiae velit. Aut error reiciendis corporis fugiat eius quia natus. Qui illo eaque suscipit in. Quibusdam ea et sed quae voluptatem. Eos occaecati qui saepe doloremque quasi dicta vel. Ut consequatur cupiditate consequatur aliquid voluptas. Ad quibusdam qui fugiat ratione consequatur qui. Consectetur fuga repudiandae consequatur officiis dicta sit expedita.', 1, '["products\\/barbecue.png","products\\/cycling.png","products\\/river-1.png","products\\/beach-3.png","products\\/hunting.png"]', 0, '2018-07-05 19:14:02', '2015-04-05 15:49:34'),
(65, 'PD-64', 'Recusandae illum omnis.', 33, 324, 100, 41031, 'A aut voluptatem quo ad unde omnis. Officiis aliquid hic maxime eum enim culpa. Et blanditiis voluptas dolores. Non cumque cum dolorem corrupti non.<br><br>Omnis qui cum ea in asperiores quae. Sunt voluptas qui optio facilis. Corporis voluptatem doloribus aliquam. Cupiditate voluptatem quibusdam aut odit qui impedit.<br><br>Temporibus cumque mollitia et sapiente dolorum. Id repellat dolorem eius.<br><br>Reiciendis et dolor quia numquam enim. Harum nostrum debitis harum et voluptas doloremque enim. Ea corporis cumque pariatur.', '0.54', 'cloth', 'white,blue,red,white', 'XL', 'Beatae totam dolor consequuntur quo voluptatem suscipit. Ex ut iusto occaecati voluptates eum molestiae. Aliquid ut fugiat et et assumenda qui ut. Occaecati saepe quos rerum et dicta. Veniam aut eum non aut veniam velit ipsa ullam. Et sint sapiente expedita alias odit voluptates. Accusantium qui qui minima unde sit facere. Cupiditate perferendis dolor itaque ipsum qui mollitia asperiores culpa.', 2, '["products\\/new-york.png","products\\/boat.png","products\\/camping-6.png","products\\/boat-2.png","products\\/taj-mahal.png","products\\/forest.png","products\\/shore.png","products\\/beach.png","products\\/autumn.png","products\\/sea-bottom.png","products\\/mountains.png","products\\/river-1.png","products\\/park-1.png","products\\/mountains.png"]', 0, '2011-01-13 18:53:21', '1994-02-22 19:26:57'),
(66, 'PD-65', 'Neque consequatur maiores.', 9, 430, 38, 22937, 'Eos ut iste numquam ea ex error. Doloribus vitae accusamus illum aut reprehenderit odio. Illo dolore unde eum quis.<br><br>Saepe ut aut doloremque illo qui repellat. Rerum mollitia repellat explicabo dicta accusantium et repellat. Sit veniam est quam vel libero provident veritatis qui. Perspiciatis dolor vel voluptatum a est maiores harum.<br><br>Omnis aspernatur odio minus deleniti. Aut sed qui sit unde consequatur et consequatur. Veritatis a ut iure sit quo aut sit. Laudantium dolores ut omnis veritatis nisi sit.<br><br>Odit iste reiciendis est. Modi dolore rerum aperiam quia. Est consequuntur iusto tenetur voluptatem ut aperiam iste.', '0.55', 'silver', 'blue,red,black', 'M,M,M', 'Fugit iure et tenetur qui doloremque ratione dolor. Cumque sed velit sint aliquam. Dolorem doloremque nisi rerum aut assumenda. Expedita alias quaerat vel. Qui sed porro alias harum corporis sapiente voluptates. Molestiae provident facilis voluptas ipsa. Officia nemo esse debitis. Porro illum odio natus doloremque velit cumque.', 0, '["products\\/camping-2.png","products\\/compass.png","products\\/parachute.png","products\\/coliseum.png","products\\/hunting.png","products\\/circus.png","products\\/road-2.png","products\\/desert-3.png","products\\/camping-4.png","products\\/volcano.png"]', 0, '1998-06-16 19:05:03', '1997-10-11 21:03:44'),
(67, 'PD-66', 'Veniam velit quibusdam consequatur.', 32, 422, 71, 48882, 'In libero sed nisi quaerat. Adipisci ipsum qui harum dignissimos dolor ipsa nam. Illo enim animi pariatur distinctio cumque iure. Dolorem magnam dolorem est quod.<br><br>Velit commodi vitae totam at dolorem qui. Quis et et aliquam dolor est qui. Rerum et vero voluptatem cupiditate sit nobis. Culpa ipsum ad sit inventore.<br><br>Non minus similique cum earum. Est quia pariatur velit ipsa dolores est. Est non qui nobis cumque cupiditate. Excepturi voluptas placeat rem voluptatem nam recusandae nihil.<br><br>Sint est suscipit qui accusantium perspiciatis quisquam. Impedit minus rerum modi quam ut. Est nihil eaque ut. Harum recusandae et beatae doloremque dolorem.', '1.73', 'cotton', 'yellow,green,red,blue', 'XXL', 'Doloremque rerum aperiam autem neque cupiditate quaerat molestias. Quo aperiam commodi ut dolor eius. Rerum nesciunt magni quibusdam tempore eveniet nam. Explicabo voluptatem et porro quis. Ea dolorem sint aut possimus qui deleniti culpa. Est veniam doloribus sunt ut. Ab quasi dolorem dolores occaecati sunt. Fugiat libero eveniet pariatur a occaecati qui.', 3, '["products\\/beach-4.png","products\\/school.png","products\\/school.png","products\\/hunting.png","products\\/camping.png","products\\/river-2.png","products\\/camping-1.png","products\\/new-york.png","products\\/iceberg.png"]', 0, '1975-12-27 00:55:29', '2006-05-17 23:49:02'),
(68, 'PD-67', 'Ut aliquid aut.', 23, 479, 95, 70367, 'Dolore corrupti eveniet placeat natus minima eos. Eaque culpa aut temporibus et impedit porro. Et totam suscipit laboriosam dolorum.<br><br>Doloremque beatae corrupti ipsum. Ratione ullam ratione consequatur voluptatem. Nemo odit quasi impedit soluta accusamus perferendis adipisci.<br><br>Iure corporis consectetur possimus dolorem placeat explicabo sed veritatis. Ipsam excepturi repellendus aliquid aut adipisci dolorem fugiat in. Minima repudiandae aut numquam voluptatum repellat voluptate. Aut et et aut. Et ab non fuga perspiciatis.<br><br>Voluptatem quae veritatis saepe dolor. At ratione accusamus est. Nostrum molestiae voluptatem ipsa voluptatem. Quibusdam quibusdam omnis cumque consequatur voluptates maxime voluptas.', '1.3', 'cotton', 'white,blue,red', 'M,XL', 'Aut dolorum aut facilis rerum similique at. Quia hic quis commodi veniam doloremque. Aut veritatis voluptate temporibus. Pariatur dolor minima nobis voluptas autem qui aut. Rem incidunt explicabo fugiat beatae porro maiores. Saepe consequatur et ipsa eos et nesciunt magni quae. Blanditiis ipsa voluptatem blanditiis ipsa. Maiores aut deserunt necessitatibus quis incidunt quasi. Laborum rerum dolores ducimus rerum aut iure velit. Illum ut exercitationem quam sint. Sed sit quia eaque esse.', 1, '["products\\/park.png","products\\/sea-1.png"]', 0, '1998-08-30 12:25:59', '1992-03-04 21:05:20'),
(69, 'PD-68', 'Labore quasi non perspiciatis.', 8, 50, 8, 9193, 'Sit id veritatis nobis ea sunt et at. In provident et nulla unde. Rerum aut corrupti repellat vel ab adipisci sunt autem. Tempora pariatur assumenda itaque.<br><br>Qui officia voluptates minima vel. Sed rerum libero et rerum aut voluptatem. Minima totam corrupti voluptatibus placeat. Quis aut architecto voluptatem itaque aut qui autem.<br><br>Mollitia aut illo aut non officiis. Accusantium non voluptate sed. Rerum natus earum commodi id nesciunt.<br><br>Rerum consequatur aperiam doloribus fugit ut blanditiis dignissimos. Voluptatem aliquam doloribus exercitationem unde. Quis odit veniam repellendus magni.', '1.93', 'wood', 'blue,red', 'L', 'Fugiat sit veniam delectus sunt consequatur optio. Minima quaerat et animi repellat quia voluptatem laboriosam. Quia debitis non quia quidem natus. Omnis et consequatur autem repudiandae debitis. Iusto autem non odit architecto. Quia saepe voluptatibus cumque delectus aut culpa. Eligendi odit eaque ea sed aliquid quaerat quisquam. Magnam voluptatum consequatur quis enim quidem. Ipsum deleniti et voluptates. Eum eligendi incidunt facilis sit minima. Deserunt vel facere odio vitae. Mollitia explicabo nobis perferendis placeat earum illum. Velit dolores ipsam ut optio. Neque asperiores explicabo quia dolor totam impedit ut.', 1, '["products\\/camping-4.png","products\\/field.png","products\\/snowman.png","products\\/cliff.png","products\\/camping-6.png","products\\/forest.png","products\\/hunting.png","products\\/forest.png","products\\/forest-3.png","products\\/default.png","products\\/vacations.png","products\\/school.png","products\\/field.png","products\\/lagoon.png","products\\/mountains.png","products\\/shower.png","products\\/river-3.png","products\\/hunting.png","products\\/dump-truck.png","products\\/vacations.png"]', 0, '1988-04-04 05:37:20', '2018-06-15 14:41:39'),
(70, 'PD-69', 'Delectus rerum eos corrupti molestias.', 34, 323, 28, 7810, 'Fuga ratione ut accusantium dolor expedita. Sed qui consequatur dolore nam explicabo. Ex nisi sint magnam sunt error.<br><br>Sit beatae est magni. Unde soluta omnis quaerat velit expedita. Molestias enim corrupti omnis enim nulla sapiente quod voluptate. Quasi dolor officiis nihil quia modi facilis.<br><br>Explicabo sed in officiis et fuga sed. Natus quae inventore nesciunt assumenda harum doloremque fuga. Eaque numquam sed consequuntur debitis maxime. Accusamus aut rerum aut laudantium voluptatum modi.<br><br>Omnis alias ut ullam sed sint expedita temporibus. Et omnis vero iusto aspernatur esse non. Quasi maiores fugiat qui voluptas velit fugit.', '1.64', 'silver', 'black,blue,white,white', 'XXL', 'Velit est commodi nihil quas facere accusantium. Totam possimus nesciunt officia excepturi eum labore qui occaecati. Consequatur quidem voluptatem vero omnis dicta. Non non qui fuga. Fuga et provident est. Et aut quidem velit ut quam nostrum quasi voluptatem. Accusamus perspiciatis porro at dolorum quia beatae praesentium distinctio. Sit assumenda minima delectus accusantium reprehenderit quia. Temporibus inventore autem nihil sed necessitatibus nostrum atque. Sunt ea aliquid est. Ducimus dignissimos eligendi modi voluptate laborum. Natus recusandae quos vero dolorum dolor maiores deleniti.', 2, '["products\\/park-1.png","products\\/desert-1.png","products\\/camping-4.png","products\\/train.png"]', 0, '2004-11-01 13:11:05', '1999-08-10 20:35:53'),
(71, 'PD-70', 'Vel repudiandae cupiditate.', 18, 223, 77, 81442, 'Doloremque possimus modi aut distinctio numquam voluptas non. Perspiciatis vitae vitae non dolore tempora sed excepturi ut. Ut eum tempore architecto occaecati ut numquam consequatur.<br><br>Autem voluptatum vitae fugiat inventore delectus quis at maiores. Voluptate nihil expedita accusamus aut incidunt. Error architecto dolor exercitationem.<br><br>Voluptatibus enim est quia quae eum cupiditate. Aut et doloribus eveniet. Similique ducimus magni ut ipsum autem architecto. Sed labore est id minima.<br><br>Animi id inventore veritatis expedita. Dicta dolorum dicta sit sint ut ab cumque. Alias rerum quaerat vel eius quaerat et.', '1.15', 'cotton', 'blue,white,blue,blue', 'XXL', 'Ut dolorem accusamus aut non. Dolorum dolor molestiae qui. Est quas vel nihil et sed quia. Fugit possimus labore dolorum tempora sed et omnis. Delectus corporis a quasi reprehenderit iste porro qui. Eos laudantium placeat quis harum sit similique. Sequi optio ea corporis. Nobis accusantium dolorem nihil earum odit. Suscipit id in provident incidunt quo repellendus est. Delectus dignissimos est ut quae non laudantium. Possimus sed repellendus non quo. Voluptas ea debitis minus nemo. Ducimus minima tempore et nemo. Corporis et commodi accusamus alias doloremque nobis eum. Voluptate quos laborum facilis similique. Eos consequatur nesciunt et blanditiis. Labore tenetur neque eos error sint quis voluptas non.', 0, '["products\\/road-3.png","products\\/circus.png","products\\/hunting.png","products\\/photo-camera.png","products\\/shower.png","products\\/road.png","products\\/great-wall-of-china.png","products\\/desert-3.png"]', 0, '2018-09-28 05:16:25', '2012-12-27 13:10:37'),
(72, 'PD-71', 'Iure voluptatem aut.', 10, 498, 13, 84603, 'Labore vel quaerat laborum eum sequi officia architecto. Voluptas impedit in aliquid veritatis. Eos et repellendus placeat. Odio temporibus aut perferendis harum maiores laudantium ab.<br><br>Quisquam facilis sit in eveniet possimus. Et in itaque fuga itaque. Voluptas dignissimos architecto non recusandae et sit.<br><br>Est qui placeat eveniet eos. Ab reprehenderit recusandae unde nam. Qui et est perferendis sunt sequi laboriosam. Exercitationem at ut possimus.<br><br>Est non modi fugit ipsum. Voluptatem est quia porro placeat commodi expedita repellat. Quo a ut occaecati dolore.', '0.3', 'gold', 'yellow,green,white', 'L,XL', 'Doloremque sit nostrum est in. Expedita quos id eligendi ea voluptatem dolorem. Excepturi cum laudantium natus maxime. Vitae architecto nemo quaerat placeat odio. Dignissimos aperiam et similique ea voluptatum dolores enim qui. Ut totam porro voluptatem ut sapiente consequatur. Temporibus aut ad odit voluptatem. Delectus eum ea eveniet. At dolores aut voluptas consectetur reprehenderit. Aspernatur ut quae ipsum velit neque voluptate quisquam voluptas. Corporis facere aliquid ullam. Veritatis ducimus deleniti velit corporis veritatis natus. Asperiores dolores voluptates laboriosam quos laboriosam voluptatem. Ut cupiditate neque ea omnis dolor sunt illum. Tempore consequatur vitae voluptas et. Aut repellat repudiandae quia non adipisci dignissimos dignissimos eius. Fuga repudiandae est accusantium quia.', 3, '["products\\/school.png","products\\/taj-mahal.png","products\\/park.png","products\\/mountain.png","products\\/park-2.png","products\\/deer.png","products\\/road.png","products\\/car.png"]', 0, '1972-08-28 14:49:18', '2014-02-25 18:11:04'),
(73, 'PD-72', 'Reiciendis vitae.', 13, 143, 41, 69104, 'Consequatur quia et eum consequatur ut voluptas. Vel nesciunt fuga placeat ut. Porro dolorem voluptatem quisquam quis molestias repellat laborum exercitationem.<br><br>Aut quisquam et sed magnam animi. Enim vel ratione doloribus illo. Magnam perferendis rerum beatae consectetur. Unde accusamus ut sequi recusandae officiis nihil officia.<br><br>Blanditiis porro similique molestiae. Et ut unde voluptas reiciendis et laudantium. Corrupti modi qui quia saepe. Pariatur beatae dolor voluptas quia dolores.<br><br>Itaque cupiditate sequi iste corrupti id laboriosam. Molestiae fuga quaerat voluptatibus asperiores sint qui perferendis. Non dolorem similique at provident eos.', '0.95', 'nilon', 'white', 'L,M,XXL', 'Ullam est ipsa libero voluptate consequatur. Cupiditate id ipsa cumque atque. Dolorem quaerat ut qui dolorem incidunt blanditiis. Ducimus culpa laudantium expedita sed voluptatum id. Aut dolore facilis repellendus ducimus vel et. Sit ut mollitia cum qui. Delectus fugiat delectus aut voluptas. Vel voluptatem sunt sit veniam sed ut et. Nihil laborum laboriosam odio aut fugit quo. Sint molestiae est quod non vero aut. Voluptatum omnis laudantium eaque inventore. Cumque dicta voluptate ratione quidem et. Ab est inventore mollitia est sapiente qui. Consequatur consectetur quod architecto ullam dolorem quisquam. Aut iure possimus inventore. Quo id distinctio atque dolores dolorem nulla. Sit reiciendis culpa facere eum pariatur expedita.', 3, '["products\\/reindeer.png","products\\/autumn.png","products\\/tornado.png","products\\/lagoon.png","products\\/road-3.png","products\\/cityscape-3.png","products\\/hotel.png","products\\/park-2.png"]', 0, '2002-06-28 22:32:24', '1971-02-16 22:02:07'),
(74, 'PD-73', 'Eligendi dolores aut tempora.', 34, 470, 37, 48454, 'Esse incidunt dolor soluta accusantium molestiae. Aut et ut distinctio adipisci doloremque beatae. Molestiae facere a dolores ratione fugit minima quo. Ut qui similique voluptas tempora sit enim reprehenderit nihil. Aut dignissimos occaecati veniam ut autem aliquid.<br><br>Id perferendis beatae cupiditate est illum. Cum qui vel et vel blanditiis aut qui. Architecto error voluptas harum sit laboriosam fugiat.<br><br>Vel doloremque enim earum impedit vel veniam. Earum delectus iste quae harum dolore tenetur et. Sed adipisci ipsam est labore qui perspiciatis quasi qui. Consectetur corporis est sapiente laudantium.<br><br>Adipisci et officia et voluptate iste et est dolorem. Necessitatibus sint itaque iure modi facere omnis. Ducimus cum labore harum voluptatem.', '0.93', 'metal', 'yellow,white,black', 'XXL,L,XXL', 'Minus dolor ex doloribus sunt. Consequatur ipsa veniam vel odio at. Accusamus praesentium rerum rem ut quibusdam est. Ex occaecati non aut. Ducimus dolorem voluptas velit facere. Et provident ut assumenda minima suscipit. Voluptas dignissimos sequi temporibus ipsa omnis qui quis qui. Est eum odio aperiam pariatur et facere est. Dolorem ea commodi officiis cum illum voluptate quia. Enim tempore illo aut excepturi. Dolores dolores inventore exercitationem ullam sed ipsam. Fuga eligendi quas numquam voluptatem. Et voluptatem nesciunt dolorem ut perspiciatis. Quidem ut impedit veniam quia ad minima ex. Consequatur odit voluptatibus unde quo ipsam autem. Qui est et labore pariatur voluptatem vel dignissimos.', 3, '["products\\/caravan.png","products\\/valley.png","products\\/camping-3.png","products\\/park-1.png","products\\/cityscape-2.png","products\\/boat-2.png","products\\/cityscape.png","products\\/park-2.png","products\\/beach-3.png","products\\/camping-7.png","products\\/hunting.png","products\\/beach-4.png","products\\/farm-2.png","products\\/forest-2.png","products\\/haunted-house.png","products\\/road-2.png","products\\/shore-1.png","products\\/planet-earth.png","products\\/cityscape-3.png","products\\/digger.png","products\\/vacations.png","products\\/map.png"]', 0, '2016-08-12 17:43:39', '2013-12-20 11:52:05'),
(75, 'PD-74', 'Et tempora fugiat.', 13, 145, 14, 47696, 'Officia officia harum numquam quo est quisquam. Facilis et minus nesciunt qui excepturi et perspiciatis. Voluptate qui voluptates iure sed voluptatum omnis voluptatem autem. Quisquam deserunt magnam numquam dolorem a nostrum numquam.<br><br>Voluptatem blanditiis et consequatur incidunt sunt corrupti aut. Praesentium voluptatem alias exercitationem a corporis sunt est. Officiis atque explicabo quibusdam quasi sit.<br><br>Aspernatur assumenda rerum aut nam. Dolor provident qui et et asperiores dignissimos.<br><br>Sint aut voluptatem corporis nulla eos. Consequatur voluptas facilis sequi earum veritatis consequatur. Qui veniam ut et eius voluptatum sunt quidem. Tempora officiis ipsam exercitationem.', '1.07', 'cotton', 'white', 'XL,XXL,M', 'Voluptas ipsam unde itaque tenetur. Et atque occaecati rerum nam nisi nulla. Quis non repudiandae quos corporis. Dolore expedita eos accusantium aut quia similique. Ex autem architecto provident ex. Inventore minus expedita provident quia non odio. Quos voluptas molestiae et veniam molestiae eum. Minus accusamus deleniti tempore enim quo aut. Libero itaque sint odio rem. Sequi accusamus temporibus ab eos.', 1, '["products\\/autumn.png","products\\/beach-1.png","products\\/park.png","products\\/haunted-house.png","products\\/cityscape-2.png","products\\/forest-3.png","products\\/bulldozer.png","products\\/tornado.png"]', 0, '1978-08-02 06:26:01', '2007-09-25 17:14:05'),
(76, 'PD-75', 'Error eveniet omnis.', 19, 488, 46, 11304, 'Voluptas molestias at labore beatae. Quam eos sit ipsa quidem ratione alias. Et sit totam alias fugiat aperiam ut illum mollitia. Blanditiis et quibusdam quod.<br><br>Voluptatum dolorem non et enim enim vero. Sed et quod blanditiis animi aliquam dolorum aliquid. Aspernatur voluptatem voluptatum quam commodi consectetur rerum.<br><br>Magnam nam corrupti qui sunt accusamus id. Quasi deserunt eos sequi aliquid nihil laudantium fugiat. Rerum nemo in fugit est eos. Et sed doloribus et ut.<br><br>Est maxime aut et quod qui reiciendis. Repudiandae rerum mollitia rerum dolore error. Omnis reprehenderit maiores sapiente ut quia autem.', '0.73', 'gold', 'green,black,white,red,red', 'L,XXL', 'Totam vitae ea et quo voluptate illum aliquam. Quisquam et voluptas dolorem cupiditate enim. Rem unde rerum voluptas a rerum. Distinctio sunt a dolor pariatur. Eum fugiat ex impedit saepe qui voluptatibus laborum. Reiciendis eos perferendis enim quam. Inventore eos vel sit nostrum. Laboriosam dolor excepturi quasi.', 1, '["products\\/parachute.png","products\\/river.png","products\\/photo-camera.png","products\\/field.png","products\\/park.png","products\\/lagoon.png","products\\/train.png","products\\/beach-1.png","products\\/sky.png","products\\/hospital.png","products\\/cliff.png","products\\/cycling.png"]', 0, '1984-05-29 05:47:30', '2014-03-31 10:59:55'),
(77, 'PD-76', 'Deserunt sunt reprehenderit.', 3, 331, 39, 10843, 'Cumque et qui qui blanditiis ratione dolor. Eveniet omnis et soluta nihil eveniet.<br><br>Illum repudiandae aut et ex sint numquam impedit necessitatibus. Quisquam quidem qui saepe possimus perspiciatis modi ratione dolorem. Fugiat impedit expedita rerum autem quia officia similique. Et aliquid nemo esse nesciunt et assumenda et amet. Ut laborum odit consectetur eum sint repellat.<br><br>Non fugiat eveniet assumenda. Corrupti aut aut reprehenderit omnis amet dolor.<br><br>Voluptatem placeat error dignissimos qui et. Esse quae doloribus a consequuntur nostrum eum debitis deleniti. Dicta occaecati sunt est eaque amet.', '1.18', 'wool', 'white,blue,red,white', 'XXL,L,M', 'Sit impedit ipsa eveniet eum. Tenetur sed qui ut. Qui sed mollitia velit ea consequatur aut. Minima iste ab sed facilis tenetur iure. Qui dicta dolores quo quo. Et soluta et autem exercitationem. Et quo assumenda aspernatur. Animi quia repellat distinctio debitis ut iste. Atque est et repellendus temporibus aliquam repellendus deleniti voluptas. Et quos ipsam esse dicta quaerat. Sed cupiditate odit saepe dolores laudantium nostrum saepe. Non voluptatibus culpa sed. Maiores sequi sequi ut cumque. Et rerum dolor natus esse. Repellat eum consequatur commodi odio. Nihil aut ducimus impedit ea quos porro.', 1, '["products\\/camping-5.png","products\\/desert-1.png","products\\/church.png","products\\/forest-3.png","products\\/whale.png","products\\/park-1.png","products\\/hotel.png","products\\/lagoon.png"]', 0, '2003-09-27 02:48:01', '1977-12-18 07:23:15'),
(78, 'PD-77', 'Vel amet amet quo.', 34, 218, 66, 74850, 'Ut quisquam est sit. Veniam ut maxime provident porro omnis maiores. Impedit voluptatem at corporis magnam voluptas. Voluptatem et quia error et doloribus sed.<br><br>Et fugiat rem porro sequi ut itaque. Possimus eos omnis id autem. Ad sed nobis autem vel sunt labore et. Ab ex debitis est quaerat est totam iusto. Similique et repudiandae est debitis est nam quis.<br><br>Nostrum dolor non consequuntur magni voluptatem consequatur qui. Minima earum aperiam magni aspernatur pariatur nobis nulla laboriosam. Animi et pariatur et ea est ea consequatur.<br><br>Iste sint distinctio nihil quas sint voluptas recusandae. Optio fugit aut minus qui et. Quia non adipisci itaque quasi vitae ullam vitae.', '1.17', 'metal', 'red,blue,black,blue', 'XL', 'Voluptatibus ab id reiciendis vero cum incidunt non. Aut quisquam aliquid eos nihil qui. Accusamus accusantium quis placeat et eum dolore architecto. Voluptatem aliquam quod et rem deleniti. Assumenda iusto eum et quasi mollitia officiis aut. Saepe voluptate molestiae quaerat atque a non quasi. Vel nihil maiores officiis et provident rem. Et blanditiis nemo maiores vitae distinctio. Ut officia hic sint necessitatibus deleniti exercitationem possimus voluptatibus. Quaerat laboriosam accusamus sint unde rem qui hic. Est nihil optio corrupti sapiente et cum dolorum. Et et voluptas et et quo. Vero nam aut voluptatibus harum quisquam et porro. Tenetur sed dolores quia enim doloremque et consectetur et. Molestias natus repellendus minima tenetur labore necessitatibus minima culpa. Incidunt quo voluptates officiis nisi molestiae. Quos dolorum unde eos laborum.', 2, '["products\\/boat-1.png","products\\/shore-1.png","products\\/cityscape.png","products\\/autumn.png","products\\/shore-1.png","products\\/car.png","products\\/hospital.png","products\\/forest-2.png"]', 0, '1976-11-19 09:58:42', '1974-11-23 08:14:08'),
(79, 'PD-78', 'Nihil quis architecto doloribus.', 25, 272, 26, 20379, 'Minima sequi nisi quia provident error voluptas quibusdam. Tenetur a facilis sequi ex sint placeat consequatur. Tempore culpa unde est libero minima porro ut. Libero odit assumenda est voluptatum expedita quos.<br><br>Amet placeat eum totam dolores. Quae deleniti consequuntur ratione est porro non rerum et. Soluta eligendi sed voluptas. Impedit consectetur quis autem est nesciunt beatae dolorem totam.<br><br>Sed fugit quo sit eos atque quaerat voluptas. Dolore molestiae qui sed reprehenderit et.<br><br>Corrupti in sunt ut vitae dolorum facere et. Doloremque aliquam consectetur aut dolores at tempora. Aperiam quidem aut et maxime quibusdam dolorum. Accusamus ut laudantium quo aut corporis non. Et omnis velit nam ratione.', '1.1', 'wool', 'blue', 'M,XXL', 'Eos corporis beatae veniam. Et sapiente labore eos eos. Consequatur officiis occaecati quia ullam minima. Et non suscipit temporibus aut rerum et ut. Aliquam quas dolorem in assumenda. Repudiandae voluptate dolor incidunt sint velit. Aliquam delectus nam ut ab quidem doloremque est. Excepturi laboriosam ratione eum sequi qui consequatur qui. Tempore debitis blanditiis ut voluptatem.', 0, '["products\\/palace.png","products\\/planet-earth.png","products\\/compass.png","products\\/eiffel-tower.png","products\\/river-2.png"]', 0, '1978-07-27 19:49:23', '1990-08-05 12:52:26'),
(80, 'PD-79', 'Sit suscipit laborum quos.', 24, 443, 57, 47039, 'Fuga ipsum a praesentium laboriosam possimus. Sint qui assumenda ullam et commodi. Accusantium itaque blanditiis rerum ab adipisci at est qui. Et sed velit natus rerum.<br><br>Fugit quia quidem quo fuga. Eaque omnis sed inventore nulla dignissimos rerum. Similique consequatur debitis quis et ullam laborum. Distinctio sed maiores alias nihil earum ut velit et.<br><br>Sunt quo aut corrupti esse. Illum inventore ut aliquid dolorem dolores. Facere aut quia cumque distinctio deleniti ea.<br><br>Accusantium ut ipsam nostrum asperiores. Dolorum nobis hic ipsam qui dolorem consectetur. Sit quam quis et ut molestias. Amet incidunt architecto consequatur atque.', '0.46', 'cotton', 'yellow,white', 'XL,L', 'Ad autem est delectus qui mollitia molestiae mollitia. Maxime perspiciatis temporibus eos quo voluptatibus sapiente. Eligendi iusto nam aut dicta. Et quasi nobis odio cum. Minima voluptatum magni quibusdam. Omnis optio sed distinctio ut. Qui libero ipsam rerum illum perspiciatis. Consequatur ea nostrum quisquam quidem. Cum eos veniam tenetur magnam labore. Rerum quibusdam impedit soluta. Fugiat esse aut voluptatum consequatur laudantium aperiam quod. Harum voluptate porro ut. Rerum inventore est similique vero ipsum eaque.', 2, '["products\\/farm-2.png","products\\/river-3.png","products\\/beach-4.png","products\\/boat-1.png","products\\/park-2.png","products\\/road-2.png","products\\/beach.png"]', 0, '1970-06-16 23:39:35', '2009-01-12 13:18:54'),
(81, 'PD-80', 'Dolor sed molestiae et.', 11, 31, 79, 85948, 'Quibusdam quis unde minima eos quisquam et. Velit consequatur qui nesciunt magni rerum. Cupiditate sapiente et ad labore non quis deleniti. Consequatur ut doloribus et consequatur doloremque.<br><br>Possimus rem eligendi nemo repellendus nihil aliquid. Expedita minima vel voluptatem. Dolores reprehenderit voluptatem vel et a. At accusantium ratione velit laborum cupiditate est.<br><br>Ut et excepturi dicta et eaque tempore. Fugiat assumenda ex magni necessitatibus sapiente quae placeat voluptas. Aut blanditiis aliquam commodi aut. Inventore architecto molestiae et et aliquid occaecati.<br><br>Architecto dolore est voluptas et enim ipsam iusto et. Molestias delectus numquam sapiente eos qui est nihil. Aut facere provident et similique ut. Omnis iste et rem consequatur.', '1.05', 'silver', 'white', 'XXL', 'Suscipit deleniti enim quae sunt. Iusto deserunt possimus est soluta. Placeat delectus quae quasi. Qui qui odio eaque dolorem neque accusantium voluptas. Delectus ex facere dolorum sunt error. Et est at rerum. Rerum ipsum enim non sed et enim dolorem omnis. Accusamus qui odio eos non. Excepturi tenetur soluta molestias necessitatibus atque.', 2, '["products\\/cityscape-4.png","products\\/circus.png","products\\/sky.png","products\\/camping-5.png","products\\/forest.png","products\\/map.png","products\\/camping-1.png","products\\/road-1.png","products\\/park-1.png","products\\/autumn.png"]', 0, '1986-07-15 07:02:42', '1985-08-21 08:39:38'),
(82, 'PD-81', 'Quo et necessitatibus.', 11, 74, 38, 74827, 'Laboriosam voluptatem ut sapiente blanditiis cupiditate tenetur qui maxime. Distinctio nemo blanditiis illo dicta ut molestias sunt consectetur. Asperiores maxime sint laudantium dolore sit ea. Sint autem temporibus rerum. Unde dicta sunt quia sed itaque in.<br><br>Et voluptatem earum voluptatibus possimus quia provident at molestiae. Architecto eveniet et quod eaque occaecati odit in. Illum assumenda nulla omnis pariatur atque voluptas. Vel provident possimus fuga vero vitae.<br><br>Sit repudiandae sed doloribus quia omnis consequatur. Sunt iusto non nobis at. Id voluptas aliquam ut tempora consectetur.<br><br>Quam tempore libero sed tempora ipsam inventore qui illo. Veritatis at possimus ea omnis. Sed voluptatum recusandae eos esse facere. Non error dignissimos esse voluptatum sed quia sed.', '0.21', 'metal', 'white,white', 'XL', 'Est doloremque doloribus aperiam molestiae ut fugit mollitia similique. Maxime quia sit adipisci blanditiis. Unde eligendi dolorem rerum. Veritatis eum in molestiae nobis corrupti. Vel modi sint et ex reiciendis et quod. Illum temporibus dicta dicta unde. Reprehenderit nam suscipit quas sed. Harum et numquam officia maxime. Libero quidem sapiente qui quis nihil. Est ipsum odio qui voluptates temporibus. Ullam dicta et et voluptate aliquid. Illo quis at est.', 2, '["products\\/road-1.png","products\\/lagoon.png","products\\/walkie-talkie.png","products\\/compass.png","products\\/palace.png","products\\/eiffel-tower.png","products\\/compass.png","products\\/whale.png","products\\/cityscape-3.png","products\\/road.png","products\\/eiffel-tower.png","products\\/shore.png","products\\/cityscape-2.png","products\\/cityscape-3.png","products\\/whale.png"]', 0, '2004-12-07 01:11:08', '1996-05-28 11:12:19'),
(83, 'PD-82', 'Dolorem dolor sit.', 12, 168, 75, 89907, 'Similique molestias eveniet illum facilis. Fugiat placeat aut aut. Placeat repudiandae eaque aliquid. Alias eaque corporis aut ea omnis maiores.<br><br>Cum consequuntur sit omnis et at aut. Delectus quisquam sed neque qui illo velit sit. Dolor et aspernatur maxime repudiandae. Sed expedita quasi incidunt pariatur.<br><br>Doloremque molestias deleniti earum impedit repellat deserunt voluptatem. Quaerat sequi magni accusantium eum sapiente.<br><br>Et eius quas aperiam et ipsam. Non magni dolorem doloremque dolore beatae recusandae animi. Labore autem sequi ipsa tempore accusamus neque perferendis qui.', '0.72', 'cotton', 'green,red,blue', 'M,M', 'Quisquam nesciunt est dolores perspiciatis et. Voluptas aut fuga non occaecati quia. At sint laborum debitis mollitia et quibusdam officiis. Aperiam aspernatur qui voluptatum laudantium similique consequatur in. Est doloribus reiciendis in ratione unde vel. Nostrum et odio expedita facere quidem molestiae. Molestiae odit ea laboriosam excepturi omnis. Placeat voluptatem et facere. Magni quas consequatur enim quae. Consequatur saepe quis sapiente reiciendis. Voluptatem cumque placeat omnis rerum aspernatur alias illo. Perferendis dolores corporis est rerum id eius at. Impedit deleniti sequi quia esse omnis reiciendis earum.', 0, '["products\\/boat-2.png","products\\/lagoon-1.png","products\\/deer.png","products\\/cityscape-2.png","products\\/farm-1.png","products\\/sydney-opera-house.png","products\\/cityscape-3.png","products\\/volcano.png","products\\/autumn.png","products\\/school.png","products\\/park.png","products\\/planet-earth.png","products\\/cityscape-4.png"]', 0, '1972-05-18 08:58:20', '2003-01-18 01:09:14'),
(84, 'PD-83', 'Soluta doloribus quos quis.', 9, 332, 67, 91387, 'Suscipit accusamus quisquam iste sit odit. Sed magnam in repellat ut. Et officia dolore qui ut nulla quo.<br><br>Porro itaque et suscipit debitis. Cupiditate est at non. Et optio voluptas ut praesentium. Velit at vel doloribus modi. Consequuntur qui ut et reiciendis inventore incidunt velit quia.<br><br>Corrupti voluptatem et dolor nesciunt est dicta. Ut similique reprehenderit repudiandae laboriosam doloremque molestiae ex. Suscipit natus velit id perferendis perferendis sit. Facere fuga qui recusandae cum voluptates.<br><br>In quisquam suscipit eligendi consectetur et maiores a. Reprehenderit eaque velit deleniti voluptates quia quo. Unde et voluptatibus doloremque doloribus veniam aut perspiciatis. Voluptatem molestiae qui est doloremque ut ea.', '0.47', 'wool', 'black,white,black,blue', 'XL,XXL', 'Exercitationem quos et consequatur ipsam esse consectetur perspiciatis. Atque voluptatem aliquid earum sed minus. Eveniet quia et perferendis illo temporibus. Ipsum saepe suscipit qui laboriosam eum. Voluptatum nostrum omnis molestiae dolore non non neque. Commodi itaque quisquam hic mollitia rerum adipisci. Consequatur sequi placeat veniam. Mollitia quis ut sunt optio rem. Saepe accusamus in in non possimus tenetur voluptatem. Aperiam facilis quod iure voluptas. Nobis molestiae ullam repudiandae animi et. Accusantium dicta magnam quidem natus eius. Non occaecati voluptatibus impedit necessitatibus. Delectus qui eos quia et possimus labore maxime. Aut qui quia sint deleniti.', 3, '["products\\/cycling.png","products\\/river.png","products\\/whale.png","products\\/church.png","products\\/dump-truck.png","products\\/palace.png","products\\/walkie-talkie.png","products\\/beach-1.png","products\\/bulldozer.png","products\\/map.png","products\\/digger.png","products\\/sky.png"]', 0, '2012-09-21 15:04:13', '1975-07-29 20:55:35'),
(85, 'PD-84', 'Consequuntur recusandae et.', 8, 97, 97, 53623, 'Omnis et et quia. Consequatur voluptatum facilis molestiae incidunt provident molestias. Nihil maiores eos quos dolorem. Culpa et optio voluptatibus asperiores eum. Est ut illo optio.<br><br>Amet magni molestiae ea totam. Sint et et quos laudantium sint quisquam.<br><br>Rerum aut deleniti aliquam ipsum. Distinctio facilis magni aperiam. Quia vitae veniam dolorem nesciunt nostrum non quae. Rerum maxime magnam qui vero recusandae non id iste.<br><br>Maxime itaque esse numquam assumenda et vel repellat saepe. Et magni accusantium eum ut. Ut ut hic et iure necessitatibus eum.', '1.12', 'nilon', 'red,blue,white,white', 'XXL,XL', 'Error esse et molestiae at ut. Aut soluta quam aliquam enim et. Quia dolorem veniam cumque consequatur provident ut. Dolor hic et nesciunt architecto a sed voluptatem repudiandae. Facere vel vel totam ut libero facilis. Placeat eum cupiditate eaque aut dignissimos culpa sunt. Soluta explicabo voluptatem ullam facere numquam. Eveniet cupiditate illum enim non voluptate eaque doloribus. Voluptas officiis itaque inventore qui dolores asperiores nam. Ut et non laborum fugit fugit consectetur ut. Dolores eos non quibusdam eligendi optio doloremque amet. Iure praesentium quam provident aperiam nisi voluptas. Sequi et ratione vero enim. Praesentium ipsum et voluptate dolor eaque esse omnis. Qui id atque dolorem. Voluptatem dolor est harum vel temporibus beatae.', 1, '["products\\/farm.png","products\\/cycling.png","products\\/shore-1.png","products\\/road.png","products\\/autumn.png","products\\/coliseum.png","products\\/digger.png","products\\/tornado.png","products\\/autumn.png","products\\/sea-1.png","products\\/cityscape-2.png","products\\/cityscape-4.png"]', 0, '2012-10-27 04:18:09', '1971-09-21 12:37:43'),
(86, 'PD-85', 'Aspernatur corporis aut sit.', 11, 160, 85, 61784, 'Voluptatem voluptatem non accusamus quo perspiciatis dolores numquam veniam. Accusamus ut cum ipsa nihil dolorum dolores. Ullam doloribus numquam cum. Aut reprehenderit minus fugit laborum ut.<br><br>Et alias rerum sint error quia blanditiis minima. Hic dolorem commodi quidem explicabo voluptate ut. Est labore ut fugit dolores aut labore vel. Eveniet ullam minus quibusdam et et sunt.<br><br>Amet accusantium excepturi voluptatem nisi qui rem. Doloribus error vel et ut. Omnis ad accusantium beatae qui. Ex at reprehenderit dicta inventore iste deserunt.<br><br>Provident voluptatem fugiat eius quo reiciendis. Perspiciatis culpa voluptate perspiciatis voluptatem voluptate. Nobis neque omnis inventore sed illo nesciunt. Maiores expedita quasi sapiente deleniti recusandae recusandae. Neque quia aliquid minima.', '1.81', 'wool', 'blue', 'XL,M', 'Mollitia quae natus sapiente. Similique doloribus qui ea. Ut qui repudiandae assumenda. Iusto quisquam quis cupiditate et. Accusantium sed quis et culpa laborum aut. Laborum dolorem tempore quod quia dolorem sit voluptates. Occaecati nihil libero et sed. Temporibus et modi aut sunt facere deleniti. Eius neque nesciunt molestias excepturi autem autem non quia. Ipsam officiis non rerum sequi quis aut eius. Placeat officiis corrupti quia quia quia. Et esse molestias sunt neque ipsum quas. Nam et et rem voluptatem occaecati.', 3, '["products\\/lake-1.png","products\\/farm-2.png","products\\/field.png","products\\/sea-1.png","products\\/cityscape-2.png","products\\/train.png","products\\/shore.png","products\\/road-3.png","products\\/camping-3.png","products\\/coliseum.png","products\\/river.png","products\\/lake-1.png","products\\/cityscape-4.png","products\\/camping.png","products\\/hunting.png"]', 0, '2016-08-30 23:41:08', '1973-11-13 14:43:06'),
(87, 'PD-86', 'Quis sint sunt eius.', 21, 5, 57, 55749, 'Delectus provident maxime distinctio voluptates eum labore. Voluptatibus quidem et molestiae in distinctio qui magni. Dolores modi expedita sit dolor beatae voluptas quo optio.<br><br>Quia excepturi omnis consequuntur sit ea. Exercitationem illum et minima et odio rerum. Laboriosam nobis labore perspiciatis molestiae.<br><br>Harum cupiditate consectetur et maxime ipsa. Quasi vitae quia perspiciatis voluptatum molestiae doloremque sunt mollitia. Quia adipisci ut perferendis aut ab magni ut. Ipsam vel asperiores eos.<br><br>Aut voluptas nemo repellendus soluta accusamus eos. Hic reiciendis illo eligendi rerum est. Dolore quae assumenda velit illo ea voluptates.', '0.03', 'gold', 'black,white', 'M', 'Animi quia in quos at ducimus blanditiis. Incidunt iure non praesentium est et voluptas. Enim odio fugiat sint nulla quam vel quia. Magnam error ullam repellat et expedita id. Omnis qui asperiores voluptatibus velit autem. Molestiae tempora sapiente aut. Similique sed eligendi cum sit illo id delectus. Sequi vitae earum nam tempora ducimus dolores tempora amet. Fugiat ea sint aut quas. Beatae unde nemo autem eum distinctio. Deserunt ratione fugit rerum vel minus omnis atque. Quibusdam dolor dolor et at. Aut sit commodi blanditiis non ullam nesciunt voluptatem. Unde autem culpa illum et doloremque sequi consequatur.', 2, '["products\\/deer.png","products\\/road-2.png","products\\/eiffel-tower.png","products\\/camping-7.png","products\\/road-2.png","products\\/bulldozer.png","products\\/great-wall-of-china.png","products\\/farm-1.png","products\\/sky.png"]', 0, '2002-02-01 13:04:12', '1978-01-16 00:35:20'),
(88, 'PD-87', 'Quod ipsa est in.', 3, 403, 42, 93807, 'Est omnis aliquam quo in blanditiis quis non. Pariatur qui nostrum voluptas et. Asperiores ea ipsam commodi nihil doloribus.<br><br>Repudiandae et atque aliquam autem. Eveniet omnis eos rerum. Repellendus est adipisci qui. Soluta dignissimos reprehenderit voluptatem molestiae voluptas distinctio ipsum.<br><br>Perferendis maxime provident ea qui qui nesciunt. Voluptatem est commodi delectus magnam. Voluptas amet explicabo molestiae.<br><br>Iste temporibus tempora molestias quis aut vel assumenda. Sit doloremque fugit explicabo voluptatem. Dolor rerum ea aliquid et laborum voluptas odit pariatur.', '0.76', 'wool', 'red,white,blue,black,white', 'XXL', 'Repudiandae tempore sit aperiam. Mollitia deserunt ea ut earum. Eum occaecati voluptatibus recusandae sunt. Pariatur nemo sed omnis praesentium laborum architecto. Inventore natus iure cumque. Amet praesentium a omnis perspiciatis blanditiis aut. Iure temporibus magni eligendi autem. Commodi iste et id eum inventore nisi nostrum. Similique nihil perferendis ut iure exercitationem deleniti cumque. Ut soluta atque veritatis cum. Neque voluptatem soluta quisquam excepturi. Est optio cumque et velit fuga eos. Et quia autem itaque molestias eos minima.', 2, '["products\\/hunting.png","products\\/road.png","products\\/map.png","products\\/boat.png","products\\/sea-1.png","products\\/hospital.png"]', 0, '1998-05-10 20:12:44', '1979-12-20 22:08:27'),
(89, 'PD-88', 'Commodi nihil asperiores accusantium.', 26, 166, 39, 60434, 'Nulla nihil tempore enim nobis. Omnis inventore optio et consequatur voluptatibus quisquam. Vel blanditiis est laboriosam. Voluptatibus omnis sit nobis ullam dolorem.<br><br>Cumque voluptas culpa illo iure. Aut distinctio nihil enim numquam nobis. Tempore corrupti inventore hic voluptatem consectetur impedit voluptatibus cumque. Rerum et exercitationem sit.<br><br>Beatae quod debitis natus dolor illum non et. Asperiores aut amet sit. Similique eveniet et numquam.<br><br>Eum dolor velit hic magni rerum vitae aut. Nisi quis nobis error est nesciunt. Aperiam fuga velit quo consectetur incidunt. Pariatur nostrum architecto et et id consequuntur.', '0.16', 'wood', 'red,white', 'M,XXL', 'Quia quisquam consectetur deleniti consequuntur aut sit. Hic soluta suscipit sit rem. Laboriosam qui est fugiat consequatur totam accusantium id. Voluptas voluptas modi et minus reiciendis odit. Veritatis exercitationem distinctio qui aut. Perferendis dolorem aut aut sapiente. Enim expedita minus qui molestiae fugiat. Sit odit ut dolores et consectetur veritatis rerum. Ea et saepe sapiente quis.', 0, '["products\\/cityscape-2.png","products\\/forest.png","products\\/desert-2.png","products\\/field.png","products\\/sea-1.png","products\\/road-3.png","products\\/coliseum.png","products\\/desert-1.png"]', 0, '1992-09-24 12:25:10', '1994-12-04 19:32:28'),
(90, 'PD-89', 'Recusandae non esse in.', 26, 207, 71, 11573, 'Vitae aliquam dolore odit et odio itaque qui. Ullam numquam sit magnam voluptate. Libero vitae architecto sint dicta. Sapiente dignissimos dolorum mollitia maiores.<br><br>Harum eveniet dolor sunt ullam est iusto qui. Voluptate hic praesentium eveniet incidunt. Et vel itaque consectetur quas laudantium.<br><br>Et quaerat atque eligendi maiores voluptates non expedita. Tenetur tempore quia id corrupti et molestiae molestias odio.<br><br>Porro repudiandae sint libero corporis officia iusto. Consequuntur dolor reprehenderit ipsa quaerat optio repellendus dolorem. Est impedit dolores ratione molestiae.', '0.07', 'wool', 'red,white,blue', 'M', 'Aut quis eum eaque aut velit nihil pariatur. Est voluptates ut est veritatis. Et nobis totam ad aliquam ipsum maiores possimus. Esse autem et reiciendis est non ut. Veniam sit sunt et voluptates. Earum sit provident ullam ab. Aut nam qui quo. Vel distinctio incidunt qui quis. Necessitatibus eos eos quia sed voluptatem optio. Fugit omnis dignissimos cupiditate sit. Voluptates laborum alias dignissimos corporis ab nam. Cupiditate nam id quas distinctio repellendus voluptatem.', 0, '["products\\/boat-2.png","products\\/taj-mahal.png","products\\/shower.png","products\\/camping-3.png","products\\/sea-1.png","products\\/park.png","products\\/farm-2.png","products\\/barbecue.png","products\\/forest-2.png"]', 0, '1993-04-17 20:24:45', '1991-02-07 04:12:53');
INSERT INTO `products` (`id`, `code`, `name`, `provinceId`, `cityId`, `stock`, `price`, `description`, `weight`, `material`, `color`, `size`, `note`, `label`, `images`, `deleted`, `created_at`, `updated_at`) VALUES
(91, 'PD-90', 'In tempora vel ea non.', 30, 101, 91, 12122, 'Corrupti ea debitis est veniam quis ad nisi et. Repellendus iste aut consequuntur. Placeat non consequuntur perspiciatis debitis laudantium. Ullam ut voluptate at non dignissimos alias atque amet.<br><br>Nulla ut omnis minima. Quibusdam tempore explicabo labore. Ut culpa vero sapiente cum fuga earum eum minus. Fuga fugiat vero quod error ut animi.<br><br>Aut aliquid officiis quisquam praesentium animi. Veniam ipsum officia eaque id. Possimus magni laudantium aliquam aut quod.<br><br>Qui et voluptatem ullam animi laborum. Vero est dolorum sed aspernatur natus recusandae distinctio.', '0.82', 'nilon', 'black', 'XL,XL,L', 'Qui eos cupiditate nostrum maiores quam ipsa et. Rerum aliquid dolorem et maiores cumque velit esse. Quis et officiis deserunt at. Magnam dolores non quo sit sint. Tempore consequatur vel sit debitis tempora officiis. Voluptatum dignissimos deserunt ipsa consequuntur fugit qui blanditiis officia. Distinctio voluptatibus dolorum architecto quas sed in. Voluptas sint magni quisquam perferendis deserunt ipsam. Corrupti alias eos ut. Laudantium soluta dolore vel natus eaque aspernatur nihil. Officia minus temporibus temporibus beatae omnis et occaecati suscipit.', 3, '["products\\/river.png","products\\/backpack.png","products\\/planet-earth.png","products\\/bulldozer.png","products\\/forest-3.png","products\\/car.png","products\\/cityscape.png"]', 0, '1987-10-10 20:15:14', '1981-11-21 21:07:48'),
(92, 'PD-91', 'Nam nulla mollitia necessitatibus.', 14, 167, 95, 2424, 'Officiis dolorem nobis dolore perspiciatis est. Harum dolores blanditiis esse optio molestiae quia hic.<br><br>Omnis soluta vel nisi aspernatur. Aliquid autem explicabo aut blanditiis et dolores tempora.<br><br>Qui inventore aut facilis sint sed est quod. Perspiciatis laborum velit inventore non. Dicta temporibus velit suscipit et error.<br><br>Non nisi debitis facilis vero consequatur. Mollitia repudiandae tempora sint velit occaecati architecto inventore tempore. Tempora sapiente ut doloremque aperiam.', '0.33', 'cloth', 'black,white', 'M,XL,XL', 'Voluptatem iusto nobis dolor perferendis nesciunt et enim autem. Occaecati doloribus corporis placeat deserunt blanditiis deleniti quaerat voluptate. Occaecati nihil voluptatum dolore est ipsam nihil quas. Nihil ea occaecati ea quia veniam qui. Harum aliquam quas cupiditate qui animi nulla quia. Accusamus quis nisi ut esse. Dignissimos error voluptas quia exercitationem mollitia autem autem distinctio. Ullam explicabo magnam molestiae eum ratione amet aut. Nesciunt veritatis dolor veritatis dolor. Ullam ab voluptas impedit ea amet qui. Mollitia qui quod minus omnis. Sint dignissimos aliquam fugit quia odio omnis illo. Aut beatae illo ut cum est explicabo a. Voluptas magnam perspiciatis eum tempore esse quod et. Harum ratione ducimus fugiat. Sit deleniti odio aut laborum sit nostrum cupiditate. Aliquid quo quae et reiciendis iure ut iure vel.', 3, '["products\\/camping-4.png","products\\/cityscape-3.png","products\\/dump-truck.png","products\\/boat.png","products\\/cityscape-2.png","products\\/church.png","products\\/sea.png"]', 0, '1994-01-05 11:40:28', '1987-04-03 05:48:06'),
(93, 'PD-92', 'Aspernatur placeat eos repellat quaerat.', 31, 81, 13, 71531, 'Ut est suscipit rerum et. Ipsum autem ab sint quia numquam. Eum doloribus sed est cum.<br><br>Nobis quia omnis consequatur dolorem possimus rem. Architecto fuga tempora atque amet. Non cumque laudantium deserunt cum est expedita. Aperiam voluptatum nam unde culpa quia aut. Ratione quod reiciendis voluptatum illum doloribus at laudantium.<br><br>Magni qui ipsum et suscipit aut quos. Dolorum voluptas voluptate enim omnis. Facere voluptatibus laboriosam ducimus omnis quia. Consequuntur sit illum sit autem.<br><br>Quo quam officiis aut architecto ut. Excepturi reiciendis sapiente dolor dolorem tempore voluptate. Et voluptas eum tempora voluptatem eos dolor. Non quia dolorum et molestiae. Nemo accusamus asperiores aperiam occaecati sit placeat facilis reiciendis.', '1.12', 'metal', 'white,blue,red', 'XXL,L', 'Fugit cumque ut sit expedita est nostrum qui. Hic mollitia a tempore neque perspiciatis quas. Odio rerum non consequatur eveniet eum. Labore qui repellendus harum sed quia. Ea et fuga eius voluptatibus. Nobis optio voluptatem cum pariatur quas consectetur quis. Fugit labore eum nam temporibus. Aperiam quo vero aut. Sunt suscipit et ut sit. Reiciendis quisquam aperiam numquam officiis. Magni nostrum pariatur deleniti sed quia sit id itaque.', 2, '["products\\/hospital.png","products\\/beach-1.png","products\\/farm.png","products\\/parachute.png","products\\/coliseum.png","products\\/desert-1.png","products\\/field.png","products\\/digger.png","products\\/mountain.png"]', 0, '1972-08-27 00:22:25', '2005-07-20 20:38:06'),
(94, 'PD-93', 'Consequuntur doloremque.', 8, 194, 10, 36360, 'Dolor praesentium molestias culpa accusamus dolore. Ut illo id officiis est itaque excepturi similique. Aut enim modi distinctio numquam iste officia.<br><br>Ipsum vero doloremque non quibusdam nesciunt. Perspiciatis et amet incidunt enim perferendis aliquam eveniet consequuntur. Id est fuga et.<br><br>Enim sunt minus modi illum atque perspiciatis. Beatae porro aliquid dolores et quam. Est sed voluptate sapiente repellat minima. Sunt atque voluptatem laboriosam.<br><br>Quod molestiae soluta labore enim animi recusandae ab. Unde ab aut et veniam magnam dolorem. Consequatur sunt qui atque asperiores. Quaerat ut ab sed voluptates.', '1.48', 'wool', 'red,white', 'M,L', 'Libero architecto fugit dolore rerum ipsam magni officia voluptatem. Neque beatae nisi officia possimus. Consequuntur dolores enim molestiae vero voluptatem qui incidunt. Doloremque eum illo totam at. Amet perspiciatis quisquam voluptatum ut sequi qui. Dolorem qui maxime iste veritatis ipsam nulla. Illo corporis aut maiores tempora accusantium quibusdam. Aut est rerum quos est labore cupiditate voluptatem. Aut earum maiores et. Saepe ipsum voluptatem id quas qui cupiditate voluptatem. Quae perspiciatis reiciendis omnis praesentium quod sunt. Eius voluptatem laudantium est velit reiciendis delectus nemo. Voluptatem eum assumenda omnis iusto. Nemo et rerum dolor rerum qui. Adipisci quibusdam ducimus dolores necessitatibus debitis cum ut. Rem recusandae debitis architecto officia adipisci soluta ab.', 1, '["products\\/planet-earth.png","products\\/road.png","products\\/deer.png","products\\/hunting.png","products\\/park-1.png","products\\/beach-2.png","products\\/camping.png","products\\/road-3.png","products\\/volcano.png","products\\/cityscape-3.png","products\\/park.png","products\\/camping-1.png","products\\/river-1.png","products\\/taj-mahal.png","products\\/snowman.png","products\\/camping-1.png","products\\/cityscape-3.png","products\\/eiffel-tower.png","products\\/road.png","products\\/camping-3.png","products\\/walkie-talkie.png","products\\/hotel.png"]', 0, '1994-06-08 10:49:52', '2014-05-10 04:11:38'),
(95, 'PD-94', 'Et nobis quia autem.', 23, 58, 26, 47345, 'Et consequatur est dolorum rerum maiores quis. Numquam error dolorem itaque molestiae odit totam.<br><br>Ut dolor tenetur nam velit accusamus doloribus corporis. Quis voluptatem odit veritatis veritatis. Aut nam animi repudiandae quia repudiandae.<br><br>Facere consequatur illo dolorum fugit at. Libero aut labore dolorem. Eius deserunt eos necessitatibus blanditiis natus.<br><br>Laudantium quia inventore ea dolor facilis eaque quasi. Voluptatem adipisci eius sit distinctio voluptatem eligendi quo. Commodi dolores est expedita dolores. Sit voluptatem deserunt in magnam qui commodi.', '0.95', 'wood', 'black,blue,white', 'L,XL,M', 'Itaque est mollitia aut dicta aut. Aut corrupti quos et tenetur ea. Sapiente cumque vel qui quisquam quas recusandae mollitia. Eum fugit delectus quibusdam commodi neque aliquid. Ut sed numquam amet ut debitis exercitationem consectetur. Earum harum ad dolore accusantium et. Quia reprehenderit officiis explicabo doloremque perferendis quibusdam placeat temporibus. Rerum vero laborum ex facere incidunt consequatur nam rerum.', 3, '["products\\/school.png","products\\/camping-2.png","products\\/new-york.png"]', 0, '1988-12-09 18:08:15', '1977-09-16 22:33:15'),
(96, 'PD-95', 'Quia occaecati nemo.', 6, 153, 23, 89292, 'Autem praesentium suscipit quibusdam ut delectus sint quia error. Nesciunt et repellendus quo. Ipsa voluptatem quam beatae voluptatem eos.<br><br>Rem ea ullam vel eveniet quis dignissimos eos aut. Vero quaerat quas et sit reprehenderit. Officia fuga qui at eos. Dolor velit et suscipit repudiandae inventore.<br><br>Placeat voluptatibus ut dolorum dolor. Unde earum ut atque animi voluptatem qui.<br><br>Alias voluptates minima aut ipsam. A dolorem similique optio aspernatur sed pariatur. Illo in voluptatum repellendus reiciendis quia et a.', '1.35', 'wood', 'white,black,green', 'XXL,XL,XXL,M', 'Assumenda aut perspiciatis molestiae est. Pariatur provident nobis iure doloremque. Sed ad voluptatem culpa voluptatem odit. Voluptate asperiores magnam non amet. Minima voluptas architecto voluptas harum reiciendis esse vel. Corporis voluptate ipsam et veniam repellat adipisci quas. Nemo omnis maxime quia cupiditate. Aut et voluptatem accusamus deleniti ipsum expedita.', 2, '["products\\/valley.png","products\\/road-2.png","products\\/hills.png","products\\/river.png","products\\/great-wall-of-china.png","products\\/cityscape.png","products\\/great-wall-of-china.png","products\\/sea.png","products\\/desert-3.png","products\\/parachute.png","products\\/park.png","products\\/camping-2.png","products\\/haunted-house.png","products\\/haunted-house.png","products\\/snowman.png","products\\/valley.png","products\\/road-3.png","products\\/forest-1.png","products\\/great-wall-of-china.png","products\\/park-2.png","products\\/tractor.png"]', 0, '1990-09-01 07:11:05', '1990-11-10 08:03:49'),
(97, 'PD-96', 'A ea facilis adipisci.', 32, 12, 5, 13163, 'Expedita quas qui optio expedita cupiditate quisquam. Repellat et explicabo asperiores asperiores. Non sit occaecati quasi repellat praesentium qui dolor. Ducimus ratione expedita sed cum. Inventore nobis aut officia sit deserunt nobis reprehenderit.<br><br>Laudantium ipsum mollitia repudiandae officia tempora excepturi et. Et quibusdam et velit sit dolore accusantium. Non ut tenetur et praesentium autem omnis ut. Animi ab in non aliquam et similique temporibus in.<br><br>Quia alias a architecto excepturi. Et enim recusandae molestiae aut blanditiis maxime vel. Ad aspernatur aut labore sed exercitationem vitae labore. Nobis non totam voluptatibus quae qui.<br><br>Esse voluptas quis et dolorem. Sunt aut minima praesentium qui ratione quia. Maxime aut modi et id autem dolores et.', '1.81', 'gold', 'red,white', 'M,M', 'Est consequuntur libero non expedita. Pariatur quae similique dolorem temporibus aut illo deleniti. Recusandae veritatis enim tempore quo. Minus necessitatibus maiores vero illo officiis quia maxime rerum. Sunt officiis culpa velit sint nobis veritatis magnam. Est recusandae occaecati qui velit quae quam. Sit vitae autem distinctio sed id a. Voluptate tempora consequatur quasi qui et ea eum. Sed ut nam labore veritatis. Doloribus quasi non quia sunt et esse. Necessitatibus sequi eos sit asperiores occaecati saepe vero beatae. Est saepe officiis enim et harum.', 2, '["products\\/shore-1.png","products\\/sea.png","products\\/car.png","products\\/park-1.png","products\\/beach-4.png","products\\/river-3.png","products\\/mountain.png","products\\/road.png","products\\/circus.png","products\\/shore.png","products\\/iceberg.png","products\\/haunted-house.png","products\\/camping-3.png","products\\/boat.png","products\\/default.png","products\\/forest-2.png","products\\/beach-3.png","products\\/road.png"]', 0, '2002-09-19 19:06:33', '1999-09-07 14:03:51'),
(98, 'PD-97', 'Maxime distinctio aut.', 23, 13, 35, 37110, 'Suscipit nostrum nisi illum et dolores maiores quo dolorem. Quae et sit eos aut fugiat velit. Explicabo qui error aut ut amet soluta ut. Accusantium eius est similique et aut.<br><br>Similique vel optio suscipit autem. Soluta voluptate voluptatum sunt iste quasi aut libero placeat. Nemo minus occaecati ea autem error ad qui.<br><br>Quia perferendis cum cum aperiam repellat porro. Maiores velit pariatur repellat non minus. Quaerat ipsa sit praesentium nobis ducimus et. Officiis numquam maiores maxime voluptate qui maxime velit.<br><br>Quis aut et id est maiores fuga. Nam molestiae non quos. Corporis autem sed sit doloremque maiores nesciunt. Unde explicabo harum ex non optio. Facere quisquam veritatis velit doloremque aut autem sed.', '1.69', 'cotton', 'white,red,blue,blue', 'XXL', 'Sapiente itaque porro est commodi fuga doloremque. Quasi rerum ullam voluptatem voluptas. Sunt blanditiis sed praesentium. Quasi nam laboriosam quia consectetur. Esse eligendi amet voluptatem dolore. Quia minus fuga beatae optio illum. Qui ea illo esse. Eveniet minus distinctio quasi minus assumenda quisquam.', 3, '["products\\/circus.png","products\\/river-1.png","products\\/reindeer.png"]', 0, '1995-12-05 05:13:16', '2013-11-26 01:42:04'),
(99, 'PD-98', 'Non quo fugiat magni.', 31, 485, 42, 89101, 'Reprehenderit architecto voluptate quis unde dolor. Qui optio qui similique. Modi consequatur officiis quas aspernatur recusandae officiis praesentium.<br><br>Dolor beatae ullam dolores nostrum eum. Atque animi delectus accusantium eum. Pariatur molestias sit et quia ducimus architecto dolores.<br><br>Iure voluptas eligendi labore in porro. Sit sit ullam harum accusamus vel. Quia quo optio tenetur magnam. Voluptatem placeat ut sequi ipsa voluptatibus facilis.<br><br>Reiciendis voluptates non hic facere tempora eos. Iure libero minima et. Architecto asperiores quidem similique nobis placeat qui assumenda qui. Commodi nihil est voluptatem.', '1.73', 'wool', 'white,white', 'XL', 'Rem aut atque aut ea architecto possimus dolorem. Alias quas eveniet quis. Et odio voluptatem dignissimos cupiditate tempore deleniti. Atque sed suscipit sed voluptatem aut cumque. Commodi asperiores repellendus laudantium ratione consectetur. Ratione quia nesciunt illo beatae et iste. Omnis omnis sapiente laudantium dolorem ut dolor doloribus. Qui et voluptatem quas quis a molestiae quaerat cum. Dolorem deserunt architecto non eum ut consequatur. Ea qui aut quo. Quas adipisci est accusamus excepturi quia. Asperiores porro voluptatem eaque omnis. Voluptatum rerum recusandae magnam atque. Consequatur et quia nam quis eaque ducimus eum iusto.', 3, '["products\\/deer.png","products\\/road-3.png","products\\/boat-2.png","products\\/tractor.png","products\\/desert-3.png","products\\/circus.png","products\\/lake.png","products\\/cliff.png","products\\/photo-camera.png","products\\/default.png"]', 0, '1987-04-21 17:39:21', '1987-07-26 02:17:05'),
(100, 'PD-99', 'Exercitationem distinctio omnis illum.', 28, 95, 27, 33576, 'Corporis et ipsam maiores aliquam neque necessitatibus nihil. Qui quis ipsa debitis fugiat. Nemo provident eaque est quia aut.<br><br>Ipsam et quia exercitationem vero modi quos. Voluptatum consequatur porro incidunt blanditiis assumenda voluptatibus quia animi.<br><br>Repudiandae necessitatibus impedit totam repellendus est quis. Quia sed eligendi distinctio sed dolorem aut dolores. Nesciunt blanditiis maiores nobis odio.<br><br>Iusto voluptatum dolor quod. Et ratione dolor earum ab. Sint qui cum in maxime.', '1.27', 'nilon', 'white,green,black', 'XL', 'Saepe aliquid in eaque corrupti asperiores quisquam qui dolore. Et ipsum et ut sit voluptate. Unde temporibus sint est et quo eum. Ab laudantium cupiditate commodi soluta aut est qui. Sed inventore minima a et. Quibusdam sunt id perspiciatis. Soluta in et sit inventore. Aspernatur eligendi inventore et officiis tempora reiciendis soluta blanditiis. Doloremque porro nemo sint debitis illo. Quam provident iusto porro qui quo quasi. Voluptas et qui iste dolorum voluptatibus eligendi qui.', 0, '["products\\/park.png","products\\/camping-6.png","products\\/road-1.png"]', 0, '1985-07-18 12:31:58', '2001-04-26 11:33:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `provinces`
--

INSERT INTO `provinces` (`id`, `name`) VALUES
(1, 'Bali'),
(2, 'Bangka Belitung'),
(3, 'Banten'),
(4, 'Bengkulu'),
(5, 'DI Yogyakarta'),
(6, 'DKI Jakarta'),
(7, 'Gorontalo'),
(8, 'Jambi'),
(9, 'Jawa Barat'),
(10, 'Jawa Tengah'),
(11, 'Jawa Timur'),
(12, 'Kalimantan Barat'),
(13, 'Kalimantan Selatan'),
(14, 'Kalimantan Tengah'),
(15, 'Kalimantan Timur'),
(16, 'Kalimantan Utara'),
(17, 'Kepulauan Riau'),
(18, 'Lampung'),
(19, 'Maluku'),
(20, 'Maluku Utara'),
(21, 'Nanggroe Aceh Darussalam (NAD)'),
(22, 'Nusa Tenggara Barat (NTB)'),
(23, 'Nusa Tenggara Timur (NTT)'),
(24, 'Papua'),
(25, 'Papua Barat'),
(26, 'Riau'),
(27, 'Sulawesi Barat'),
(28, 'Sulawesi Selatan'),
(29, 'Sulawesi Tengah'),
(30, 'Sulawesi Tenggara'),
(31, 'Sulawesi Utara'),
(32, 'Sumatera Barat'),
(33, 'Sumatera Selatan'),
(34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryId` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `subcategories`
--

INSERT INTO `subcategories` (`id`, `categoryId`, `name`, `deleted`) VALUES
(1, 3, 'Itaque quia sit.', 0),
(2, 3, 'Facere dolor.', 0),
(3, 2, 'Dolorum facere esse.', 0),
(4, 4, 'Repudiandae dolores.', 0),
(5, 3, 'Molestias exercitationem.', 0),
(6, 4, 'Inventore assumenda.', 0),
(7, 2, 'Nihil ea.', 0),
(8, 1, 'Nisi quia ea.', 0),
(9, 2, 'Esse non.', 0),
(10, 2, 'Unde nisi doloribus.', 0),
(11, 3, 'Deleniti facilis est.', 0),
(12, 3, 'Quidem aperiam natus.', 0),
(13, 5, 'Quod commodi nemo.', 0),
(14, 1, 'Ad nisi cumque.', 0),
(15, 4, 'Sed dolores.', 0),
(16, 3, 'Fugit consequuntur.', 0),
(17, 2, 'Fugiat quia repudiandae.', 0),
(18, 1, 'Consequatur et inventore.', 0),
(19, 5, 'Blanditiis id.', 0),
(20, 4, 'Quia quibusdam tempora.', 0),
(21, 3, 'Velit dolor cumque.', 0),
(22, 4, 'At id.', 0),
(23, 2, 'Voluptatem debitis aperiam.', 0),
(24, 2, 'Sint sequi itaque.', 0),
(25, 2, 'Quo tempore excepturi.', 0),
(26, 5, 'Ad ab velit.', 0),
(27, 4, 'Quae quo quaerat.', 0),
(28, 2, 'Optio vitae omnis.', 0),
(29, 5, 'Voluptatem et.', 0),
(30, 1, 'Voluptatum et.', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'USER',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinceId` int(11) DEFAULT '0',
  `cityId` int(11) DEFAULT '0',
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `postcode` int(11) DEFAULT '0',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'users/default.png',
  `registerType` int(11) DEFAULT '1',
  `updated` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderaddress`
--
ALTER TABLE `orderaddress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderconfirmation`
--
ALTER TABLE `orderconfirmation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_code_unique` (`code`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_code_unique` (`code`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;
--
-- AUTO_INCREMENT for table `orderaddress`
--
ALTER TABLE `orderaddress`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderconfirmation`
--
ALTER TABLE `orderconfirmation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderproduct`
--
ALTER TABLE `orderproduct`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
