<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderAddress', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId');
            $table->integer('provinceId');
            $table->integer('cityId');
            $table->string('district');
            $table->text('address');
            $table->integer('postcode');
            $table->string('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderAddress');
    }
}
