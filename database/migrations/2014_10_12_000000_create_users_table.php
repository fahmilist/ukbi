<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('role')->default('USER')->nullable();
            $table->string('phone')->nullable();
            $table->integer('provinceId')->default(0)->nullable();
            $table->integer('cityId')->default(0)->nullable();
            $table->string('district')->nullable();
            $table->text('address')->nullable();
            $table->integer('postcode')->default(0)->nullable();
            $table->string('photo')->default('users/default.png');
            $table->integer('registerType')->default(1)->nullable();
            $table->integer('updated')->default(0)->nullable();
            $table->integer('deleted')->default(0)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
