<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->integer('provinceId')->default(0);
            $table->integer('cityId')->default(0);
            $table->integer('stock');
            $table->double('price');
            $table->text('description');
            $table->string('weight');
            $table->string('material');
            $table->string('color');
            $table->string('size');
            $table->text('note');
            $table->integer('label');
            $table->text('images');
            $table->integer('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
