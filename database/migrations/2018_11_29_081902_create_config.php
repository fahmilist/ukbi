<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('tagline');
            $table->string('logo')->default('public/logo.png');
            $table->string('icon')->default('public/icon.png');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('lat');
            $table->string('lng');
            $table->text('about');
            $table->text('faq');
            $table->text('slider');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config');
    }
}
