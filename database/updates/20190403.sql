ALTER TABLE `bookPages` ADD COLUMN `parent` VARCHAR(191) NULL DEFAULT NULL AFTER `book_id`;

ALTER TABLE `books`
  ADD COLUMN `totalPage` INT NULL AFTER `description`,
  ADD COLUMN `skipPage` INT NULL AFTER `totalPage`;ALTER TABLE `books`

CHANGE COLUMN `skipPage` `skipPage` INT(11) NULL DEFAULT '0' AFTER `totalPage`;

CREATE TABLE `bookMarks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `book_id` INT NULL,
  `name` VARCHAR(250) NULL,
  `page` INT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='latin1_swedish_ci'
;

UPDATE books SET totalPage = 25, skipPage = 1;


