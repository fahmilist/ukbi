<div class="preloader">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix" data-offset-top="100">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand logo">
                    <!-- <h2>colid</h2> -->
                    <img src="<?php echo e(url('frontend/images/acuan-kebahasaan.png')); ?>" alt="kemendikbud.go.id">
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#beranda-page">Beranda</a></li>
                    <li><a href="#tentang-page">Tentang</a></li>
                    <li><a href="#statistik-page">Statistik</a></li>
                    <li><a href="#pengurus-page">Pengurus</a></li>
                    <!-- <li><a href="#team-page">Team</a></li>
                    <li><a href="#faq-page">FAQ</a></li> -->
                    <li><a href="#blog-page">Artikel</a></li>
                    <li><a href="#kontak-page">Kontak</a></li>
                    <li><a href="kincatonly.com/ukbi/detail_responsive.html">Detail Buku(Development)</a></li>
                    <li><a href="#signup" data-toggle="modal" data-target=".log-sign">Masuk</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->