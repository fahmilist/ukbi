<?php $__env->startSection('content'); ?>
    <tr>
        <td  class="main-header" style="color: #484848; font-size: 14px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
            Hai, <b><?php echo e(ucwords(@$contact['name'])); ?></b>
        </td>
    </tr>
    <tr><td height="10"></td></tr>
    <tr>
        <td>
            Terima kasih telah menghubungi <b><?php echo e($CONF->title); ?></b>. Berikut ini pertanyaan anda :
        </td>
    </tr>
    <tr><td height="20"></td></tr>
    <tr>
        <td><?php echo e(@$contact['message']); ?></td>
    </tr>
    <tr><td height="20"></td></tr>
    <tr>
        <td>
            Mohon menunggu staff kami untuk meresponse pertanyaan anda.
        </td>
    </tr>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.email', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>