<?php $__env->startSection('pageTitle','Detail Member'); ?>

<?php $__env->startPush('customCss'); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li><a href="<?php echo e(route('admin.dashboard')); ?>">Beranda</a> <i class="fa fa-circle"></i></li>
                    <li><a href="<?php echo e(route('books')); ?>">Buku</a> <i class="fa fa-circle"></i></li>
                    <li><span>Detail Buku</span></li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-dark"></i> 
                                    <span
                                    class="caption-subject font-dark bold uppercase">   Detail Buku
                                    </span>
                            </div>
                            <div class="actions">
                                
                            </div>

                        </div>

                        <div class="portlet-body form">
                            <form action="javascript:;" id="formBook" enctype="multipart/form-data">
                                <input type="hidden" name="idBook" value="<?php echo e(empty($book->id)?0:$book->id); ?>">
                                <?php echo e(csrf_field()); ?>

                                <h4>Informasi Book</h4>
                                <?php 
                                    $i = 0;
                                 ?>
                                <?php $__currentLoopData = \App\Book::FORM_FIELD; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyField => $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($i%2==0): ?>
                                <div class="row">
                                <?php endif; ?>
                                    <div class="col-md-6">
                                        <div class="form-group" id="<?php echo e($keyField); ?>">
                                            <label class="control-label"><?php echo e(\App\Book::FORM_LABEL[$keyField]); ?></label>
                                            <?php if($field == 'text'): ?>
                                            <input type="<?php echo e($field); ?>" name="<?php echo e($keyField); ?>" class="form-control" value="<?php echo e(@$book->$keyField); ?>" placeholder="<?php echo e(\App\Book::FORM_LABEL[$keyField]); ?>">
                                            <?php elseif($field=='date'): ?>
                                            <input type="text" name="<?php echo e($keyField); ?>" class="form-control datepickerinput" value="<?php echo e(@$book->$keyField); ?>" placeholder="<?php echo e(\App\Book::FORM_LABEL[$keyField]); ?>">
                                            <?php elseif($field == 'file'): ?>
                                            <input type="<?php echo e($field); ?>" name="<?php echo e($keyField); ?>" class="form-control" value="<?php echo e(@$book->$keyField); ?>" placeholder="<?php echo e($book::FORM_LABEL[$keyField]); ?>">
                                            <br>
                                            <a href="<?php echo e(url('storage/'.@$book->$keyField)); ?>" target="_blank" title="preview"><img style="background-color: #e1e1e1;max-width: 200px" src="<?php echo e(url('storage/'.@$book->$keyField)); ?>"  height="100"></a>
                                            <?php elseif($field == 'select'): ?>
                                                <?php if($keyField == 'member'): ?>
                                                    <select class="form-control" name="<?php echo e($keyField); ?>">
                                                        <?php $__currentLoopData = \App\Util\Constant::PRODUCT_TYPE; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type => $label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($type); ?>" <?php echo e($type==@$book->member?'selected':''); ?>><?php echo e($label); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                <?php else: ?>
                                                    <select class="form-control" name="<?php echo e($keyField); ?>">
                                                        <?php $__currentLoopData = \App\Util\Constant::PRODUCT_STATUS; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status => $label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($status); ?>" <?php echo e($status==@$book->status?'selected':''); ?>><?php echo e($label); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                <?php endif; ?>
                                            <?php else: ?>
                                            <textarea class="form-control" name="<?php echo e($keyField); ?>"><?php echo e(@$book->$keyField); ?></textarea>
                                            <?php endif; ?>
                                            <div class="help-block"><?php echo e(in_array($keyField,\App\Book::FORM_HELP_LIST)? \App\Book::FORM_LABEL_HELP[$keyField]:''); ?></div>
                                            <div id="<?php echo e($keyField); ?>_error" class="help-block help-block-error"> </div>
                                        </div>
                                    </div>
                                <?php if(($i+1)%2==0): ?>
                                </div>
                                <?php endif; ?>
                                <?php 
                                    $i++;
                                 ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <hr>
                                <h4>Informasi Daftar Isi</h4>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Nama</label>
                                            <input type="text" class="form-control" name="detailName0" placeholder="Nama Daftar Isi"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Halaman</label>
                                            <input type="number" class="form-control" name="detailPage0" placeholder="Halaman"/>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label">Tambah</label>
                                        <button class="btn btn-sm btn-success" type="button" onclick="saveSub(0)"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="col-md-7">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center">Nama</th>
                                                    <th style="text-align: center">Halaman</th>
                                                    <th style="text-align: center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="listSubCat">
                                            <?php if(!empty($book->id)): ?>
                                                <?php $__currentLoopData = @$book->page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr class="tr_<?php echo e($key+1); ?>">
                                                    <td><input type="text" class="form-control" name="detailName<?php echo e($key+1); ?>" onchange="saveSub(<?php echo e($key+1); ?>)" placeholder="Nama Daftar Isi" value="<?php echo e(@$page->name); ?>"/></td>
                                                    <td><input type="number" class="form-control" name="detailPage<?php echo e($key+1); ?>" onchange="saveSub(<?php echo e($key+1); ?>)" placeholder="Halaman" value="<?php echo e(@$page->page); ?>"/></td>
                                                    <td class="text-center">
                                                        <button class="btn btn-sm btn-danger" onclick="deleteSub(<?php echo e($key+1); ?>)"><i class="fa fa-remove"></i></button>
                                                    </td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            </tbody>
                                            <tfoot id="listSub">
                                            <?php if(!empty($book->id)): ?>
                                                <?php $__currentLoopData = @$book->page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <input type="hidden" name="pages[]" class="tr_<?php echo e($key+1); ?>" value="<?php echo e($page->name.'-'.$page->page); ?>">
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <?php if(!empty($book->id)): ?>
                                <hr>
                                <h4>Informasi Folder</h4>
                                <div class="row">
                                    <iframe src="<?php echo e(url('elfinder?folder='.$book->folder)); ?>" style="width : 100%; min-height: 400px" frameborder="0"></iframe>
                                </div>
                                <?php endif; ?>
                            </form>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9 text-right">
                                    <a href="<?php echo e(route('books')); ?>" class="btn default" >Kembali</a>
                                    <button type="button" class="btn btn-primary" onclick="save()">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page content -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

<?php $__env->stopSection(); ?> 

<?php $__env->startPush('customJs'); ?>
    <script type="text/javascript">
        var no_page = 0;
        var arr_sub = [];
        <?php if(!empty($book->id)): ?>
        no_page = <?php echo e($book->page->count()); ?>;

        <?php $__currentLoopData = $book->page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        arr_sub.push(<?php echo e($key+1); ?>);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

        function save(){
            swal({
                title: "Yakin Simpan Data Buku?",
                text : "Data Buku akan disimpan",
                icon: "warning",
                buttons: {
                    cancel:true,
                    confirm: {
                        text:'Simpan!',
                        closeModal: false,
                    },
                },
            })
            .then((process) => {
                if(process){

                    if(arr_sub.length == 0){
                        return swal({
                            title: 'Periksa Data Kembali',
                            text: 'Minimal Satu Daftar Isi',
                            icon: 'error',
                            timer: '1500'
                        });
                    }

                    $('.form-group').removeClass('has-error');
                    $('.help-block-error').html('');

                    $.ajax({
                        url: "<?php echo e(route('book.save')); ?>",
                        type: "POST",
                        data: new FormData($("#formBook")[0]),
                        processData: false,
                        contentType: false,
                        async:false,
                        success: function(response) {
                            if(response.status){
                                swal({
                                    title: 'Berhasil Simpan Data Buku',
                                    text: response.message,
                                    icon: 'success',
                                    timer: '1500'
                                }).then((done)=>{
                                    location.href = '<?php echo e(route("books")); ?>';
                                });
                            }else{
                                swal({
                                    title: 'Gagal Simpan Data Buku',
                                    text: response.message,
                                    icon: 'error',
                                    timer: '1500'
                                });
                                var error_arr = ['name','publish','description','content','image','featured'];
                                for(var i=0;i < error_arr.length;i++){
                                    if(error_arr[i] in response.error){
                                        $('#'+error_arr[i]).addClass('has-error');
                                        $('#'+error_arr[i]+'_error').html(response.error[error_arr[i]]);
                                    }
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '1500'
                            });
                        }
                    });
                }else{
                    swal('Data Buku tidak jadi disimpan');
                }
            });
        }   
    </script>

    <script type="text/javascript">
        function saveSub(id){
            var current = id;
            if(current == 0){
                no_page++;
                current = no_page;
            }
            console.log(id);
            var name = $('[name=detailName'+id+']').val();
            var page = $('[name=detailPage'+id+']').val();
            if(id == 0){
                var row = '<tr class="tr_'+current+'">\n' +
                    '                    <td style="text-align: center;font-weight: 100;"><input type="text" class="form-control" name="detailName'+current+'" onchange="saveSub('+current+')" placeholder="Nama Daftar Isi" value="'+name+'"/></td>\n' +
                    '                    <td style="text-align: center;font-weight: 100;"><input type="number" class="form-control" name="detailPage'+current+'" onchange="saveSub('+current+')" placeholder="Halaman" value="'+page+'"/></td>\n' +
                    '                    <td style="text-align: center">\n' +
                    '                      <button class="btn btn-sm btn-danger" onclick="deleteSub('+current+')"><i class="fa fa-remove"></i></button>\n' +
                    '                    </td>\n' +
                    '                  </tr>';
                $('#listSub').append(row);
                $('#listSub').append('<input type="hidden" name="pages[]" class="tr_'+current+'" value="'+name+'-'+page+'">');
                arr_sub.push(current);
            }else{
                $('.tr_'+current).val(name+'-'+page);
            }
        }

        function deleteSub(id){
            console.log(id);
            var index = arr_sub.indexOf(id);
            if(index === -1) return;
            arr_sub.splice(index,1);
            $('.tr_'+id).remove();
        }
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>