<!-- Sidebar -->
<aside class="wrap-sidebar js-sidebar">
	<div class="s-full js-hide-sidebar"></div>

	<div class="sidebar flex-col-l p-t-22 p-b-25">
		<div class="flex-r w-full p-b-30 p-r-27">
			<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-sidebar">
				<i class="zmdi zmdi-close"></i>
			</div>
		</div>

		<div class="sidebar-content flex-w w-full p-lr-65 js-pscroll">
			<div class="sidebar-gallery w-full p-tb-30">
				<span class="mtext-101 cl5">
					@ <?php echo e($CONF->title); ?>

				</span>

				<div class="flex-w flex-sb p-t-36 gallery-lb">
					<?php $__currentLoopData = $randProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $randProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<!-- item gallery sidebar -->
					<div class="wrap-item-gallery m-b-10">
						<a class="item-gallery bg-img1" href="<?php echo e(route('productDetail',['id'=>$randProduct->id])); ?>" 
						style="background-image: url('<?php echo e(url('storage/'.json_decode($randProduct->images)[0])); ?>');"></a>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>

			<div class="sidebar-gallery w-full">
				<span class="mtext-101 cl5">
					About Us
				</span>

				<p class="stext-108 cl6 p-t-27">
					<?php echo e($CONF->about); ?> 
				</p>
			</div>
		</div>
	</div>
</aside>