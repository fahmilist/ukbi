<?php $__env->startSection('pageTitle','Beranda'); ?>

<?php $__env->startPush('customCss'); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

	<!-- Page Beranda -->
    <header class="header-area overlay full-height relative v-center" id="beranda-page">
        <div class="absolute anlge-bg"></div>        
        <div class="container">            
            <div class="row v-center">                
				<div class="col-md-6">
					<div class="header-text">
						<h2>It’s all about Promoting your Business</h2>
					</div>
                    <p>Molestie at elementum eu facilisis sed odio. Scelerisque in dictum non consectetur a erat. Aliquam id diam maecenas ultricies mi eget mauris. Ultrices sagittis orci a scelerisque purus.</p>
                    <a href="#" class="button white">Details</a>					
				</div>
				<div class="col-md-6">
					<div id="about-slider" class="screen-box screen-slider">						
                        <div class="item">
                            <img src="<?php echo e(url('frontend/images/bookcover.jpg')); ?>" alt="" style=""> 
                        </div>
                        <div class="item">
                            <img src="<?php echo e(url('frontend/images/bookcover-2.jpg')); ?>" alt="" style="">
                        </div>
                        <div class="item">
                            <img src="<?php echo e(url('frontend/images/bookcover-3.jpg')); ?>" alt="" style="">
                        </div>
					</div>
				</div>
            </div>
        </div>
    </header>

    <!-- Page Tentang -->
    <section class="gray-bg section-padding" id="tentang-page">
        <div class="container">
            <!-- <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Tentang Kami</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <!-- <div class="col-xs-12 col-sm-3">
                    <div class="box">
                        <div class="box-icon">
                            <img src="<?php echo e(url('frontend/images/service-icon-1.png')); ?>" alt="">
                        </div>
                        <h4>EASY TO BUILD</h4>
                        <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="box">
                        <div class="box-icon">
                            <img src="<?php echo e(url('frontend/images/service-icon-2.png')); ?>" alt="">
                        </div>
                        <h4>EASY TO USE</h4>
                        <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="box">
                        <div class="box-icon">
                            <img src="<?php echo e(url('frontend/images/service-icon-3.png')); ?>" alt="">
                        </div>
                        <h4>EASY TO CUSTOMIZE</h4>
                        <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="box">
                        <div class="box-icon">
                            <img src="<?php echo e(url('frontend/images/service-icon-4.png')); ?>" alt="">
                        </div>
                        <h4>AWESOME DESIGN</h4>
                        <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod tepo raraincididunt ugt labore.</p>
                    </div>
                </div> -->
                <!-- why choose us content -->
				<div class="col-md-6">
					<div class="page-title2">
						<h2>Tentang Kami</h2>
					</div>
					<p>Molestie at elementum eu facilisis sed odio. Scelerisque in dictum non consectetur a erat. Aliquam id diam maecenas ultricies mi eget mauris. Ultrices sagittis orci a scelerisque purus.</p>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Quis varius quam quisque id diam vel quam elementum.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Mauris augue neque gravida in fermentum.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Orci phasellus egestas tellus rutrum.</p>
					</div>
					<div class="feature">
						<i class="fa fa-check"></i>
						<p>Nec feugiat nisl pretium fusce id velit ut tortor pretium.</p>
					</div>
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div id="about-slider" class="screen-box screen-slider">
						<!-- <img class="" src="<?php echo e(url('frontend/images/about1.jpg')); ?>" alt="">
						<img class="" src="<?php echo e(url('frontend/images/about2.jpg')); ?>" alt="">
						<img class="" src="<?php echo e(url('frontend/images/about1.jpg')); ?>" alt="">
                        <img class="" src="<?php echo e(url('frontend/images/about2.jpg')); ?>" alt=""> -->
                        <div class="item">
                            <img src="<?php echo e(url('frontend/images/about1.jpg')); ?>" alt="" style=""> 
                        </div>
                        <div class="item">
                            <img src="<?php echo e(url('frontend/images/about2.jpg')); ?>" alt="" style="">
                        </div>
                        <div class="item">
                            <img src="<?php echo e(url('frontend/images/about1.jpg')); ?>" alt="" style="">
                        </div>
					</div>
				</div>
				<!-- /About slider -->
            </div>
        </div>
    </section>

	<!-- Page Tentang -->
    <section class="angle-bg sky-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Buku ke 1</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover-2.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Buku ke 2</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover-3.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover-2.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Buku ke 3</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover-3.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Buku ke 4</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('frontend/images/bookcover-3.jpg')); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators caption-indector">
                            <li data-target="#caption_slide" data-slide-to="0" class="active">
                                <strong>1 </strong>consectetur adipisicing elit.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="1">
                                <strong>2 </strong>consectetur adipisicing elit.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="2">
                                <strong>3 </strong>consectetur adipisicing elit.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="3">
                                <strong>4 </strong>consectetur adipisicing elit.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Statistik -->
    <section class="gray-bg section-padding" id="statistik-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>statistik</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3>100</h3>
                        </div>
                        <h3>Jumlah Pengunjung</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3>6</h3>
                        </div>
                        <h3>Jumlah Buku</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3>40</h3>
                        </div>
                        <h3>Jumlah Pendaftar</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Pengurus -->
    <section class="section-padding sky-bg client-area overlay" id="pengurus-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Pengurus</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-sm-offset-1 col-md-8 col-md-offset-2">
                <div class="clients">
                <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-1.jpg')); ?>" alt="">
                        </div>
                        <h5>Yohan KKP.</h5>
                        <h6>Co. Founder</h6>
                        <!-- <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul> -->
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-2.jpg')); ?>" alt="">
                        </div>
                        <h5>Fahmi A.</h5>
                        <h6>UX Designer</h6>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-1.jpg')); ?>" alt="">
                        </div>
                        <h5>Yohan KKP.</h5>
                        <h6>Founder</h6>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-6"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-2.jpg')); ?>" alt="">
                        </div>
                        <h5>Fahmi A.</h5>
                        <h6>Creative Director</h6>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-1.jpg')); ?>" alt="">
                        </div>
                        <h5>Yohan KKP.</h5>
                        <h6>Founder</h6>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-6"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-2.jpg')); ?>" alt="">
                        </div>
                        <h5>Fahmi A.</h5>
                        <h6>Creative Director</h6>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-1.jpg')); ?>" alt="">
                        </div>
                        <h5>Yohan KKP.</h5>
                        <h6>Founder</h6>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- <div class="col-xs-12 col-sm-6 col-md-6"> -->
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('frontend/images/pengurus-2.jpg')); ?>" alt="">
                        </div>
                        <h5>Fahmi A.</h5>
                        <h6>Creative Director</h6>
                        </div>
                    </div>
                <!-- </div> -->
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Blog -->
    <section class="section-padding gray-bg" id="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="single-blog">
                        <div class="blog-photo">
                            <img src="<?php echo e(url('frontend/images/small1.jpg')); ?>" alt="">
                        </div>
                        <div class="blog-content">
                            <h3>Cara Meningkatkan Keberminatan Membaca</h3>
                            <ul class="blog-meta">
                                <li><span class="fa fa-user"></span> Admin</li>
                                <li><span class="fa fa-calendar"></span> 1 Januari, 2019</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit nemo eaque expedita aliquid dolorem repellat perferendis, facilis aut fugit, impedit.</p>
                            <a href="artikel.html">Baca lebih lanjut..</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="single-blog">
                        <div class="blog-photo">
                            <img src="<?php echo e(url('frontend/images/small2.jpg')); ?>" alt="">
                        </div>
                        <div class="blog-content">
                            <h3>Pentingnya Membaca Saat Usia Dini</h3>
                            <ul class="blog-meta">
                                <li><span class="fa fa-user"></span> Admin</li>
                                <li><span class="fa fa-calendar"></span> 1 Januari, 2019</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit nemo eaque expedita aliquid dolorem repellat perferendis, facilis aut fugit, impedit.</p>
                            <a href="artikel.html">Baca lebih lanjut..</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="single-blog">
                        <div class="blog-photo">
                            <img src="<?php echo e(url('frontend/images/small3.jpg')); ?>" alt="">
                        </div>
                        <div class="blog-content">
                            <h3>Membaca Adalah Gerbang Menuju Pengetahuan yang Tidak Terlampau</h3>
                            <ul class="blog-meta">
                                <li><span class="fa fa-user"></span> Admin</li>
                                <li><span class="fa fa-calendar"></span> 1 Januari, 2019</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit nemo eaque expedita aliquid dolorem repellat perferendis, facilis aut fugit, impedit.</p>
                            <a href="artikel.html">Baca lebih lanjut..</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>