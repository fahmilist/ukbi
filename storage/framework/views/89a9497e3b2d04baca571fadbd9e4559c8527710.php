<?php $__env->startSection('pageTitle','Dashboard'); ?>

<?php $__env->startPush('customCss'); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar" style="padding:10px 0px;">
            <div class="col-md-1 label-control"><span style="margin-top: 7px;font-size: 16px;font-weight: bold;">Filter</span></div>
            <form method="GET" action="<?php echo e(route('admin.dashboard')); ?>">
                <div class="col-md-4">
                    <div class="input-group input-date-range" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                        <input id="dateFrom" name="dateFrom" type="text" value="<?php echo e(@$dateFrom); ?>" class="datepickerinput form-control">
                        <span class="input-group-addon">To</span>
                        <input id="dateTo" name="dateTo" type="text" value="<?php echo e(@$dateTo); ?>" class="datepickerinput form-control">
                    </div>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-success">Filter</button>
                </div>
            </form>
            <div class="col-md-6 label-control" style="text-align: right;">
                <span style="margin-top: 7px;font-size: 16px;font-weight: bold;"><?php echo e(date('D, F Y')); ?></span>
            </div>
        </div>
        <!-- END PAGE BAR -->
        <!-- page content -->
        <div class="row portlet-body">
            <div class="col-lg-12 text-center">
                <br>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo e(route('users')); ?>">
                        <div class="visual">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span><?php echo e(number_format($user)); ?></span>
                            </div>
                            <div class="desc">Pendaftar</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                        <div class="visual">
                            <i class="fa fa-life-ring"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span><?php echo e(number_format($book)); ?></span>
                            </div>
                            <div class="desc">Buku</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                        <div class="visual">
                            <i class="fa fa-tags"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span><?php echo e(number_format($visitor)); ?></span>
                            </div>
                            <div class="desc">Pengunjung</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="container"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>

    <script src="<?php echo e(url('backend/assets/global/highchart/highcharts.js')); ?>"></script>
    <script src="<?php echo e(url('backend/assets/global/highchart/series-label.js')); ?>"></script>
    <script src="<?php echo e(url('backend/assets/global/highchart/exporting.js')); ?>"></script>
    <script src="<?php echo e(url('backend/assets/global/highchart/export-data.js')); ?>"></script>

    <script>
        var label = [];
        var value = [];

        <?php $__currentLoopData = $chart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $crt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        label.push('<?php echo e($crt['periode']); ?>');
        value.push(<?php echo e($crt['value']); ?>);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Grafik Kunjungan Setahun Terakhir'
            },
            subtitle: {
                text: 'Dalam Jumlah'
            },
            xAxis: {
                categories: label
            },
            yAxis: {
                title: {
                    text: 'Jumlah Kunjungan'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: true
                }
            },
            /*tooltip: {
                pointFormat: "Rp {point.y:.2f}"
            },*/
            series: [{
                name: 'Jumlah',
                data: value
            }]
        });
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>