<?php $__env->startSection('pageTitle','Pesanan'); ?>

<?php $__env->startPush('customCss'); ?>
    <link href="<?php echo e(url('backend/assets/global/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>">Beranda</a> <i class="fa fa-circle"></i></li>
                <li><span>Pesanan</span></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark"></i> <span
                                class="caption-subject font-dark bold uppercase">Daftar Pesanan</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12 col-lg-12" style="text-align: right;">
                                <a href="<?php echo e(route('order.export')); ?>" target="_blank" class="btn btn-success">Export</a>
                            </div>
                        </div>
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Customer</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Tanggal</th>
                                    <th style="min-width: 150px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td class="all">
                                    </td>
                                    <td class="all">
                                        <input placeholder="Cari Kode" type="text" class="form-control" name="s_code" onchange="filter()">
                                    </td>
                                    <td class="all" >
                                        <input placeholder="Cari Customer" type="text" class="form-control" name="s_customer" onchange="filter()">
                                    </td>
                                    <td class="all" >
                                    </td>
                                    <td class="all" >
                                        <select class="form-control" name="s_status" onchange="filter()">
                                            <option value="">Pilih Status</option>
                                            <?php $__currentLoopData = \App\Util\Constant::ORDER_STATUS; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value); ?>"><?php echo e($value); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </td>
                                    <td class="all" >
                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('backend.order.modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?> 

<?php $__env->startPush('customJs'); ?>

<script src="<?php echo e(url('backend/assets/global/datatable.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/datatables/datatables.min.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/table-datatables-responsive.min.js')); ?>"
    type="text/javascript"></script>

<script type="text/javascript">
    var table = $('#myTable').DataTable({
        'processing'  : true,
        'serverSide'  : true,
        'ajax'        : {
            url: "<?php echo e(route('order.data')); ?>",
            data: function (d) {
                d.code = $('[name=s_code]').val();
                d.customer = $('[name=s_customer]').val();
                d.status = $('[name=s_status]').val();
            }
        },
        'dataType'    : 'json',
        'searching'   : false,
        'paging'      : true,
        'lengthChange': true,
        'columns'     : [
            {data:'no', name: 'no'},
            {data:'code', name: 'code'},
            {data:'customer', name: 'customer'},
            {data:'totalPrice', name: 'totalPrice'},
            {data:'statusLabel', name: 'statusLabel'},
            {data:'created_at', name: 'created_at'},
            {data:'aksi', name: 'aksi', orderable: false, searchable: false},
        ],
        'info'        : true,
        'autoWidth'   : false
    });

    function filter() {
        table.draw();
    }

    function editData(id){
        $('#myModal form')[0].reset();
        $.ajax({
            url: "<?php echo e(route('order.get',['id'=>''])); ?>"+"/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#myModal').modal('show');
                $('.modal-title').text('Edit Data Pesanan');

                $('[name=id]').val(data.id);
                $('[name=status]').val(data.status);
                $('[name=code]').val(data.code);
                $('[name=note]').val(data.noteAdmin);
            },
            error: function(jqXHR, textStatus, errorThrown){
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '3000'
                });
            }
        });
    }

    $('#submit').click(function(e){
      e.preventDefault();
      var id = $('#id').val();
      url = "<?php echo e(route('order.save')); ?>";
      
      $('.form-group').removeClass('has-error');
      $('.help-block-error').html('');

      $('#myModal').modal('hide');

        swal({
            title: "Yakin Edit Data Pesanan?",
            text : "Data status akan dirubah, dan mengirim email",
            icon: "warning",
            buttons: {
                cancel:true,
                confirm: {
                    text:'Ubah!',
                    closeModal: false,
                },
            },
        })
        .then((process) => {
            if(process){
                $.ajax({
                    url:url,
                    type:'POST',
                    data: $('#myModal form').serialize(),
                    success: function(data){
                        if(data.status){
                            table.ajax.reload();
                            swal({
                                title: 'Berhasil Simpan Data Pesanan',
                                text: data.message,
                                icon: 'success',
                                timer: '1500'
                            });
                        }else{
                            swal({
                                title: 'Gagal Simpan Data Pesanan',
                                text: data.message,
                                icon: 'error',
                                timer: '1500'
                            });
                            var error_arr = ['status'];
                            for(var i=0;i < error_arr.length;i++){
                                if(error_arr[i] in data.error){
                                    $('#'+error_arr[i]).addClass('has-error');
                                    $('#'+error_arr[i]+'_error').html(data.error[error_arr[i]]);
                                }
                            }

                            $('#myModal').modal('show');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '1500'
                        });
                    }
                });
            }else{
                swal('Data pesanan tidak jadi diubah');
        }
      });
    });

    $('#note').hide();

    function setStatus() {
        if($('[name=status]').val() == '<?php echo e(\App\Util\Constant::ORDER_CANCELED); ?>'){
            $('#note').show();
        }else{
            $('#note').hide()
        }
    }

</script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>