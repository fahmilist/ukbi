	<link rel="icon" type="image/png" href="<?php echo e(url('storage/'.$CONF->icon)); ?>"/>

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/bootstrap/css/bootstrap.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/fonts/font-awesome-4.7.0/css/font-awesome.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/fonts/iconic/css/material-design-iconic-font.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/fonts/linearicons-v1.0.0/icon-font.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/animate/animate.css')); ?>">
	
	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/css-hamburgers/hamburgers.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/animsition/css/animsition.min.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/select2/select2.min.css')); ?>">
	
	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/daterangepicker/daterangepicker.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/slick/slick.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/MagnificPopup/magnific-popup.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/vendor/perfect-scrollbar/perfect-scrollbar.css')); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/css/util.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(url('frontend/css/main.css')); ?>">