<?php $__env->startSection('pageTitle','Member'); ?>

<?php $__env->startPush('customCss'); ?>
    <link href="<?php echo e(url('backend/assets/global/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>">Beranda</a> <i class="fa fa-circle"></i></li>
                <li><span>Member</span></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark"></i> <span
                                class="caption-subject font-dark bold uppercase">Daftar Member</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12 col-lg-12" style="text-align: right;">
                                <a href="<?php echo e(route('user.export')); ?>" target="_blank" class="btn btn-success">Export</a>
                                <button onclick="tambahData()" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Negara</th>
                                    <th>Pekerjaan</th>
                                    <th>Role</th>
                                    <th style="min-width: 150px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td class="all">
                                        <input placeholder="Cari Nama" type="text" class="form-control" name="s_name" onchange="filter()">
                                    </td>
                                    <td class="all" >
                                        <input placeholder="Cari Email" type="text" class="form-control" name="s_email" onchange="filter()">
                                    </td>
                                    <td class="all" >
                                        <input placeholder="Cari Telepon" type="text" class="form-control" name="s_phone" onchange="filter()">
                                    </td>
                                    <td class="all" >
                                        <select class="form-control" name="s_country" onchange="filter()">
                                            <option value="">Pilih Negara</option>
                                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($country->name); ?>"><?php echo e($country->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </td>
                                    <td class="all" >
                                        <select class="form-control" name="s_job" onchange="filter()">
                                            <option value="">Pilih Pekerjaan</option>
                                            <?php $__currentLoopData = $jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($job->name); ?>"><?php echo e($job->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </td>
                                    <td class="all" >
                                        <select class="form-control" name="s_role" onchange="filter()">
                                            <option value="">Pilih Role</option>
                                            <?php $__currentLoopData = \App\Util\Constant::USER_ROLES; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($role); ?>"><?php echo e($role); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('backend.user.modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?> 

<?php $__env->startPush('customJs'); ?>

<script src="<?php echo e(url('backend/assets/global/datatable.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/datatables/datatables.min.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/table-datatables-responsive.min.js')); ?>"
    type="text/javascript"></script>

<script type="text/javascript">
    var table = $('#myTable').DataTable({
        'processing'  : true,
        'serverSide'  : true,
        'ajax'        : {
            url: "<?php echo e(route('user.data')); ?>",
            data: function (d) {
                d.name = $('[name=s_name]').val();
                d.email = $('[name=s_email]').val();
                d.phone = $('[name=s_phone]').val();
                d.country = $('[name=s_country]').val();
                d.job = $('[name=s_job]').val();
                d.role = $('[name=s_role]').val();
            }
        },
        'dataType'    : 'json',
        'searching'   : false,
        'paging'      : true,
        'lengthChange': true,
        'columns'     : [
            {data:'name', name: 'name'},
            {data:'email', name: 'email'},
            {data:'phone', name: 'phone'},
            {data:'country', name: 'country'},
            {data:'job', name: 'job'},
            {data:'role', name: 'role'},
            {data:'aksi', name: 'aksi', orderable: false, searchable: false},
        ],
        'info'        : true,
        'autoWidth'   : false
    });

    function filter() {
        table.draw();
    }

    function tambahData() {
        $('#myModal').modal('show');
        $('#myModal form')[0].reset();
        $('[name=id]').val(0);
        $('[name=email]').attr('disabled',false);
        $('[name=password]').attr('placeholder','Masukan password');
        $('[name=method]').val('ADD');
        $('.modal-title').text('Tambah Data Pengguna');
    }

    function editPengguna(id){
        $('#myModal form')[0].reset();
        $('[name=password]').attr('placeholder','Kosongkan password jika tidak ingin mengganti');
        $('[name=email]').attr('disabled',true);
        $('[name=method]').val('EDIT');
        $.ajax({
            url: "<?php echo e(route('user.get',['id'=>''])); ?>"+"/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#myModal').modal('show');
                $('.modal-title').text('Edit Data Pengguna');

                $('[name=id]').val(data.id);
                $('[name=name]').val(data.name);
                $('[name=email]').val(data.email);
                $('[name=phone]').val(data.phone);
                $('[name=role]').val(data.role);
                $('[name=countryId]').val(data.countryId);
                $('[name=jobId]').val(data.jobId);
            },
            error: function(jqXHR, textStatus, errorThrown){
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '3000'
                });
            }
        });
    }

    function deletePengguna(id) {
        swal({
            title: "Yakin Hapus Data Member?",
            text : "Data member akan dihapus permanen",
            icon: "warning",
            buttons: {
                cancel:true,
                confirm: {
                    text:'Hapus!',
                    closeModal: false,
                },
              },
            })
        .then((process) => {
            if(process){
                $.ajax({
                    url: "<?php echo e(route('user.delete',['id'=>''])); ?>" + '/' + id,
                    type: "POST",
                    data: {
                        '_token': '<?php echo e(csrf_token()); ?>' 
                    },
                    success: function(data) {
                        swal({
                            title: 'Berhasil Hapus Member!',
                            text: 'Member berhasil di hapus',
                            icon: 'success',
                            timer: '1500'
                        });
                        table.ajax.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '1500'
                        });
                    }
                });
            }else{
                swal('Data member tidak jadi dihapus');
            }
        });
    }

    $('#submit').click(function(e){
      e.preventDefault();
      var id = $('#id').val();
      url = "<?php echo e(route('user.save')); ?>";
      
      $('.form-group').removeClass('has-error');
      $('.help-block-error').html('');

      $('#myModal').modal('hide');

      $.ajax({
        url:url,
        type:'POST',
          // data: $('#myModal form').serialize(),
        data: $('#myModal form').serialize(),
        success: function(data){
            if(data.status){
                table.ajax.reload();
                swal({
                    title: 'Berhasil Simpan Data Member',
                    text: data.message,
                    icon: 'success',
                    timer: '1500'
                });
            }else{
                swal({
                    title: 'Gagal Simpan Data Member',
                    text: data.message,
                    icon: 'error',
                    timer: '1500'
                });
                var error_arr = ['email','name','password','role','countryId','jobId'];
                for(var i=0;i < error_arr.length;i++){
                    if(error_arr[i] in data.error){
                        $('#'+error_arr[i]).addClass('has-error');
                        $('#'+error_arr[i]+'_error').html(data.error[error_arr[i]]);
                    }
                }

                $('#myModal').modal('show');
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            swal({
                title: 'System Error',
                text: errorThrown,
                icon: 'error',
                timer: '1500'
            });
        }
      });
    });

</script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>