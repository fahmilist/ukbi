<!-- Page Login -->
    <div class="modal fade bs-modal-sm log-sign" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">                
                <div class="bs-example bs-example-tabs">
                    <ul id="myTab" class="nav nav-tabs">
                        <li id="tab1" class="active tab-style login-shadow"><a href="#signin" data-toggle="tab">Masuk</a></li>
                        <li id="tab2" class="tab-style"><a href="#signup" data-toggle="tab">Daftar</a></li>                      
                    </ul>
                </div>  
                <div class="modal-body">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="signin">
                        <form class="form-horizontal" action="<?php echo e(route('login')); ?>" id="formConfirm" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <?php if($errors->has('email') || $errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <?php echo e(csrf_field()); ?>

                            <!-- Sign In Form -->
                            <!-- Text input-->
                                <div class="group">
                                    <input required class="input" type="text" name="email">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label class="label" for="email">Email</label>
                                </div>
                                <!-- Password input-->
                                <div class="group">
                                    <input required="" class="input" type="password" name="password">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label class="label" for="date">Password</label>
                                </div>
                                <em>minimal 6 karakter</em>

                                <div class="forgot-link">
                                    <a href="#forgot" data-toggle="modal" data-target="#forgot-password">Lupa kata sandi?</a>
                                </div>

                                <!-- Button -->
                                <div class="control-group">
                                    <label class="control-label" for="signin"></label>
                                    <div class="controls">
                                        <button id="signin" name="signin" class="btn btn-primary btn-block">Masuk</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    
                    <div class="tab-pane fade" id="signup">
                        <form class="form-horizontal" action="<?php echo e(route('save.registration')); ?>" method="post">
                            <fieldset>
                            <!-- Sign Up Form -->
                            <!-- Text input-->
                            <?php echo e(csrf_field()); ?>

                            <div class="group">
                                <input required="" class="input" type="text" name="name">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label class="label" for="date">Nama Lengkap</label>
                            </div>
                            <!-- Password input-->
                            <div class="group">
                                <input required="" class="input" type="email" name="email">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label class="label" for="date">Email</label>
                            </div>
                            <!-- Text input-->
                            <div class="group">
                                <input required="" class="input" type="password" name="password">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label class="label" for="date">Kata Sandi</label>
                            </div>
                            <div class="group">
                                <input required="" class="input" type="number" name="phone">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label class="label" for="date">Telepon</label>
                            </div>
                            <div class="group2">
                                <label class="label2" for="date">Negara</label>
                                <select name="countryId" class="bar form-control" required="">
                                    <option value="">-- Pilih Negara --</option>
                                    <?php $__currentLoopData = \App\Country::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($country->id); ?>"><?php echo e($country->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                            <div class="group2">
                                <label class="label2" for="date">Pekerjaan</label>
                                <select name="jobId" class="bar form-control" required="">
                                    <option value="">-- Pilih Pekerjaan --</option>
                                    <?php $__currentLoopData = \App\Job::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($job->id); ?>"><?php echo e($job->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                            <!-- Button -->
                            <div class="control-group">
                                <label class="control-label" for="confirmsignup"></label>
                                <div class="controls">
                                    <button id="confirmsignup" name="confirmsignup" class="btn btn-primary btn-block">Daftar</button>
                                </div>
                            </div>

                            </fieldset>
                        </form>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer">
                <center>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </center>
                </div>-->
                </div>
            </div>
        </div>
            
        <!--modal2-->
        <div class="modal fade bs-modal-sm" id="forgot-password" tabindex="0" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Password will be sent to your email</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>
                        <div class="group">
                            <input required="" class="input" type="text">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="label" for="date">Email address</label>
                        </div>
                  
                  
                        <div class="control-group">
                            <label class="control-label" for="forpassword"></label>
                            <div class="controls">
                                <button id="forpassword" name="forpassword" class="btn btn-primary btn-block">Send</button>
                            </div>
                        </div>
                        </fieldset>
                        </form>                    
                    </div>
                </div>              
            </div>
        </div>