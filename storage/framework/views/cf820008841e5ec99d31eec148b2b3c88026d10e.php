<?php $__env->startSection('pageTitle','Beranda'); ?>

<?php $__env->startPush('customCss'); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

	<!-- Page Beranda -->
    <header class="header-area overlay full-height relative v-center" id="beranda-page">
        <div class="absolute anlge-bg"></div>        
        <div class="container">            
            <div class="row v-center">                
				<div class="col-md-6">
					<div class="header-text">
						<h2><?php echo e(@$CONF->titleBanner); ?></h2>
					</div>
                    <p><?php echo e(@$CONF->descriptionBanner); ?></p>
                </div>
				<div class="col-md-6">
					<div id="about-slider" class="screen-box screen-slider home-slider">
                        <?php $__currentLoopData = @$books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item">
                            <img src="<?php echo e(url('storage/'.$book->featured)); ?>" alt="<?php echo e($book->name); ?>">
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
            </div>
        </div>
    </header>

    <!-- Page Tentang -->
    <section class="gray-bg section-padding" id="tentang-page">
        <div class="container">
                <!-- why choose us content -->
				<div class="col-md-6">
					<div class="page-title2">
						<h2>Tentang Kami</h2>
					</div>
					<p class="text-justify"><?php echo e(json_decode(@$CONF->aboutDetail)->content1); ?></p>
					
				</div>
				<!-- /why choose us content -->

				<!-- About slider -->
				<div class="col-md-6">
					<div id="about-slider" class="screen-box about-slider screen-slider">
                        <div class="item">
                            <img src="<?php echo e(url('storage/'.json_decode(@$CONF->aboutDetail)->image1)); ?>" alt="" style="">
                        </div>
                        <div class="item">
                            <img src="<?php echo e(url('storage/'.json_decode(@$CONF->aboutDetail)->image2)); ?>" alt="" style="">
                        </div>
					</div>
				</div>
				<!-- /About slider -->
            </div>
        </div>
    </section>

	<!-- Page Tentang -->
    <section class="angle-bg sky-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <?php $__currentLoopData = @$books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2><?php echo e(@$book->name); ?></h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p><?php echo e(@$book->description); ?></p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="<?php echo e(route('book.detail',['id'=>@$book->id,'name'=>@$book->folder])); ?>" class="button">Baca Selengkapnya</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="<?php echo e(url('storage/'.@$book->image)); ?>" alt="<?php echo e(@$book->name); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators caption-indector">
                            <?php $__currentLoopData = @$books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li data-target="#caption_slide" data-slide-to="<?php echo e($key); ?>" <?php echo e($key==0?'class="active"':''); ?>>
                                <strong><?php echo e($key+1); ?> </strong><?php echo e(@$book->name); ?>

                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Statistik -->
    <section class="gray-bg section-padding" id="statistik-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Statistik</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3><?php echo e(\App\Tracker::count()); ?></h3>
                        </div>
                        <h3>Jumlah Pengunjung</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3><?php echo e(\App\Book::count()); ?></h3>
                        </div>
                        <h3>Jumlah Buku</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box">
                        <div class="stat-count">
                            <h3><?php echo e(\App\User::where('role',\App\Util\Constant::USER_ROLE_USER)->count()); ?></h3>
                        </div>
                        <h3>Jumlah Pendaftar</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas nulla est adipisci,</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Pengurus -->
    <section class="section-padding sky-bg client-area overlay" id="pengurus-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Pengurus</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                <div class="clients">
                <!-- <div class="col-xs-12 col-sm-6 col-md-3"> -->
                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">
                        <div class="single-team">
                        <div class="team-photo">
                            <img src="<?php echo e(url('storage/'.$team->image)); ?>" alt="<?php echo e(@$team->name); ?>">
                        </div>
                        <h5><?php echo e(@$team->name); ?></h5>
                        <h6 class="role"><?php echo e(@$team->role); ?></h6>
                        <h6 class="division"><?php echo e((@$team->division)); ?></h6>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- </div> -->
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Blog -->
    <section class="section-padding gray-bg" id="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Artikel</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row blog-row">
                <?php $__currentLoopData = @$blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-xs-12 col-sm-4 blog-col">
                    <div class="single-blog">
                        <div class="blog-photo">
                            <img src="<?php echo e(url('storage/'.$blog->featured)); ?>" alt="<?php echo e(@$blog->title); ?>">
                        </div>
                        <div class="blog-content">
                            <h3><?php echo e(@$blog->title); ?></h3>
                            <ul class="blog-meta">
                                <li><span class="fa fa-user"></span> <?php echo e(@$blog->creator->name); ?></li>
                                <li><span class="fa fa-calendar"></span> <?php echo e(\Carbon\Carbon::parse(@$blog->created_at)->format('d F, Y')); ?></li>
                            </ul>
                            <p><?php echo e(@$blog->description); ?></p>
                            <a href="<?php echo e(route('blog.detail',['id'=>@$blog->id,'title'=>str_replace(' ','-',@$blog->title)])); ?>">Baca Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>