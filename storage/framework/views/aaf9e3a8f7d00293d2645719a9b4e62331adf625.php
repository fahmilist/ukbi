<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed "
			data-keep-expanded="false" data-auto-scroll="true"
			data-slide-speed="200" style="padding-top: 20px">
			<li class="nav-item <?php echo e(@$sidebar === 'dashboard'? 'active' : ''); ?> "><a
				href="<?php echo e(url('admin')); ?>" class="nav-link nav-toggle"> <i
					class="icon-home"></i> <span class="title">Beranda</span> <span
					class="arrow <?php echo e(@$sidebar === 'dashboard'? '' : 'hidden'); ?>"></span>
			</a></li>
			<?php if(\Auth::user()->role == \App\Util\Constant::USER_ROLE_ADMIN): ?>
			<li class="nav-item <?php echo e(@$sidebar === 'user'? 'active' : ''); ?>"><a
				href="<?php echo e(route('users')); ?>" class="nav-link nav-toggle"> <i
					class="fa fa-users"></i> <span class="title">Member</span> <span
					class="arrow <?php echo e(@$sidebar === 'user'? '' : 'hidden'); ?>"></span>
			</a></li>
			<li class="nav-item <?php echo e(@$sidebar === 'category'? 'active' : ''); ?>"><a
				href="<?php echo e(route('categories')); ?>" class="nav-link nav-toggle"> <i
					class="fa fa-life-ring"></i> <span class="title">Kategori</span> <span
					class="arrow <?php echo e(@$sidebar === 'category'? '' : 'hidden'); ?>"></span>
			</a></li>
			<li class="nav-item <?php echo e(@$sidebar === 'product'? 'active' : ''); ?> "><a
				href="<?php echo e(route('products')); ?>" class="nav-link nav-toggle"> <i
					class="fa fa-tags"></i> <span class="title">Produk</span> <span
					class="arrow <?php echo e(@$sidebar === 'product'? '' : 'hidden'); ?>"></span>
			</a></li>
			<?php endif; ?>
			<li class="nav-item <?php echo e(@$sidebar === 'order'? 'active' : ''); ?>"><a
				href="<?php echo e(route('orders')); ?>" class="nav-link nav-toggle"> <i
					class="fa fa-shopping-cart"></i> <span class="title">Pesanan</span>
					<span class="arrow <?php echo e(@$sidebar === 'order'? '' : 'hidden'); ?>"></span>
			</a></li>
			<?php if(\Auth::user()->role == \App\Util\Constant::USER_ROLE_ADMIN): ?>
			<li class="nav-item <?php echo e(@$sidebar === 'setting'? 'active' : ''); ?>">
				<a href="javascript" class="nav-link nav-toggle"> <i
						class="fa fa-gears"></i> <span class="title">Pengaturan</span>
				<span class="arrow <?php echo e(@$sidebar === 'setting'? '' : 'hidden'); ?>"></span>
			</a>
				<ul class="sub-menu">
					<li>
						<a href="<?php echo e(route('settings')); ?>">
							<i class="icon-share"></i>
							Umum</a>
					</li>
					<li>
						<a href="<?php echo e(route('abouts')); ?>">
							<i class="icon-bulb"></i>
							Tentang</a>
					</li>
					<li>
						<a href="<?php echo e(route('faqs')); ?>">
							<i class="icon-question"></i>
							Bantuan & FAQ</a>
					</li>
					<li>
						<a href="<?php echo e(route('sliders')); ?>">
							<i class="icon-picture"></i>
							Slider</a>
					</li>
				</ul>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</div>