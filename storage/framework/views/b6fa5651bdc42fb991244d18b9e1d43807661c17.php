<?php $__env->startSection('pageTitle','Detail Artikel'); ?>

<?php $__env->startPush('customCss'); ?>
    <link rel="stylesheet" href="<?php echo e(url('frontend/css/artikel-style.css')); ?>">
    <style type="text/css">
        .mainmenu-area.affix-top {
            background-color: #1e3163;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div id="artikel-page" class="section md-padding">
        <div class="container">
            <div class="row v-center">
                <div class="col-xs-12 col-md-7 header-text">
                    <h2><?php echo e(@$blog->title); ?></h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Beranda</a></li>
                        <li class="breadcrumb-item active"><?php echo e(@$blog->title); ?></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Container -->
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Main -->
                <main id="main" class="col-md-9">
                    <div class="blog">
                        <?php if(!empty($blog->image)): ?>
                        <div class="blog-img">
                            <img class="img-responsive" src="<?php echo e(url('storage/'.$blog->image)); ?>" alt="<?php echo e(@$blog->title); ?>">
                        </div>
                        <?php endif; ?>
                        <div class="blog-content">
                            <ul class="blog-meta">
                                <li><i class="fa fa-user"></i><?php echo e(@$blog->creator->name); ?></li>
                                <li><i class="fa fa-clock-o"></i><?php echo e(\Carbon\Carbon::parse(@$blog->created_at)->format('D F, Y')); ?></li>
                            </ul>
                            <?php echo @$blog->content; ?>

                        </div>
                    </div>
                </main>
                <!-- /Main -->
                <!-- Aside -->
                <aside id="aside" class="col-md-3">
                    
                    <!-- Posts sidebar -->

                    <div class="widget">
                        <h3 class="title">Artikel Lainnya</h3>
                        <!-- single post -->
                        <?php $__currentLoopData = @$others; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $other): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="widget-post">
                            <a href="<?php echo e(route('blog.detail',['id'=>@$other->id,'title'=>str_replace(' ','-',@$other->title)])); ?>">
                                <img src="<?php echo e(url('storage/'.$other->featured)); ?>" alt="<?php echo e(@$other->title); ?>" width="100" height="100"> <?php echo e(@$other->title); ?>

                            </a>
                            <ul class="blog-meta">
                                <li><?php echo e(\Carbon\Carbon::parse(@$other->created_at)->format('D F, Y')); ?></li>
                            </ul>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <!-- /single post -->
                    </div>
                    <!-- /Posts sidebar -->
                </aside>
                <!-- /Aside -->
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>