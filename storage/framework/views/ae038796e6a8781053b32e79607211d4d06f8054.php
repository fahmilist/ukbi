<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <form id="form-invoice" class="form-horizontal" action="#" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id" id="id" value="0">
            <input type="hidden" name="method" id="method">
            <div class="form-group" id="email">
              <label class="control-label col-sm-2" for="email">Email</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="email" placeholder="Masukan Email" required = "true">
                <div id="email_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="name">
              <label class="control-label col-sm-2" for="name">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="name" placeholder="Masukan Nama Lengkap" required = "true">
                <div id="name_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="phone">
              <label class="control-label col-sm-2" for="phone">Telepon</label>
              <div class="col-sm-10">
                <input type="phone" class="form-control" name="phone" placeholder="Masukan Telepon" required = "true">
                <div id="phone_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="password">
              <label class="control-label col-sm-2" for="password">Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" name="password" placeholder="Masukan Password (kosongkan jika tidak ingin ganti password)" required = "true">
                <div id="password_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="role">
              <label class="control-label col-sm-2" for="role">Role</label>
              <div class="col-sm-10">
                <select class="form-control" name="role">
                  <option value="">Pilih role</option>
                  <?php $__currentLoopData = \App\Util\Constant::USER_ROLES; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($role); ?>"><?php echo e($role); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <div id="role_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="countryId">
              <label class="control-label col-sm-2" for="countryId">Negara</label>
              <div class="col-sm-10">
                <select class="form-control" name="countryId">
                  <option value="">Pilih Provinsi</option>
                  <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($country->id); ?>" data-object="<?php echo e($country); ?>"><?php echo e($country->name); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <div id="countryId_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="jobId">
              <label class="control-label col-sm-2" for="jobId">Pekerjaan</label>
              <div class="col-sm-10">
                <select class="form-control" name="jobId">
                  <option value="">Pilih Pekerjaan</option>
                  <?php $__currentLoopData = $jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($job->id); ?>" data-object="<?php echo e($job); ?>"><?php echo e($job->name); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <div id="jobId_error" class="help-block help-block-error"> </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button id="submit" type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>

    </div>
  </div>
<!-- End Modal -->