<?php $__env->startSection('pageTitle','Login'); ?>

<?php $__env->startPush('customCss'); ?>
    <style>
        .error-auth{
            text-align: center;
            padding: 20px;
            background-color: #ffdada;
            color: #7c7c7c
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="<?php echo e(route('home')); ?>" class="stext-109 cl8 hov-cl1 trans-04">
                Beranda
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
				Login
			</span>
        </div>
    </div>

    <!-- Login Section -->
    <form action="<?php echo e(route('login')); ?>" id="formConfirm" method="post" enctype="multipart/form-data" class="bg0 p-t-75 p-b-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                            <h4 class="mtext-109 cl2 p-b-30">
                                Masuk
                            </h4>

                            <?php if($errors->has('username') || $errors->has('password')): ?>
                            <div class="bor8 bg0 m-b-12 error-auth">
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('username')); ?></strong>
                                </span>
                            </div>
                            <?php endif; ?>

                            <?php echo e(csrf_field()); ?>

                            <div class="bor8 bg0 m-b-12">
                                <input id="username" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Username" name="username" value="<?php echo e(old('username')); ?>" required autofocus>
                            </div>
                            <div class="bor8 bg0 m-b-12">
                                <input id="password" type="password" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Password" name="password" required>
                            </div>
                            <div class="bg0 m-b-12">
                                <small><a href="<?php echo e(route('password.request')); ?>">Lupa Password?</a></small>
                            </div>
                        </div>

                        <div class="flex-w flex-sb-m p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="float: right">
                            <button type="submit" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                Masuk
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3 p-b-80">
                    <div class="side-menu">
                        <div class="bor17 of-hidden pos-relative">
                            <form action="<?php echo e(route('product')); ?>" method="get">
                                <input class="stext-103 cl2 plh4 size-116 p-l-28 p-r-55" type="text" name="keyword" placeholder="Cari Produk">

                                <button type="submit" class="flex-c-m size-122 ab-t-r fs-18 cl4 hov-cl1 trans-04">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                        </div>

                        <div class="p-t-55">
                            <h4 class="mtext-112 cl2 p-b-33">
                                Kategori
                            </h4>

                            <ul>
                                <?php $__currentLoopData = $resultCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="bor18">
                                        <a href="<?php echo e(route('product').'?category='.$category->id); ?>" class="dis-block stext-115 cl6 hov-cl1 trans-04 p-tb-8 p-lr-4">
                                            <?php echo e(ucwords($category->name.' ('.$category->total.')')); ?>

                                        </a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>