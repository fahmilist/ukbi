<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <form id="form-invoice" class="form-horizontal" action="#" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id" id="id" value="0">
            <div class="form-group" id="code">
              <label class="control-label col-sm-2" for="code">Kode Pesanan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="code" placeholder="Kode Pesanan" disabled required = "true">
                <div id="code_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="status">
              <label class="control-label col-sm-2" for="status">Status</label>
              <div class="col-sm-10">
                <select class="form-control" name="status" onchange="setStatus()">
                  <?php $__currentLoopData = \App\Util\Constant::ORDER_STATUS; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($status); ?>"><?php echo e($value); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <div id="status_error" class="help-block help-block-error"> </div>
              </div>
            </div>
            <div class="form-group" id="note">
              <label class="control-label col-sm-2" for="note">Kode Pesanan</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="note" placeholder="Catatan"></textarea>
                <div id="note_error" class="help-block help-block-error"> </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button id="submit" type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>
      </div>

    </div>
  </div>
<!-- End Modal -->