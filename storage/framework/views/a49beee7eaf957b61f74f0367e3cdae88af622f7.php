<?php $__env->startSection('content'); ?>
<table>
    <tr>
        <td  class="main-header" style="color: #484848; font-size: 14px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
            Informasi pesanan <b><?php echo e(strtoupper($order->code)); ?></b>, berikut penjelasan detail pesanan:
        </td>
    </tr>
    <tr>
        <td>
            <b>Detil Pesanan</b>
            <table style="margin-top:10px;width: 100%">
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Tanggal Pemesanan</th>
                    <td><?php echo e(date('l, d F Y (H:i)',strtotime($order->created_at))); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Pengiriman</th>
                    <td><?php echo e($order->shippingType.' - '.$order->shippingMethod); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Subtotal</th>
                    <td>Rp. <?php echo e(number_format($order->amount)); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Shipping</th>
                    <td>Rp. <?php echo e(number_format($order->shippingFee)); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Unique Code</th>
                    <td>Rp. <?php echo e(number_format($order->uniquePrice)); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Total</th>
                    <td>Rp. <?php echo e(number_format($order->totalPrice)); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Keterangan</th>
                    <td><?php echo e($order->note); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td class="break"></td></tr>
    <tr>
        <td>
            <b>Detil Konfirmasi</b>
            <table style="margin-top:10px;width: 100%">
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Tanggal Konfirmasi</th>
                    <td><?php echo e(date('l, d F Y',strtotime($order->orderConfirmation->first()->confirmationDate))); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Nama Rekening</th>
                    <td><?php echo e($order->orderConfirmation->first()->name); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Nomor Rekening</th>
                    <td><?php echo e($order->orderConfirmation->first()->accountNumber.' ('.$order->orderConfirmation->first()->bankName.')'); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Jumlah Pembayaran</th>
                    <td>Rp. <?php echo e(number_format($order->orderConfirmation->first()->amount)); ?></td>
                </tr>
                <tr style="background-color:#eee;">
                    <th style="text-align: left">Bukti Bayar</th>
                    <td><a href="<?php echo e(url('storage/'.$order->orderConfirmation->first()->image)); ?>">Lihat</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td class="break"></td></tr>
    <tr>
        <td>
            <b>Detil Produk</b>
            <table style="margin-top:10px;width: 100%;text-align: center;" border="1px #000 solid">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Varian</th>
                    <th>Jumlah</th>
                </tr>
                <?php $__currentLoopData = $order->orderProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($key+1); ?></td>
                    <td><?php echo e(ucwords($orderProduct->product->name)); ?></td>
                    <td>Rp <?php echo e(number_format($orderProduct->product->price)); ?></td>
                    <td><?php echo e(ucwords($orderProduct->color.' | '.$orderProduct->size)); ?></td>
                    <td><?php echo e(ucwords($orderProduct->qty)); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>
        </td>
    </tr>
    <tr><td class="break"></td></tr>
    <tr>
        <td>
            <b>Alamat tujuan pengiriman</b>
            <br>
            <?php echo e(ucwords($order->user->name)); ?>

            <br>
            <?php echo e($order->orderAddress->address); ?>

            <br>
            <?php echo e($order->orderAddress->kecamatan->nama); ?>

            <br>
            <?php echo e($order->orderAddress->kabupaten->nama.' - '.$order->orderAddress->provinsi->nama.' ( '.$order->orderAddress->postcode.' )'); ?>

            <br>
            <?php echo e($order->user->phone); ?>

        </td>
    </tr>
</table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.pdf', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>