<?php $__env->startSection('pageTitle','Detail Buku'); ?>

<?php $__env->startPush('customCss'); ?>
    <style type="text/css">
        .mainmenu-area.affix-top {
            background-color: #1e3163;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <section class="gray-bg section-padding" id="tentang-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                    <div class="page-title">
                        <h2><?php echo e(@$book->name); ?></h2>
                        <?php echo @$book->content; ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h3>Daftar Isi</h3>
                    <ul class="nav">
                        <?php $__currentLoopData = @$book->page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="javascript:;" onclick="setPage(<?php echo e(@$page->page); ?>)"><?php echo e(@$page->name); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                    <hr>
                    <h3>Cari Halaman</h3>
                    <ul class="search-form">
                        <li><input type="number" class="form-control" name="searchPage"></li>
                        <li><button class="btn btn-md btn-info form-control" style="height: 46px;margin-left: 4px;" type="button" onclick="setPage(0)">Go</button></li>
                    </ul>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="t">
                        <div class="tc rel">
                            <div class="book" id="book">
                                <div class="page"><img src="<?php echo e(url('storage/'.$book->featured)); ?>" alt="" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>
    <!-- Turn JS -->
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/extras/jquery.mousewheel.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/lib/hash.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/lib/turn.min.js')); ?>"></script>

    <script type="text/javascript">

        var book = $("#book");
        var currentPage = 0;
        var totalPage = 25;

        (function () {
            'use strict';

            var module = {
                ratio: 1.38,
                init: function (id) {
                    var me = this;

                    // if older browser then don't run javascript
                    if (document.addEventListener) {
                        this.el = document.getElementById(id);
                        this.resize();
                        this.plugins();

                        // on window resize, update the plugin size
                        window.addEventListener('resize', function (e) {
                            var size = me.resize();
                            $(me.el).turn('size', size.width, size.height);
                        });
                    }
                },
                resize: function () {
                    // reset the width and height to the css defaults
                    this.el.style.width = '';
                    this.el.style.height = '';

                    var width = this.el.clientWidth,
                        height = Math.round(width / this.ratio),
                        padded = Math.round(document.body.clientHeight * 0.9);

                    // if the height is too big for the window, constrain it
                    if (height > padded) {
                        height = padded;
                        width = Math.round(height * this.ratio);
                    }

                    // set the width and height matching the aspect ratio
                    this.el.style.width = width + 'px';
                    this.el.style.height = height + 'px';

                    return {
                        width: width,
                        height: height
                    };
                },
                plugins: function () {
                    // run the plugin
                    $(this.el).turn({
                        gradients: true,
                        acceleration: true
                    });
                    // hide the body overflow
                    document.body.className = 'hide-overflow';
                }
            };

            module.init('book');
        }());

        book.bind("turning", function(event, page, view) {
            var check = page+5;
            if(page>=totalPage) {
                book.turn('stop');
            }else if(check>=totalPage){
                increasePage(page,totalPage-page);
            }else{
                increasePage(page,5);
            }

            book.turn('resize');
        });

        book.bind("missing", function(event, pages) {
            for (var i = 0; i < pages.length; i++) {
                $(this).turn("addPage", $("<div />"), pages[i]);
            }
        });

        $(document).keydown(function(e){

            var previous = 37, next = 39;

            switch (e.keyCode) {
                case previous:

                    book.turn('previous');

                    break;
                case next:

                    book.turn('next');

                    break;
            }

        });

        function checkPage(){
            var range = $("#flipbook").turn("range", 10);

            for (var page = range[0]; page<=range[1]; page++){
                if (!book.turn("hasPage", page)) {
                    addPage(page);
                }
            }
        }

        /*book.bind("end", function(event, pageObject, turned){
            event.preventDefault();
        });*/

        function increasePage(page,num){
            for (var i=page;i<(page+num);i++){
                addPage(i);
            }
        }

        function removePage(page){
            book.turn('removePage',page);
        }

        increasePage(book.turn('page'),6);

        function loadPage(page) {

            var img = $('<img />');
            img.load(function() {
                var container = $('.book .p'+page);
                img.css({width: '100%', height: '100%'});
                img.appendTo($('.book .p'+page));
            });

            img.attr('src', '<?php echo e(url('frontend/book-preview/books')); ?>/' +  page + '.png');

        }

        function addPage(page) {

            var element = $('<div />', {});

            if (book.turn('addPage', element, page)) {
                loadPage(page);
            }
        }

        function setPage(page) {
            if(page == 0 ) page = $('[name=searchPage]').val();
            var current = book.turn('page');
            console.log(page);
            if(page>totalPage) return;
            if(page>current){
                var num = (page-current)+3;
                increasePage(current,num);
                if (!book.turn("hasPage", page)) {
                    addPage(page);
                }
                book.turn("page", page);
            }else{
                if (!book.turn("hasPage", page)) {
                    addPage(page);
                }
                book.turn("page", page);
            }
        }
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>