<?php $__env->startSection('pageTitle','Profil Saya'); ?>

<?php $__env->startPush('customCss'); ?>
    <style>
        .stepwizard-step p {
            margin-top: 10px;
        }

        .stepwizard-row {
            display: table-row;
        }

        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }

        .stepwizard-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }

        .help-block {
            color:#ff5e5e;
            font-size: 0.9em;
        }

        .btn-preview{
            margin-top: 24px;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="<?php echo e(route('home')); ?>" class="stext-109 cl8 hov-cl1 trans-04">
                Beranda
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="<?php echo e(route('login')); ?>" class="stext-109 cl8 hov-cl1 trans-04">
                Member
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
				Profil
			</span>
        </div>
    </div>

    <!-- Profile Section -->
    <form action="<?php echo e(route('member.profile')); ?>" id="formMember" method="post" enctype="multipart/form-data" class="bg0 p-t-75 p-b-85">
        <div class="container">
            <?php echo e(csrf_field()); ?>

            <div class="row" style="margin-bottom: 20px;">
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" class="btn btn-primary btn-circle"><i class="fa fa-user"></i></a>
                            <p>Akun</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-university"></i></a>
                            <p>Sekolah</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-3" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-file"></i></a>
                            <p>Dokumen</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-4" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-life-ring"></i></a>
                            <p>Administrasi</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-1">
                <div class="col-lg-12 col-xl-9 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                            <h4 class="mtext-109 cl2 p-b-30">
                                Informasi Akun
                            </h4>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Username</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="username" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" disabled placeholder="Username" name="username" value="<?php echo e(@$member->username); ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="password" type="password" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Kosongkan jika tidak mengganti password" name="password">
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Nama</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="name" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Nama Sekolah" name="name" value="<?php echo e(@$member->name); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_name"></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="email" type="email" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Email Akun untuk pengiriman notifikasi" name="email" required value="<?php echo e(@$member->email); ?>">
                                    </div>
                                    <div class="help-block" id="error_email"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Telepon</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="phone" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Telepon Akun untuk pengiriman notifikasi" name="phone" value="<?php echo e(@$member->phone); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_phone"></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Alamat</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <textarea id="address" class="stext-111 cl8 plh3 size-111 p-lr-15" rows="5" placeholder="Alamat" name="address"><?php echo e(@$member->address); ?></textarea>
                                    </div>
                                    <div class="help-block" id="error_address"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Provinsi</label>
                                    <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
                                        <select class="js-select2" name="provinceId" onchange="getKabupaten()">
                                            <option value="">Pilih Provinsi</option>
                                            <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prov): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($prov->id); ?>" <?php echo e(@$member->provinceId==$prov->id?'selected':''); ?> ><?php echo e($prov->nama); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="help-block" id="error_provinceId"></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Kota</label>
                                    <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
                                        <select class="js-select2" name="cityId" onchange="getKecamatan()">
                                            <option value="">Pilih Kota</option>
                                            <?php if(!empty($member->provinsi)): ?>
                                            <?php $__currentLoopData = $member->provinsi->kabupaten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($city->id); ?>" <?php echo e($city->id == $member->cityId ? 'selected':''); ?>><?php echo e($city->nama); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="help-block" id="error_cityId"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label>Kecamatan</label>
                                    <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
                                        <select class="js-select2" name="districtId">
                                            <option value="">Pilih Kecamatan </option>
                                            <?php if(!empty($member->kabupaten)): ?>
                                            <?php $__currentLoopData = $member->kabupaten->kecamatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($district->id); ?>" <?php echo e($district->id == $member->districtId ? 'selected':''); ?>><?php echo e($district->nama); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="help-block" id="error_districtId"></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Kelurahan</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="subdistrict" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Kelurahan" name="subdistrict" value="<?php echo e(@$member->subdistrict); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_subdistrict"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Kode Pos</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="postcode" type="text" maxlength="5" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Kode Pos" name="postcode" value="<?php echo e(@$member->postcode); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_postcode"></div>
                                </div>
                            </div>
                        </div>

                        <div class="flex-w flex-sb-m p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="float: right">
                            <a href="#step-2" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10 navStep">
                                Selanjutnya
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row setup-content" id="step-2">
                <div class="col-lg-12 col-xl-9 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                            <h4 class="mtext-109 cl2 p-b-30">
                                Informasi Sekolah
                            </h4>

                            <div class="alert alert-danger hidden" id="step-2-error"></div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Nama</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="memberName" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Nama Sekolah" name="memberName" value="<?php echo e(@$member->member->memberName); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_memberName"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Email</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="memberEmail" type="email" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Email Sekolah" name="memberEmail" value="<?php echo e(@$member->member->memberEmail); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_memberEmail"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Telepon</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="memberPhone" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Telepon Sekolah" name="memberPhone" value="<?php echo e(@$member->member->memberPhone); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_memberPhone"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Fax</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="fax" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Fax Sekolah" name="fax" value="<?php echo e(@$member->member->fax); ?>" required>
                                    </div>
                                    <div class="help-block" id="error_memberFax"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Nama Kepala Sekolah</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="masterName" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Nama Kepala Sekolah" name="masterName" value="<?php echo e(@$member->member->masterName); ?>">
                                    </div>
                                    <div class="help-block" id="error_masterName"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Email Kepala Sekolah</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="masterEmail" type="email" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Email Kepala Sekolah" name="masterEmail" value="<?php echo e(@$member->member->masterEmail); ?>">
                                    </div>
                                    <div class="help-block" id="error_masterEmail"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Telepon Kepala Sekolah</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="masterPhone" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Telepon Kepala Sekolah" name="masterPhone" value="<?php echo e(@$member->member->masterPhone); ?>">
                                    </div>
                                    <div class="help-block" id="error_masterPhone"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Nama Yayasan</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="institutionName" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Nama Yayasan" name="institutionName" value="<?php echo e(@$member->member->institutionName); ?>">
                                    </div>
                                    <div class="help-block" id="error_institutionName"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Nama Kepala Yayasan</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="institutionMaster" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Nama Kepala Yayasan" name="institutionMaster" value="<?php echo e(@$member->member->institutionMaster); ?>">
                                    </div>
                                    <div class="help-block" id="error_institutionMaster"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label>Jenjang Pendidikan</label>
                                    <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
                                        <select class="js-select2" name="level">
                                            <option value="">Pilih Jenjang</option>
                                            <?php $__currentLoopData = \App\Util\Constant::SCHOOL_LEVEL_LIST; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($level); ?>" <?php echo e($level==@$member->member->level?'selected':''); ?>><?php echo e($value); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="help-block" id="error_level"></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Akreditasi</label>
                                    <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
                                        <select class="js-select2" name="rating">
                                            <option value="">Pilih Akreditasi</option>
                                            <?php $__currentLoopData = \App\Util\Constant::SCHOOL_RATING_LIST; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rating => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($rating); ?>" <?php echo e($rating==@$member->member->rating?'selected':''); ?>><?php echo e($val); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="help-block" id="error_rating"></div>
                                </div>
                            </div>
                        </div>

                        <div class="flex-w flex-sb-m p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="float: right">
                            <a href="#step-1" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10 navStep">
                                Sebelumnya
                            </a>
                            <a href="#step-3" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10 navStep">
                                Selanjutnya
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row setup-content" id="step-3">
                <div class="col-lg-12 col-xl-9 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                            <h4 class="mtext-109 cl2 p-b-30">
                                Dokumen & Foto
                            </h4>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control">Foto Sekolah</label>
                                        <div class="bor8 bg0 m-b-12">
                                            <input id="image" type="file" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Logo sekolah" name="image">
                                        </div>
                                        <div class="help-block">* Format File (.jpg,.png,.jpeg) | Max File Size (500 KB) </div>
                                        <div class="help-block" id="error_image"></div>
                                    </div>
                                    <?php if(!empty($member->member->image)): ?>
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control"> </label>
                                        <a class="btn btn-info btn-preview" target="_blank" href="<?php echo e(url('storage/'.$member->member->image)); ?>"><i class="fa fa-search"></i> Lihat</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control">Logo</label>
                                        <div class="bor8 bg0 m-b-12">
                                            <input id="logo" type="file" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Logo sekolah" name="logo">
                                        </div>
                                        <div class="help-block">* Format File (.jpg,.png,.jpeg) | Max File Size (500 KB) </div>
                                        <div class="help-block" id="error_logo"></div>
                                    </div>
                                    <?php if(!empty($member->member->logo)): ?>
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control"> </label>
                                        <a class="btn btn-info btn-preview" target="_blank" href="<?php echo e(url('storage/'.$member->member->logo)); ?>"><i class="fa fa-search"></i> Lihat</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control">Akta</label>
                                        <div class="bor8 bg0 m-b-12">
                                            <input id="certificate" type="file" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Logo sekolah" name="certificate">
                                        </div>
                                        <div class="help-block">* Format File (.jpg,.png,.jpeg,.doc,.pdf) | Max File Size (2 MB)</div>
                                        <div class="help-block" id="error_certificate"></div>
                                    </div>
                                    <?php if(!empty($member->member->certificate)): ?>
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control"> </label>
                                        <a class="btn btn-info btn-preview" target="_blank" href="<?php echo e(url('storage/'.$member->member->certificate)); ?>"><i class="fa fa-search"></i> Lihat</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control">NPSN</label>
                                        <div class="bor8 bg0 m-b-12">
                                            <input id="npsnImage" type="file" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Logo sekolah" name="npsnImage">
                                        </div>
                                        <div class="help-block">* Format File (.jpg,.png,.jpeg,.doc,.pdf) | Max File Size (2 MB)</div>
                                        <div class="help-block" id="error_npsnImage"></div>
                                    </div>
                                    <?php if(!empty($member->member->npsnImage)): ?>
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control"> </label>
                                        <a class="btn btn-info btn-preview" target="_blank" href="<?php echo e(url('storage/'.$member->member->npsnImage)); ?>"><i class="fa fa-search"></i> Lihat</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control">Akreditasi</label>
                                        <div class="bor8 bg0 m-b-12">
                                            <input id="ratingImage" type="file" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Logo sekolah" name="ratingImage">
                                        </div>
                                        <div class="help-block">* Format File (.jpg,.png,.jpeg,.doc,.pdf) | Max File Size (2 MB)</div>
                                        <div class="help-block" id="error_ratingImage"></div>
                                    </div>
                                    <?php if(!empty($member->member->ratingImage)): ?>
                                    <div class="col-md-6 m-b-12">
                                        <label class="label-control"> </label>
                                        <a class="btn btn-info btn-preview" target="_blank" href="<?php echo e(url('storage/'.$member->member->ratingImage)); ?>"><i class="fa fa-search"></i> Lihat</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="flex-w flex-sb-m p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="float: right">
                            <a href="#step-2" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10 navStep">
                                Sebelumnya
                            </a>
                            <a href="#step-4" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10 navStep">
                                Selanjutnya
                            </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row setup-content" id="step-4">
                <div class="col-lg-12 col-xl-9 m-lr-auto m-b-50">
                    <div class="m-l-25 m-r--38 m-lr-0-xl">
                        <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                            <h4 class="mtext-109 cl2 p-b-30">
                                Administrasi
                            </h4>

                            <div class="alert alert-danger hidden" id="step-3-error"></div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Jumlah Murid Pria</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="maleCount" type="number" onchange="calculate()" min="0" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Jumlah Murid Pria" name="maleCount" value="<?php echo e(!empty(@$member->member->maleCount)?@$member->member->maleCount:0); ?>">
                                    </div>
                                    <div class="help-block" id="error_maleCount"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Jumlah Murid Perempuan</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="femaleCount" type="number" min="0" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Jumlah Murid Perempuan" name="femaleCount" onchange="calculate()" value="<?php echo e(!empty(@$member->member->femaleCount)?@$member->member->femaleCount:0); ?>">
                                    </div>
                                    <div class="help-block" id="error_femaleCount"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Jumlah Guru Pria</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="maleTeacher" type="number" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Jumlah Guru Pria" name="maleTeacher" value="<?php echo e(@$member->member->maleTeacher); ?>">
                                    </div>
                                    <div class="help-block" id="error_maleTeacher"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Jumlah Guru Perempuan</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="femaleTeacher" type="number" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Jumlah Guru Perempuan" name="femaleTeacher" value="<?php echo e(@$member->member->femaleTeacher); ?>">
                                    </div>
                                    <div class="help-block" id="error_femaleTeacher"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Rata - Rata SPP /Bulan</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="sppAverage" type="number" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Rata - Rata SPP per Bulan" min="0" name="sppAverage" onchange="calculate()" value="<?php echo e(!empty(@$member->member->sppAverage)?@$member->member->sppAverage:0); ?>">
                                    </div>
                                    <div class="help-block" id="error_sppAverage"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Iuran Anggota /Tahun</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="costAmount" type="number" disabled class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Otomatis dari SPP/Jumlah Murid" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="label-control">Tahun Berdiri</label>
                                    <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
                                        <select class="js-select2" name="year">
                                            <option value="">Pilih Tahun</option>
                                            <?php for($i=date('Y');$i>=date('Y',strtotime( (date('Y')-50).date('-m-d') ));$i--): ?>
                                                <option value="<?php echo e($i); ?>" <?php echo e(@$member->member->year==$i?'selected':''); ?>><?php echo e($i); ?></option>
                                            <?php endfor; ?>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="help-block" id="error_year"></div>
                                </div>
                                <div class="col-md-6">
                                    <label class="label-control">Website</label>
                                    <div class="bor8 bg0 m-b-12">
                                        <input id="website" type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" placeholder="Website Sekolah" name="website" value="<?php echo e(@$member->member->website); ?>">
                                    </div>
                                    <div class="help-block" id="error_website"></div>
                                </div>
                            </div>
                        </div>

                        <div class="flex-w flex-sb-m p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="float: right">
                            <a href="#step-3" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10 navStep">
                                Sebelumnya
                            </a>
                            <button type="button" onclick="save()" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>
    <script type="text/javascript">
        $(document).ready(function () {

            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-info');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            var nextList = $('.navStep');

            nextList.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-info');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        });
    </script>
    <script type="text/javascript">

        <?php if(!empty($member->member->maleCount) && !empty($member->member->femaleCount) && !empty($member->member->sppAverage)): ?>
        calculate();
        <?php endif; ?>

       function calculate(){
         var male = parseFloat($('[name=maleCount]').val());
         var female = parseFloat($('[name=femaleCount]').val());
         var spp = parseFloat($('[name=sppAverage]').val());
         var total = parseFloat(((male+female)/100)*spp);
         $('#costAmount').val(total.toFixed(2));
       }

       function getKabupaten(){
            $('.modal-loading').addClass('modal-loading-show');
            var prov = $("[name=provinceId]").val();
            $.ajax({
                url : '<?php echo e(url("getKabupaten")); ?>/'+prov,
                type: "GET",
                dataType: "JSON",
                success: function(response)
                {
                    $('[name=cityId]').empty();

                    $('[name=cityId]').append(
                        $("<option></option>")
                            .attr("value","")
                            .text('Pilih Kota')
                    );

                    $.each(response, function(cityKey,cityValue) {
                        var currentCity = $("<option></option>")
                            .attr("value",cityValue.id)
                            .text(cityValue.nama);

                        $('[name=cityId]').append(currentCity);
                    });

                    $('[name=cityId]').removeAttr('disabled');

                    $('.modal-loading').removeClass('modal-loading-show');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    swal({
                        title: 'system error!',
                        text: errorThrown,
                        icon: "error",
                    });
                    $('.modal-loading').removeClass('modal-loading-show');
                }
            });
        }

        function getKecamatan(){
            $('.modal-loading').addClass('modal-loading-show');
            var kab = $("[name=cityId]").val();
            $.ajax({
                url : '<?php echo e(url("getKecamatan")); ?>/'+kab,
                type: "GET",
                dataType: "JSON",
                success: function(response)
                {
                    $('[name=districtId]').empty();

                    $('[name=districtId]').append(
                        $("<option></option>")
                            .attr("value","")
                            .text('Pilih Kecamatan')
                    );

                    $.each(response, function(districtKey,districtValue) {
                        var currentDistrict = $("<option></option>")
                            .attr("value",districtValue.id)
                            .text(districtValue.nama);

                        $('[name=districtId]').append(currentDistrict);
                    });

                    $('[name=districtId]').removeAttr('disabled');

                    $('.modal-loading').removeClass('modal-loading-show');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    swal({
                        title: 'system error!',
                        text: errorThrown,
                        icon: "error",
                    });
                    $('.modal-loading').removeClass('modal-loading-show');
                }
            });
        }

        $('.hidden').hide();

        function save(){
            swal({
                title: "Yakin Simpan Data Profil?",
                text : "Pastikan seluruh data terisi dengan benar!",
                icon: "warning",
                buttons: {
                    cancel:true,
                    confirm: {
                        text:'Simpan!',
                        closeModal: false,
                    },
                },
            })
                .then((process) => {
                if(process){
                    $.ajax({
                        url: "<?php echo e(route('member.profile')); ?>",
                        type: "POST",
                        data: new FormData($("#formMember")[0]),
                        processData: false,
                        contentType: false,
                        async:false,
                        success: function(response) {
                            if(response.status){
                                swal({
                                    title: 'Berhasil Simpan Data Profil',
                                    text: response.message,
                                    icon: 'success',
                                    timer: '1500'
                                }).then((done)=>{
                                    location.reload();
                                });
                            }else{
                                swal({
                                    title: 'Gagal Simpan Data Profil',
                                    text: response.message,
                                    icon: 'error',
                                    timer: '1500'
                                });

                                var error_arr = ['name','email','phone','address','provinceId','cityId','districtId','subdistrict','postcode','memberName','memberEmail','memberPhone','level','rating','maleCount','femaleCount','sppAverage','year','maleTeacher','femaleTeacher'];

                                for(var i=0;i < error_arr.length;i++){
                                    $('#error_'+error_arr[i]).html('');
                                    if(error_arr[i] in response.error){
                                        $('#error_'+error_arr[i]).html(response.error[error_arr[i]]);
                                    }
                                }
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '1500'
                            });
                        }
                    });
                }else{
                    swal('Data Profil tidak jadi disimpan');
                }
            });
        }
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>