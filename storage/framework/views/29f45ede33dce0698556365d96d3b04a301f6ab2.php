<?php $__env->startSection('pageTitle','Detail Buku'); ?>

<?php $__env->startPush('customCss'); ?>
    <style type="text/css">
        .mainmenu-area.affix-top {
            background-color: #1e3163;
        }        
    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <section class="gray-bg section-padding" id="tentang-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                    <div class="page-title">
                        <h2><?php echo e(@$book->name); ?></h2>
                        <?php echo @$book->content; ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <a class="btn btn-app" href="#search-collapse" title="cari halaman ini" data-parent="#accordion" data-toggle="collapse">
                            <i class="fa fa-search"></i><br> Cari
                        </a>
                        <a class="btn btn-app" href="#save-collapse" title="tandai halaman ini" data-parent="#accordion" data-toggle="collapse">
                            <i class="fa fa-bookmark"></i><br> Tandai
                        </a>
                        
                        <div id="accordion">
                            <div class="panel search">
                                <div id="search-collapse" class="collapse in">
                                    <ul class="search-form">
                                        <li><input type="number" class="form-control" name="searchPage" placeholder="masukan nomor halaman.."></li>
                                        <li><button class="btn btn-md btn-info form-control"  style="height: 46px;margin-left: 4px;" type="button" onclick="setPage(0)">Cari Halaman</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel bookmark">
                                <div id="save-collapse" class="collapse">
                                    <ul class="search-form">
                                        <li><input type="text" class="form-control" name="savePage" placeholder="beri nama halaman.."></li>
                                        <li><button class="btn btn-md btn-info form-control" style="height: 46px;margin-left: 4px;" type="button">Simpan Halaman</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 jquery-accordion-menu txt-uppercase mg20" id="jquery-accordion-menu">
                                      
                    <div class="accordion js-accordion">
                        
                        
                        <div class="accordion-header txt-center"><strong>daftar isi</strong></div>                      
                        <div class="accordion__item js-accordion-item">
                            <div class="accordion-header js-accordion-header">Bab I Pendahuluan</div>
                            <div class="accordion-body js-accordion-body">
                                <div class="accordion-body__contents">
                                    <ul class="nav">
                                        <li><a href="#">1.1 Kedudukan Bahasa Indonesia </a></li>
                                        <li><a href="#">1.2 Ragam Bahasa </a></li>
                                        <li><a href="#">1.3 Diglosia </a></li>
                                        <li><a href="#">1.4 Pembakuan Bahasa</a></li>
                                        <li><a href="#">1.5 Bahasa Baku</a></li>
                                        <li><a href="#">1.6 Fungsi Bahasa Baku</a></li>
                                        <li><a href="#">1.7 Bahasa Yang Baik dan Benar</a></li>
                                        <li><a href="#">1.8 Hubungan Bahasa Indonesia Dengan Bahasa Daerah Dan Bahasa
                                                Asing</a></li>
                                    </ul>
                                </div>
                            </div> <!-- end of accordion body -->
                        </div><!-- end of accordion item -->
                        <div class="accordion__item js-accordion-item">
                            <div class="accordion-header js-accordion-header">Bab II tata bahasa: tinjauan selayang
                                pandang</div>
                            <div class="accordion-body js-accordion-body">
                                <div class="accordion-body__contents">
                                    <ul class="nav">
                                        <li><a href="#">2.1 deskripsi dan teori </a></li>
                                        <li><a href="#">2.2 pengertian tata bahasa </a></li>
                                        <li><a href="#">2.3 semantik, pragmatik, dan relasi makna </a></li>
                                    </ul>
                                </div>
                            </div><!-- end of accordion body -->
                        </div><!-- end of accordion item -->
                        <div class="accordion__item js-accordion-item">
                            <div class="accordion-header js-accordion-header">Bab III bunyi bahasa dan tata bunyi</div>
                            <div class="accordion-body js-accordion-body">
                                <div class="accordion-body__contents">
                                    <ul class="nav">
                                        <li><a href="#">3.1 batasan dan ciri bunyi bahasa </a></li>
                                        <li><a href="#">3.2 vokal dan konsonan </a></li>
                                        <li><a href="#">3.3 struktur suku kata dan kata </a></li>
                                        <li><a href="#">3.4 pemenggalan kata </a></li>
                                        <li><a href="#">3.5 ciri suprasegmental </a></li>
                                    </ul>
                                </div>
                            </div><!-- end of accordion body -->
                        </div><!-- end of accordion item -->
                        <div class="accordion__item js-accordion-item">
                            <div class="accordion-header js-accordion-header">Bab IV verba</div>
                            <div class="accordion-body js-accordion-body">
                                <div class="accordion-body__contents">
                                    <ul class="nav">
                                        <li><a href="#">4.1 batasan dan ciri verba </a></li>
                                        <li><a href="#">4.2 morfologi dan semantik verba transitif </a></li>
                                        <li><a href="#">4.3 morfologi dan semantik verba taktransitif </a></li>
                                        <li><a href="#">4.4 verba hasil reduplikasi </a></li>
                                        <li><a href="#">4.5 verba majemuk </a></li>
                                        <li><a href="#">4.6 frasa verbal dan fungsinya </a></li>
                                    </ul>
                                </div>
                            </div><!-- end of accordion body -->
                        </div><!-- end of accordion item -->
                        <div class="accordion__item js-accordion-item">
                            <div class="accordion-header js-accordion-header">Bab V adjektiva</div>
                            <div class="accordion-body js-accordion-body">
                                <div class="accordion-body__contents">
                                    <ul class="nav">
                                        <li><a href="#">5.1 batasan dan ciri adjektiva </a></li>
                                        <li><a href="#">5.2 jenis adjektiva berdasarkan ciri semantis </a></li>
                                        <li><a href="#">5.3 adjektiva dari segi perilaku sintaktis </a></li>
                                        <li><a href="#">5.4 pertarafan adjektiva </a></li>
                                        <li><a href="#">5.5 adjektiva dari segi bentuk </a></li>
                                        <li><a href="#">5.6 frasa adjektival </a></li>
                                        <li><a href="#">5.7 adjektiva dan kelas kata lain </a></li>
                                    </ul>
                                </div>
                            </div><!-- end of accordion body -->
                        </div><!-- end of accordion item -->
                        <div class="accordion__item js-accordion-item">
                            <div class="accordion-header js-accordion-header">Bab VI adverbia</div>
                            <div class="accordion-body js-accordion-body">
                                <div class="accordion-body__contents">
                                    <ul class="nav">
                                        <li><a href="#">6.1 batasan dan ciri adverbia </a></li>
                                        <li><a href="#">6.2 adverbia dari segi perilaku semantisnya </a></li>
                                        <li><a href="#">6.3 adverbia dari segi perilaku sintaktisnya </a></li>
                                        <li><a href="#">6.4 adverbia dari segi bentuknya </a></li>
                                        <li><a href="#">6.5 bentuk adverbial </a></li>
                                        <li><a href="#">6.6 adverbia dari kelas kata lain </a></li>
                                    </ul>
                                </div>                                
                            </div><!-- end of accordion body -->
                        </div><!-- end of accordion item -->
                    </div><!-- end of accordion -->

                    <ul class="nav">
                        <?php $__currentLoopData = @$book->page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="javascript:;" onclick="setPage(<?php echo e(@$page->page); ?>)"><?php echo e(@$page->name); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                        
                    </ul>                    
                    <hr>                 
                    
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="t">
                        <div class="tc rel">
                            <div class="book" id="book">
                                <div class="page"><img src="<?php echo e(url('storage/'.$book->featured)); ?>" alt="" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('customJs'); ?>
    <!-- Turn JS -->
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/extras/jquery.mousewheel.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/lib/hash.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/lib/turn.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('frontend/book-preview/js/book-style.js')); ?>"></script>

    <script type="text/javascript">

        var book = $("#book");
        var currentPage = 0;
        var totalPage = 25;

        (function () {
            'use strict';

            var module = {
                ratio: 1.38,
                init: function (id) {
                    var me = this;

                    // if older browser then don't run javascript
                    if (document.addEventListener) {
                        this.el = document.getElementById(id);
                        this.resize();
                        this.plugins();

                        // on window resize, update the plugin size
                        window.addEventListener('resize', function (e) {
                            var size = me.resize();
                            $(me.el).turn('size', size.width, size.height);
                        });
                    }
                },
                resize: function () {
                    // reset the width and height to the css defaults
                    this.el.style.width = '';
                    this.el.style.height = '';

                    var width = this.el.clientWidth,
                        height = Math.round(width / this.ratio),
                        padded = Math.round(document.body.clientHeight * 0.9);

                    // if the height is too big for the window, constrain it
                    if (height > padded) {
                        height = padded;
                        width = Math.round(height * this.ratio);
                    }

                    // set the width and height matching the aspect ratio
                    this.el.style.width = width + 'px';
                    this.el.style.height = height + 'px';

                    return {
                        width: width,
                        height: height
                    };
                },
                plugins: function () {
                    // run the plugin
                    $(this.el).turn({
                        gradients: true,
                        acceleration: true
                    });
                    // hide the body overflow
                    document.body.className = 'hide-overflow';
                }
            };

            module.init('book');
        }());

        book.bind("turning", function(event, page, view) {
            var check = page+5;
            if(page>=totalPage) {
                book.turn('stop');
            }else if(check>=totalPage){
                increasePage(page,totalPage-page);
            }else{
                increasePage(page,5);
            }

            book.turn('resize');
        });

        book.bind("missing", function(event, pages) {
            for (var i = 0; i < pages.length; i++) {
                $(this).turn("addPage", $("<div />"), pages[i]);
            }
        });

        $(document).keydown(function(e){

            var previous = 37, next = 39;

            switch (e.keyCode) {
                case previous:

                    book.turn('previous');

                    break;
                case next:

                    book.turn('next');

                    break;
            }

        });

        function checkPage(){
            var range = $("#flipbook").turn("range", 10);

            for (var page = range[0]; page<=range[1]; page++){
                if (!book.turn("hasPage", page)) {
                    addPage(page);
                }
            }
        }

        /*book.bind("end", function(event, pageObject, turned){
            event.preventDefault();
        });*/

        function increasePage(page,num){
            for (var i=page;i<(page+num);i++){
                addPage(i);
            }
        }

        function removePage(page){
            book.turn('removePage',page);
        }

        increasePage(book.turn('page'),6);

        function loadPage(page) {

            var img = $('<img />');
            img.load(function() {
                var container = $('.book .p'+page);
                img.css({width: '100%', height: '100%'});
                img.appendTo($('.book .p'+page));
            });

            img.attr('src', '<?php echo e(url('frontend/book-preview/books')); ?>/' +  page + '.png');

        }

        function addPage(page) {

            var element = $('<div />', {});

            if (book.turn('addPage', element, page)) {
                loadPage(page);
            }
        }

        function setPage(page) {
            if(page == 0 ) page = $('[name=searchPage]').val();
            var current = book.turn('page');
            console.log(page);
            if(page>totalPage) return;
            if(page>current){
                var num = (page-current)+3;
                increasePage(current,num);
                if (!book.turn("hasPage", page)) {
                    addPage(page);
                }
                book.turn("page", page);
            }else{
                if (!book.turn("hasPage", page)) {
                    addPage(page);
                }
                book.turn("page", page);
            }
        }
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('frontend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>