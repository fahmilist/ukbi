<?php $__env->startSection('pageTitle','Produk'); ?>

<?php $__env->startPush('customCss'); ?>
    <link href="<?php echo e(url('backend/assets/global/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>">Beranda</a> <i class="fa fa-circle"></i></li>
                <li><span>Buku</span></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-dark"></i> <span
                                class="caption-subject font-dark bold uppercase">Daftar Buku</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12 col-lg-12" style="text-align: right;">
                                <a href="<?php echo e(route('admin.book.detail',['id'=>0])); ?>" class="btn btn-primary">Tambah</a>
                            </div>
                        </div>
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th style="max-width: 200px;">Deskripsi</th>
                                    <th style="min-width: 100px;">Rilis</th>
                                    <th>Kover</th>
                                    <th style="min-width: 150px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td class="all">
                                        <input placeholder="Cari Nama" type="text" class="form-control" name="s_name" onchange="filter()">
                                    </td>
                                    <td class="all" >
                                        <input placeholder="Cari Deskripsi" type="text" class="form-control" name="s_description" onchange="filter()">
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?> 

<?php $__env->startPush('customJs'); ?>

<script src="<?php echo e(url('backend/assets/global/datatable.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/datatables/datatables.min.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>"
    type="text/javascript"></script>
<script
    src="<?php echo e(url('backend/assets/global/table-datatables-responsive.min.js')); ?>"
    type="text/javascript"></script>

<script type="text/javascript">
    var table = $('#myTable').DataTable({
        'processing'  : true,
        'serverSide'  : true,
        'ajax'        : {
            url: "<?php echo e(route('book.data')); ?>",
            data: function (d) {
                d.name = $('[name=s_name]').val();
                d.code = $('[name=s_description]').val();
            }
        },
        'dataType'    : 'json',
        'searching'   : false,
        'paging'      : true,
        'lengthChange': true,
        'columns'     : [
            {data:'name', name: 'name'},
            {data:'description', name: 'description'},
            {data:'publish', name: 'publish'},
            {data:'featured', name: 'featured'},
            {data:'aksi', name: 'aksi', orderable: false, searchable: false},
        ],
        'info'        : true,
        'autoWidth'   : false
    });

    function filter() {
        table.draw();
    }

    function deleteData(id) {
        swal({
            title: "Yakin Hapus Data Buku?",
            text : "Data buku akan dihapus permanen",
            icon: "warning",
            buttons: {
                cancel:true,
                confirm: {
                    text:'Hapus!',
                    closeModal: false,
                },
              },
            })
        .then((process) => {
            if(process){
                $.ajax({
                    url: "<?php echo e(route('book.delete')); ?>",
                    type: "POST",
                    data: {
                        '_token': '<?php echo e(csrf_token()); ?>',
                        'id':id,
                    },
                    success: function(data) {
                        swal({
                            title: 'Berhasil Hapus Buku!',
                            text: 'Buku berhasil di hapus',
                            icon: 'success',
                            timer: '2000'
                        });
                        table.ajax.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '2000'
                        });
                    }
                });
            }else{
                swal('Data buku tidak jadi dihapus');
            }
        });
    }

</script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('backend.layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>