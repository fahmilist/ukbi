<!--Vendor-JS-->
    <script src="<?php echo e(url('frontend/js/vendor/jquery-1.12.4.min.js')); ?>"></script>
    <script src="<?php echo e(url('frontend/js/vendor/bootstrap.min.js')); ?>"></script>
    <!--Plugin-JS-->
    <script src="<?php echo e(url('frontend/js/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(url('frontend/js/contact-form.js')); ?>"></script>
    <script src="<?php echo e(url('frontend/js/jquery.parallax-1.1.3.js')); ?>"></script>
    <script src="<?php echo e(url('frontend/js/scrollUp.min.js')); ?>"></script>
    <script src="<?php echo e(url('frontend/js/magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(url('frontend/js/wow.min.js')); ?>"></script>
    <!--Main-active-JS-->
    <script src="<?php echo e(url('frontend/js/main.js')); ?>"></script>
    <!-- LoginJS -->
    <script>
        $(window, document, undefined).ready(function() {

        $('.input').blur(function() {
        var $this = $(this);
        if ($this.val())
            $this.addClass('used');
        else
            $this.removeClass('used');
        });

        });

        $('#tab1').on('click' , function(){
        $('#tab1').addClass('login-shadow');
        $('#tab2').removeClass('signup-shadow');
        });

        $('#tab2').on('click' , function(){
        $('#tab2').addClass('signup-shadow');
        $('#tab1').removeClass('login-shadow');

        });
    </script>